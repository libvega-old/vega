/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Buffer/StagingBufferImpl.hpp"


namespace vega
{

#define THIS_IMPL_TYPE StagingBufferImpl

// ====================================================================================================================
StagingBuffer::StagingBuffer()
{

}

// ====================================================================================================================
StagingBuffer::~StagingBuffer()
{

}

// ====================================================================================================================
uint64 StagingBuffer::size() const
{
	return THIS_IMPL->size_;
}

// ====================================================================================================================
bool StagingBuffer::isPending() const
{
	return !THIS_IMPL->fence_.check();
}

// ====================================================================================================================
void* StagingBuffer::data()
{
	return THIS_IMPL->hostPtr();
}

// ====================================================================================================================
const void* StagingBuffer::data() const
{
	return THIS_IMPL->hostPtr();
}

} // namespace vega
