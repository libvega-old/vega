/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./ShaderImpl.hpp"
#include "./Managers/BindingManager.hpp"
#include <Vega/Core/InlineVector.hpp>


namespace vega
{

// ====================================================================================================================
ShaderImpl::ShaderImpl(Modules&& mods, Reflection&& refl)
	: Shader()
	, ResourceBase(vk::ObjectType::ePipelineLayout)
	, modules_{ std::move(mods) }
	, refl_{ std::move(refl) }
	, pipelineLayout_{ }
	, inputLayout_{ }
{
	const auto bm = BindingManager::Get();

	// Get binding layouts
	InlineVector<vk::DescriptorSetLayout, 3> layouts{ };
	layouts.push_back(bm->tableLayout());
	layouts.push_back(bm->uniformLayout());
	if (!refl_.subpassInputs.empty()) {
		inputLayout_ = bm->inputLayout(uint32(refl_.subpassInputs.size()));
		layouts.push_back(inputLayout_);
	}

	// Calculate push constant ranges
	vk::PushConstantRange pcr{};
	if (!refl_.bindings.empty()) {
		pcr.stageFlags = vk::ShaderStageFlags(refl_.stageMask.rawValue());
		pcr.offset = 0;
		const auto maxIndex = refl_.bindings.size() - 1;
		pcr.size = uint32((maxIndex + 2) / 2) * 4;
	}

	// Create the layout
	vk::PipelineLayoutCreateInfo plci{};
	plci.setLayoutCount = uint32(layouts.size());
	plci.pSetLayouts = layouts.data();
	plci.pushConstantRangeCount = refl_.bindings.empty() ? 0 : 1;
	plci.pPushConstantRanges = &pcr;
	pipelineLayout_ = Vulkan::Device().createPipelineLayout(plci);
}

// ====================================================================================================================
ShaderImpl::~ShaderImpl()
{
	queueDestroy({ pipelineLayout_, modules_.vert, modules_.frag });
}

} // namespace vega
