/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./ShaderImpl.hpp"

#include <array>


namespace vega
{

// ====================================================================================================================
String to_string(ShaderStageMask mask)
{
	if (mask.rawValue() == 0) {
		return "None";
	}

	char str[16];
	uint32 sz{ 0 };
	if (bool(mask & ShaderStage::Vertex))   str[sz++] = 'V';
	if (bool(mask & ShaderStage::Fragment)) str[sz++] = 'F';
	return String(str, sz);
}


// ====================================================================================================================
// ====================================================================================================================
String to_string(BindingType type)
{
	static const std::array<String, uint32(BindingType::ROTexels) + 1> NAMES{
		"Sampler1D", "Sampler2D", "Sampler3D", /*"Sampler1DArray", "Sampler2DArray", "SamplerCube", "Image1D", "Image2D",*/
		/*"Image3D", "Image1DArray", "Image2DArray", "ImageCube",*/ "ROBuffer", /*"RWBuffer",*/ "ROTexels", /*"RWTexels"*/
	};
	return NAMES.at(uint32(type));
}

} // namespace vega
