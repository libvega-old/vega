/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./Buffer/StorageBufferImpl.hpp"


namespace vega
{

#define THIS_IMPL_TYPE StorageBufferImpl

// ====================================================================================================================
StorageBuffer::StorageBuffer()
{

}

// ====================================================================================================================
StorageBuffer::~StorageBuffer()
{

}

// ====================================================================================================================
uint32 StorageBuffer::elementSize() const
{
	return THIS_IMPL->elementSize_;
}

// ====================================================================================================================
uint32 StorageBuffer::elementCount() const
{
	return THIS_IMPL->elementCount_;
}

// ====================================================================================================================
uint64 StorageBuffer::size()
{
	return THIS_IMPL->buffer_.size();
}

// ====================================================================================================================
void StorageBuffer::setData(const void* data, uint32 elementCount, uint32 elementOffset)
{
	const uint64 elemSize{ THIS_IMPL->elementSize_ };
	THIS_IMPL->buffer_.setData(elementOffset * elemSize, data, elementCount * elemSize);
}

// ====================================================================================================================
void StorageBuffer::setData(const StagingBuffer& data, uint32 elementCount, uint64 dataOffset, uint32 elementOffset)
{
	const uint64 elemSize{ THIS_IMPL->elementSize_ };
	THIS_IMPL->buffer_.setData(elementOffset * elemSize, data, elementCount * elemSize, dataOffset);
}

} // namespace vega
