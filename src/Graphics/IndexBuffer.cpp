/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./Buffer/IndexBufferImpl.hpp"

#include <array>


namespace vega
{

// ====================================================================================================================
const String& IndexType::str() const
{
	static const std::array<String, IndexType::U32.value() + 1> NAMES{
		"U16", "U32"
	};
	return NAMES.at(value_);
}

// ====================================================================================================================
uint32 IndexType::size() const
{
	static const std::array<uint32, IndexType::U32.value() + 1> SIZES{
		2, 4
	};
	return SIZES.at(value_);
}


#define THIS_IMPL_TYPE IndexBufferImpl
// ====================================================================================================================
// ====================================================================================================================
IndexBuffer::IndexBuffer()
{

}

// ====================================================================================================================
IndexBuffer::~IndexBuffer()
{

}

// ====================================================================================================================
IndexType IndexBuffer::indexType() const
{
	return THIS_IMPL->indexType_;
}

// ====================================================================================================================
uint32 IndexBuffer::indexCount() const
{
	return THIS_IMPL->count_;
}

// ====================================================================================================================
uint64 IndexBuffer::size() const
{
	return THIS_IMPL->buffer_.size();
}

// ====================================================================================================================
void IndexBuffer::setData(const void* data, uint32 indexCount, uint32 indexOffset)
{
	const uint64 idxsize{ THIS_IMPL->indexType_.size() };
	THIS_IMPL->buffer_.setData(indexOffset * idxsize, data, indexCount * idxsize);
}

// ====================================================================================================================
void IndexBuffer::setData(const StagingBuffer& data, uint32 indexCount, uint64 dataOffset, uint32 indexOffset)
{
	const uint64 idxsize{ THIS_IMPL->indexType_.size() };
	THIS_IMPL->buffer_.setData(indexOffset * idxsize, data, indexCount * idxsize, dataOffset);
}

} // namespace vega
