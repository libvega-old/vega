/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./ResourceBase.hpp"
#include "./Managers/ResourceManager.hpp"
#include <Vega/Core/Time.hpp>

#include <atomic>


namespace vega
{

// ====================================================================================================================
static std::atomic_uint64_t ResourceUUID_{ 1 };

// ====================================================================================================================
ResourceBase::ResourceBase(vk::ObjectType type)
	: uuid_{ ResourceUUID_.fetch_add(1) }
	, type_{ type }
	, name_{ }
	, lastUse_{ NEVER_USED }
{

}

// ====================================================================================================================
void ResourceBase::resourceName(const StringView& name)
{
	name_ = name;
	if (type_ != vk::ObjectType::eUnknown) {
		if (const auto handle = resourceHandle(); handle.has_value()) {
			Vulkan::SetObjectName(handle.value(), vk::ObjectType(type_), name_);
		}
	}
}

// ====================================================================================================================
void ResourceBase::resourceMarkUsed() const
{
	lastUse_ = Time::FrameCount();
}

// ====================================================================================================================
void ResourceBase::queueDestroy(const std::initializer_list<VkHandle>& handles, bool immediate)
{
	ResourceManager::Get()->safeDestroy(*this, handles, immediate);
}

} // namespace vega
