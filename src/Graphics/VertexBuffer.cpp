/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./Buffer/VertexBufferImpl.hpp"


namespace vega
{

#define THIS_IMPL_TYPE VertexBufferImpl

// ====================================================================================================================
VertexBuffer::VertexBuffer()
{

}

// ====================================================================================================================
VertexBuffer::~VertexBuffer()
{

}

// ====================================================================================================================
const VertexLayout& VertexBuffer::layout() const
{
	return THIS_IMPL->layout_;
}

// ====================================================================================================================
uint32 VertexBuffer::vertexCount() const
{
	return THIS_IMPL->count_;
}

// ====================================================================================================================
uint64 VertexBuffer::size() const
{
	return THIS_IMPL->buffer_.size();
}

// ====================================================================================================================
void VertexBuffer::setData(const void* data, uint32 vertexCount, uint32 vertexOffset)
{
	const uint64 stride{ THIS_IMPL->layout_.stride() };
	THIS_IMPL->buffer_.setData(vertexOffset * stride, data, vertexCount * stride);
}

// ====================================================================================================================
void VertexBuffer::setData(const StagingBuffer& data, uint32 vertexCount, uint64 dataOffset, uint32 vertexOffset)
{
	const uint64 stride{ THIS_IMPL->layout_.stride() };
	THIS_IMPL->buffer_.setData(vertexOffset * stride, data, vertexCount * stride, dataOffset);
}

} // namespace vega
