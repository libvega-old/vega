/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./GraphicsEngineImpl.hpp"
#include "./Buffer/StagingBufferImpl.hpp"
#include "./Buffer/IndexBufferImpl.hpp"
#include "./Buffer/VertexBufferImpl.hpp"
#include "./Buffer/TexelBufferImpl.hpp"
#include "./Buffer/StorageBufferImpl.hpp"
#include "./Image/TextureImpl.hpp"
#include "../Render/RenderLayoutImpl.hpp"
#include "../Render/Renderer/RendererImpl.hpp"
#include "../Render/PipelineImpl.hpp"


namespace vega
{

#define THIS_IMPL_TYPE GraphicsEngineImpl

// ====================================================================================================================
GraphicsEngine* GraphicsEngine::Instance_{ nullptr };

// ====================================================================================================================
GraphicsEngine::GraphicsEngine()
{
	Instance_ = this;
}

// ====================================================================================================================
GraphicsEngine::~GraphicsEngine()
{
	Instance_ = nullptr;
}

// ====================================================================================================================
const GraphicsDevice* GraphicsEngine::device() const
{
	return THIS_IMPL->device_;
}

// ====================================================================================================================
bool GraphicsEngine::isThreadRegistered() const
{
	return GraphicsEngineImpl::ThreadMonitor_.isRegistered();
}

// ====================================================================================================================
UPtr<StagingBuffer> GraphicsEngine::createStagingBuffer(uint64 size)
{
	// Validate
	if (size == 0) {
		throw std::runtime_error("Cannot create a staging buffer with zero size");
	}

	// Create
	return std::make_unique<StagingBufferImpl>(size);
}

// ====================================================================================================================
UPtr<IndexBuffer> GraphicsEngine::createIndexBuffer(const IndexType& indexType, uint32 indexCount)
{
	// Validate
	if (indexCount == 0) {
		throw std::runtime_error("Cannot create an index buffer with zero indices");
	}

	// Create
	return std::make_unique<IndexBufferImpl>(indexType, indexCount, nullptr, 0);
}

// ====================================================================================================================
UPtr<IndexBuffer> GraphicsEngine::createIndexBuffer(const IndexType& indexType, uint32 indexCount,
	const StagingBuffer& buffer, uint64 dataOffset)
{
	// Validate
	if (indexCount == 0) {
		throw std::runtime_error("Cannot create an index buffer with zero indices");
	}
	if ((dataOffset + (uint64(indexType.size()) * indexCount)) > buffer.size()) {
		throw std::runtime_error("Source buffer not large enough to create index buffer");
	}

	// Create
	return std::make_unique<IndexBufferImpl>(indexType, indexCount, &buffer, dataOffset);
}

// ====================================================================================================================
UPtr<VertexBuffer> GraphicsEngine::createVertexBuffer(const VertexLayout& layout, uint32 vertexCount)
{
	// Validate
	if (vertexCount == 0) {
		throw std::runtime_error("Cannot create an vertex buffer with zero vertices");
	}

	// Create
	return std::make_unique<VertexBufferImpl>(layout, vertexCount, nullptr, 0);
}

// ====================================================================================================================
UPtr<VertexBuffer> GraphicsEngine::createVertexBuffer(const VertexLayout& layout, uint32 vertexCount,
	const StagingBuffer& buffer, uint64 dataOffset)
{
	// Validate
	if (vertexCount == 0) {
		throw std::runtime_error("Cannot create an vertex buffer with zero vertices");
	}
	if ((dataOffset + (uint64(layout.stride()) * vertexCount)) > buffer.size()) {
		throw std::runtime_error("Source buffer not large enough to create vertex buffer");
	}

	// Create
	return std::make_unique<VertexBufferImpl>(layout, vertexCount, &buffer, dataOffset);
}

// ====================================================================================================================
UPtr<TexelBuffer> GraphicsEngine::createROTexelBuffer(const TexelFormat& format, uint32 texelCount)
{
	const auto maxElems = THIS_IMPL->device_->properties()->maxTexelBufferCount;

	// Validate
	if (!format.isColor()) {
		throw std::runtime_error("Cannot create a read-only texel buffer with a non-color format");
	}
	if (texelCount == 0) {
		throw std::runtime_error("Cannot create a read-only texel buffer with zero texels");
	}
	if (texelCount > maxElems) {
		throw std::runtime_error(to_string("Texel buffer count out of limit (%u > %u)", texelCount, maxElems));
	}

	// Create
	return std::make_unique<TexelBufferImpl>(format, texelCount, nullptr, 0);
}

// ====================================================================================================================
UPtr<TexelBuffer> GraphicsEngine::createROTexelBuffer(const TexelFormat& format, uint32 texelCount,
	const StagingBuffer& buffer, uint64 dataOffset)
{
	const auto maxElems = THIS_IMPL->device_->properties()->maxTexelBufferCount;

	// Validate
	if (!format.isColor()) {
		throw std::runtime_error("Cannot create a read-only texel buffer with a non-color format");
	}
	if (texelCount == 0) {
		throw std::runtime_error("Cannot create a read-only texel buffer with zero texels");
	}
	if (texelCount > maxElems) {
		throw std::runtime_error(to_string("Texel buffer count out of limit (%u > %u)", texelCount, maxElems));
	}
	if ((dataOffset + (uint64(format.size()) * texelCount)) > buffer.size()) {
		throw std::runtime_error("Source buffer not large enough to create texel buffer");
	}

	// Create
	return std::make_unique<TexelBufferImpl>(format, texelCount, &buffer, dataOffset);
}

// ====================================================================================================================
UPtr<StorageBuffer> GraphicsEngine::createROStorageBuffer(uint32 elementSize, uint32 elementCount)
{
	const auto maxSize = THIS_IMPL->device_->properties()->maxStorageBufferSize;

	// Validate
	if (elementSize == 0) {
		throw std::runtime_error("Cannot create a read-only storage buffer with zero-size elements");
	}
	if (elementCount == 0) {
		throw std::runtime_error("Cannot create a read-only storage buffer with zero elements");
	}
	const uint64 dataSize{ uint64(elementSize) * elementCount };
	if (dataSize > maxSize) {
		throw std::runtime_error(to_string("Storage buffer size out of limit (%llu > %u)", dataSize, maxSize));
	}

	// Create
	return std::make_unique<StorageBufferImpl>(elementSize, elementCount, nullptr, 0);
}

// ====================================================================================================================
UPtr<StorageBuffer> GraphicsEngine::createROStorageBuffer(uint32 elementSize, uint32 elementCount,
	const StagingBuffer& buffer, uint64 dataOffset)
{
	const auto maxSize = THIS_IMPL->device_->properties()->maxStorageBufferSize;

	// Validate
	if (elementSize == 0) {
		throw std::runtime_error("Cannot create a read-only storage buffer with zero-size elements");
	}
	if (elementCount == 0) {
		throw std::runtime_error("Cannot create a read-only storage buffer with zero elements");
	}
	const uint64 dataSize{ uint64(elementSize) * elementCount };
	if (dataSize > maxSize) {
		throw std::runtime_error(to_string("Storage buffer size out of limit (%llu > %u)", dataSize, maxSize));
	}
	if ((dataOffset + dataSize) > buffer.size()) {
		throw std::runtime_error("Source buffer not large enough to create storage buffer");
	}

	// Create
	return std::make_unique<StorageBufferImpl>(elementSize, elementCount, &buffer, dataOffset);
}

// ====================================================================================================================
UPtr<Texture> GraphicsEngine::create1DTexture(const TexelFormat& format, uint32 width)
{
	const auto maxSize = THIS_IMPL->device_->properties()->maxImageSize1D;

	// Validate
	if (!format.isColor()) {
		throw std::runtime_error("Cannot create a texture with a non-color format");
	}
	if ((width == 0) || (width > maxSize)) {
		throw std::runtime_error(to_string("Invalid texture width %u - valid range [1, %u]", width, maxSize));
	}

	// Create
	return std::make_unique<TextureImpl>(Texture::Dims::D1, Vec3ui{ width, 1, 1 }, format, nullptr, 0);
}

// ====================================================================================================================
UPtr<Texture> GraphicsEngine::create1DTexture(const TexelFormat& format, uint32 width,
	const StagingBuffer& buffer, uint64 dataOffset)
{
	const auto maxSize = THIS_IMPL->device_->properties()->maxImageSize1D;

	// Validate
	if (!format.isColor()) {
		throw std::runtime_error("Cannot create a texture with a non-color format");
	}
	if ((width == 0) || (width > maxSize)) {
		throw std::runtime_error(to_string("Invalid texture width %u - valid range [1, %u]", width, maxSize));
	}
	const auto dataSize = uint64(format.size()) * width;
	if ((dataSize + dataOffset) > buffer.size()) {
		throw std::runtime_error("Not enough data in staging buffer to supply new 1D texture");
	}

	// Create
	return std::make_unique<TextureImpl>(Texture::Dims::D1, Vec3ui{ width, 1, 1 }, format, &buffer, dataOffset);
}

// ====================================================================================================================
UPtr<Texture> GraphicsEngine::create2DTexture(const TexelFormat& format, uint32 width, uint32 height)
{
	const auto maxSize = THIS_IMPL->device_->properties()->maxImageSize2D;

	// Validate
	if (!format.isColor()) {
		throw std::runtime_error("Cannot create a texture with a non-color format");
	}
	if ((width == 0) || (width > maxSize)) {
		throw std::runtime_error(to_string("Invalid texture width %u - valid range [1, %u]", width, maxSize));
	}
	if ((height == 0) || (height > maxSize)) {
		throw std::runtime_error(to_string("Invalid texture height %u - valid range [1, %u]", width, maxSize));
	}

	// Create
	return std::make_unique<TextureImpl>(Texture::Dims::D2, Vec3ui{ width, height, 1 }, format, nullptr, 0);
}

// ====================================================================================================================
UPtr<Texture> GraphicsEngine::create2DTexture(const TexelFormat& format, uint32 width, uint32 height,
	const StagingBuffer& buffer, uint64 dataOffset)
{
	const auto maxSize = THIS_IMPL->device_->properties()->maxImageSize2D;

	// Validate
	if (!format.isColor()) {
		throw std::runtime_error("Cannot create a texture with a non-color format");
	}
	if ((width == 0) || (width > maxSize)) {
		throw std::runtime_error(to_string("Invalid texture width %u - valid range [1, %u]", width, maxSize));
	}
	if ((height == 0) || (height > maxSize)) {
		throw std::runtime_error(to_string("Invalid texture height %u - valid range [1, %u]", height, maxSize));
	}
	const auto dataSize = uint64(format.size()) * width * height;
	if ((dataSize + dataOffset) > buffer.size()) {
		throw std::runtime_error("Not enough data in staging buffer to supply new 2D texture");
	}

	// Create
	return std::make_unique<TextureImpl>(Texture::Dims::D2, Vec3ui{ width, height, 1 }, format, &buffer, dataOffset);
}

// ====================================================================================================================
UPtr<Texture> GraphicsEngine::create3DTexture(const TexelFormat& format, uint32 width, uint32 height, uint32 depth)
{
	const auto maxSize = THIS_IMPL->device_->properties()->maxImageSize3D;

	// Validate
	if (!format.isColor()) {
		throw std::runtime_error("Cannot create a texture with a non-color format");
	}
	if ((width == 0) || (width > maxSize)) {
		throw std::runtime_error(to_string("Invalid texture width %u - valid range [1, %u]", width, maxSize));
	}
	if ((height == 0) || (height > maxSize)) {
		throw std::runtime_error(to_string("Invalid texture height %u - valid range [1, %u]", height, maxSize));
	}
	if ((depth == 0) || (depth > maxSize)) {
		throw std::runtime_error(to_string("Invalid texture depth %u - valid range [1, %u]", depth, maxSize));
	}

	// Create
	return std::make_unique<TextureImpl>(Texture::Dims::D3, Vec3ui{ width, height, depth }, format, nullptr, 0);
}

// ====================================================================================================================
UPtr<Texture> GraphicsEngine::create3DTexture(const TexelFormat& format, uint32 width, uint32 height, uint32 depth,
	const StagingBuffer& buffer, uint64 dataOffset)
{
	const auto maxSize = THIS_IMPL->device_->properties()->maxImageSize3D;

	// Validate
	if (!format.isColor()) {
		throw std::runtime_error("Cannot create a texture with a non-color format");
	}
	if ((width == 0) || (width > maxSize)) {
		throw std::runtime_error(to_string("Invalid texture width %u - valid range [1, %u]", width, maxSize));
	}
	if ((height == 0) || (height > maxSize)) {
		throw std::runtime_error(to_string("Invalid texture height %u - valid range [1, %u]", height, maxSize));
	}
	if ((depth == 0) || (depth > maxSize)) {
		throw std::runtime_error(to_string("Invalid texture depth %u - valid range [1, %u]", depth, maxSize));
	}
	const auto dataSize = uint64(format.size()) * width * height * depth;
	if ((dataSize + dataOffset) > buffer.size()) {
		throw std::runtime_error("Not enough data in staging buffer to supply new 3D texture");
	}

	// Create
	return std::make_unique<TextureImpl>(Texture::Dims::D3, Vec3ui{ width, height, depth }, format, &buffer, dataOffset);
}

// ====================================================================================================================
UPtr<RenderLayoutBuilder> GraphicsEngine::startNewRenderLayout(uint32 subpassCount, Flags<Subpass> msaaMask)
{
	// Validate
	if (subpassCount == 0) {
		throw BadLayoutError("Cannot have a RenderLayout with zero subpasses");
	}
	if (subpassCount > RenderLayout::MAX_SUBPASSES) {
		throw BadLayoutError(
			to_string("RenderLayout subpass count out of range (%u > %u)", subpassCount, RenderLayout::MAX_SUBPASSES));
	}
	for (uint32 si = 0; si < RenderLayout::MAX_SUBPASSES; ++si) {
		const bool inMask{ bool(msaaMask.rawValue() & (1 << si)) };
		if (inMask && (si >= subpassCount)) {
			throw BadLayoutError(
				to_string("Subpass %u in msaa mask is out of range for the layout subpass count", si));
		}
	}

	// Create
	return std::make_unique<RenderLayoutBuilderImpl>(subpassCount, msaaMask);
}

// ====================================================================================================================
UPtr<Renderer> GraphicsEngine::createRenderer(const SPtr<RenderLayout>& layout, const Vec2ui& extent, MSAA msaa)
{
	// Validate
	const auto props = device()->properties();
	if (extent.x == 0 || extent.x > props->maxFramebufferWidth) {
		throw std::runtime_error(to_string("Invalid Renderer width %u, valid range: [1, %u]",
			extent.x, props->maxFramebufferWidth));
	}
	if (extent.y == 0 || extent.y > props->maxFramebufferHeight) {
		throw std::runtime_error(to_string("Invalid Renderer height %u, valid range: [1, %u]",
			extent.y, props->maxFramebufferHeight));
	}
	if (msaa != MSAA::X1 && !layout->supportsMSAA()) {
		throw std::runtime_error("Cannot use MSAA operations with a RenderLayout that does not support MSAA");
	}
	if (!bool(props->supportedMSAA & msaa)) {
		throw std::runtime_error(to_string("Unsupported MSAA level %s for Renderer", to_string(msaa).c_str()));
	}

	// Create
	return std::make_unique<RendererImpl>(layout, extent, msaa, false);
}

// ====================================================================================================================
UPtr<Pipeline> GraphicsEngine::createPipeline(const Shader& shader, const RenderState& state)
{
	// Validate (TODO)

	// Create
	return std::make_unique<PipelineImpl>(AssetHandle<Shader>(shader), state);
}

} // namespace vega
