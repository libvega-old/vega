/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include "./DeviceBuffer.hpp"
#include <Vega/Graphics/VertexBuffer.hpp>


namespace vega
{

// Implementation of VertexBuffer
class VertexBufferImpl final
	: public VertexBuffer
{
	friend class VertexBuffer;

public:
	VertexBufferImpl(const VertexLayout& layout, uint32 count, const StagingBuffer* data, uint64 dataOffset);
	~VertexBufferImpl();

	inline const DeviceBuffer* buffer() const { return &buffer_; }

private:
	const VertexLayout layout_;
	const uint32 count_;
	DeviceBuffer buffer_;

	VEGA_NO_COPY(VertexBufferImpl)
	VEGA_NO_MOVE(VertexBufferImpl)
}; // class VertexBufferImpl

DECLARE_IMPL_GETTER(VertexBuffer)

} // namespace vega
