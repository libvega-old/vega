/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include "./DeviceBuffer.hpp"
#include <Vega/Graphics/StorageBuffer.hpp>
#include "../Vulkan.hpp"


namespace vega
{

// Implementation for StorageBuffer
class StorageBufferImpl final :
	public StorageBuffer
{
	friend class StorageBuffer;

public:
	StorageBufferImpl(uint32 elementSize, uint32 elementCount, const StagingBuffer* data, uint64 dataOffset);
	~StorageBufferImpl();

	inline const DeviceBuffer* buffer() const { return &buffer_; }
	inline uint16 bindingIndex() const { return bindingIndex_; }

private:
	const uint32 elementSize_;
	const uint32 elementCount_;
	DeviceBuffer buffer_;
	uint16 bindingIndex_;

	VEGA_NO_COPY(StorageBufferImpl)
	VEGA_NO_MOVE(StorageBufferImpl)
}; // class StorageBufferImpl

DECLARE_IMPL_GETTER(StorageBuffer)

} // namespace vega
