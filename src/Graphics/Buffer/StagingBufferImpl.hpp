/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../../Common.Impl.hpp"
#include <Vega/Graphics/StagingBuffer.hpp>
#include <Vega/Core/Time.hpp>
#include "../ResourceBase.hpp"
#include "../Managers/MemoryManager.hpp"
#include "../Managers/CommandManager.hpp"
#include "../Vulkan.hpp"


namespace vega
{

// Implementation of StagingBuffer
class StagingBufferImpl final
	: public StagingBuffer, public ResourceBase
{
	friend class StagingBuffer;

public:
	StagingBufferImpl(uint64 size);
	~StagingBufferImpl();

	inline Opt<uint64> resourceHandle() const override {
		return uint64(VkBuffer(buffer_));
	}

	inline vk::Buffer buffer() const { return buffer_; }
	inline const MemoryAllocation* memory() const { return memory_.get(); }

	inline void* hostPtr() { return memory_->map(); }
	inline const void* hostPtr() const { return memory_->map(); }

	inline const CommandFence::Wait& fence() const { return fence_; }
	inline TimeSpan lastUseTime() const { return lastUseTime_; }

	inline void markUsed(const CommandFence::Wait& wait) const {
		fence_ = wait;
		lastUseTime_ = Time::ElapsedRaw();
		resourceMarkUsed();
	}

private:
	const uint64 size_;
	vk::Buffer buffer_;
	MemoryHandle memory_;
	mutable CommandFence::Wait fence_;
	mutable TimeSpan lastUseTime_;

	VEGA_NO_COPY(StagingBufferImpl)
	VEGA_NO_MOVE(StagingBufferImpl)
}; // class StagingBufferImpl

DECLARE_IMPL_GETTER(StagingBuffer)

} // namespace vega
