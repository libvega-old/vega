/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./DeviceBuffer.hpp"
#include "../Managers/TransferManager.hpp"
#include "./StagingBufferImpl.hpp"


namespace vega
{

// ====================================================================================================================
DeviceBuffer::DeviceBuffer(BufferType type, uint64 size, const StagingBuffer* data, uint64 dataOffset)
	: ResourceBase(vk::ObjectType::eBuffer)
	, type_{ type }
	, size_{ size }
	, handle_{ }
	, memory_{ }
{
	const auto vkdevice = Vulkan::Device();

	// Create buffer
	vk::BufferCreateInfo bci{};
	bci.size = size;
	bci.usage = GetTypeUsageFlags(type);
	bci.sharingMode = vk::SharingMode::eExclusive;
	handle_ = vkdevice.createBuffer(bci);

	// Allocate and bind memory
	memory_ = MemoryManager::Get()->allocateDevice(handle_);
	vkdevice.bindBufferMemory(handle_, memory_->memory(), memory_->offset());

	// Set buffer memory (TODO: DMA queue)
	if (data) {
		TransferManager::Get()->setBufferDataImpl(handle_, 0, *get_impl(data), dataOffset, size_, bci.usage);
	}
}

// ====================================================================================================================
DeviceBuffer::~DeviceBuffer()
{
	queueDestroy({ handle_, memory_.release() });
}

// ====================================================================================================================
void DeviceBuffer::setData(uint64 dstOffset, const void* dataPtr, uint64 size)
{
	// Validate
	if (!dataPtr) {
		throw std::runtime_error("Cannot set buffer data with null data");
	}
	if ((dstOffset + size) > size_) {
		throw std::runtime_error("Invalid data range for destination buffer (size too large)");
	}

	// Set the data
	TransferManager::Get()->setBufferData(handle_, GetTypeUsageFlags(type_), dstOffset, dataPtr, size);
}

// ====================================================================================================================
void DeviceBuffer::setData(uint64 dstOffset, const StagingBuffer& data, uint64 size, uint64 dataOffset)
{
	// Validate
	if ((dstOffset + size) > size_) {
		throw std::runtime_error("Invalid data range for destination buffer (size too large)");
	}
	if ((dataOffset + size) > data.size()) {
		throw std::runtime_error("Invalid data range for source buffer (size too large)");
	}

	// Set the data
	TransferManager::Get()->setBufferDataImpl(
		handle_, dstOffset, get_impl(data), dataOffset, size, GetTypeUsageFlags(type_)
	);
}

// ====================================================================================================================
vk::BufferUsageFlags DeviceBuffer::GetTypeUsageFlags(BufferType type)
{
	if (type == BufferType::Vertex) {
		return vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eTransferSrc |
			vk::BufferUsageFlagBits::eVertexBuffer;
	}
	else if (type == BufferType::Index) {
		return vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eTransferSrc |
			vk::BufferUsageFlagBits::eIndexBuffer;
	}
	else if (type == BufferType::ROTexel) {
		return vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eTransferSrc |
			vk::BufferUsageFlagBits::eUniformTexelBuffer;
	}
	else if (type == BufferType::Storage) {
		return vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eTransferSrc |
			vk::BufferUsageFlagBits::eStorageBuffer;
	}

	assert(false && "Bad buffer type");
	return { };
}

} // namespace 
