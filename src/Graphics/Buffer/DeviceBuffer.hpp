/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include "../ResourceBase.hpp"
#include "../Managers/MemoryManager.hpp"
#include "../Vulkan.hpp"


namespace vega
{

class StagingBuffer;

// Implementation of device-side data buffers
class DeviceBuffer final
	: public ResourceBase
{
public:
	enum BufferType { Index, Vertex, ROTexel, Storage };

	DeviceBuffer(BufferType type, uint64 size, const StagingBuffer* data, uint64 dataOffset);
	~DeviceBuffer();

	inline BufferType type() const { return type_; }
	inline uint64 size() const { return size_; }
	inline vk::Buffer handle() const { return handle_; }
	inline const MemoryAllocation* memory() const { return memory_.get(); }

	void setData(uint64 dstOffset, const void* dataPtr, uint64 size);
	void setData(uint64 dstOffset, const StagingBuffer& data, uint64 size, uint64 dataOffset);

	static vk::BufferUsageFlags GetTypeUsageFlags(BufferType type);

protected:
	inline Opt<uint64> resourceHandle() const override {
		return { uint64(VkBuffer(handle_)) };
	}

private:
	const BufferType type_;
	const uint64 size_;
	vk::Buffer handle_;
	MemoryHandle memory_;

	VEGA_NO_COPY(DeviceBuffer)
	VEGA_NO_MOVE(DeviceBuffer)
}; // class DeviceBuffer

} // namespace vega
