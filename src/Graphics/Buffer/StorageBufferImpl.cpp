/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./StorageBufferImpl.hpp"
#include "../Managers/BindingManager.hpp"


namespace vega
{

// ====================================================================================================================
StorageBufferImpl::StorageBufferImpl(uint32 elementSize, uint32 elementCount, const StagingBuffer* data, 
		uint64 dataOffset)
	: StorageBuffer()
	, elementSize_{ elementSize }
	, elementCount_{ elementCount }
	, buffer_{ DeviceBuffer::Storage, uint64(elementSize) * elementCount, data, dataOffset }
	, bindingIndex_{ 0 }
{
	// Reserve the binding index
	bindingIndex_ = BindingManager::Get()->reserveBuffer(buffer_.handle());
}

// ====================================================================================================================
StorageBufferImpl::~StorageBufferImpl()
{
	// Release binding index
	BindingManager::Get()->freeBuffer(bindingIndex_);
}

} // namespace vega
