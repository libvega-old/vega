/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./TexelBufferImpl.hpp"
#include "../Managers/ResourceManager.hpp"
#include "../Managers/BindingManager.hpp"


namespace vega
{

// ====================================================================================================================
TexelBufferImpl::TexelBufferImpl(TexelFormat format, uint32 count, const StagingBuffer* data, uint64 dataOffset)
	: TexelBuffer()
	, format_{ format }
	, count_{ count }
	, buffer_{ DeviceBuffer::BufferType::ROTexel, uint64(count) * format.size(), data, dataOffset }
	, view_{ }
	, bindingIndex_{ 0 }
{
	// Create the view
	vk::BufferViewCreateInfo bvci{};
	bvci.buffer = buffer_.handle();
	bvci.format = vk::Format(format.formatId());
	bvci.offset = 0;
	bvci.range = VK_WHOLE_SIZE;
	view_ = Vulkan::Device().createBufferView(bvci);

	// Reserve binding index
	bindingIndex_ = BindingManager::Get()->reserveROTexel(view_);
}

// ====================================================================================================================
TexelBufferImpl::~TexelBufferImpl()
{
	// Free binding index
	BindingManager::Get()->freeROTexel(bindingIndex_);

	// Free view
	buffer_.queueDestroy({ view_ });
}

} // namespace vega
