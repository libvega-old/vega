/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include "./DeviceBuffer.hpp"
#include <Vega/Graphics/TexelBuffer.hpp>
#include "../Vulkan.hpp"


namespace vega
{

// Implementation for TexelBuffer
class TexelBufferImpl final :
	public TexelBuffer
{
	friend class TexelBuffer;

public:
	TexelBufferImpl(TexelFormat format, uint32 count, const StagingBuffer* data, uint64 dataOffset);
	~TexelBufferImpl();

	inline const DeviceBuffer* buffer() const { return &buffer_; }
	inline vk::BufferView view() const { return view_; }
	inline uint16 bindingIndex() const { return bindingIndex_; }

private:
	const TexelFormat format_;
	const uint32 count_;
	DeviceBuffer buffer_;
	vk::BufferView view_;
	uint16 bindingIndex_;

	VEGA_NO_COPY(TexelBufferImpl)
	VEGA_NO_MOVE(TexelBufferImpl)
}; // class TexelBufferImpl

DECLARE_IMPL_GETTER(TexelBuffer)

} // namespace vega
