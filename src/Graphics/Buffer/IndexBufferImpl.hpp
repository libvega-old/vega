/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../../Common.Impl.hpp"
#include <Vega/Graphics/IndexBuffer.hpp>
#include "./DeviceBuffer.hpp"


namespace vega
{

// Implementation of IndexBuffer
class IndexBufferImpl final
	: public IndexBuffer
{
	friend class IndexBuffer;

public:
	IndexBufferImpl(IndexType indexType, uint32 count, const StagingBuffer* data, uint64 dataOffset);
	~IndexBufferImpl();

	inline const DeviceBuffer* buffer() const { return &buffer_; }

private:
	const IndexType indexType_;
	const uint32 count_;
	DeviceBuffer buffer_;

	VEGA_NO_COPY(IndexBufferImpl)
	VEGA_NO_MOVE(IndexBufferImpl)
}; // class IndexBufferImpl

DECLARE_IMPL_GETTER(IndexBuffer)

} // namespace vega
