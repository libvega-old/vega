/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./VertexBufferImpl.hpp"


namespace vega
{

// ====================================================================================================================
VertexBufferImpl::VertexBufferImpl(const VertexLayout& layout, uint32 count, const StagingBuffer* data,
		uint64 dataOffset)
	: VertexBuffer()
	, layout_{ layout }
	, count_{ count }
	, buffer_{ DeviceBuffer::Vertex, uint64(layout.stride()) * count, data, dataOffset }
{

}

// ====================================================================================================================
VertexBufferImpl::~VertexBufferImpl()
{

}

} // namespace vega
