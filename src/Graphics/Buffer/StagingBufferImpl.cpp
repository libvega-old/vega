/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./StagingBufferImpl.hpp"


namespace vega
{

// ====================================================================================================================
StagingBufferImpl::StagingBufferImpl(uint64 size)
	: ResourceBase(vk::ObjectType::eBuffer)
	, StagingBuffer()
	, size_{ size }
	, buffer_{ }
	, memory_{ }
	, fence_{ }
	, lastUseTime_{ }
{
	const auto vkdevice = Vulkan::Device();

	// Create buffer
	vk::BufferCreateInfo bci{};
	bci.size = size;
	bci.usage = vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eTransferSrc;
	bci.sharingMode = vk::SharingMode::eExclusive;
	buffer_ = vkdevice.createBuffer(bci);

	// Alloc and bind memory
	memory_ = MemoryManager::Get()->allocateUpload(buffer_);
	vkdevice.bindBufferMemory(buffer_, memory_->memory(), memory_->offset());
}

// ====================================================================================================================
StagingBufferImpl::~StagingBufferImpl()
{
	fence_.wait();
	queueDestroy({ buffer_, memory_.release() }, true); // Safe to destroy immediately thanks to fence wait
}

} // namespace vega
