/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Graphics/VertexLayout.hpp>

#include <array>


namespace vega
{

// ====================================================================================================================
const String& VertexUse::str() const
{
	static const std::array<String, VertexUse::INST3.value() + 1> NAMES{
		"POS0", "POS1", "COLOR0", "COLOR1", "COLOR2", "COLOR3", "NORM0", "NORM1", "TAN0", "TAN1", "BINORM0", "BINORM1",
		"UV0", "UV1", "UV2", "UV3", "BWEIGHT0", "BWEIGHT1", "BWEIGHT2", "BWEIGHT3", "BINDEX0", "BINDEX1", "BINDEX2",
		"BINDEX3", "USER0", "USER1", "USER2", "USER3", "INST0", "INST1", "INST2", "INST3"
	};
	return NAMES.at(value_);
}


// ====================================================================================================================
// ====================================================================================================================
VertexLayout::VertexLayout(const std::initializer_list<VertexElement>& elems, Opt<uint32> stride)
	: hash_{ }
	, elements_{ }
	, stride_{ }
	, useMask_{ }
{
	if (elems.size() == 0) {
		throw std::invalid_argument("Cannot create a VertexLayout from 0 elements");
	}
	if (elems.size() > MAX_ELEMENTS) {
		throw std::invalid_argument("Cannot create a VertexLayout from more than the max elements");
	}

	uint32 maxExtent{ 0 };
	const bool isInst = elems.begin()->use().isInstanceUse();
	for (const auto elem : elems) {
		if (bool(useMask_ & elem.use())) {
			throw std::invalid_argument(to_string("Duplicate vertex usage in layout (%s)", elem.use().str().c_str()));
		}
		if (elem.use().isInstanceUse() != isInst) {
			throw std::invalid_argument("Cannot mix instance-rate and vertex-rate data in a vertex layout");
		}
		useMask_ |= elem.use();
		const uint32 extent{ elem.offset() + (elem.format().formatSize() * elem.arraySize()) };
		if (extent > maxExtent) {
			maxExtent = extent;
		}
		elements_.push_back(elem);
	}

	stride_ = stride.value_or(maxExtent);
	hash_ = Hash64::XXHash64(elements_.data(), uint32(elements_.size() * sizeof(VertexElement)), 0);
}

} // namespace vega
