/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Vulkan.hpp"
#include <Vega/Core/AppConfig.hpp>

#include <array>
#include <mutex>
#include <fstream>
#include <algorithm>

VULKAN_HPP_DEFAULT_DISPATCH_LOADER_DYNAMIC_STORAGE


namespace vega
{

namespace
{

// ====================================================================================================================
template<typename T, typename U>
inline static bool VecContains(const std::vector<T>& vec, const U& val)
{
	return std::find(vec.begin(), vec.end(), val) != vec.end();
}

// ====================================================================================================================
class VulkanDebugLogger final
{
public:
	VulkanDebugLogger()
		: file_{ new std::ofstream }, mutex_{ }
	{
		file_->open("./vulkan.log", std::ofstream::trunc);
		if (!file_->is_open()) {
			file_.reset();
		}
	}
	~VulkanDebugLogger()
	{
		if (file_) {
			file_->close();
			file_.reset();
		}
	}

	void writeMessage(
		VkDebugUtilsMessageSeverityFlagsEXT severity,
		VkDebugUtilsMessageTypeFlagsEXT type,
		const VkDebugUtilsMessengerCallbackDataEXT* data
	)
	{
		static thread_local String Message_{};

		if (!file_) {
			return;
		}

		// Reset string
		String& ss = Message_;
		ss.clear();

		// Get the enum characters
		char sChar =
			(severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT) ? 'I' :
			(severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) ? 'W' :
			(severity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) ? 'E' : 'V';
		char tChar =
			(type & VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT) ? 'G' :
			(type & VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT) ? 'P' : 'V';

		// Get the object name
		String objName{
			((data->objectCount > 0) && data->pObjects[0].pObjectName) ? data->pObjects[0].pObjectName : ""
		};

		// Build the message
		ss += '[';
		ss += tChar;
		ss += '|';
		ss += sChar;
		ss += "]:  ";
		if (!objName.empty()) {
			((ss += "(\"") += objName) += "\")  ";
		}
		ss += data->pMessage;

		// Lock the file and write
		std::lock_guard lock{ mutex_ };
		*file_ << ss << std::endl;
	}

	static VkBool32 HandleDebugMessage(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData
	) {
		static VulkanDebugLogger Logger_{};
		Logger_.writeMessage(messageSeverity, messageType, pCallbackData);
		return VK_TRUE;
	}

private:
	UPtr<std::ofstream> file_;
	std::mutex mutex_;
}; // class VulkanDebugLogger

}


// ====================================================================================================================
// ====================================================================================================================
const vk::DynamicLoader Vulkan::Loader_{ };
decltype(Vulkan::Instance_) Vulkan::Instance_{ };
decltype(Vulkan::Device_) Vulkan::Device_{ };

// ====================================================================================================================
void Vulkan::Initialize()
{
	// Check if already initialized
	if (Instance_.handle) {
		return;
	}

	// Init the loader
	VULKAN_HPP_DEFAULT_DISPATCHER.init(Loader_.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr"));

	// Check runtime version
	{
		uint32 vers{};
		if (vk::enumerateInstanceVersion(&vers) != vk::Result::eSuccess) {
			throw std::runtime_error("Vulkan runtime support not found");
		}
		if (vers < VK_MAKE_VERSION(1, 2, 0)) {
			throw std::runtime_error("Runtime environment does not support Vulkan 1.2");
		}
	}

	// Load available layers and instance extensions
	std::vector<String> instExts{}, instLayers{};
	for (const auto& ext : vk::enumerateInstanceExtensionProperties(nullptr)) {
		instExts.push_back(ext.extensionName);
	}
	for (const auto& layer : vk::enumerateInstanceLayerProperties()) {
		instLayers.push_back(layer.layerName);
	}

	// Setup debug message filtering
#if defined(VEGA_DEBUG)
	{
		const auto rawEnv = getenv("VK_LAYER_MESSAGE_ID_FILTER");
		String current{ rawEnv ? rawEnv : "" };
		current += ";0x880879b9"; // Allow stride==0 to be specified in vkCmdBindVertexBuffers2EXT
#pragma warning(suppress : 4996)
		(void)putenv(("VK_LAYER_MESSAGE_ID_FILTER=" + current.substr(rawEnv ? 0 : 1)).data());
	}
#endif // defined(VEGA_DEBUG)

	// Prepare instance layers and extensions
	std::vector<const char*> usedExts{}, usedLayers{};
	{
		if (!VecContains(instExts, PlatformSurfaceExtensionName_)) {
			throw std::runtime_error("Runtime environment does not support presentation surfaces");
		}
		usedExts.push_back(PlatformSurfaceExtensionName_);
		usedExts.push_back(VK_KHR_SURFACE_EXTENSION_NAME);
		Instance_.extensions.surface = true;

#if defined(VEGA_DEBUG)
		if (VecContains(instExts, VK_EXT_DEBUG_UTILS_EXTENSION_NAME) && VecContains(instLayers, ValidationLayerName_)) {
			usedExts.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
			usedLayers.push_back(ValidationLayerName_);
			Instance_.extensions.debugUtils = true;
			Instance_.layers.validation = true;
		}
#endif // defined(VEGA_DEBUG)
	}

	// Create the instance
	{
		// Infos
		vk::ApplicationInfo appi{};
		appi.pApplicationName = ApplicationInfo::AppName().c_str();
		appi.applicationVersion = ApplicationInfo::AppVersion().value();
		appi.pEngineName = "Vega";
		appi.engineVersion = VEGA_VERSION;
		appi.apiVersion = VK_MAKE_VERSION(1, 2, 0);
		vk::InstanceCreateInfo insti{};
		insti.pApplicationInfo = &appi;
		insti.enabledLayerCount = uint32(usedLayers.size());
		insti.ppEnabledLayerNames = usedLayers.data();
		insti.enabledExtensionCount = uint32(usedExts.size());
		insti.ppEnabledExtensionNames = usedExts.data();

		// Create the instance
		Instance_.handle = vk::createInstance(insti);
		VULKAN_HPP_DEFAULT_DISPATCHER.init(Instance_.handle);
	}

	// Setup debug messaging
	if (Instance_.extensions.debugUtils) {
		vk::DebugUtilsMessengerCreateInfoEXT mci{};
		mci.messageSeverity =
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eError | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning |
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo;
		mci.messageType =
			vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance |
			vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation;
		mci.pfnUserCallback = VulkanDebugLogger::HandleDebugMessage;
		mci.pUserData = nullptr;
		Instance_.debugUtils = Instance_.handle.createDebugUtilsMessengerEXT(mci);
	}
}

// ====================================================================================================================
void Vulkan::OpenDevice(vk::PhysicalDevice pdev)
{
	// Populate the required extensions and features
	std::array<const char*, 2> devExts{
		VK_KHR_SWAPCHAIN_EXTENSION_NAME,
		VK_EXT_EXTENDED_DYNAMIC_STATE_EXTENSION_NAME
	};
	DeviceFeatureChain devFeats{};
	vk::PhysicalDeviceExtendedDynamicStateFeaturesEXT dynamicStateFeatures{};
	{
		auto& f10 = devFeats.f10();
		f10.independentBlend = VK_TRUE;

		auto& f11 = devFeats.f11();

		auto& f12 = devFeats.f12();
		f12.timelineSemaphore = VK_TRUE;
		f12.scalarBlockLayout = VK_TRUE;
		f12.descriptorIndexing = VK_TRUE;
		f12.descriptorBindingPartiallyBound = VK_TRUE;
		f12.descriptorBindingUpdateUnusedWhilePending = VK_TRUE;
		f12.descriptorBindingSampledImageUpdateAfterBind = VK_TRUE;
		f12.descriptorBindingStorageImageUpdateAfterBind = VK_TRUE;
		f12.descriptorBindingStorageBufferUpdateAfterBind = VK_TRUE;
		f12.descriptorBindingUniformTexelBufferUpdateAfterBind = VK_TRUE;
		f12.descriptorBindingStorageTexelBufferUpdateAfterBind = VK_TRUE;
		f12.shaderStorageTexelBufferArrayDynamicIndexing = VK_TRUE;
		f12.shaderUniformTexelBufferArrayDynamicIndexing = VK_TRUE;

		dynamicStateFeatures.extendedDynamicState = VK_TRUE;
		f12.pNext = &dynamicStateFeatures;
	}

	// Prepare the queue information
	static constexpr float QUEUE_PRIORITIES[2]{ 1.0f, 0.5f };
	vk::DeviceQueueCreateInfo queueInfo{};
	GetQueueInfo(pdev, &(queueInfo.queueFamilyIndex));
	queueInfo.queueCount = 1;
	queueInfo.pQueuePriorities = QUEUE_PRIORITIES;

	// Create the device
	vk::DeviceCreateInfo dci{};
	dci.queueCreateInfoCount = 1;
	dci.pQueueCreateInfos = &queueInfo;
	dci.enabledExtensionCount = uint32(devExts.size());
	dci.ppEnabledExtensionNames = devExts.data();
	dci.pEnabledFeatures = nullptr; // This is handled with vk::PhysicalDeviceFeatures2 in pNext
	dci.pNext = &(devFeats.get<vk::PhysicalDeviceFeatures2>());
	Device_.handle = pdev.createDevice(dci);
	Device_.physicalDevice = pdev;

	// Finish dispatcher init
	VULKAN_HPP_DEFAULT_DISPATCHER.init(Device_.handle);

	// Save the device properties and queue info
	Device_.physicalDevice.getProperties2(&Device_.properties.get<vk::PhysicalDeviceProperties2>());
	Device_.graphicsQueue = { queueInfo.queueFamilyIndex, 0 };
}

// ====================================================================================================================
void Vulkan::CloseDevice()
{
	// Cleanup device
	if (Device_.handle) {
		Device_.handle.waitIdle();
		Device_.handle.destroy();
	}
	Device_ = { };
}

// ====================================================================================================================
void Vulkan::Terminate()
{
	// Cleanup instance
	if (Instance_.handle) {
		if (Instance_.debugUtils) {
			Instance_.handle.destroyDebugUtilsMessengerEXT(Instance_.debugUtils);
		}
		Instance_.handle.destroy();
	}
	Instance_ = { };
}

// ====================================================================================================================
void Vulkan::SetObjectName(uint64 handle, vk::ObjectType type, const String& name)
{
	if (Instance_.debugUtils && Device_.handle) {
		vk::DebugUtilsObjectNameInfoEXT nameInfo{};
		nameInfo.objectHandle = handle;
		nameInfo.objectType = type;
		nameInfo.pObjectName = name.c_str();
		Device_.handle.setDebugUtilsObjectNameEXT(nameInfo);
	}
}

// ====================================================================================================================
void Vulkan::GetQueueInfo(vk::PhysicalDevice pdev, uint32* gFamily)
{
	const auto queues = pdev.getQueueFamilyProperties();

	for (uint32 fi = 0; fi < queues.size(); ++fi) {
		const auto& queue = queues[fi];
		if (bool(queue.queueFlags & vk::QueueFlagBits::eGraphics)) {
			// Implementations *must* provide a graphics-enabled queue per spec, so this will always give valid values
			if (gFamily) { *gFamily = fi; }
		}
	}
}

// ====================================================================================================================
bool Vulkan::CheckDeviceValid(vk::PhysicalDevice pdev, DeviceFeatureChain& feats, DevicePropertyChain& props)
{
#define CHECK_EXTENSION(extName) if (!VecContains(exts, extName)) { return false; }
#define CHECK_FEATURE(fLevel,fName) if (!f##fLevel.fName) { return false; }

	// Get the features, properties, and extensions
	const auto& p10 = props.p10();
	const auto& p11 = props.p11();
	const auto& p12 = props.p12();
	const auto& f10 = feats.f10();
	const auto& f11 = feats.f11();
	const auto& f12 = feats.f12();
	pdev.getProperties2(&(props.get<vk::PhysicalDeviceProperties2>()));
	pdev.getFeatures2(&(feats.get<vk::PhysicalDeviceFeatures2>()));
	std::vector<String> exts{};
	{
		const auto rawExts = pdev.enumerateDeviceExtensionProperties();
		exts.resize(rawExts.size());
		std::transform(rawExts.begin(), rawExts.end(), exts.begin(),
			[](const vk::ExtensionProperties& props) -> String { return props.extensionName; });
	}

	// Check features and extensions
	if (p10.apiVersion < VK_MAKE_VERSION(1, 2, 0)) { return false; }
	CHECK_EXTENSION(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
	CHECK_EXTENSION(VK_EXT_EXTENDED_DYNAMIC_STATE_EXTENSION_NAME);
	CHECK_FEATURE(10, independentBlend);
	CHECK_FEATURE(12, timelineSemaphore);
	CHECK_FEATURE(12, scalarBlockLayout);
	CHECK_FEATURE(12, descriptorIndexing);
	CHECK_FEATURE(12, descriptorBindingPartiallyBound);
	CHECK_FEATURE(12, descriptorBindingUpdateUnusedWhilePending);
	CHECK_FEATURE(12, descriptorBindingSampledImageUpdateAfterBind);
	CHECK_FEATURE(12, descriptorBindingStorageImageUpdateAfterBind);
	CHECK_FEATURE(12, descriptorBindingStorageBufferUpdateAfterBind);
	CHECK_FEATURE(12, descriptorBindingUniformTexelBufferUpdateAfterBind);
	CHECK_FEATURE(12, descriptorBindingStorageTexelBufferUpdateAfterBind);
	CHECK_FEATURE(12, shaderStorageTexelBufferArrayDynamicIndexing);
	CHECK_FEATURE(12, shaderUniformTexelBufferArrayDynamicIndexing);

	return true;

#undef CHECK_FEATURE
#undef CHECK_EXTENSION
}

} // namespace vega
