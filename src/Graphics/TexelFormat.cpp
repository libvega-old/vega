/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Graphics/TexelFormat.hpp>
#include "./Vulkan.hpp"

#include <array>
#include <unordered_map>


namespace vega
{

// ====================================================================================================================
const String& TexelFormat::str() const
{
	static const std::array<String, uint32(TexelFormats::A2BGR10.value()) + 1> NAMES{
		"Undefined",
		"UNorm", "UNorm2", "UNorm4", "Unorm4Bgra",
		"SNorm", "SNorm2", "SNorm4",
		"U16Norm", "U16Norm2", "U16Norm4",
		"S16Norm", "S16Norm2", "S16Norm4",
		"UByte", "UByte2", "UByte4",
		"SByte", "SByte2", "SByte4",
		"UShort", "UShort2", "UShort4",
		"SShort", "SShort2", "SShort4",
		"UInt",  "UInt2",  "UInt4",
		"SInt",  "SInt2",  "SInt4",
		"Float", "Float2", "Float4",
		"Depth16", "Depth32", "Depth32Stencil8",
		"A2BGR10"
	};
	return NAMES.at(value_);
}

// ====================================================================================================================
TexelFormat TexelFormat::FromId(uint32 id)
{
#define FMAP(fmt,vfmt) { uint32(vk::Format::e##fmt), TexelFormats::vfmt }
	static const std::unordered_map<uint32, TexelFormat> FORMATS{
		FMAP(Undefined, Undefined),
		FMAP(R8Unorm, UNorm), FMAP(R8G8Unorm, UNorm2), FMAP(R8G8B8A8Unorm, UNorm4), FMAP(B8G8R8A8Unorm, Unorm4Bgra),
		FMAP(R8Snorm, SNorm), FMAP(R8G8Snorm, SNorm2), FMAP(R8G8B8A8Snorm, SNorm4),
		FMAP(R16Unorm, U16Norm), FMAP(R16G16Unorm, U16Norm2), FMAP(R16G16B16A16Unorm, U16Norm4),
		FMAP(R16Snorm, S16Norm), FMAP(R16G16Snorm, S16Norm2), FMAP(R16G16B16A16Snorm, S16Norm4),
		FMAP(R8Uint, UByte), FMAP(R8G8Uint, UByte2), FMAP(R8G8B8A8Uint, UByte4),
		FMAP(R8Sint, SByte), FMAP(R8G8Sint, SByte2), FMAP(R8G8B8A8Sint, SByte4),
		FMAP(R16Uint, UShort), FMAP(R16G16Uint, UShort2), FMAP(R16G16B16A16Uint, UShort4),
		FMAP(R16Sint, SShort), FMAP(R16G16Sint, SShort2), FMAP(R16G16B16A16Sint, SShort4),
		FMAP(R32Uint, UInt),    FMAP(R32G32Uint, UInt2),    FMAP(R32G32B32A32Uint, UInt4),
		FMAP(R32Sint, SInt),    FMAP(R32G32Sint, SInt2),    FMAP(R32G32B32A32Sint, SInt4),
		FMAP(R32Sfloat, Float), FMAP(R32G32Sfloat, Float2), FMAP(R32G32B32A32Sfloat, Float4),
		FMAP(D16Unorm, Depth16), FMAP(D32Sfloat, Depth32), FMAP(D32SfloatS8Uint, Depth32Stencil8),
		FMAP(A2B10G10R10UnormPack32, A2BGR10)
	};
#undef FMAP
	const auto it = FORMATS.find(id);
	return (it != FORMATS.end()) ? it->second : TexelFormats::Undefined;
}

// ====================================================================================================================
const TexelFormat::FormatInfo& TexelFormat::info() const
{
	constexpr decltype(FormatInfo::flags) CF{ 1, 0, 0, 0 }, DF{ 0, 1, 0, 0 }, DSF{ 0, 1, 1, 0 }, CPF{ 1, 0, 0, 1 };
#define FINFO(fmt, sz, dims, flags) { uint32(vk::Format::e##fmt), sz, dims, flags }
	static const std::array<FormatInfo, uint32(TexelFormats::A2BGR10.value()) + 1> INFOS{ {
		FINFO(Undefined, 0, 0, {}),
		FINFO(R8Unorm, 1, 1, CF), FINFO(R8G8Unorm, 2, 2, CF), FINFO(R8G8B8A8Unorm, 4, 4, CF), FINFO(B8G8R8A8Unorm, 4, 4, CF),
		FINFO(R8Snorm, 1, 1, CF), FINFO(R8G8Snorm, 2, 2, CF), FINFO(R8G8B8A8Snorm, 4, 4, CF),
		FINFO(R16Unorm, 2, 1, CF), FINFO(R16G16Unorm, 4, 2, CF), FINFO(R16G16B16A16Unorm, 8, 4, CF),
		FINFO(R16Snorm, 2, 1, CF), FINFO(R16G16Snorm, 4, 2, CF), FINFO(R16G16B16A16Snorm, 8, 4, CF),
		FINFO(R8Uint, 1, 1, CF), FINFO(R8G8Uint, 2, 2, CF), FINFO(R8G8B8A8Uint, 4, 4, CF),
		FINFO(R8Sint, 1, 1, CF), FINFO(R8G8Sint, 2, 2, CF), FINFO(R8G8B8A8Sint, 4, 4, CF),
		FINFO(R16Uint, 2, 1, CF), FINFO(R16G16Uint, 4, 2, CF), FINFO(R16G16B16A16Uint, 8, 4, CF),
		FINFO(R16Sint, 2, 1, CF), FINFO(R16G16Sint, 4, 2, CF), FINFO(R16G16B16A16Sint, 8, 4, CF),
		FINFO(R32Uint, 4, 1, CF), FINFO(R32G32Uint, 8, 2, CF), FINFO(R32G32B32A32Uint, 16, 4, CF),
		FINFO(R32Sint, 4, 1, CF), FINFO(R32G32Sint, 8, 2, CF), FINFO(R32G32B32A32Sint, 16, 4, CF),
		FINFO(R32Sfloat, 4, 1, CF), FINFO(R32G32Sfloat, 8, 2, CF), FINFO(R32G32B32A32Sfloat, 16, 4, CF),
		FINFO(D16Unorm, 2, 1, DF), FINFO(D32Sfloat, 4, 1, DF), FINFO(D32SfloatS8Uint, 8, 1, DSF),
		FINFO(A2B10G10R10UnormPack32, 4, 4, CPF)
	} };
#undef FINFO
	return INFOS.at(value_);
}

} // namespace vega
