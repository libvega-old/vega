/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Graphics/GraphicsDevice.hpp>
#include <Vega/Core/Threading.hpp>
#include "./Vulkan.hpp"

#include <array>


namespace vega
{

// ====================================================================================================================
String to_string(DeviceType type)
{
	static const std::array<String, 3> NAMES{ "Discrete", "Integrated", "Virtual" };
	return NAMES.at(uint32(type));
}


// ====================================================================================================================
// ====================================================================================================================
namespace vendorid { 
	inline static constexpr uint32
		Unknown{ 0 }, AMD{ 0x1002 }, ARM{ 0x13B5 }, ImgTec{ 0x1010 }, Intel{ 0x8086 }, Nvidia{ 0x10DE }, 
		Qualcomm{ 0x5143 };
}
const DeviceVendor DeviceVendor::Unknown { vendorid::Unknown };
const DeviceVendor DeviceVendor::AMD     { vendorid::AMD };
const DeviceVendor DeviceVendor::ARM     { vendorid::ARM };
const DeviceVendor DeviceVendor::ImgTec  { vendorid::ImgTec };
const DeviceVendor DeviceVendor::Intel   { vendorid::Intel };
const DeviceVendor DeviceVendor::Nvidia  { vendorid::Nvidia };
const DeviceVendor DeviceVendor::Qualcomm{ vendorid::Qualcomm };

// ====================================================================================================================
const String& DeviceVendor::name() const
{
	switch (value_)
	{
	case vendorid::AMD:      { static const String NAME{ "AMD" }; return NAME; }
	case vendorid::ARM:      { static const String NAME{ "ARM" }; return NAME; }
	case vendorid::ImgTec:   { static const String NAME{ "ImgTec" }; return NAME; }
	case vendorid::Intel:    { static const String NAME{ "Intel" }; return NAME; }
	case vendorid::Nvidia:   { static const String NAME{ "Nvidia" }; return NAME; }
	case vendorid::Qualcomm: { static const String NAME{ "Qualcomm" }; return NAME; }
	default:                 { static const String NAME{ "Unknown" }; return NAME; }
	}
}


// ====================================================================================================================
// ====================================================================================================================
GraphicsDevice::GraphicsDevice(uint64 handle)
	: handle_{ handle }
	, type_{ }
	, vendor_{ DeviceVendor::Unknown }
	, name_{ }
	, properties_{ }
{

}

// ====================================================================================================================
const std::vector<UPtr<const GraphicsDevice>>& GraphicsDevice::AllDevices()
{
	static std::vector<UPtr<const GraphicsDevice>> DEVICES{ };
	static bool Initialized_{ false };

	Threading::AssertMainThread("GraphicsDevice::AllDevices()");

	if (!Initialized_) {
		Vulkan::Initialize();

		// Get the physical devices
		const auto pdevs = Vulkan::Instance().enumeratePhysicalDevices();
		for (const auto pdev : pdevs) {
			// Check validity
			Vulkan::DeviceFeatureChain feats{};
			Vulkan::DevicePropertyChain props{};
			if (!Vulkan::CheckDeviceValid(pdev, feats, props)) {
				continue;
			}

			// Populate device
			const auto& p10 = props.p10();
			UPtr<GraphicsDevice> device{ new GraphicsDevice(uint64(VkPhysicalDevice(pdev))) };
			device->type_ = 
				(p10.deviceType == vk::PhysicalDeviceType::eDiscreteGpu) ? DeviceType::Discrete :
				(p10.deviceType == vk::PhysicalDeviceType::eVirtualGpu) ? DeviceType::Virtual :
				DeviceType::Integrated;
			device->vendor_ = { p10.vendorID };
			device->name_ = { p10.deviceName.data() };

			// Populate properties
			auto& dp = device->properties_;
			dp.supportedMSAA = MSAA(
				uint32(p10.limits.framebufferColorSampleCounts) & uint32(p10.limits.framebufferDepthSampleCounts)
			);
			dp.maxImageSize1D = p10.limits.maxImageDimension1D;
			dp.maxImageSize2D = p10.limits.maxImageDimension2D;
			dp.maxImageSize3D = p10.limits.maxImageDimension3D;
			dp.maxStorageBufferSize = p10.limits.maxStorageBufferRange;
			dp.maxTexelBufferCount = p10.limits.maxTexelBufferElements;
			dp.maxFramebufferWidth = p10.limits.maxFramebufferWidth;
			dp.maxFramebufferHeight = p10.limits.maxFramebufferHeight;
			dp.uniformAlignment = p10.limits.minUniformBufferOffsetAlignment;

			// Add to list
			DEVICES.emplace_back(std::move(device));
		}

		Initialized_ = true;
	}

	return DEVICES;
}

// ====================================================================================================================
const GraphicsDevice* GraphicsDevice::DefaultDevice()
{
	Threading::AssertMainThread("GraphicsDevice::DefaultDevice()");
	
	const auto& all = AllDevices();
	for (const auto& dev : all) {
		if (dev->type() == DeviceType::Discrete) {
			return dev.get();
		}
	}
	return !all.empty() ? all[0].get() : nullptr;
}

} // namespace vega
