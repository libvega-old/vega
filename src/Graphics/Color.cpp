/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Graphics/Color.hpp>

#include <random>


namespace vega
{

// ====================================================================================================================
String Color::str() const
{
	return to_string("0x%08x", packed_);
}

// ====================================================================================================================
Color Color::Random(bool opaque)
{
	static std::mt19937 RandomEngine_{ std::random_device{}() };
	static std::uniform_int_distribution<uint32> RandomDistribution_{ 0, UINT32_MAX };

	const auto packed{ RandomDistribution_(RandomEngine_) };
	return Color{ opaque ? (packed | A_MASK) : packed };
}

} // namespace vega
