/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./Image/TextureImpl.hpp"
#include "./Managers/TransferManager.hpp"

#include <array>


namespace vega
{

#define THIS_IMPL_TYPE TextureImpl

// ====================================================================================================================
Texture::Texture()
	: AssetBase(typeid(Texture))
{

}

// ====================================================================================================================
Texture::~Texture()
{

}

// ====================================================================================================================
Texture::Dims Texture::dimensions() const
{
	const auto type = THIS_IMPL->image_.type();
	return (type == vk::ImageViewType::e1D) ? Dims::D1 : (type == vk::ImageViewType::e2D) ? Dims::D2 : Dims::D3;
}

// ====================================================================================================================
const Vec3ui& Texture::size() const
{
	return THIS_IMPL->image_.imageExtent().size;
}

// ====================================================================================================================
TexelFormat Texture::format() const
{
	return TexelFormat::FromId(uint32(THIS_IMPL->image_.format()));
}

// ====================================================================================================================
bool Texture::hasData() const
{
	return THIS_IMPL->image_.hasData();
}

// ====================================================================================================================
void Texture::setData(const void* data, uint32 x, uint32 width)
{
	THIS_IMPL->image_.setData(data, { x, 0, 0, width, 1, 1, 0, 1 });
}

// ====================================================================================================================
void Texture::setData(const StagingBuffer& data, uint32 x, uint32 width, uint64 dataOffset)
{
	THIS_IMPL->image_.setData(data, { x, 0, 0, width, 1, 1, 0, 1 }, dataOffset);
}

// ====================================================================================================================
void Texture::setData(const void* data, uint32 x, uint32 y, uint32 width, uint32 height)
{
	THIS_IMPL->image_.setData(data, { x, y, 0, width, height, 1, 0, 1 });
}

// ====================================================================================================================
void Texture::setData(const StagingBuffer& data, uint32 x, uint32 y, uint32 width, uint32 height, uint64 dataOffset)
{
	THIS_IMPL->image_.setData(data, { x, y, 0, width, height, 1, 0, 1 }, dataOffset);
}

// ====================================================================================================================
void Texture::setData(const void* data, uint32 x, uint32 y, uint32 z, uint32 width, uint32 height, uint32 depth)
{
	THIS_IMPL->image_.setData(data, { x, y, z, width, height, depth, 0, 1 });
}

// ====================================================================================================================
void Texture::setData(const StagingBuffer& data, uint32 x, uint32 y, uint32 z, uint32 width, uint32 height, 
	uint32 depth, uint64 dataOffset)
{
	THIS_IMPL->image_.setData(data, { x, y, z, width, height, depth, 0, 1 }, dataOffset);
}


// ====================================================================================================================
// ====================================================================================================================
const String& to_string(Texture::Dims dims)
{
	static const std::array<String, uint32(Texture::Dims::D3) + 1> NAMES{ "D1", "D2", "D3" };
	return NAMES.at(uint32(dims));
}

} // namespace vega
