/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include "./Vulkan.hpp"

#include <initializer_list>


namespace vega
{

class MemoryAllocation;

// Base type for graphics resource objects.
class ResourceBase
{
public:
	// Special value of lastUse_ to indicate the resource has never been used
	inline static constexpr uint64 NEVER_USED{ UINT64_MAX };

	// Represents a specific typed resource object handle
	struct VkHandle final
	{
		VkHandle() = default;
		VkHandle(uint64 handle, vk::ObjectType type) : handle{ handle }, type{ type } { }
		template<typename VkT>
		VkHandle(VkT vk) : handle{ uint64((typename VkT::CType)vk) }, type{ VkT::objectType } { }
		VkHandle(MemoryAllocation* mem) : handle{ uint64(mem) }, type{ vk::ObjectType::eDeviceMemory } { }

		uint64 handle;
		vk::ObjectType type;
	}; // struct VkHandle

	inline uint64 resourceId() const { return uuid_; }
	inline vk::ObjectType resourceType() const { return type_; }
	inline const String& resourceName() const { return name_; }
	void resourceName(const StringView& name);
	inline uint64 resourceLastUse() const { return lastUse_; }
	void resourceMarkUsed() const;

	// Called in child type dtor or containing type dtors to schedule safe object destruction
	void queueDestroy(const std::initializer_list<VkHandle>& handles, bool immediate = false);

protected:
	ResourceBase(vk::ObjectType type);


	// Gets the object handle, or std::nullopt if the resource type does not have a handle
	virtual Opt<uint64> resourceHandle() const = 0;

private:
	const uint64 uuid_;
	const vk::ObjectType type_;
	String name_;
	mutable uint64 lastUse_;

	VEGA_NO_COPY(ResourceBase)
	VEGA_NO_MOVE(ResourceBase)
}; // class Resource

} // namespace vega
