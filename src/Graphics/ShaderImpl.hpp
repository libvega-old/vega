/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <Vega/Graphics/Shader.hpp>
#include "./ResourceBase.hpp"

#include <array>
#include <vector>


namespace vega
{

//! Implementation for Shader
class ShaderImpl final
	: public Shader, public ResourceBase
{
	friend class Shader;

public:
	using VertexUseMap = std::array<uint8, VertexUse::COUNT>;
	using Bytecode = std::vector<uint32>;

	struct Modules final
	{
		vk::ShaderModule vert{ };
		vk::ShaderModule frag{ };
	}; // struct Modules

	struct Reflection final
	{
		ShaderStageMask stageMask{ };
		VertexUseMask vertexMask{ };
		VertexUseMap vertexMap{ }; // Uses UINT8_MAX to indicate an invalid use
		std::vector<ShaderInput> inputs{ };
		std::vector<ShaderOutput> outputs{ };
		std::vector<ShaderBinding> bindings{ };
		std::vector<ShaderSubpassInput> subpassInputs{ };
		struct
		{
			ShaderStageMask stageMask{ };
			uint32 size{ };
			std::vector<ShaderUniformMember> members{ };
		} uniform{ };
	}; // struct Reflection

	ShaderImpl(Modules&& mods, Reflection&& refl);
	~ShaderImpl();

	inline const Modules& modules() const { return modules_; }
	inline const Reflection& refl() const { return refl_; }
	inline vk::PipelineLayout pipelineLayout() const { return pipelineLayout_; }
	inline vk::DescriptorSetLayout inputLayout() const { return inputLayout_; }

protected:
	inline Opt<uint64> resourceHandle() const override { return uint64(VkPipelineLayout(pipelineLayout_)); }

private:
	const Modules modules_;
	const Reflection refl_;
	vk::PipelineLayout pipelineLayout_;
	vk::DescriptorSetLayout inputLayout_;

	VEGA_NO_COPY(ShaderImpl)
	VEGA_NO_MOVE(ShaderImpl)
}; // class ShaderImpl

DECLARE_IMPL_GETTER(Shader)

} // namespace vega
