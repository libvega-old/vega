/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./ShaderImpl.hpp"

#include <algorithm>


namespace vega
{

#define THIS_IMPL_TYPE ShaderImpl

// ====================================================================================================================
Shader::Shader()
	: AssetBase(typeid(Shader))
{

}

// ====================================================================================================================
Shader::~Shader()
{

}

// ====================================================================================================================
ShaderStageMask Shader::stageMask() const
{
	return THIS_IMPL->refl_.stageMask;
}

// ====================================================================================================================
VertexUseMask Shader::vertexMask() const
{
	return THIS_IMPL->refl_.vertexMask;
}

// ====================================================================================================================
const std::vector<ShaderInput>& Shader::inputs() const
{
	return THIS_IMPL->refl_.inputs;
}

// ====================================================================================================================
const std::vector<ShaderOutput>& Shader::outputs() const
{
	return THIS_IMPL->refl_.outputs;
}

// ====================================================================================================================
const std::vector<ShaderBinding>& Shader::bindings() const
{
	return THIS_IMPL->refl_.bindings;
}

// ====================================================================================================================
const std::vector<ShaderSubpassInput>& Shader::subpassInputs() const
{
	return THIS_IMPL->refl_.subpassInputs;
}

// ====================================================================================================================
uint32 Shader::uniformSize() const
{
	return THIS_IMPL->refl_.uniform.size;
}

// ====================================================================================================================
const std::vector<ShaderUniformMember>& Shader::uniformMembers() const
{
	return THIS_IMPL->refl_.uniform.members;
}

// ====================================================================================================================
const ShaderInput* Shader::findInput(VertexUse use) const
{
	// Check against the map for a quick return
	if (THIS_IMPL->refl_.vertexMap[use.value()] == UINT8_MAX) {
		return nullptr;
	}

	// Perform the lookup
	const auto& inputs = THIS_IMPL->refl_.inputs;
	const auto it = std::find_if(inputs.begin(), inputs.end(),
		[use](const ShaderInput& input) { return input.use() == use; });
	return &*it;
}

// ====================================================================================================================
const ShaderBinding* Shader::findBinding(uint32 slot) const
{
	// Check for quick return
	if (slot >= MAX_BINDING_COUNT) {
		return nullptr;
	}

	// Search the binding list
	const auto& binds = THIS_IMPL->refl_.bindings;
	const auto it = std::find_if(binds.begin(), binds.end(), [slot](const ShaderBinding& bind) {
		return bind.slot() == slot;
	});
	return (it != binds.end()) ? &*it : nullptr;
}

} // namespace vega
