/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./Buffer/TexelBufferImpl.hpp"


namespace vega
{

#define THIS_IMPL_TYPE TexelBufferImpl

// ====================================================================================================================
TexelBuffer::TexelBuffer()
{

}

// ====================================================================================================================
TexelBuffer::~TexelBuffer()
{

}

// ====================================================================================================================
uint32 TexelBuffer::texelCount() const
{
	return THIS_IMPL->count_;
}

// ====================================================================================================================
TexelFormat TexelBuffer::format() const
{
	return THIS_IMPL->format_;
}

// ====================================================================================================================
uint64 TexelBuffer::size() const
{
	return THIS_IMPL->buffer_.size();
}

// ====================================================================================================================
void TexelBuffer::setData(const void* data, uint32 texelCount, uint32 texelOffset)
{
	const uint64 texelSize{ THIS_IMPL->format_.size() };
	THIS_IMPL->buffer_.setData(texelOffset * texelSize, data, texelCount * texelSize);
}

// ====================================================================================================================
void TexelBuffer::setData(const StagingBuffer& data, uint32 texelCount, uint64 dataOffset, uint32 texelOffset)
{
	const uint64 texelSize{ THIS_IMPL->format_.size() };
	THIS_IMPL->buffer_.setData(texelOffset * texelSize, data, texelCount * texelSize, dataOffset);
}

} // namespace vega
