/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Graphics/Multisampling.hpp>

#include <sstream>


namespace vega
{

// ====================================================================================================================
String to_string(MSAAMask msaa)
{
	if (msaa.rawValue() == 0) {
		return "None";
	}

	std::stringstream ss{};
	uint32 maskCount{ 0 };
	if (msaa & MSAA::X1) {
		ss << "X1|";
		maskCount++;
	}
	if (msaa & MSAA::X2) {
		ss << "X2|";
		maskCount++;
	}
	if (msaa & MSAA::X4) {
		ss << "X4|";
		maskCount++;
	}
	if (msaa & MSAA::X8) {
		ss << "X8|";
		maskCount++;
	}
	if (msaa & MSAA::X16) {
		ss << "X16|";
		maskCount++;
	}
	const String res{ ss.str() };
	return (maskCount == 1) 
		? res.substr(0, res.size() - 1) 
		: ('[' + res.substr(0, res.size() - 1) + ']');
}

} // namespace vega
