/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include "../ResourceBase.hpp"
#include "../Vulkan.hpp"
#include <Vega/Render/Sampler.hpp>

#include <array>
#include <atomic>
#include <mutex>
#include <vector>


namespace vega
{

class MemoryAllocation;

// Manages shared global resources and delayed use-safe destruction of resources
// This handles two resource paradigms:
//   1. Frame Resources - used within the app frame context, can be destroyed without checks after N frames
//   2. Async Resorces - used outside of app frames, must be queued with a fence to check for safe destruction
class ResourceManager final
{
	// Contains a record for a specific object to destroy
	struct DestroyItem final
	{
		DestroyItem(uint64 h, vk::ObjectType t, uint64 sh = 0) : handle{ h }, type{ t }, secondaryHandle{ sh } { }
		uint64 handle{ };
		vk::ObjectType type{ };
		uint64 secondaryHandle{ };
	}; // struct DestroyItem

	// Manages a queue of items to destroy
	struct DestroyQueue final
	{
		DestroyQueue() = default;
		std::vector<DestroyItem> items{ };
		std::mutex mutex{ };
	}; // struct DestroyQueue

public:
	// Number of samplers 
	inline static constexpr uint32 SAMPLER_COUNT{ uint32(Sampler::LinearLinearTransparent) + 1 };

	ResourceManager();
	~ResourceManager();

	void registerThread();
	void unregisterThread();
	void update();

	// Resource Destroy
	void safeDestroy(const ResourceBase& resource, const std::initializer_list<ResourceBase::VkHandle>& handles, 
		bool immediate);
	void safeDestroy(uint64 handle, vk::ObjectType type, uint64 secondaryHandle = 0);
	template<typename VulkanT>
	inline void safeDestroy(VulkanT vkobj) {
		safeDestroy(uint64((typename VulkanT::CType)vkobj), VulkanT::objectType, 0);
	}
	template<typename VulkanT, typename VulkanU>
	inline void safeDestroy(VulkanT vkobj, VulkanU vksec) {
		safeDestroy(uint64((typename VulkanT::CType)vkobj), VulkanT::objectType, uint64((typename VulkanU::CType)vksec));
	}
	inline void safeDestroy(MemoryAllocation* mem) {
		safeDestroy(uint64(mem), vk::ObjectType::eDeviceMemory, 0);
	}
	void destroyItem(uint64 handle, vk::ObjectType type, uint64 secondaryHandle = 0);
	template<typename VulkanT>
	inline void destroyItem(VulkanT vkobj) {
		destroyItem(uint64((typename VulkanT::CType)vkobj), VulkanT::objectType, 0);
	}
	template<typename VulkanT, typename VulkanU>
	inline void destroyItem(VulkanT vkobj, VulkanU vksec) {
		destroyItem(uint64((typename VulkanT::CType)vkobj), VulkanT::objectType, uint64((typename VulkanU::CType)vksec));
	}
	inline void destroyItem(MemoryAllocation* mem) {
		destroyItem(uint64(mem), vk::ObjectType::eDeviceMemory, 0);
	}

	// Samplers
	vk::Sampler getSampler(Sampler sampler);

	// Global resources
	inline vk::Buffer getZeroBuffer() { return zeroBuffer_.buffer; }

	static ResourceManager* Get();

private:
	static vk::Sampler CreateSampler(Sampler sampler);

private:
	struct
	{
		std::array<DestroyQueue, Vulkan::MAX_PARALLEL_FRAMES> itemQueues{ };
		std::atomic<DestroyQueue*> activeItemQueue{ };
		uint32 frameIndex{ 0 };
	} destroy_;
	struct
	{
		std::array<vk::Sampler, SAMPLER_COUNT> handles{ };
		std::mutex mutex{ };
	} samplers_;
	struct
	{
		vk::Buffer buffer{ };
		vk::DeviceMemory memory{ };
	} zeroBuffer_;

	VEGA_NO_COPY(ResourceManager)
	VEGA_NO_MOVE(ResourceManager)
}; // class ResourceManager

} // namespace vega
