/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./MemoryManager.hpp"
#include "../GraphicsEngineImpl.hpp"


namespace vega
{

// ====================================================================================================================
MemoryType::MemoryType(const vk::MemoryHeap* heap, const vk::MemoryType* type, uint32 typeIndex)
	: heap_{ heap }
	, type_{ type }
	, typeIndex_{ typeIndex }
{

}

// ====================================================================================================================
MemoryType::~MemoryType()
{

}


// ====================================================================================================================
// ====================================================================================================================
MemoryAllocation::MemoryAllocation(vk::DeviceMemory memory, uint64 offset, uint64 size, vk::MemoryPropertyFlags flags)
	: memory_{ memory }
	, offset_{ offset }
	, size_{ size }
	, mapped_{ nullptr }
	, flags_{ flags }
{

}

// ====================================================================================================================
MemoryAllocation::~MemoryAllocation()
{
	if (mapped_) {
		unmap();
	}
	Vulkan::Device().freeMemory(memory_);
}

// ====================================================================================================================
void* MemoryAllocation::map()
{
	// Check if can map or already mapped
	assert(isMappable() && "Attempted to map unmappable memory");
	if (mapped_) {
		return mapped_;
	}

	// Create new map
	mapped_ = Vulkan::Device().mapMemory(memory_, offset_, size_); // TODO: Change when pooling is added
	return mapped_;
}

// ====================================================================================================================
void MemoryAllocation::unmap()
{
	// Unmap if mapped
	if (mapped_) {
		Vulkan::Device().unmapMemory(memory_); // TODO: Change when pooling is added
	}
	mapped_ = nullptr;
}


// ====================================================================================================================
// ====================================================================================================================
MemoryManager::MemoryManager()
	: heaps_{ }
	, types_{ }
	, deviceMem_{ }
	, hostMem_{ }
	, uploadMem_{ }
	, dynamicMem_{ }
	, transientMem_{ }
{
	// Get the memory objects
	const auto memProps = Vulkan::PhysicalDevice().getMemoryProperties();
	heaps_.assign(memProps.memoryHeaps.data(), memProps.memoryHeaps.data() + memProps.memoryHeapCount);
	types_.assign(memProps.memoryTypes.data(), memProps.memoryTypes.data() + memProps.memoryTypeCount);

	// Find the type indices
	const Opt<uint32>
		deviceIdx = FindMemoryType(types_,
			vk::MemoryPropertyFlagBits::eDeviceLocal,
			{ },
			vk::MemoryPropertyFlagBits::eHostVisible
		),
		hostIdx = FindMemoryType(types_,
			vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCached,
			vk::MemoryPropertyFlagBits::eHostCoherent,
			vk::MemoryPropertyFlagBits::eDeviceLocal
		),
		uploadIdx = FindMemoryType(types_,
			vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent,
			{ },
			vk::MemoryPropertyFlagBits::eHostCached
		),
		dynamicIdx = FindMemoryType(types_,
			vk::MemoryPropertyFlagBits::eDeviceLocal | vk::MemoryPropertyFlagBits::eHostVisible,
			{ },
			vk::MemoryPropertyFlagBits::eHostCached
		),
		transientIdx = FindMemoryType(types_,
			vk::MemoryPropertyFlagBits::eDeviceLocal | vk::MemoryPropertyFlagBits::eLazilyAllocated,
			{ },
			vk::MemoryPropertyFlagBits::eHostVisible
		);

	// Check and assign memory types
	if (const auto idx = deviceIdx.value_or(0); deviceIdx.has_value()) {
		deviceMem_ = std::make_shared<MemoryType>(&heaps_[types_[idx].heapIndex], &types_[idx], idx);
	}
	else {
		throw std::runtime_error("Selected device does not report any video memory"); // Non-compliant driver?
	}
	if (const auto idx = hostIdx.value_or(0); hostIdx.has_value()) {
		hostMem_ = (idx == deviceMem_->typeIndex())
			? deviceMem_ // Integrated processor
			: std::make_shared<MemoryType>(&heaps_[types_[idx].heapIndex], &types_[idx], idx); // Discrete processor
	}
	else {
		throw std::runtime_error("Selected device does not report any host memory"); // How???
	}
	if (const auto idx = uploadIdx.value_or(0); uploadIdx.has_value()) {
		uploadMem_ = (idx == hostMem_->typeIndex()) 
			? hostMem_ 
			: std::make_shared<MemoryType>(&heaps_[types_[idx].heapIndex], &types_[idx], idx);
	}
	else {
		uploadMem_ = hostMem_;
	}
	if (const auto idx = dynamicIdx.value_or(0); dynamicIdx.has_value()) {
		dynamicMem_ = 
			(idx == hostMem_->typeIndex()) ? hostMem_ :
			(idx == uploadMem_->typeIndex()) ? uploadMem_ :
			std::make_shared<MemoryType>(&heaps_[types_[idx].heapIndex], &types_[idx], idx);
	}
	else {
		dynamicMem_ = uploadMem_;
	}
	if (const auto idx = transientIdx.value_or(0); transientIdx.has_value()) {
		transientMem_ = (idx == deviceMem_->typeIndex())
			? deviceMem_
			: std::make_shared<MemoryType>(&heaps_[types_[idx].heapIndex], &types_[idx], idx);
	}
	else {
		transientMem_ = deviceMem_;
	}
}

// ====================================================================================================================
MemoryManager::~MemoryManager()
{
	deviceMem_.reset();
	hostMem_.reset();
	uploadMem_.reset();
	dynamicMem_.reset();
	transientMem_.reset();
}

// ====================================================================================================================
void MemoryManager::registerThread()
{

}

// ====================================================================================================================
void MemoryManager::unregisterThread()
{

}

// ====================================================================================================================
void MemoryManager::update()
{

}

// ====================================================================================================================
MemoryManager* MemoryManager::Get()
{
	return GraphicsEngineImpl::Get()->memoryManager();
}

// ====================================================================================================================
MemoryHandle MemoryManager::allocateImage(vk::Image image, bool dedicated, MemoryType& type)
{
	const auto vkdevice = Vulkan::Device();
	const auto memReq = vkdevice.getImageMemoryRequirements(image);

	// Check the types are correct
	if (!((1u << type.typeIndex()) & memReq.memoryTypeBits)) {
		assert(false && "Attempted to allocate image memory from an invalid type");
	}

	// Allocate the memory
	vk::MemoryAllocateInfo mai{};
	mai.allocationSize = memReq.size;
	mai.memoryTypeIndex = type.typeIndex();
	vk::DeviceMemory memory{};
	const auto res = vkdevice.allocateMemory(&mai, nullptr, &memory);

	// Check the result
	if (res == vk::Result::eErrorOutOfDeviceMemory) {
		throw std::runtime_error("Failed to allocate image memory - out of device memory");
	}
	else if (res == vk::Result::eErrorOutOfHostMemory) {
		throw std::runtime_error("Failed to allocate image memory - out of host memory");
	}
	else if (res != vk::Result::eSuccess) {
		throw std::runtime_error(to_string("Failed to allocate image memory - %s", vk::to_string(res).c_str()));
	}

	// Create the allocation
	return std::make_unique<MemoryAllocation>(memory, 0, mai.allocationSize, type.type()->propertyFlags);
}

// ====================================================================================================================
MemoryHandle MemoryManager::allocateBuffer(vk::Buffer buffer, bool dedicated, MemoryType& type)
{
	const auto vkdevice = Vulkan::Device();
	const auto memReq = vkdevice.getBufferMemoryRequirements(buffer);

	// Check the types are correct
	if (!((1u << type.typeIndex()) & memReq.memoryTypeBits)) {
		assert(false && "Attempted to allocate buffer memory from an invalid type");
	}

	// Allocate the memory
	vk::MemoryAllocateInfo mai{};
	mai.allocationSize = memReq.size;
	mai.memoryTypeIndex = type.typeIndex();
	vk::DeviceMemory memory{};
	const auto res = vkdevice.allocateMemory(&mai, nullptr, &memory);

	// Check the result
	if (res == vk::Result::eErrorOutOfDeviceMemory) {
		throw std::runtime_error("Failed to allocate buffer memory - out of device memory");
	}
	else if (res == vk::Result::eErrorOutOfHostMemory) {
		throw std::runtime_error("Failed to allocate buffer memory - out of host memory");
	}
	else if (res != vk::Result::eSuccess) {
		throw std::runtime_error(to_string("Failed to allocate buffer memory - %s", vk::to_string(res).c_str()));
	}

	// Create the allocation
	return std::make_unique<MemoryAllocation>(memory, 0, mai.allocationSize, type.type()->propertyFlags);
}

// ====================================================================================================================
Opt<uint32> MemoryManager::FindMemoryType(const std::vector<vk::MemoryType>& types, vk::MemoryPropertyFlags reqFlags, 
	vk::MemoryPropertyFlags prefFlags, vk::MemoryPropertyFlags notPrefFlags)
{
	for (const auto noMask : { notPrefFlags, vk::MemoryPropertyFlags{ } }) {
		for (const auto yesMask : { reqFlags | prefFlags, reqFlags }) {
			for (uint32 mi = 0; mi < types.size(); ++mi) {
				const auto& type = types[mi];
				const bool yesPass = (type.propertyFlags & yesMask) == yesMask;
				const bool noPass = uint32(type.propertyFlags & noMask) == 0;
				if (yesPass && noPass) {
					return mi;
				}
			}
		}
	}
	return { };
}

} // namespace vega
