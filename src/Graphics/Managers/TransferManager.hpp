/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include <Vega/Core/Time.hpp>
#include "../Vulkan.hpp"

#include <mutex>
#include <vector>


namespace vega
{

class StagingBufferImpl;

// Describes a region of texel data
struct ImageRegion final
{
	uint32 x;
	uint32 y;
	uint32 z;
	uint32 width;
	uint32 height;
	uint32 depth;
	uint32 layer;
	uint32 layerCount;

	ImageRegion() = default;
	ImageRegion(uint32 x, uint32 y, uint32 z, uint32 w, uint32 h, uint32 d, uint32 l, uint32 lc)
		: x{ x }, y{ y }, z{ z }, width{ w }, height{ h }, depth{ d }, layer{ l }, layerCount{ lc }
	{ }
}; // struct ImageRegion


// Manages uploads and downloads of buffer and image data
class TransferManager final
{
	// Number of non-transient stagers to keep around
	inline static constexpr uint32 PERMANENT_STAGER_COUNT{ 4 };
	// Size of the non-transient stagers
	inline static constexpr uint64 INITIAL_STAGER_SIZE{ 2 * 1024 * 1024 /* 2MB */ };
	// Maximum number of stagers globally
	inline static constexpr uint32 MAX_STAGER_COUNT{ 16 };
	// Amount of time before stagers are cleaned up from disuse (unless permanent)
	inline static constexpr TimeSpan UNUSED_STAGER_DELETE_TIME{ 5_sec };

	// Thread-safe wrapper for a staging buffer
	class Stager final
	{
	public:
		Stager(uint64 size, bool temp);
		~Stager();

		inline StagingBufferImpl* buffer() const { return buffer_.get(); }
		inline bool isUsed() const { return used_; }
		inline bool isTemp() const { return temp_; }

		inline void markUsed() { used_ = true; }
		inline void markFree() { used_ = false; }

		bool isIdle() const;

	private:
		UPtr<StagingBufferImpl> buffer_;
		bool used_;
		bool temp_;

		VEGA_NO_COPY(Stager)
		VEGA_NO_MOVE(Stager)
	}; // class Stager

public:
	TransferManager();
	~TransferManager();

	void registerThread();
	void unregisterThread();
	void update();

	// Buffer Data
	void setBufferData(vk::Buffer dstBuffer, vk::BufferUsageFlags usage, uint64 dstOffset, const void* srcPtr,
		uint64 count);
	void setBufferDataImpl(vk::Buffer dstBuffer, uint64 dstOffset, const StagingBufferImpl& srcBuffer,
		uint64 srcOffset, uint64 count, vk::BufferUsageFlags dstUsage);

	// Image Data
	void setImageData(vk::Image dstImage, vk::ImageUsageFlags usage, const ImageRegion& reg, const void* srcPtr,
		uint64 dataSize, bool wholeImage);
	void setImageDataImpl(vk::Image dstImage, const ImageRegion& dstRegion, const StagingBufferImpl& srcBuffer,
		uint64 srcOffset, vk::ImageUsageFlags dstUsage, bool wholeImage);

	static TransferManager* Get();

private:
	Stager* reserveOrAllocateStager(uint64 size);

	static void PopulateSrcBarrier(vk::BufferUsageFlags usage, vk::BufferMemoryBarrier* bmb,
		vk::PipelineStageFlags* srcStages, vk::PipelineStageFlags* dstStages);
	static void PopulateDstBarrier(vk::BufferUsageFlags usage, vk::BufferMemoryBarrier* bmb,
		vk::PipelineStageFlags* srcStages, vk::PipelineStageFlags* dstStages);
	static void PopulateSrcBarrier(vk::ImageUsageFlags usage, vk::ImageMemoryBarrier* imb,
		vk::PipelineStageFlags* srcStages, vk::PipelineStageFlags* dstStages, bool wholeImage);
	static void PopulateDstBarrier(vk::ImageUsageFlags usage, vk::ImageMemoryBarrier* imb,
		vk::PipelineStageFlags* srcStages, vk::PipelineStageFlags* dstStages);

private:
	std::vector<UPtr<Stager>> stagers_;
	std::mutex stagerMutex_;

	VEGA_NO_COPY(TransferManager)
	VEGA_NO_MOVE(TransferManager)
}; // class TransferManager

} // namespace vega
