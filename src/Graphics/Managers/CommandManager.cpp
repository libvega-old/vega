/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./CommandManager.hpp"
#include "../GraphicsEngineImpl.hpp"
#include <Vega/Core/Time.hpp>

#include <algorithm>
#include <atomic>


namespace vega
{

// ====================================================================================================================
bool CommandFence::Wait::check() const
{
	if (const auto fence = fence_.lock(); fence) {
		return Vulkan::Device().getSemaphoreCounterValue(fence->handle_) >= waitVal_;
	}
	return true; // The fence has been deleted
}

// ====================================================================================================================
bool CommandFence::Wait::wait(uint64 timeout) const
{
	if (const auto fence = fence_.lock(); fence) {
		return fence->wait(waitVal_, timeout);
	}
	return true; // Fence has been deleted, no waiting
}

// ====================================================================================================================
CommandFence::CommandFence()
	: handle_{ }
	, useCount_{ 0 }
{
	static std::atomic_uint32_t FenceCount_{ 0 };

	vk::SemaphoreTypeCreateInfo tci{};
	tci.semaphoreType = vk::SemaphoreType::eTimeline;
	tci.initialValue = 0;
	vk::SemaphoreCreateInfo ci{};
	ci.pNext = &tci;
	handle_ = Vulkan::Device().createSemaphore(ci);
	Vulkan::SetObjectName(handle_, to_string("CommandFenceSemaphore%u", FenceCount_.fetch_add(1)));
}

// ====================================================================================================================
CommandFence::~CommandFence()
{
	if (pending()) {
		wait(useCount_);
	}
	Vulkan::Device().destroySemaphore(handle_);
}

// ====================================================================================================================
bool CommandFence::pending() const
{
	return Vulkan::Device().getSemaphoreCounterValue(handle_) < useCount_;
}

// ====================================================================================================================
bool CommandFence::wait(uint64 value, uint64 timeout) const
{
	vk::SemaphoreWaitInfo wait{};
	wait.semaphoreCount = 1;
	wait.pSemaphores = &handle_;
	wait.pValues = &value;
	return Vulkan::Device().waitSemaphores(wait, timeout) == vk::Result::eSuccess;
}


// ====================================================================================================================
// ====================================================================================================================
CommandManager::FrameCommands::FrameCommands(CommandManager* manager)
	: manager_{ manager }
	, frames_{ }
	, frameIndex_{ 0 }
{
	const auto vkdevice = Vulkan::Device();
	const auto tidx = GraphicsEngineImpl::Get()->threadIndex();

	// Create the transient pools
	vk::CommandPoolCreateInfo cpci{};
	cpci.flags = vk::CommandPoolCreateFlagBits::eTransient;
	cpci.queueFamilyIndex = manager->graphicsFamily();
	uint32 frameIndex{ 0 };
	for (auto& frame : frames_) {
		frame.pool = vkdevice.createCommandPool(cpci);
		Vulkan::SetObjectName(frame.pool, to_string("FrameCommandPool%u.%u", tidx, frameIndex++));
	}

	// Initial pool allocations
	for (auto& frame : frames_) {
		growPool(frame, vk::CommandBufferLevel::ePrimary);
		growPool(frame, vk::CommandBufferLevel::eSecondary);
	}
}

// ====================================================================================================================
CommandManager::FrameCommands::~FrameCommands()
{
	const auto vkdevice = Vulkan::Device();

	// Simply wait for the device to be idle
	vkdevice.waitIdle();
	for (auto& frame : frames_) {
		vkdevice.destroyCommandPool(frame.pool);
	}
}

// ====================================================================================================================
void CommandManager::FrameCommands::nextFrame()
{
	frameIndex_ = (frameIndex_ + 1u) % Vulkan::MAX_PARALLEL_FRAMES;
	auto& frame = frames_[frameIndex_];
	Vulkan::Device().resetCommandPool(frame.pool, vk::CommandPoolResetFlagBits::eReleaseResources);
	frame.primaryOffset = 0;
	frame.secondaryOffset = 0;
}

// ====================================================================================================================
vk::CommandBuffer CommandManager::FrameCommands::allocate(vk::CommandBufferLevel level)
{
	// Get the objects
	auto& frame = frames_[frameIndex_];
	auto& buffers = (level == vk::CommandBufferLevel::ePrimary) ? frame.primaries : frame.secondaries;
	auto& offset = (level == vk::CommandBufferLevel::ePrimary) ? frame.primaryOffset : frame.secondaryOffset;

	// Return a push-allocated buffer, growing first if needed
	if (offset == buffers.size()) {
		growPool(frame, level);
	}
	return buffers[offset++];
}

// ====================================================================================================================
void CommandManager::FrameCommands::growPool(Frame& frame, vk::CommandBufferLevel level)
{
	// Allocate new buffers
	vk::CommandBufferAllocateInfo cbai{};
	cbai.commandPool = frame.pool;
	cbai.level = level;
	cbai.commandBufferCount = GROW_SIZE;
	const auto newBuffers = Vulkan::Device().allocateCommandBuffers(cbai);

	// Add to the pool
	auto& buffers = (level == vk::CommandBufferLevel::ePrimary) ? frame.primaries : frame.secondaries;
	buffers.insert(buffers.end(), newBuffers.begin(), newBuffers.end());
}


// ====================================================================================================================
// ====================================================================================================================
CommandManager::OneTimeCommands::OneTimeCommands(CommandManager* manager)
	: pool_{ }
	, buffers_{ }
	, fences_{ }
	, index_{ 0 }
{
	// Create the pool
	vk::CommandPoolCreateInfo cpci{};
	cpci.flags = vk::CommandPoolCreateFlagBits::eTransient | vk::CommandPoolCreateFlagBits::eResetCommandBuffer;
	cpci.queueFamilyIndex = manager->graphicsFamily();
	pool_ = Vulkan::Device().createCommandPool(cpci);
	Vulkan::SetObjectName(pool_, to_string("OneTimeCommandPool%u", GraphicsEngineImpl::Get()->threadIndex()));

	// Allocate the buffers and fences
	vk::CommandBufferAllocateInfo cbai{};
	cbai.commandPool = pool_;
	cbai.level = vk::CommandBufferLevel::ePrimary;
	cbai.commandBufferCount = uint32(buffers_.size());
	const auto ringBuffers = Vulkan::Device().allocateCommandBuffers(cbai);
	for (uint32 bi = 0; bi < buffers_.size(); ++bi) {
		buffers_[bi] = ringBuffers[bi];
		fences_[bi] = std::make_shared<CommandFence>();
	}
}

// ====================================================================================================================
CommandManager::OneTimeCommands::~OneTimeCommands()
{
	// Destroy all fences (also causes a wait)
	for (auto& fence : fences_) {
		fence.reset();
	}

	// Destroy the pool
	Vulkan::Device().destroyCommandPool(pool_);
}

// ====================================================================================================================
std::tuple<vk::CommandBuffer, SPtr<CommandFence>> CommandManager::OneTimeCommands::recordGraphicsCommands(
	OneTimeCommand func)
{
	assert(func && "Cannot pass a null one-time command function");

	const auto vkdevice = Vulkan::Device();

	// Find and wait on the buffer to be used
	index_ = (index_ + 1) % uint32(buffers_.size());
	auto cmd = buffers_[index_];
	auto& cmdfence = fences_[index_];
	cmdfence->wait(cmdfence->useCount());

	// Record the commands
	vk::CommandBufferBeginInfo cbbi{};
	cbbi.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;
	cmd.begin(cbbi);
	const bool good = func(cmd);
	cmd.end();

	return { cmd, cmdfence };
}


// ====================================================================================================================
// ====================================================================================================================
thread_local UPtr<CommandManager::FrameCommands> CommandManager::FrameCommands_{ };
thread_local UPtr<CommandManager::OneTimeCommands> CommandManager::OneTimeCommands_{ };

// ====================================================================================================================
CommandManager::CommandManager()
	: graphicsQueue_{ }
	, frame_{ }
{
	// Create the graphics queue
	const auto [gFam, gIdx] = Vulkan::GraphicsQueueInfo();
	graphicsQueue_ = std::make_shared<DeviceQueue>(gFam, gIdx);
	Vulkan::SetObjectName(graphicsQueue_->handle, "GraphicsQueue");
}

// ====================================================================================================================
CommandManager::~CommandManager()
{
	graphicsQueue_.reset();
}

// ====================================================================================================================
void CommandManager::registerThread()
{
	auto& oneTimeCmds = OneTimeCommands_;
	oneTimeCmds = std::make_unique<OneTimeCommands>(this);

	auto& frameCmds = FrameCommands_;
	frameCmds = std::make_unique<FrameCommands>(this);
	std::lock_guard lock{ frame_.mutex };
	frame_.cmds.push_back(frameCmds.get());
}

// ====================================================================================================================
void CommandManager::unregisterThread()
{
	{
		std::lock_guard lock{ frame_.mutex };
		const auto it = std::find(frame_.cmds.begin(), frame_.cmds.end(), FrameCommands_.get());
		frame_.cmds.erase(it);
	}
	FrameCommands_.reset();

	OneTimeCommands_.reset();
}

// ====================================================================================================================
void CommandManager::update()
{
	// Update per-frame commands on all threads
	{
		std::lock_guard lock{ frame_.mutex };
		for (auto pool : frame_.cmds) {
			pool->nextFrame();
		}
	}
}

// ====================================================================================================================
vk::CommandBuffer CommandManager::allocateFrameCommand(vk::CommandBufferLevel level)
{
	return FrameCommands_->allocate(level);
}

// ====================================================================================================================
vk::Result CommandManager::present(const vk::PresentInfoKHR& info)
{
	std::lock_guard lock{ graphicsQueue_->mutex };
	return graphicsQueue_->handle.presentKHR(&info); // & is important here to skip the auto-exception on valid errors
}

// ====================================================================================================================
void CommandManager::submitGraphics(const CommandVector& buffers, const WaitVector& waits, const SignalVector& signals, 
	vk::Fence fence)
{
	constexpr uint32 MAX_SEMAPHORES{ 8 };
	static std::array<vk::Semaphore, MAX_SEMAPHORES> WaitSems_{ };
	static std::array<vk::Semaphore, MAX_SEMAPHORES> SignalSems_{ };
	static std::array<vk::PipelineStageFlags, MAX_SEMAPHORES> WaitStages_{ };
	static std::array<uint64, MAX_SEMAPHORES> WaitValues_{ };
	static std::array<uint64, MAX_SEMAPHORES> SignalValues_{ };

	std::lock_guard lock{ graphicsQueue_->mutex };

	// Write the semaphores
	uint32 waitCount{ 0 }, signalCount{ 0 };
	bool fenceOps{ false };
	for (const auto& wait : waits) {
		if (const auto semWait = std::get_if<SemaphoreWait>(&wait); semWait) {
			WaitSems_[waitCount]   = semWait->semaphore;
			WaitStages_[waitCount] = semWait->stageMask;
		}
		else {
			const auto fenceWait = std::get_if<CommandFence::DeviceWait>(&wait);
			WaitSems_[waitCount]   = fenceWait->fence_.lock()->handle_;
			WaitStages_[waitCount] = fenceWait->flags_;
			WaitValues_[waitCount] = fenceWait->waitVal_;
			fenceOps = true;
		}
		++waitCount;
	}
	for (const auto& signal : signals) {
		if (const auto semSignal = std::get_if<vk::Semaphore>(&signal); semSignal) {
			SignalSems_[signalCount] = *semSignal;
		}
		else {
			const auto fenceSignal = std::get_if<CommandFence::Signal>(&signal);
			const auto& cmdFence = fenceSignal->fence_.lock();
			SignalSems_[signalCount]   = cmdFence->handle_;
			SignalValues_[signalCount] = (cmdFence->useCount_ += 1);
			if (fenceSignal->wait_) {
				*fenceSignal->wait_ = { *cmdFence, cmdFence->useCount_ };
			}
			fenceOps = true;
		}
		++signalCount;
	}

	// Create the timeline sem info
	vk::TimelineSemaphoreSubmitInfo tssi{};
	if (fenceOps) {
		tssi.waitSemaphoreValueCount = waitCount;
		tssi.pWaitSemaphoreValues = WaitValues_.data();
		tssi.signalSemaphoreValueCount = signalCount;
		tssi.pSignalSemaphoreValues = SignalValues_.data();
	}

	// Create the submit info
	vk::SubmitInfo si{};
	si.waitSemaphoreCount   = waitCount;
	si.pWaitSemaphores      = WaitSems_.data();
	si.pWaitDstStageMask    = WaitStages_.data();
	si.commandBufferCount   = uint32(buffers.size());
	si.pCommandBuffers      = buffers.begin();
	si.signalSemaphoreCount = signalCount;
	si.pSignalSemaphores    = SignalSems_.data();
	si.pNext                = fenceOps ? &tssi : nullptr;
	
	// Submit
	graphicsQueue_->handle.submit(si, fence);
}

// ====================================================================================================================
void CommandManager::submitGraphics(const vk::SubmitInfo& info, vk::Fence fence)
{
	std::lock_guard lock{ graphicsQueue_->mutex };
	graphicsQueue_->handle.submit(info, fence);
}

// ====================================================================================================================
CommandFence::Wait CommandManager::submitGraphicsCommand(OneTimeCommand func,
	const std::initializer_list<WaitObject>& waits, const std::initializer_list<SignalObject>& signals,
	vk::Fence fence)
{
	const auto [cmd, cmdfence] = OneTimeCommands_->recordGraphicsCommands(func);

	CommandFence::Wait fencewait{};
	SignalVector signalVec{ signals };
	signalVec.emplace_back(std::in_place_type<CommandFence::Signal>, *cmdfence, &fencewait);

	submitGraphics({ cmd }, waits, signalVec, fence);
	return fencewait;
}

// ====================================================================================================================
CommandManager* CommandManager::Get()
{
	return GraphicsEngineImpl::Get()->commandManager();
}

} // namespace vega
