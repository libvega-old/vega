/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include <Vega/Core/Time.hpp>
#include <Vega/Math/Hash.hpp>
#include "../Vulkan.hpp"

#include <mutex>
#include <unordered_map>


namespace vega
{

class PipelineImpl; class RendererImpl; class ShaderImpl; struct StencilMode; struct ColorBlend;

// Contains a pipeline handle in the pipeline cache and info about its use
struct CachedPipeline final
{
public:
	CachedPipeline(vk::Pipeline handle) : handle_{ handle }, lastUse_{ 0 } { }

	inline vk::Pipeline handle() const { return handle_; }
	inline double lastUse() const { return lastUse_; }
	inline void markUsed() const { lastUse_ = Time::ElapsedRaw().seconds(); }

private:
	vk::Pipeline handle_;
	mutable double lastUse_;

	VEGA_NO_COPY(CachedPipeline)
	VEGA_NO_MOVE(CachedPipeline)
}; // struct CachedPipeline


// Manages pipeline caching and creation
class PipelineManager final
{
	using PipelineKey = std::tuple<Hash64, Hash64>;
	struct PipelineKeyHash final
	{
		inline size_t operator () (const PipelineKey& key) const {
			return std::get<0>(key).value() ^ std::get<1>(key).value();
		}
	}; // struct PipelineKeyHash

public:
	PipelineManager();
	~PipelineManager();

	void registerThread();
	void unregisterThread();
	void update();

	const CachedPipeline* getPipeline(const PipelineImpl& pipeline, const RendererImpl& renderer, uint32 subpass);

	static PipelineManager* Get();

private:
	vk::Pipeline createPipeline(const PipelineImpl& pipeline, const RendererImpl& renderer, uint32 subpass);
	void populateVertexBindingInfo(const ShaderImpl& shader,
		std::vector<vk::VertexInputBindingDescription>& bindings);
	void populateVertexAttributeInfo(const ShaderImpl& shader,
		std::vector<vk::VertexInputAttributeDescription>& attribs);
	void populateStencilState(const StencilMode& mode, vk::StencilOpState& vkstate);
	void populateColorBlend(const ColorBlend& blend, vk::PipelineColorBlendAttachmentState& vkstate);

private:
	vk::PipelineCache handle_;
	std::unordered_map<PipelineKey, CachedPipeline, PipelineKeyHash> roCache_;
	std::unordered_map<PipelineKey, CachedPipeline, PipelineKeyHash> rwCache_;
	std::mutex rwMutex_;

	VEGA_NO_COPY(PipelineManager)
	VEGA_NO_MOVE(PipelineManager)
}; // class PipelineManager

} // namespace vega
