/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./PipelineManager.hpp"
#include "./BindingManager.hpp"
#include "../GraphicsEngineImpl.hpp"
#include "../ShaderImpl.hpp"
#include "../../Render/PipelineImpl.hpp"
#include "../../Render/Renderer/RendererImpl.hpp"
#include "../../Render/RenderLayoutImpl.hpp"

#include <array>


namespace vega
{

// ====================================================================================================================
PipelineManager::PipelineManager()
	: handle_{ }
	, roCache_{ }
	, rwCache_{ }
	, rwMutex_{ }
{
	// Create the pipeline cache (TODO: Look into disk save/load)
	vk::PipelineCacheCreateInfo pcci{};
	pcci.initialDataSize = 0;
	pcci.pInitialData = nullptr;
	handle_ = Vulkan::Device().createPipelineCache(pcci);
}

// ====================================================================================================================
PipelineManager::~PipelineManager()
{
	const auto vkdev = Vulkan::Device();

	// Cleanup the cached pipelines
	for (const auto& pair : roCache_) {
		vkdev.destroyPipeline(pair.second.handle());
	}
	for (const auto& pair : rwCache_) {
		vkdev.destroyPipeline(pair.second.handle());
	}

	// Destroy the cache object
	if (handle_) {
		// TODO: Look into disk save/load
		Vulkan::Device().destroyPipelineCache(handle_);
	}
}

// ====================================================================================================================
void PipelineManager::registerThread()
{
	// NOP
}

// ====================================================================================================================
void PipelineManager::unregisterThread()
{
	// NOP
}

// ====================================================================================================================
void PipelineManager::update()
{
	// Move the rw pipelines into the ro cache
	for (const auto& pair : rwCache_) {
		roCache_.emplace(pair.first, pair.second.handle());
	}
	rwCache_.clear();
}

// ====================================================================================================================
const CachedPipeline* PipelineManager::getPipeline(const PipelineImpl& pipeline, const RendererImpl& renderer, 
	uint32 subpass)
{
	// Create the key
	const PipelineKey key{ pipeline.hash(), renderer.subpassHash(subpass) };

	// Lookup in ro cache
	{
		const auto it = roCache_.find(key);
		if (it != roCache_.end()) {
			return &(it->second);
		}
	}

	// Lookup in rw cache
	{
		std::lock_guard lock{ rwMutex_ };
		const auto it = rwCache_.find(key);
		if (it != rwCache_.end()) {
			return &(it->second);
		}

		// Create new pipeline, cache and return
		const auto newpl = createPipeline(pipeline, renderer, subpass);
		const auto& pair = rwCache_.emplace(key, newpl);
		return &(pair.first->second);
	}
}

// ====================================================================================================================
vk::Pipeline PipelineManager::createPipeline(const PipelineImpl& pipeline, const RendererImpl& renderer, 
	uint32 subpass)
{
	const auto vkdev = Vulkan::Device();
	const auto& states = pipeline.renderState();
	const auto isMsaa = renderer.msaa() != MSAA::X1;
	const auto& subpassLayout = isMsaa
		? get_impl(*renderer.layout()).msaaSubpasses()[subpass]
		: get_impl(*renderer.layout()).subpasses()[subpass];
	const auto& shader = get_impl(*pipeline.shader());

	vk::GraphicsPipelineCreateInfo pinfo{};
	pinfo.flags = { };

	// Shader binding table specialization
	static const uint32 BINDING_TABLE_SIZES[5]{
		BindingManager::TABLE_SAMPLER_COUNT, BindingManager::TABLE_IMAGE_COUNT, BindingManager::TABLE_BUFFER_COUNT,
		BindingManager::TABLE_ROTEXEL_COUNT, BindingManager::TABLE_RWTEXEL_COUNT
	};
	static const vk::SpecializationMapEntry SPEC_MAPS[5]{ // { id, offset, size } for all 5 table sizes
		{ 0, 0, 4 }, { 1, 4, 4 }, { 2, 8, 4 }, { 3, 12, 4 }, { 4, 16, 4, }
	};
	static const vk::SpecializationInfo SPEC_INFO{ 5, SPEC_MAPS, 20, BINDING_TABLE_SIZES };

	// Setup shader info (right now, vert and frag will always be the only ones present)
	std::array<vk::PipelineShaderStageCreateInfo, 2> shaderInfos{};
	{
		auto& vert = shaderInfos[0];
		vert.stage = vk::ShaderStageFlagBits::eVertex;
		vert.module = get_impl(*pipeline.shader()).modules().vert;
		vert.pName = "main";
		vert.pSpecializationInfo = &SPEC_INFO;
		auto& frag = shaderInfos[1];
		frag.stage = vk::ShaderStageFlagBits::eFragment;
		frag.module = get_impl(*pipeline.shader()).modules().frag;
		frag.pName = "main";
		frag.pSpecializationInfo = &SPEC_INFO;
	}
	pinfo.stageCount = uint32(shaderInfos.size());
	pinfo.pStages = shaderInfos.data();

	// Setup vertex input
	vk::PipelineVertexInputStateCreateInfo vertexState{};
	std::vector<vk::VertexInputBindingDescription> vertexBindings{};
	std::vector<vk::VertexInputAttributeDescription> vertexAttribs{};
	populateVertexBindingInfo(get_impl(*pipeline.shader()), vertexBindings);
	populateVertexAttributeInfo(get_impl(*pipeline.shader()), vertexAttribs);
	vertexState.vertexBindingDescriptionCount = uint32(vertexBindings.size());
	vertexState.pVertexBindingDescriptions = vertexBindings.data();
	vertexState.vertexAttributeDescriptionCount = uint32(vertexAttribs.size());
	vertexState.pVertexAttributeDescriptions = vertexAttribs.data();
	pinfo.pVertexInputState = &vertexState;

	// Setup input assembly
	vk::PipelineInputAssemblyStateCreateInfo assemblyState{};
	assemblyState.topology = vk::PrimitiveTopology(states.topology());
	assemblyState.primitiveRestartEnable = states.primitiveRestart() ? VK_TRUE : VK_FALSE;
	pinfo.pInputAssemblyState = &assemblyState;

	// Setup tessellation (TODO: Support once VSL adds tessellation stages)
	vk::PipelineTessellationStateCreateInfo tessState{};
	tessState.patchControlPoints = 0;
	pinfo.pTessellationState = nullptr;

	// Setup (dummy) viewport state, actually dynamically specified
	static const vk::Viewport DUMMY_VIEWPORT{ };
	static const vk::Rect2D DUMMY_SCISSOR{ };
	vk::PipelineViewportStateCreateInfo viewportState{};
	viewportState.viewportCount = 1;
	viewportState.pViewports = &DUMMY_VIEWPORT;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &DUMMY_SCISSOR;
	pinfo.pViewportState = &viewportState;

	// Setup rasterization
	vk::PipelineRasterizationStateCreateInfo rasterState{};
	rasterState.depthClampEnable = VK_FALSE; // TODO: Support enabling the feature and state for depth clamping
	rasterState.polygonMode = vk::PolygonMode(states.fillMode());
	rasterState.cullMode = vk::CullModeFlagBits(states.cullMode());
	rasterState.frontFace = vk::FrontFace::eClockwise;
	rasterState.depthBiasEnable = VK_FALSE; // TODO: Support depth bias
	rasterState.depthBiasConstantFactor = 0; // TODO: Support depth bias
	rasterState.depthBiasClamp = 0; // TODO: Support depth bias
	rasterState.depthBiasSlopeFactor = 0; // TODO: Support depth bias
	rasterState.lineWidth = 1; // TODO: Support enabling wideLines feature and settings
	pinfo.pRasterizationState = &rasterState;

	// Setup multisampling info
	vk::PipelineMultisampleStateCreateInfo msaaState{};
	msaaState.rasterizationSamples = 
		vk::SampleCountFlagBits((isMsaa && subpassLayout.msaa) ? renderer.msaa() : MSAA::X1);
	msaaState.sampleShadingEnable = VK_FALSE; // TODO: Support sample shading
	msaaState.minSampleShading = 0; // TODO: Support sample shading
	pinfo.pMultisampleState = &msaaState;

	// Setup depth/stencil states
	vk::PipelineDepthStencilStateCreateInfo depthState{};
	if (subpassLayout.depthTarget != VK_ATTACHMENT_UNUSED) {
		const bool hasStencil{ 
			isMsaa
				? get_impl(*renderer.layout()).msaaTargets()[subpassLayout.depthTarget].format.hasStencil()
				: get_impl(*renderer.layout()).targets()[subpassLayout.depthTarget].format.hasStencil()
		};

		depthState.depthTestEnable = states.depthMode() != DepthMode::Disabled;
		depthState.depthWriteEnable = states.depthMode() == DepthMode::ReadWrite;
		depthState.depthCompareOp = vk::CompareOp(states.depthOp());
		depthState.depthBoundsTestEnable = VK_FALSE; // TODO: Support depth bounds
		if (hasStencil) {
			depthState.stencilTestEnable = states.stencilFront().isEnabled() || states.stencilBack().isEnabled();
			populateStencilState(states.stencilFront(), depthState.front);
			populateStencilState(states.stencilBack(), depthState.back);
		}
		else {
			depthState.stencilTestEnable = VK_FALSE;
		}
		depthState.minDepthBounds = 0; // TODO: Support depth bounds
		depthState.maxDepthBounds = 1; // TODO: Support depth bounds
	}
	pinfo.pDepthStencilState = &depthState;

	// Setup color blending 
	vk::PipelineColorBlendStateCreateInfo blendState{};
	std::vector<vk::PipelineColorBlendAttachmentState> colorBlends{};
	if (subpassLayout.colorCount > 0) {
		populateColorBlend(states.color0().value_or(shader.outputs()[0].blend()), colorBlends.emplace_back());
	}
	if (subpassLayout.colorCount > 1) {
		populateColorBlend(states.color1().value_or(shader.outputs()[1].blend()), colorBlends.emplace_back());
	}
	if (subpassLayout.colorCount > 2) {
		populateColorBlend(states.color2().value_or(shader.outputs()[2].blend()), colorBlends.emplace_back());
	}
	if (subpassLayout.colorCount > 3) {
		populateColorBlend(states.color3().value_or(shader.outputs()[3].blend()), colorBlends.emplace_back());
	}
	blendState.attachmentCount = uint32(colorBlends.size());
	blendState.pAttachments = colorBlends.data();
	blendState.blendConstants.fill(0); // TODO: Support custom blend constants
	pinfo.pColorBlendState = &blendState;

	// Setup dynamic state
	const bool noInputs = shader.inputs().empty();
	static const std::array<vk::DynamicState, 4> DYNAMIC_STATES{ // Expand on this as needed
		vk::DynamicState::eViewport,          // Required dynamic state
		vk::DynamicState::eScissor,           // Required dynamic state
		vk::DynamicState::eStencilReference,  // Common dynamic state
		vk::DynamicState::eVertexInputBindingStrideEXT // Required for the mapping scheme used by Vega (keep at end)
	};
	vk::PipelineDynamicStateCreateInfo dynamicState{};
	dynamicState.dynamicStateCount = uint32(DYNAMIC_STATES.size()) - (noInputs ? 1u : 0u);
	dynamicState.pDynamicStates = DYNAMIC_STATES.data();
	pinfo.pDynamicState = &dynamicState;

	// Setup additional objects
	pinfo.layout = shader.pipelineLayout();
	pinfo.renderPass = renderer.renderPass();
	pinfo.subpass = subpass;

	// Create
	return Vulkan::Device().createGraphicsPipeline(handle_, pinfo).value;
}

// ====================================================================================================================
void PipelineManager::populateVertexBindingInfo(const ShaderImpl& shader,
	std::vector<vk::VertexInputBindingDescription>& bindings)
{
	bindings.resize(shader.inputs().size());
	for (uint32 ii = 0; ii < bindings.size(); ++ii) {
		const auto& input = shader.inputs()[ii];
		auto& binding = bindings[ii];
		binding.binding = ii;
		binding.stride = 0; // This is dynamically set when binding vertex buffers
		binding.inputRate = input.use().isInstanceUse() ? vk::VertexInputRate::eInstance : vk::VertexInputRate::eVertex;
	}
}

// ====================================================================================================================
void PipelineManager::populateVertexAttributeInfo(const ShaderImpl& shader,
	std::vector<vk::VertexInputAttributeDescription>& attribs)
{
	attribs.resize(shader.inputs().size());
	for (uint32 ii = 0; ii < attribs.size(); ++ii) {
		const auto& input = shader.inputs()[ii];
		auto& attrib = attribs[ii];
		attrib.location = input.location();
		attrib.binding = ii;
		attrib.format = vk::Format(input.format().formatId());
		attrib.offset = 0; // Always zero since offsets are dynamic
	}
}

// ====================================================================================================================
void PipelineManager::populateStencilState(const StencilMode& mode, vk::StencilOpState& vkstate)
{
	vkstate.failOp = vk::StencilOp(mode.fail());
	vkstate.passOp = vk::StencilOp(mode.pass());
	vkstate.depthFailOp = vk::StencilOp(mode.depthFail());
	vkstate.compareOp = vk::CompareOp(mode.compare());
	vkstate.compareMask = ~0;
	vkstate.writeMask = ~0;
	vkstate.reference = 0; // Dynamic state
}

// ====================================================================================================================
void PipelineManager::populateColorBlend(const ColorBlend& blend, vk::PipelineColorBlendAttachmentState& vkstate)
{
	vkstate.blendEnable = blend.enabled();
	if (blend.enabled()) {
		vkstate.srcColorBlendFactor = vk::BlendFactor(blend.srcColor());
		vkstate.dstColorBlendFactor = vk::BlendFactor(blend.dstColor());
		vkstate.colorBlendOp = vk::BlendOp(blend.colorOp());
		vkstate.srcAlphaBlendFactor = vk::BlendFactor(blend.srcAlpha());
		vkstate.dstAlphaBlendFactor = vk::BlendFactor(blend.dstAlpha());
		vkstate.alphaBlendOp = vk::BlendOp(blend.alphaOp());
	}
	vkstate.colorWriteMask = vk::ColorComponentFlags(blend.writeMask().rawValue());
}

// ====================================================================================================================
PipelineManager* PipelineManager::Get()
{
	return GraphicsEngineImpl::Get()->pipelineManager();
}

} // namespace vega
