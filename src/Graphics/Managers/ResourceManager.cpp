/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./ResourceManager.hpp"
#include "./MemoryManager.hpp"
#include "./CommandManager.hpp"
#include "../GraphicsEngineImpl.hpp"
#include <Vega/Core/AppBase.hpp>
#include <Vega/Core/Time.hpp>


namespace vega
{

// ====================================================================================================================
ResourceManager::ResourceManager()
	: destroy_{ }
	, samplers_{ }
	, zeroBuffer_{ }
{
	const auto vkdev = Vulkan::Device();

	// Initial destroy queue setup
	destroy_.activeItemQueue.store(&destroy_.itemQueues[0]);
}

// ====================================================================================================================
ResourceManager::~ResourceManager()
{
	// Destroy all pending items
	for (auto& queue : destroy_.itemQueues) {
		for (const auto& item : queue.items) {
			destroyItem(item.handle, item.type, item.secondaryHandle);
		}
	}

	// Destroy samplers
	for (auto sampler : samplers_.handles) {
		if (sampler) {
			destroyItem(sampler);
		}
	}

	// Destroy zero buffer
	destroyItem(zeroBuffer_.buffer);
	Vulkan::Device().freeMemory(zeroBuffer_.memory);
}

// ====================================================================================================================
void ResourceManager::registerThread()
{
	// Create zero buffer (first thread only)
	if (!zeroBuffer_.buffer) {
		const auto vkdev = Vulkan::Device();

		// Create buffer
		vk::BufferCreateInfo bci{};
		bci.size = 1024;
		bci.usage = vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eVertexBuffer |
			vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eStorageBuffer;
		bci.sharingMode = vk::SharingMode::eExclusive;
		zeroBuffer_.buffer = vkdev.createBuffer(bci);

		// Allocate and bind memory
		const auto memreq = vkdev.getBufferMemoryRequirements(zeroBuffer_.buffer);
		vk::MemoryAllocateInfo mai{};
		mai.allocationSize = memreq.size;
		mai.memoryTypeIndex = MemoryManager::Get()->deviceType()->typeIndex();
		zeroBuffer_.memory = vkdev.allocateMemory(mai);
		vkdev.bindBufferMemory(zeroBuffer_.buffer, zeroBuffer_.memory, 0);

		// Zero memory
		CommandManager::Get()->submitGraphicsCommand([this](vk::CommandBuffer cmd) -> bool {
			cmd.fillBuffer(zeroBuffer_.buffer, 0, VK_WHOLE_SIZE, 0);
			return true;
		}).wait();
	}
}

// ====================================================================================================================
void ResourceManager::unregisterThread()
{

}

// ====================================================================================================================
void ResourceManager::update()
{
	// Update to next frame
	destroy_.frameIndex = (destroy_.frameIndex + 1u) % Vulkan::MAX_PARALLEL_FRAMES;
	destroy_.activeItemQueue.store(&destroy_.itemQueues[destroy_.frameIndex]);

	// Perform frame-based resource cleanup
	{
		const auto queue = destroy_.activeItemQueue.load();
		std::unique_lock lock{ queue->mutex, std::defer_lock };
		while (!lock.try_lock()) { ; } // Should be an unlikely and very short wait
		for (const auto& item : queue->items) {
			destroyItem(item.handle, item.type, item.secondaryHandle);
		}
		queue->items.clear();
	}
}

// ====================================================================================================================
void ResourceManager::safeDestroy(const ResourceBase& resource, 
	const std::initializer_list<ResourceBase::VkHandle>& handles, bool immediate)
{
	const auto age = Time::FrameCount() - resource.resourceLastUse();
	if (immediate || (resource.resourceLastUse() == ResourceBase::NEVER_USED) || (age >= Vulkan::MAX_PARALLEL_FRAMES)) {
		// Not in use - destroy now
		for (const auto& handle : handles) {
			destroyItem(handle.handle, handle.type, 0);
		}
	}
	else { // Potentially in use - queue for later destroy
		for (const auto& handle : handles) {
			safeDestroy(handle.handle, handle.type, 0);
		}
	}
}

// ====================================================================================================================
void ResourceManager::safeDestroy(uint64 handle, vk::ObjectType type, uint64 secondaryHandle)
{
	const auto app = AppBase::Get();
	if (app->isTerminating()) {
		destroyItem(handle, type, secondaryHandle);
	}
	else {
		const auto queue = destroy_.activeItemQueue.load();
		std::lock_guard lock{ queue->mutex };
		queue->items.emplace_back(handle, type, secondaryHandle);
	}
}

// ====================================================================================================================
void ResourceManager::destroyItem(uint64 handle, vk::ObjectType type, uint64 secondaryHandle)
{
	const auto vkdevice = Vulkan::Device();

	switch (type)
	{
	case vk::ObjectType::eBuffer: vkdevice.destroyBuffer(VkBuffer(handle)); break;
	case vk::ObjectType::eBufferView: vkdevice.destroyBufferView(VkBufferView(handle)); break;
	case vk::ObjectType::eDescriptorPool: vkdevice.destroyDescriptorPool(VkDescriptorPool(handle)); break;
	case vk::ObjectType::eDeviceMemory: {
		MemoryAllocation* mem{ reinterpret_cast<MemoryAllocation*>(handle) };
		delete mem;
	} break;
	case vk::ObjectType::eImage: vkdevice.destroyImage(VkImage(handle)); break;
	case vk::ObjectType::eImageView: vkdevice.destroyImageView(VkImageView(handle)); break;
	case vk::ObjectType::ePipelineLayout: vkdevice.destroyPipelineLayout(VkPipelineLayout(handle)); break;
	case vk::ObjectType::eSampler: vkdevice.destroySampler(VkSampler(handle)); break;
	case vk::ObjectType::eShaderModule: vkdevice.destroyShaderModule(VkShaderModule(handle)); break;
	default: assert(false && "Unknown object type to destroy in ResourceManager");
	}
}

// ====================================================================================================================
vk::Sampler ResourceManager::getSampler(Sampler sampler)
{
	auto& samp = samplers_.handles[uint32(sampler)];
	if (samp) {
		return samp;
	}
	else {
		std::lock_guard lock{ samplers_.mutex };
		if (samp) {
			return samp; // This is needed to check for race conditions against an uninitialized sampler
		}
		samp = CreateSampler(sampler);
		return samp;
	}
}

// ====================================================================================================================
vk::Sampler ResourceManager::CreateSampler(Sampler sampler)
{
#define SETINFO(sname,fname,mname,aname,cname) case Sampler::sname: filter = vk::Filter::fname;\
	mipMode = vk::SamplerMipmapMode::mname; addrMode = vk::SamplerAddressMode::aname; bcolor = vk::BorderColor::cname;\
	break;

	vk::Filter filter{}; vk::SamplerMipmapMode mipMode{}; vk::SamplerAddressMode addrMode{}; vk::BorderColor bcolor{};
	switch (sampler)
	{
		SETINFO(NearNearRepeat, eNearest, eNearest, eRepeat, eFloatOpaqueBlack);
		SETINFO(NearNearMirror, eNearest, eNearest, eMirroredRepeat, eFloatOpaqueBlack);
		SETINFO(NearNearEdge, eNearest, eNearest, eClampToEdge, eFloatOpaqueBlack);
		SETINFO(NearNearBlack, eNearest, eNearest, eClampToBorder, eFloatOpaqueBlack);
		SETINFO(NearNearTransparent, eNearest, eNearest, eClampToBorder, eFloatTransparentBlack);
		SETINFO(NearLinearRepeat, eNearest, eLinear, eRepeat, eFloatOpaqueBlack);
		SETINFO(NearLinearMirror, eNearest, eLinear, eMirroredRepeat, eFloatOpaqueBlack);
		SETINFO(NearLinearEdge, eNearest, eLinear, eClampToEdge, eFloatOpaqueBlack);
		SETINFO(NearLinearBlack, eNearest, eLinear, eClampToBorder, eFloatOpaqueBlack);
		SETINFO(NearLinearTransparent, eNearest, eLinear, eClampToBorder, eFloatTransparentBlack);
		SETINFO(LinearNearRepeat, eLinear, eNearest, eRepeat, eFloatOpaqueBlack);
		SETINFO(LinearNearMirror, eLinear, eNearest, eMirroredRepeat, eFloatOpaqueBlack);
		SETINFO(LinearNearEdge, eLinear, eNearest, eClampToEdge, eFloatOpaqueBlack);
		SETINFO(LinearNearBlack, eLinear, eNearest, eClampToBorder, eFloatOpaqueBlack);
		SETINFO(LinearNearTransparent, eLinear, eNearest, eClampToBorder, eFloatTransparentBlack);
		SETINFO(LinearLinearRepeat, eLinear, eLinear, eRepeat, eFloatOpaqueBlack);
		SETINFO(LinearLinearMirror, eLinear, eLinear, eMirroredRepeat, eFloatOpaqueBlack);
		SETINFO(LinearLinearEdge, eLinear, eLinear, eClampToEdge, eFloatOpaqueBlack);
		SETINFO(LinearLinearBlack, eLinear, eLinear, eClampToBorder, eFloatOpaqueBlack);
		SETINFO(LinearLinearTransparent, eLinear, eLinear, eClampToBorder, eFloatTransparentBlack);
	default: assert(false && "Invalid sampler");
	}
#undef SETINFO

	// Sampler info
	vk::SamplerCreateInfo sci{};
	sci.magFilter = filter;
	sci.minFilter = filter;
	sci.mipmapMode = mipMode;
	sci.addressModeU = addrMode;
	sci.addressModeV = addrMode;
	sci.addressModeW = addrMode;
	sci.borderColor = bcolor;

	// Create sampler
	const auto vksamp = Vulkan::Device().createSampler(sci);
	Vulkan::SetObjectName(vksamp, to_string("Sampler(%s)", to_string(sampler).c_str()));
	return vksamp;
}

// ====================================================================================================================
ResourceManager* ResourceManager::Get()
{
	return GraphicsEngineImpl::Get()->resourceManager();
}

} // namespace vega
