/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./TransferManager.hpp"
#include "./CommandManager.hpp"
#include "../GraphicsEngineImpl.hpp"
#include "../Buffer/StagingBufferImpl.hpp"

#include <atomic>


namespace vega
{

// ====================================================================================================================
TransferManager::Stager::Stager(uint64 size, bool temp)
	: buffer_{ std::make_unique<StagingBufferImpl>(size) }
	, used_{ false }
	, temp_{ temp }
{

}

// ====================================================================================================================
TransferManager::Stager::~Stager()
{
	buffer_.reset();
}

// ====================================================================================================================
bool TransferManager::Stager::isIdle() const
{
	return !used_ && !buffer_->isPending();
}


// ====================================================================================================================
// ====================================================================================================================
TransferManager::TransferManager()
	: stagers_{ }
	, stagerMutex_{ }
{
	stagers_.reserve(MAX_STAGER_COUNT);
	for (uint32 i = 0; i < PERMANENT_STAGER_COUNT; ++i) {
		auto stager = stagers_.emplace_back(new Stager(INITIAL_STAGER_SIZE, false)).get();
		stager->buffer()->resourceName(to_string("TransferBuffer%u", i));
	}
}

// ====================================================================================================================
TransferManager::~TransferManager()
{
	stagers_.clear();
}

// ====================================================================================================================
void TransferManager::registerThread()
{

}

// ====================================================================================================================
void TransferManager::unregisterThread()
{

}

// ====================================================================================================================
void TransferManager::update()
{

}

// ====================================================================================================================
void TransferManager::setBufferData(vk::Buffer dstBuffer, vk::BufferUsageFlags usage, uint64 dstOffset,
	const void* srcPtr, uint64 count)
{
	// Allocate a stager and copy the data in
	const auto stager = reserveOrAllocateStager(count);
	std::memcpy(stager->buffer()->data(), srcPtr, count);

	// Submit the buffer data
	setBufferDataImpl(dstBuffer, dstOffset, *stager->buffer(), 0, count, usage);
	stager->markFree();

	// Destroy temp stagers immediately
	if (stager->isTemp()) {
		delete stager;
	}
}

// ====================================================================================================================
void TransferManager::setBufferDataImpl(vk::Buffer dstBuffer, uint64 dstOffset, const StagingBufferImpl& srcBuffer,
	uint64 srcOffset, uint64 count, vk::BufferUsageFlags dstUsage)
{
	const auto fence = CommandManager::Get()->submitGraphicsCommand(
		[dstBuffer, &srcBuffer, dstOffset, srcOffset, count, dstUsage](vk::CommandBuffer cmd) -> bool {
			// Record the first barrier
			vk::BufferMemoryBarrier srcBarrier{};
			vk::PipelineStageFlags srcStages{}, dstStages{};
			PopulateSrcBarrier(dstUsage, &srcBarrier, &srcStages, &dstStages);
			srcBarrier.buffer = dstBuffer;
			srcBarrier.offset = dstOffset;
			srcBarrier.size = count;
			cmd.pipelineBarrier(
				srcStages, dstStages,
				{ },
				{ }, srcBarrier, { }
			);

			// Record the buffer copy
			vk::BufferCopy copy{};
			copy.srcOffset = srcOffset;
			copy.dstOffset = dstOffset;
			copy.size = count;
			cmd.copyBuffer(srcBuffer.buffer(), dstBuffer, copy);

			// Record the end barrier
			vk::BufferMemoryBarrier dstBarrier{};
			PopulateDstBarrier(dstUsage, &dstBarrier, &srcStages, &dstStages);
			dstBarrier.buffer = dstBuffer;
			dstBarrier.offset = dstOffset;
			dstBarrier.size = count;
			cmd.pipelineBarrier(
				srcStages, dstStages,
				{ },
				{ }, dstBarrier, { }
			);

			return true;
		}
	);

	srcBuffer.markUsed(fence);
}

// ====================================================================================================================
void TransferManager::setImageData(vk::Image dstImage, vk::ImageUsageFlags usage, const ImageRegion& reg, 
	const void* srcPtr, uint64 dataSize, bool wholeImage)
{
	// Allocate a stager and copy the data in
	const auto stager = reserveOrAllocateStager(dataSize);
	std::memcpy(stager->buffer()->data(), srcPtr, dataSize);

	// Submit the image data
	setImageDataImpl(dstImage, reg, *stager->buffer(), 0, usage, wholeImage);
	stager->markFree();

	// Destroy temp stagers immediately
	if (stager->isTemp()) {
		delete stager;
	}
}

// ====================================================================================================================
void TransferManager::setImageDataImpl(vk::Image dstImage, const ImageRegion& dstRegion, 
	const StagingBufferImpl& srcBuffer, uint64 srcOffset, vk::ImageUsageFlags dstUsage, bool wholeImage)
{
	const auto fence = CommandManager::Get()->submitGraphicsCommand(
		[dstImage, &srcBuffer, &dstRegion, srcOffset, dstUsage, wholeImage](vk::CommandBuffer cmd) -> bool {
			// Record the first barrier
			vk::ImageMemoryBarrier srcBarrier{};
			vk::PipelineStageFlags srcStages{}, dstStages{};
			PopulateSrcBarrier(dstUsage, &srcBarrier, &srcStages, &dstStages, wholeImage);
			srcBarrier.image = dstImage;
			srcBarrier.subresourceRange = { vk::ImageAspectFlagBits::eColor, 0, 1, dstRegion.layer, dstRegion.layerCount };
			cmd.pipelineBarrier(
				srcStages, dstStages,
				vk::DependencyFlagBits::eByRegion,
				{ }, { }, srcBarrier
			);

			// Perform copy
			vk::BufferImageCopy copy{};
			copy.bufferOffset = srcOffset;
			copy.bufferRowLength = copy.bufferImageHeight = 0;
			copy.imageSubresource = { vk::ImageAspectFlagBits::eColor, 0, dstRegion.layer, dstRegion.layerCount };
			copy.imageOffset = vk::Offset3D{ int32(dstRegion.x), int32(dstRegion.y), int32(dstRegion.z) };
			copy.imageExtent = vk::Extent3D{ dstRegion.width, dstRegion.height, dstRegion.depth };
			cmd.copyBufferToImage(srcBuffer.buffer(), dstImage, vk::ImageLayout::eTransferDstOptimal, copy);

			// Record the end barrier
			vk::ImageMemoryBarrier dstBarrier{};
			PopulateDstBarrier(dstUsage, &dstBarrier, &srcStages, &dstStages);
			dstBarrier.image = dstImage;
			dstBarrier.subresourceRange = srcBarrier.subresourceRange;
			cmd.pipelineBarrier(
				srcStages, dstStages,
				vk::DependencyFlagBits::eByRegion,
				{ }, { }, dstBarrier
			);

			return true;
		}
	);

	srcBuffer.markUsed(fence);
}

// ====================================================================================================================
TransferManager* TransferManager::Get()
{
	return GraphicsEngineImpl::Get()->transferManager();
}

// ====================================================================================================================
TransferManager::Stager* TransferManager::reserveOrAllocateStager(uint64 size)
{
	static std::atomic_uint32_t TempCount_{ 0 };

	// Look for a staging buffer to use
	Stager* bestStager{ nullptr };
	const auto now = Time::ElapsedRaw();
	{
		std::lock_guard lock{ stagerMutex_ };

		// Look for the smallest available buffer that fits the data (also clean fences as we go)
		uint64 bestSize{ UINT64_MAX };
		for (uint32 si = 0; si < stagers_.size(); ++si) {
			const auto& stager = stagers_[si];
			if (!stager->isIdle()) {
				continue;
			}
			const auto buffer = stager->buffer();
			if ((buffer->size() >= size) && (buffer->size() < bestSize)) {
				bestSize = buffer->size();
				bestStager = stager.get();
			}
			else if ((si >= PERMANENT_STAGER_COUNT) && ((now - buffer->lastUseTime()) >= UNUSED_STAGER_DELETE_TIME)) {
				// Unused buffers are cleaned up after some time to keep memory use down
				stagers_.erase(stagers_.begin() + si);
				si -= 1; // Need to shift down to deal with erase()
			}
		}

		// If a buffer was not found, check if we can make a new one
		if (!bestStager && (stagers_.size() < MAX_STAGER_COUNT)) {
			// We use a minimum of INITIAL_STAGER_SIZE to make this stager potentially more useful in the future
			bestStager = stagers_.emplace_back(new Stager(std::max(INITIAL_STAGER_SIZE, size), false)).get();
			bestStager->buffer()->resourceName(to_string("TransferBuffer%u", uint32(stagers_.size())));
		}

		bestStager->markUsed();
	}

	// If we still dont have a buffer, we need to create a temporary one that will be waited on and destroyed
	if (!bestStager) {
		bestStager = new Stager(size, true);
		bestStager->buffer()->resourceName(to_string("TempTransferBuffer%u", TempCount_.fetch_add(1)));
		bestStager->markUsed();
	}

	// Mark the buffer as used and return
	return bestStager;
}

// ====================================================================================================================
void TransferManager::PopulateSrcBarrier(vk::BufferUsageFlags usage, vk::BufferMemoryBarrier* bmb,
	vk::PipelineStageFlags* srcStages, vk::PipelineStageFlags* dstStages)
{
	bmb->dstAccessMask = vk::AccessFlagBits::eTransferWrite;
	*dstStages = vk::PipelineStageFlagBits::eTransfer;

	if (bool(usage & vk::BufferUsageFlagBits::eVertexBuffer)) {
		bmb->srcAccessMask = vk::AccessFlagBits::eVertexAttributeRead;
		*srcStages = vk::PipelineStageFlagBits::eVertexInput;
	}
	else if (bool(usage & vk::BufferUsageFlagBits::eIndexBuffer)) {
		bmb->srcAccessMask = vk::AccessFlagBits::eIndexRead;
		*srcStages = vk::PipelineStageFlagBits::eVertexInput;
	}
	else if (bool(usage & vk::BufferUsageFlagBits::eStorageBuffer)) {
		bmb->srcAccessMask = vk::AccessFlagBits::eShaderRead;
		*srcStages = vk::PipelineStageFlagBits::eFragmentShader;
	}
	else if (bool(usage & vk::BufferUsageFlagBits::eUniformTexelBuffer)) {
		bmb->srcAccessMask = vk::AccessFlagBits::eShaderRead;
		*srcStages = vk::PipelineStageFlagBits::eFragmentShader;
	}
	else {
		assert(false && "Invalid buffer usage for source barrier");
	}
}

// ====================================================================================================================
void TransferManager::PopulateDstBarrier(vk::BufferUsageFlags usage, vk::BufferMemoryBarrier* bmb,
	vk::PipelineStageFlags* srcStages, vk::PipelineStageFlags* dstStages)
{
	bmb->srcAccessMask = vk::AccessFlagBits::eTransferWrite;
	*srcStages = vk::PipelineStageFlagBits::eTransfer;

	if (bool(usage & vk::BufferUsageFlagBits::eVertexBuffer)) {
		bmb->dstAccessMask = vk::AccessFlagBits::eVertexAttributeRead;
		*dstStages = vk::PipelineStageFlagBits::eVertexInput;
	}
	else if (bool(usage & vk::BufferUsageFlagBits::eIndexBuffer)) {
		bmb->dstAccessMask = vk::AccessFlagBits::eIndexRead;
		*dstStages = vk::PipelineStageFlagBits::eVertexInput;
	}
	else if (bool(usage & vk::BufferUsageFlagBits::eStorageBuffer)) {
		bmb->dstAccessMask = vk::AccessFlagBits::eShaderRead;
		*dstStages = vk::PipelineStageFlagBits::eVertexShader;
	}
	else if (bool(usage & vk::BufferUsageFlagBits::eUniformTexelBuffer)) {
		bmb->dstAccessMask = vk::AccessFlagBits::eShaderRead;
		*dstStages = vk::PipelineStageFlagBits::eVertexShader;
	}
	else {
		assert(false && "Invalid buffer usage for destination barrier");
	}
}

// ====================================================================================================================
void TransferManager::PopulateSrcBarrier(vk::ImageUsageFlags usage, vk::ImageMemoryBarrier* imb,
	vk::PipelineStageFlags* srcStages, vk::PipelineStageFlags* dstStages, bool wholeImage)
{
	imb->dstAccessMask = vk::AccessFlagBits::eTransferWrite;
	*dstStages = vk::PipelineStageFlagBits::eTransfer;

	if (bool(usage & vk::ImageUsageFlagBits::eSampled)) {
		imb->srcAccessMask = vk::AccessFlagBits::eShaderRead;
		imb->oldLayout = wholeImage ? vk::ImageLayout::eUndefined : vk::ImageLayout::eShaderReadOnlyOptimal;
		imb->newLayout = vk::ImageLayout::eTransferDstOptimal;
		*srcStages = vk::PipelineStageFlagBits::eFragmentShader;
	}
	else {
		assert(false && "Invalid image usage for source barrier");
	}
}

// ====================================================================================================================
void TransferManager::PopulateDstBarrier(vk::ImageUsageFlags usage, vk::ImageMemoryBarrier* imb,
	vk::PipelineStageFlags* srcStages, vk::PipelineStageFlags* dstStages)
{
	imb->srcAccessMask = vk::AccessFlagBits::eTransferWrite;
	*srcStages = vk::PipelineStageFlagBits::eTransfer;

	if (bool(usage & vk::ImageUsageFlagBits::eSampled)) {
		imb->dstAccessMask = vk::AccessFlagBits::eShaderRead;
		imb->oldLayout = vk::ImageLayout::eTransferDstOptimal;
		imb->newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
		*dstStages = vk::PipelineStageFlagBits::eVertexShader;
	}
	else {
		assert(false && "Invalid image usage for destination barrier");
	}
}

} // namespace vega
