/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include <Vega/Core/InlineVector.hpp>
#include "../Vulkan.hpp"

#include <array>
#include <mutex>
#include <vector>


namespace vega
{

// A fence-like object that uses counting to support re-use and error-free delayed state checks
// useCount_ increases by 1 with each submit, and the Wait type will check the semaphore value
class CommandFence final
	: public std::enable_shared_from_this<CommandFence>
{
	friend class CommandManager;

public:
	struct DeviceWait;

	struct Wait final
	{
		friend class CommandManager;
		friend struct DeviceWait;

		Wait() : fence_{ }, waitVal_{ 0 } { }
		Wait(const CommandFence& fence, uint64 value) 
			: fence_{ fence.weak_from_this() }, waitVal_{ value } 
		{ }
		Wait(const Wait& o) : fence_{ o.fence_ }, waitVal_{ o.waitVal_ } { }
		Wait& operator = (const Wait& o) {
			fence_ = o.fence_;
			waitVal_ = o.waitVal_;
			return *this;
		}
		~Wait() { }

		bool check() const; // true == complete, false == timeout
		bool wait(uint64 timeout = UINT64_MAX) const; // true == complete, false == timeout

	private:
		WPtr<const CommandFence> fence_;
		uint64 waitVal_;
	}; // struct Wait

	struct DeviceWait final
	{
		friend class CommandManager;

		DeviceWait() : fence_{ }, waitVal_{ 0 }, flags_{ } { }
		DeviceWait(const CommandFence& fence, uint64 value, vk::PipelineStageFlags flags)
			: fence_{ fence.weak_from_this() }, waitVal_{ value }, flags_{ flags }
		{ }
		DeviceWait(const Wait& wait, vk::PipelineStageFlags flags)
			: fence_{ wait.fence_ }, waitVal_{ wait.waitVal_ }, flags_{ flags }
		{ }

	private:
		WPtr<const CommandFence> fence_;
		uint64 waitVal_;
		vk::PipelineStageFlags flags_;
	}; // struct DeviceWait

	struct Signal final
	{
		friend class CommandManager;

		Signal(CommandFence& fence) : fence_{ fence.weak_from_this() }, wait_{ nullptr } { }
		Signal(CommandFence& fence, CommandFence::Wait* wait)
			: fence_{ fence.weak_from_this() }, wait_{ wait }
		{ }

	private:
		WPtr<CommandFence> fence_;
		CommandFence::Wait* wait_;
	}; // struct Signal

	CommandFence();
	~CommandFence();

	inline uint64 useCount() const { return useCount_; }
	bool pending() const;
	bool wait(uint64 value, uint64 timeout = UINT64_MAX) const; // true == complete, false == timeout

private:
	vk::Semaphore handle_;
	uint64 useCount_;

	VEGA_NO_COPY(CommandFence)
	VEGA_NO_MOVE(CommandFence)
}; // class CommandFence


// Manages command buffer pooling/allocation, and command submission
class CommandManager final
{
public:
	using OneTimeCommand = std::function<bool(vk::CommandBuffer)>;
	// Semaphore/Flags pair for semaphore waits
	struct SemaphoreWait final
	{
		SemaphoreWait() : semaphore{ }, stageMask{ } { }
		SemaphoreWait(vk::Semaphore s, vk::PipelineStageFlags m) : semaphore{ s }, stageMask{ m } { }
		vk::Semaphore semaphore;
		vk::PipelineStageFlags stageMask;
	}; // struct SemaphoreWait

	// Variant of objects that can be waited on by command submissions
	using WaitObject = Var<SemaphoreWait, CommandFence::DeviceWait>;
	// Variant of objects that can be signalled by command submissions
	using SignalObject = Var<vk::Semaphore, CommandFence::Signal>;

	// Maxmimum number of wait and submit objects in a submit
	inline static constexpr uint32 MAX_SUBMIT_OBJECTS{ 8 };

	// List types
	using CommandVector = InlineVector<vk::CommandBuffer, MAX_SUBMIT_OBJECTS>;
	using WaitVector = InlineVector<WaitObject, MAX_SUBMIT_OBJECTS>;
	using SignalVector = InlineVector<SignalObject, MAX_SUBMIT_OBJECTS>;

private:
	// Manages a single Vulkan command submission queue
	struct DeviceQueue final
	{
		DeviceQueue(uint32 family, uint32 index)
			: family{ family }, index{ index }, handle{ Vulkan::Device().getQueue(family, index) }, mutex{ }
		{ }

		const uint32 family{ };
		const uint32 index{ };
		vk::Queue handle{ };
		std::mutex mutex{ };
	}; // struct DeviceQueue

	// Manages a set of per-frame command buffer pools for transient commands
	class FrameCommands final
	{
		inline static constexpr uint32 GROW_SIZE{ 8 }; // Additional buffers to allocate when out of space

		// Manages per-frame command objects
		struct Frame final
		{
			vk::CommandPool pool{ };
			std::vector<vk::CommandBuffer> primaries{ };
			std::vector<vk::CommandBuffer> secondaries{ };
			uint32 primaryOffset{ };
			uint32 secondaryOffset{ };
		}; // struct Frame

	public:
		explicit FrameCommands(CommandManager* manager);
		~FrameCommands();

		void nextFrame();

		vk::CommandBuffer allocate(vk::CommandBufferLevel level);

	private:
		void growPool(Frame& frame, vk::CommandBufferLevel level);

	private:
		CommandManager* const manager_;
		std::array<Frame, Vulkan::MAX_PARALLEL_FRAMES> frames_;
		uint32 frameIndex_;

		VEGA_NO_COPY(FrameCommands)
		VEGA_NO_MOVE(FrameCommands)
	}; // class FrameCommands

	// Manages a circular buffer of one-time-use command buffers
	class OneTimeCommands final
	{
		inline static constexpr uint32 POOL_SIZE{ 8 }; // Number of buffers in the circular buffer

	public:
		OneTimeCommands(CommandManager* manager);
		~OneTimeCommands();

		std::tuple<vk::CommandBuffer, SPtr<CommandFence>> recordGraphicsCommands(OneTimeCommand func);

	private:
		vk::CommandPool pool_;
		std::array<vk::CommandBuffer, POOL_SIZE> buffers_;
		std::array<SPtr<CommandFence>, POOL_SIZE> fences_;
		uint32 index_;

		VEGA_NO_COPY(OneTimeCommands)
		VEGA_NO_MOVE(OneTimeCommands)
	}; // class OneTimeCommands

public:
	CommandManager();
	~CommandManager();

	void registerThread();
	void unregisterThread();
	void update();

	// Queue Info
	inline uint32 graphicsFamily() const { return graphicsQueue_->family; }

	// Command allocation
	vk::CommandBuffer allocateFrameCommand(vk::CommandBufferLevel level);

	// Command submission
	vk::Result present(const vk::PresentInfoKHR& info);
	void submitGraphics(
		const CommandVector& buffers,
		const WaitVector& waits = { },
		const SignalVector& signals = { },
		vk::Fence fence = { });
	void submitGraphics(const vk::SubmitInfo& info, vk::Fence fence = {});
	CommandFence::Wait submitGraphicsCommand(
		OneTimeCommand func,
		const std::initializer_list<WaitObject>& waits = { },
		const std::initializer_list<SignalObject>& signals = {},
		vk::Fence fence = { });

	static CommandManager* Get();

private:
	SPtr<DeviceQueue> graphicsQueue_;
	struct
	{
		std::mutex mutex{ };
		std::vector<FrameCommands*> cmds{ };
	} frame_;

	static thread_local UPtr<FrameCommands> FrameCommands_;
	static thread_local UPtr<OneTimeCommands> OneTimeCommands_;

	VEGA_NO_COPY(CommandManager)
	VEGA_NO_MOVE(CommandManager)
}; // class CommandManager

} // namespace vega
