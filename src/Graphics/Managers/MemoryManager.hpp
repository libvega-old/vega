/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include "../Vulkan.hpp"

#include <vector>


namespace vega
{

// Represents a specific graphics memory heap type
class MemoryType final
{
	friend class MemoryManager;

public:
	MemoryType(const vk::MemoryHeap* heap, const vk::MemoryType* type, uint32 typeIndex);
	~MemoryType();

	inline const vk::MemoryHeap* heap() const { return heap_; }
	inline const vk::MemoryType* type() const { return type_; }
	inline uint32 typeIndex() const { return typeIndex_; }

	inline bool isHostVisible() const { 
		return bool(type_->propertyFlags & vk::MemoryPropertyFlagBits::eHostVisible);
	}
	inline bool isDeviceLocal() const {
		return bool(type_->propertyFlags & vk::MemoryPropertyFlagBits::eDeviceLocal);
	}
	inline bool isTransient() const {
		return bool(type_->propertyFlags & vk::MemoryPropertyFlagBits::eLazilyAllocated);
	}

	inline bool isSameType(const MemoryType& type) const { return typeIndex_ == type.typeIndex_; }

private:
	const vk::MemoryHeap* const heap_;
	const vk::MemoryType* const type_;
	const uint32 typeIndex_;

	VEGA_NO_COPY(MemoryType)
	VEGA_NO_MOVE(MemoryType)
}; // class MemoryType


// Represents a single area of allocated graphics memory.
class MemoryAllocation final
{
public:
	MemoryAllocation(vk::DeviceMemory memory, uint64 offset, uint64 size, vk::MemoryPropertyFlags flags);
	~MemoryAllocation();

	inline vk::DeviceMemory memory() const { return memory_; }
	inline uint64 offset() const { return offset_; }
	inline uint64 size() const { return size_; }
	inline bool isMappable() const {
		return bool(flags_ & vk::MemoryPropertyFlagBits::eHostVisible);
	}

	void* map();
	void unmap();

private:
	const vk::DeviceMemory memory_;
	const uint64 offset_;
	const uint64 size_;
	void* mapped_;
	const vk::MemoryPropertyFlags flags_;

	VEGA_NO_COPY(MemoryAllocation)
	VEGA_NO_MOVE(MemoryAllocation)
}; // class MemoryAllocation

using MemoryHandle = UPtr<MemoryAllocation>;


// Manages graphics memory allocation and stats
class MemoryManager final
{
public:
	MemoryManager();
	~MemoryManager();

	void registerThread();
	void unregisterThread();
	void update();

	// Memory Info
	inline bool hasTransient() const { return !transientMem_->isSameType(*deviceMem_); }
	inline const MemoryType* deviceType() const    { return deviceMem_.get(); }
	inline const MemoryType* hostType() const      { return hostMem_.get(); }
	inline const MemoryType* uploadType() const    { return uploadMem_.get(); }
	inline const MemoryType* dynamicType() const   { return dynamicMem_.get(); }
	inline const MemoryType* transientType() const { return transientMem_.get(); }

	// Memory Allocation
	inline MemoryHandle allocateDevice(vk::Image image, bool dedicated = false) {
		return allocateImage(image, dedicated, *deviceMem_);
	}
	inline MemoryHandle allocateHost(vk::Image image, bool dedicated = false) {
		return allocateImage(image, dedicated, *hostMem_);
	}
	inline MemoryHandle allocateUpload(vk::Image image, bool dedicated = false) {
		return allocateImage(image, dedicated, *uploadMem_);
	}
	inline MemoryHandle allocateDynamic(vk::Image image, bool dedicated = false) {
		return allocateImage(image, dedicated, *dynamicMem_);
	}
	inline MemoryHandle allocateTransient(vk::Image image) {
		return allocateImage(image, true, *transientMem_);
	}
	inline MemoryHandle allocateDevice(vk::Buffer buffer, bool dedicated = false) {
		return allocateBuffer(buffer, dedicated, *deviceMem_);
	}
	inline MemoryHandle allocateHost(vk::Buffer buffer, bool dedicated = false) {
		return allocateBuffer(buffer, dedicated, *hostMem_);
	}
	inline MemoryHandle allocateUpload(vk::Buffer buffer, bool dedicated = false) {
		return allocateBuffer(buffer, dedicated, *uploadMem_);
	}
	inline MemoryHandle allocateDynamic(vk::Buffer buffer, bool dedicated = false) {
		return allocateBuffer(buffer, dedicated, *dynamicMem_);
	}

	static MemoryManager* Get();

private:
	MemoryHandle allocateImage(vk::Image image, bool dedicated, MemoryType& type);
	MemoryHandle allocateBuffer(vk::Buffer buffer, bool dedicated, MemoryType& type);

	static Opt<uint32> FindMemoryType(const std::vector<vk::MemoryType>& types,
		vk::MemoryPropertyFlags reqFlags, vk::MemoryPropertyFlags prefFlags, vk::MemoryPropertyFlags notPrefFlags);

private:
	std::vector<vk::MemoryHeap> heaps_;
	std::vector<vk::MemoryType> types_;
	SPtr<MemoryType> deviceMem_;
	SPtr<MemoryType> hostMem_;
	SPtr<MemoryType> uploadMem_; // Fallback host
	SPtr<MemoryType> dynamicMem_; // Fallback upload
	SPtr<MemoryType> transientMem_; // Fallback device

	VEGA_NO_COPY(MemoryManager)
	VEGA_NO_MOVE(MemoryManager)
}; // class MemoryManager

} // namespace vega
