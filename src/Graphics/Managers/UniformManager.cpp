/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./UniformManager.hpp"
#include "./BindingManager.hpp"
#include "./ResourceManager.hpp"
#include "./MemoryManager.hpp"
#include "../GraphicsEngineImpl.hpp"
#include <Vega/Math/Math.hpp>


namespace vega
{

// ====================================================================================================================
UniformManager::UniformManager()
	: alignment_{ GraphicsEngineImpl::Get()->device()->properties()->uniformAlignment }
	, descPool_{ }
	, pools_{ }
	, poolIndex_{ 0 }
{
	auto vkdev = Vulkan::Device();

	// Create descriptor pool
	vk::DescriptorPoolSize poolSize{ vk::DescriptorType::eUniformBufferDynamic, 2 * Vulkan::MAX_PARALLEL_FRAMES };
	vk::DescriptorPoolCreateInfo dpci{};
	dpci.maxSets = Vulkan::MAX_PARALLEL_FRAMES;
	dpci.poolSizeCount = 1;
	dpci.pPoolSizes = &poolSize;
	descPool_ = vkdev.createDescriptorPool(dpci);

	// Describe pool buffer
	vk::BufferCreateInfo bci{};
	bci.size = POOL_SIZE;
	bci.usage = vk::BufferUsageFlagBits::eUniformBuffer;
	bci.sharingMode = vk::SharingMode::eExclusive;

	// Describe the descriptor set
	const auto layout = BindingManager::Get()->uniformLayout();
	vk::DescriptorSetAllocateInfo dsai{};
	dsai.descriptorPool = descPool_;
	dsai.descriptorSetCount = 1;
	dsai.pSetLayouts = &layout;

	// Create buffers and sets
	for (auto& pool : pools_) {
		// Setup buffer and buffer memory
		pool.buffer = vkdev.createBuffer(bci);
		pool.memory = MemoryManager::Get()->allocateDynamic(pool.buffer, true);
		vkdev.bindBufferMemory(pool.buffer, pool.memory->memory(), pool.memory->offset());
		pool.dataPtr = pool.memory->map();

		// Setup descriptor set
		pool.set = vkdev.allocateDescriptorSets(dsai)[0];
		std::array<vk::DescriptorBufferInfo, 2> bufferInfo{};
		bufferInfo[0].buffer = bufferInfo[1].buffer = pool.buffer;
		bufferInfo[0].offset = bufferInfo[1].offset = 0;
		bufferInfo[0].range = bufferInfo[1].range = MAX_UNIFORM_SIZE;
		vk::WriteDescriptorSet setWrite{};
		setWrite.dstSet = pool.set;
		setWrite.dstBinding = 0;
		setWrite.dstArrayElement = 0;
		setWrite.descriptorCount = uint32(bufferInfo.size());
		setWrite.descriptorType = vk::DescriptorType::eUniformBufferDynamic;
		setWrite.pBufferInfo = bufferInfo.data();
		vkdev.updateDescriptorSets(setWrite, { });
	}
}

// ====================================================================================================================
UniformManager::~UniformManager()
{
	const auto resmgr = ResourceManager::Get();

	// Destroy buffers
	for (auto& pool : pools_) {
		resmgr->destroyItem(pool.buffer);
		pool.memory->unmap();
		resmgr->destroyItem(pool.memory.release());
	}

	// Destroy descriptor pool
	resmgr->destroyItem(descPool_);
}

// ====================================================================================================================
void UniformManager::registerThread()
{

}

// ====================================================================================================================
void UniformManager::unregisterThread()
{

}

// ====================================================================================================================
void UniformManager::update()
{
	poolIndex_ = (poolIndex_ + 1) % Vulkan::MAX_PARALLEL_FRAMES;
	pools_[poolIndex_].offset.store(0);
}

// ====================================================================================================================
void UniformManager::allocateDataSlot(DataSlot& slot, uint64 size)
{
	assert((size != 0) && "Bad uniform size == 0");

	// Get offset
	const auto realSize = alignment_ * (1 + ((size - 1) / alignment_));
	auto& pool = pools_[poolIndex_];
	const auto offset = pool.offset.fetch_add(realSize);

	// Check offset (TODO support resizing)
	if ((offset + realSize) >= POOL_SIZE) {
		throw std::runtime_error("Out of space for uniform data"); // Allocate past end error - fix at some point
	}

	// Update the slot
	slot.set = pool.set;
	slot.dataPtr = (uint8*)pool.dataPtr + offset;
	slot.offset = uint32(offset);
	slot.size = uint32(realSize);
}

// ====================================================================================================================
UniformManager* UniformManager::Get()
{
	return GraphicsEngineImpl::Get()->uniformManager();
}

} // namespace vega
