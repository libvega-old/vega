/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include "../Vulkan.hpp"
#include <Vega/Math/Bitset.hpp>

#include <array>
#include <mutex>
#include <queue>


namespace vega
{

// Manages common binding objects, as well as the global binding table
class BindingManager final
{
	// Represents a specific binding type within the global table
	struct BindingTable final
	{
		using FreeQueue = std::queue<uint16>;

	public:
		std::mutex maskMutex{ };
		Bitset mask{ };
		std::mutex freeMutex{ };
		std::array<FreeQueue, Vulkan::MAX_PARALLEL_FRAMES> freeQueue{ };
		uint32 freeIndex{ 0 };
	}; // struct BindingTable

public:
	// Binding table sizes, taken from the VSL compiler
	inline static constexpr uint32 TABLE_SAMPLER_COUNT{ 8192 };
	inline static constexpr uint32 TABLE_IMAGE_COUNT  { 128 };
	inline static constexpr uint32 TABLE_BUFFER_COUNT { 1024 };
	inline static constexpr uint32 TABLE_ROTEXEL_COUNT{ 128 };
	inline static constexpr uint32 TABLE_RWTEXEL_COUNT{ 128 };

	BindingManager();
	~BindingManager();

	void registerThread();
	void unregisterThread();
	void update();

	// Layout Handles
	inline vk::DescriptorSetLayout tableLayout() const { return layout_.table; }
	inline vk::DescriptorSetLayout uniformLayout() const { return layout_.uniform; }
	inline vk::DescriptorSetLayout blankLayout() const { return layout_.blank; }
	inline vk::DescriptorSetLayout inputLayout(uint32 count) const { return layout_.input[count - 1]; }

	// Binding Table Functions
	inline vk::DescriptorSet globalTable() const { return table_.set; }
	uint16 reserveSampler(vk::ImageView view, vk::Sampler sampler);
	void   freeSampler(uint16 index);
	uint16 reserveBuffer(vk::Buffer buffer);
	void   freeBuffer(uint16 index);
	uint16 reserveROTexel(vk::BufferView bufferView);
	void   freeROTexel(uint16 index);

	// Thread-registration safe singleton accessor
	static BindingManager* Get();

private:
	struct
	{
		vk::DescriptorSetLayout table{ };   // Global binding table - set 0
		vk::DescriptorSetLayout uniform{ }; // Shader-defined uniforms + globals
		vk::DescriptorSetLayout blank{ };   // Special blank descriptor used for skipped sets
		std::array<vk::DescriptorSetLayout, 4> input{ }; // Input attachment layouts (1 - 4 attachments)
	} layout_;
	struct
	{
		vk::DescriptorPool pool{ };
		vk::DescriptorSet set{ };
		BindingTable samplers{ };
		BindingTable images{ };
		BindingTable buffers{ };
		BindingTable roTexels{ };
		BindingTable rwTexels{ };
	} table_;

	VEGA_NO_COPY(BindingManager)
	VEGA_NO_MOVE(BindingManager)
}; // class BindingManager

} // namespace vega
