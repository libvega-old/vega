/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./BindingManager.hpp"
#include "../GraphicsEngineImpl.hpp"


namespace vega
{

// ====================================================================================================================
BindingManager::BindingManager()
	: layout_{ }
	, table_{ }
{
	const auto vkdevice = Vulkan::Device();

	// Create the global binding table layout
	const vk::ShaderStageFlags tableStages{ vk::ShaderStageFlagBits::eAllGraphics };
	const vk::DescriptorBindingFlags tableBindFlags{
		vk::DescriptorBindingFlagBits::ePartiallyBound | vk::DescriptorBindingFlagBits::eUpdateUnusedWhilePending
	};
	const vk::DescriptorSetLayoutBinding tableLayouts[5]{
		{ 0, vk::DescriptorType::eCombinedImageSampler, TABLE_SAMPLER_COUNT, tableStages },
		{ 1, vk::DescriptorType::eStorageImage,         TABLE_IMAGE_COUNT,   tableStages },
		{ 2, vk::DescriptorType::eStorageBuffer,        TABLE_BUFFER_COUNT,  tableStages },
		{ 3, vk::DescriptorType::eUniformTexelBuffer,   TABLE_ROTEXEL_COUNT, tableStages },
		{ 4, vk::DescriptorType::eStorageTexelBuffer,   TABLE_RWTEXEL_COUNT, tableStages }
	};
	vk::DescriptorSetLayoutCreateInfo tableLayoutCI{};
	tableLayoutCI.flags = vk::DescriptorSetLayoutCreateFlagBits::eUpdateAfterBindPool;
	tableLayoutCI.bindingCount = 5;
	tableLayoutCI.pBindings = tableLayouts;
	const vk::DescriptorBindingFlags tableFlags[5]{
		tableBindFlags, tableBindFlags, tableBindFlags, tableBindFlags, tableBindFlags
	};
	vk::DescriptorSetLayoutBindingFlagsCreateInfo dslbfci{};
	dslbfci.bindingCount = 5;
	dslbfci.pBindingFlags = tableFlags;
	tableLayoutCI.pNext = &dslbfci;
	layout_.table = vkdevice.createDescriptorSetLayout(tableLayoutCI);
	Vulkan::SetObjectName(layout_.table, "GlobalBindingTableLayout");

	// Create the uniform layout
	std::array<vk::DescriptorSetLayoutBinding, 2> uniformLayouts{}; uniformLayouts.fill({ });
	uniformLayouts[0].binding = 0;
	uniformLayouts[1].binding = 1;
	uniformLayouts[0].descriptorType = uniformLayouts[1].descriptorType = vk::DescriptorType::eUniformBufferDynamic;
	uniformLayouts[0].descriptorCount = uniformLayouts[1].descriptorCount = 1;
	uniformLayouts[0].stageFlags = uniformLayouts[1].stageFlags = tableStages;
	vk::DescriptorSetLayoutCreateInfo uniformLayoutCI{};
	uniformLayoutCI.bindingCount = uint32(uniformLayouts.size());
	uniformLayoutCI.pBindings = uniformLayouts.data();
	layout_.uniform = vkdevice.createDescriptorSetLayout(uniformLayoutCI);
	Vulkan::SetObjectName(layout_.uniform, "GlobalUniformLayout");

	// Create the blank layout
	layout_.blank = vkdevice.createDescriptorSetLayout(vk::DescriptorSetLayoutCreateInfo{ });
	Vulkan::SetObjectName(layout_.blank, "BlankSetLayout");

	// Create input attachment layouts
	std::array<vk::DescriptorSetLayoutBinding, 4> inputBindings{ };
	vk::DescriptorSetLayoutCreateInfo inputLayoutCI{};
	inputLayoutCI.flags = vk::DescriptorSetLayoutCreateFlagBits::eUpdateAfterBindPool;
	inputLayoutCI.pBindings = inputBindings.data();
	for (uint32 bi = 0; bi < inputBindings.size(); ++bi) {
		auto& bind = inputBindings[bi];
		bind.binding = bi;
		bind.descriptorType = vk::DescriptorType::eInputAttachment;
		bind.descriptorCount = 1;
		bind.stageFlags = vk::ShaderStageFlagBits::eFragment;
		inputLayoutCI.bindingCount = bi + 1;
		layout_.input[bi] = vkdevice.createDescriptorSetLayout(inputLayoutCI);
		Vulkan::SetObjectName(layout_.input[bi], to_string("GlobalInputLayout%u", bi + 1));
	}

	// Create the global table pool
	vk::DescriptorPoolSize tablePoolSizes[5]{
		{ vk::DescriptorType::eCombinedImageSampler, TABLE_SAMPLER_COUNT },
		{ vk::DescriptorType::eStorageImage,         TABLE_IMAGE_COUNT },
		{ vk::DescriptorType::eStorageBuffer,        TABLE_BUFFER_COUNT },
		{ vk::DescriptorType::eUniformTexelBuffer,   TABLE_ROTEXEL_COUNT },
		{ vk::DescriptorType::eStorageTexelBuffer,   TABLE_RWTEXEL_COUNT }
	};
	vk::DescriptorPoolCreateInfo tablePoolCI{};
	tablePoolCI.flags = vk::DescriptorPoolCreateFlagBits::eUpdateAfterBind;
	tablePoolCI.maxSets = 1;
	tablePoolCI.poolSizeCount = 5;
	tablePoolCI.pPoolSizes = tablePoolSizes;
	table_.pool = vkdevice.createDescriptorPool(tablePoolCI);
	Vulkan::SetObjectName(table_.pool, "GlobalBindingPool");

	// Create the global table
	vk::DescriptorSetAllocateInfo tableAI{};
	tableAI.descriptorPool = table_.pool;
	tableAI.descriptorSetCount = 1;
	tableAI.pSetLayouts = &(layout_.table);
	table_.set = vkdevice.allocateDescriptorSets(tableAI)[0];
	Vulkan::SetObjectName(table_.set, "GlobalBindingTable");

	// Setup the tables
	table_.samplers.mask.resize(TABLE_SAMPLER_COUNT);
	table_.images.mask.resize(TABLE_IMAGE_COUNT);
	table_.buffers.mask.resize(TABLE_BUFFER_COUNT);
	table_.roTexels.mask.resize(TABLE_ROTEXEL_COUNT);
	table_.rwTexels.mask.resize(TABLE_RWTEXEL_COUNT);
}

// ====================================================================================================================
BindingManager::~BindingManager()
{
	const auto vkdev = Vulkan::Device();

	if (table_.pool) {
		vkdev.destroyDescriptorPool(table_.pool);
	}
	for (auto layout : layout_.input) {
		if (layout) {
			vkdev.destroyDescriptorSetLayout(layout);
		}
	}
	if (layout_.blank) {
		vkdev.destroyDescriptorSetLayout(layout_.blank);
	}
	if (layout_.uniform) {
		vkdev.destroyDescriptorSetLayout(layout_.uniform);
	}
	if (layout_.table) {
		vkdev.destroyDescriptorSetLayout(layout_.table);
	}
}

// ====================================================================================================================
void BindingManager::registerThread()
{

}

// ====================================================================================================================
void BindingManager::unregisterThread()
{

}

// ====================================================================================================================
void BindingManager::update()
{
	static const auto free_table = [](BindingTable& table) {
		table.freeIndex = (table.freeIndex + 1u) % Vulkan::MAX_PARALLEL_FRAMES;
		auto& queue = table.freeQueue[table.freeIndex];
		while (!queue.empty()) {
			table.mask.clear(queue.front());
			queue.pop();
		}
	};

	free_table(table_.samplers);
	free_table(table_.images);
	free_table(table_.buffers);
	free_table(table_.roTexels);
	free_table(table_.rwTexels);
}

// ====================================================================================================================
uint16 BindingManager::reserveSampler(vk::ImageView view, vk::Sampler sampler)
{
	// Get available table index
	uint16 index{ 0 };
	{
		std::lock_guard lock{ table_.samplers.maskMutex };
		const auto clear = table_.samplers.mask.firstClear();
		if (!clear.has_value()) {
			throw std::runtime_error("Out of space for textures in binding table");
		}
		table_.samplers.mask.set(index = uint16(clear.value()));
	}

	// Update the table
	vk::DescriptorImageInfo di{};
	di.sampler = sampler;
	di.imageView = view;
	di.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
	vk::WriteDescriptorSet write{};
	write.dstSet = table_.set;
	write.dstBinding = 0;
	write.dstArrayElement = index;
	write.descriptorCount = 1;
	write.descriptorType = vk::DescriptorType::eCombinedImageSampler;
	write.pImageInfo = &di;
	Vulkan::Device().updateDescriptorSets(write, { });

	// Return the index
	return index;
}

// ====================================================================================================================
void BindingManager::freeSampler(uint16 index)
{
	std::lock_guard lock{ table_.samplers.freeMutex };
	assert(table_.samplers.mask.get(index) && "Attempted to free unallocated sampler table slot");
	table_.samplers.freeQueue[table_.samplers.freeIndex].push(index);
}

// ====================================================================================================================
uint16 BindingManager::reserveBuffer(vk::Buffer buffer)
{
	// Get available table index
	uint16 index{ 0 };
	{
		std::lock_guard lock{ table_.buffers.maskMutex };
		const auto clear = table_.buffers.mask.firstClear();
		if (!clear.has_value()) {
			throw std::runtime_error("Out of space for storage buffers in binding table");
		}
		table_.buffers.mask.set(index = uint16(clear.value()));
	}

	// Update the table
	vk::DescriptorBufferInfo di{};
	di.buffer = buffer;
	di.offset = 0;
	di.range = VK_WHOLE_SIZE;
	vk::WriteDescriptorSet write{};
	write.dstSet = table_.set;
	write.dstBinding = 2;
	write.dstArrayElement = index;
	write.descriptorCount = 1;
	write.descriptorType = vk::DescriptorType::eStorageBuffer;
	write.pBufferInfo = &di;
	Vulkan::Device().updateDescriptorSets(write, { });

	// Return index
	return index;
}

// ====================================================================================================================
void BindingManager::freeBuffer(uint16 index)
{
	std::lock_guard lock{ table_.buffers.freeMutex };
	assert(table_.buffers.mask.get(index) && "Attempted to free unallocated storage buffer slot");
	table_.buffers.freeQueue[table_.buffers.freeIndex].push(index);
}

// ====================================================================================================================
uint16 BindingManager::reserveROTexel(vk::BufferView bufferView)
{
	// Get available table index
	uint16 index{ 0 };
	{
		std::lock_guard lock{ table_.roTexels.maskMutex };
		const auto clear = table_.roTexels.mask.firstClear();
		if (!clear.has_value()) {
			throw std::runtime_error("Out of space for read-only texel buffers in binding table");
		}
		table_.roTexels.mask.set(index = uint16(clear.value()));
	}

	// Update the table
	vk::WriteDescriptorSet write{};
	write.dstSet = table_.set;
	write.dstBinding = 3;
	write.dstArrayElement = index;
	write.descriptorCount = 1;
	write.descriptorType = vk::DescriptorType::eUniformTexelBuffer;
	write.pTexelBufferView = &bufferView;
	Vulkan::Device().updateDescriptorSets(write, { });

	// Return index
	return index;
}

// ====================================================================================================================
void BindingManager::freeROTexel(uint16 index)
{
	std::lock_guard lock{ table_.roTexels.freeMutex };
	assert(table_.roTexels.mask.get(index) && "Attempted to free unallocated read-only texel table slot");
	table_.roTexels.freeQueue[table_.roTexels.freeIndex].push(index);
}

// ====================================================================================================================
BindingManager* BindingManager::Get()
{
	return GraphicsEngineImpl::Get()->bindingManager();
}

} // namespace vega
