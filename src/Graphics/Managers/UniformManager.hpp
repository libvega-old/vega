/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include "../Vulkan.hpp"
#include <Vega/Math/Rect2.hpp>
#include <Vega/Math/Mat4.hpp>

#include <atomic>
#include <array>


namespace vega
{

class MemoryAllocation;

// Global shader data struct
struct GlobalShaderData final
{
public:
	float time{ };
	Vec2f deltaTime{ };
	Vec3f cameraPos{ };
	Vec3f cameraDir{ };
	Mat4f viewProj{ };
	Vec4f target{ };
	Vec2f depth{ };
}; // struct GlobalShaderData
static_assert(sizeof(GlobalShaderData) == 124, "Bad GlobalShaderData size");
static_assert(offsetof(GlobalShaderData, time)      ==   0, "Bad GlobalShaderData::time alignment");
static_assert(offsetof(GlobalShaderData, deltaTime) ==   4, "Bad GlobalShaderData::deltaTime alignment");
static_assert(offsetof(GlobalShaderData, cameraPos) ==  12, "Bad GlobalShaderData::cameraPos alignment");
static_assert(offsetof(GlobalShaderData, cameraDir) ==  24, "Bad GlobalShaderData::cameraDir alignment");
static_assert(offsetof(GlobalShaderData, viewProj)  ==  36, "Bad GlobalShaderData::viewProj alignment");
static_assert(offsetof(GlobalShaderData, target)    == 100, "Bad GlobalShaderData::target alignment");
static_assert(offsetof(GlobalShaderData, depth)     == 116, "Bad GlobalShaderData::depth alignment");


// Manages the global uniform buffer pool
class UniformManager final
{
	// Specific uniform buffer and its allocation info
	struct Pool final
	{
		vk::Buffer buffer{ };
		UPtr<MemoryAllocation> memory{ };
		void* dataPtr{ };
		vk::DescriptorSet set{ };
		std::atomic_uint64_t offset{ };
	}; // struct Pool

public:
	// Maximum uniform data size
	inline static constexpr uint64 MAX_UNIFORM_SIZE{ 256 };
	// Size for a single pool
	inline static constexpr uint64 POOL_SIZE{ (512 * 1024) + MAX_UNIFORM_SIZE };

	// Allocation for uniform data space
	struct DataSlot final
	{
		vk::DescriptorSet set{ };
		void* dataPtr{ };
		uint32 offset{ };
		uint32 size{ };
	}; // struct DataSlot

	UniformManager();
	~UniformManager();

	void registerThread();
	void unregisterThread();
	void update();

	void allocateDataSlot(DataSlot& slot, uint64 size);

	static UniformManager* Get();

private:
	const uint64 alignment_;
	vk::DescriptorPool descPool_;
	std::array<Pool, Vulkan::MAX_PARALLEL_FRAMES> pools_;
	uint32 poolIndex_;

	VEGA_NO_COPY(UniformManager)
	VEGA_NO_MOVE(UniformManager)
}; // class UniformManager

} // namespace vega
