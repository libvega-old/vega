/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Graphics/VertexFormat.hpp>
#include "./Vulkan.hpp"

#include <array>


namespace vega
{

// ====================================================================================================================
const String& VertexFormat::str() const
{
	static const std::array<String, uint32(VertexFormats::Float4x4.value()) + 1> NAMES{
		"Undefined", 
		"Byte",  "Byte2",  "Byte4", 
		"UByte", "UByte2", "UByte4", 
		"Short",  "Short2",  "Short4", 
		"UShort", "UShort2", "UShort4", 
		"Int",  "Int2",  "Int3",  "Int4", 
		"UInt", "UInt2", "UInt3", "UInt4", 
		"S8Norm", "S8Norm2", "S8Norm4", 
		"U8Norm", "U8Norm2", "U8Norm4", 
		"S16Norm", "S16Norm2", "S16Norm4", 
		"U16Norm", "U16Norm2", "U16Norm4", 
		"Half", "Half2", "Half4", 
		"Float", "Float2", "Float3", "Float4", 
		"Float2x2", "Float2x3", "Float2x4", 
		"Float3x2", "Float3x3", "Float3x4", 
		"Float4x2", "Float4x3", "Float4x4"
	};
	return NAMES.at(value_);
}

// ====================================================================================================================
VertexFormat VertexFormat::FromId(uint32 id)
{
#define FMAP(fmt,vfmt) { uint32(vk::Format::e##fmt), VertexFormats::vfmt }
	static const std::unordered_map<uint32, VertexFormat> FORMATS{
		FMAP(Undefined, Undefined),
		FMAP(R8Sint, Byte),  FMAP(R8G8Sint, Byte2),  FMAP(R8G8B8A8Sint, Byte4),
		FMAP(R8Uint, UByte), FMAP(R8G8Uint, UByte2), FMAP(R8G8B8A8Uint, UByte4),
		FMAP(R16Sint,  Short), FMAP(R16G16Sint,  Short2), FMAP(R16G16B16A16Sint,  Short4),
		FMAP(R16Uint, UShort), FMAP(R16G16Uint, UShort2), FMAP(R16G16B16A16Uint, UShort4),
		FMAP(R32Sint,  Int), FMAP(R32G32Sint,  Int2), FMAP(R32G32B32Sint,  Int3), FMAP(R32G32B32A32Sint,  Int4),
		FMAP(R32Uint, UInt), FMAP(R32G32Uint, UInt2), FMAP(R32G32B32Uint, UInt3), FMAP(R32G32B32A32Uint, UInt4),
		FMAP(R8Snorm, S8Norm), FMAP(R8G8Snorm, S8Norm2), FMAP(R8G8B8A8Snorm, S8Norm4),
		FMAP(R8Unorm, U8Norm), FMAP(R8G8Unorm, U8Norm2), FMAP(R8G8B8A8Unorm, U8Norm4),
		FMAP(R16Snorm, S16Norm), FMAP(R16G16Snorm, S16Norm2), FMAP(R16G16B16A16Snorm, S16Norm4),
		FMAP(R16Unorm, U16Norm), FMAP(R16G16Unorm, U16Norm2), FMAP(R16G16B16A16Unorm, U16Norm4),
		FMAP(R16Sfloat, Half),  FMAP(R16G16Sfloat, Half2),  FMAP(R16G16B16A16Sfloat, Half4),
		FMAP(R32Sfloat, Float), FMAP(R32G32Sfloat, Float2), FMAP(R32G32B32Sfloat, Float3), FMAP(R32G32B32A32Sfloat, Float4),
		FMAP(R32G32Sfloat, Float2x2),       FMAP(R32G32Sfloat, Float2x3),       FMAP(R32G32Sfloat, Float2x4),
		FMAP(R32G32B32Sfloat, Float3x2),    FMAP(R32G32B32Sfloat, Float3x3),    FMAP(R32G32B32Sfloat, Float3x4),
		FMAP(R32G32B32A32Sfloat, Float4x2), FMAP(R32G32B32A32Sfloat, Float4x3), FMAP(R32G32B32A32Sfloat, Float4x4)
	};
#undef FMAP
	const auto it = FORMATS.find(id);
	return (it != FORMATS.end()) ? it->second : VertexFormats::Undefined;
}

// ====================================================================================================================
const VertexFormat::FormatInfo& VertexFormat::info() const
{
#define FINFO(fmt,d0,d1,sz) { uint32(vk::Format::e##fmt), d0, d1, sz }
	static const std::array<FormatInfo, uint32(VertexFormats::Float4x4.value()) + 1> INFOS{{
		FINFO(Undefined, 0, 0, 0),
		FINFO(R8Sint, 1, 1, 1), FINFO(R8G8Sint, 2, 1, 2), FINFO(R8G8B8A8Sint, 4, 1, 4),
		FINFO(R8Uint, 1, 1, 1), FINFO(R8G8Uint, 2, 1, 2), FINFO(R8G8B8A8Uint, 4, 1, 4),
		FINFO(R16Sint, 1, 1, 2), FINFO(R16G16Sint, 2, 1, 4), FINFO(R16G16B16A16Sint, 4, 1, 8),
		FINFO(R16Uint, 1, 1, 2), FINFO(R16G16Uint, 2, 1, 4), FINFO(R16G16B16A16Uint, 4, 1, 8),
		FINFO(R32Sint, 1, 1, 4), FINFO(R32G32Sint, 2, 1, 8), FINFO(R32G32B32Sint, 4, 1, 12), FINFO(R32G32B32A32Sint, 4, 1, 16),
		FINFO(R32Uint, 1, 1, 4), FINFO(R32G32Uint, 2, 1, 8), FINFO(R32G32B32Uint, 4, 1, 12), FINFO(R32G32B32A32Uint, 4, 1, 16),
		FINFO(R8Snorm, 1, 1, 1), FINFO(R8G8Snorm, 2, 1, 2), FINFO(R8G8B8A8Snorm, 4, 1, 4),
		FINFO(R8Unorm, 1, 1, 1), FINFO(R8G8Unorm, 2, 1, 2), FINFO(R8G8B8A8Unorm, 4, 1, 4),
		FINFO(R16Snorm, 1, 1, 2), FINFO(R16G16Snorm, 2, 1, 4), FINFO(R16G16B16A16Snorm, 4, 1, 8),
		FINFO(R16Unorm, 1, 1, 2), FINFO(R16G16Unorm, 2, 1, 4), FINFO(R16G16B16A16Unorm, 4, 1, 8),
		FINFO(R16Sfloat, 1, 1, 2), FINFO(R16G16Sfloat, 2, 1, 4), FINFO(R16G16B16A16Sfloat, 4, 1, 8),
		FINFO(R32Sfloat, 1, 1, 4), FINFO(R32G32Sfloat, 2, 1, 8), FINFO(R32G32B32Sfloat, 4, 1, 12), FINFO(R32G32B32A32Sfloat, 4, 1, 16),
		FINFO(R32G32Sfloat, 2, 2, 16),       FINFO(R32G32Sfloat, 2, 3, 24),       FINFO(R32G32Sfloat, 2, 4, 32),
		FINFO(R32G32B32Sfloat, 2, 2, 24),    FINFO(R32G32B32Sfloat, 2, 3, 36),    FINFO(R32G32B32Sfloat, 2, 4, 48),
		FINFO(R32G32B32A32Sfloat, 2, 2, 32), FINFO(R32G32B32A32Sfloat, 2, 3, 48), FINFO(R32G32B32A32Sfloat, 2, 4, 64)
	}};
#undef FINFO
	return INFOS.at(value_);
}

} // namespace vega
