/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"

#include <atomic>
#include <vector>

// Pre-include check
#if defined(VULKAN_H_) || defined(VULKAN_HPP)
#	error Do not include Vulkan outside of "Graphics/Vulkan.hpp"
#endif

// Loader defines
#define VK_NO_PROTOTYPES
#define VULKAN_HPP_ENABLE_DYNAMIC_LOADER_TOOL (1)
#define VULKAN_HPP_DISPATCH_LOADER_DYNAMIC (1)
#define VULKAN_HPP_NO_SMART_HANDLE (1)

// Platform defines
#if defined(VEGA_WIN32)
#	define VK_USE_PLATFORM_WIN32_KHR
#elif defined(VEGA_OSX)
#	define VK_USE_PLATFORM_MACOS_MVK
#elif defined(VEGA_LINUX)
#	define VK_USE_PLATFORM_XLIB_KHR
#endif // defined(PL_OS_WIN)

#include <vulkan/vulkan.hpp>

// Version check
#ifndef VK_VERSION_1_2
#	error Vega requires a Vulkan SDK installation supporting at least Vulkan 1.2
#endif

// Xlib.h (included in Linux platforms) defines these macros, which causes conflicts
#undef None
#undef Always

// Thanks windows, nobody would ever need variables named this
#undef near
#undef far


namespace vega
{

// Global objects and interface for Vulkan operations
class Vulkan final
{
public:
	// Maximum parallel processing frames
	inline static constexpr uint32 MAX_PARALLEL_FRAMES{ 2 };

	struct DeviceFeatureChain : public vk::StructureChain<
		vk::PhysicalDeviceFeatures2,
		vk::PhysicalDeviceVulkan11Features,
		vk::PhysicalDeviceVulkan12Features
	> {
		DeviceFeatureChain() = default;
		inline vk::PhysicalDeviceFeatures& f10() { return get<vk::PhysicalDeviceFeatures2>().features; }
		inline vk::PhysicalDeviceVulkan11Features& f11() { return get<vk::PhysicalDeviceVulkan11Features>(); }
		inline vk::PhysicalDeviceVulkan12Features& f12() { return get<vk::PhysicalDeviceVulkan12Features>(); }
		inline const vk::PhysicalDeviceFeatures& f10() const { return get<vk::PhysicalDeviceFeatures2>().features; }
		inline const vk::PhysicalDeviceVulkan11Features& f11() const { return get<vk::PhysicalDeviceVulkan11Features>(); }
		inline const vk::PhysicalDeviceVulkan12Features& f12() const { return get<vk::PhysicalDeviceVulkan12Features>(); }
	};
	struct DevicePropertyChain : public vk::StructureChain<
		vk::PhysicalDeviceProperties2,
		vk::PhysicalDeviceVulkan11Properties,
		vk::PhysicalDeviceVulkan12Properties
	> {
		DevicePropertyChain() = default;
		inline vk::PhysicalDeviceProperties& p10() { return get<vk::PhysicalDeviceProperties2>().properties; }
		inline vk::PhysicalDeviceVulkan11Properties& p11() { return get<vk::PhysicalDeviceVulkan11Properties>(); }
		inline vk::PhysicalDeviceVulkan12Properties& p12() { return get<vk::PhysicalDeviceVulkan12Properties>(); }
		inline const vk::PhysicalDeviceProperties& p10() const { return get<vk::PhysicalDeviceProperties2>().properties; }
		inline const vk::PhysicalDeviceVulkan11Properties& p11() const { return get<vk::PhysicalDeviceVulkan11Properties>(); }
		inline const vk::PhysicalDeviceVulkan12Properties& p12() const { return get<vk::PhysicalDeviceVulkan12Properties>(); }
	};

	struct LayerSet
	{
		bool validation{ false }; // VK_LAYER_KHRONOS_validation
	}; // struct LayerSet
	struct InstanceExtensionSet
	{
		bool surface{ false }; // VK_KHR_SURFACE_EXTENSION_NAME
		bool debugUtils{ false }; // VK_EXT_DEBUG_UTILS_EXTENSION_NAME
	}; // struct InstanceExtensionSet
	struct DeviceExtensionSet
	{
		bool swapchain{ false }; // VK_KHR_SWAPCHAIN_EXTENSION_NAME
		bool extendedDynamicState{ false }; // VK_EXT_EXTENDED_DYNAMIC_STATE_EXTENSION_NAME
	}; // struct DeviceExtensionSet

	// Object Handles
	inline static vk::Instance Instance() { return Instance_.handle; }
	inline static vk::PhysicalDevice PhysicalDevice() { return Device_.physicalDevice; }
	inline static vk::Device Device() { return Device_.handle; }

	// Object Info
	inline static const DeviceExtensionSet& DeviceExtensions() { return Device_.extensions; }
	inline static const InstanceExtensionSet& InstanceExtensions() { return Instance_.extensions; }
	inline static const vk::PhysicalDeviceProperties& DeviceProperties() {
		return Device_.properties.get<vk::PhysicalDeviceProperties2>().properties;
	}
	inline static const vk::PhysicalDeviceVulkan11Properties& DeviceProperties11() {
		return Device_.properties.get<vk::PhysicalDeviceVulkan11Properties>();
	}
	inline static const vk::PhysicalDeviceVulkan12Properties& DeviceProperties12() {
		return Device_.properties.get<vk::PhysicalDeviceVulkan12Properties>();
	}

	// Global Init/Terminate
	static void Initialize();
	static void OpenDevice(vk::PhysicalDevice pdev);
	static void CloseDevice();
	static void Terminate();

	// Utilities
	template<typename T>
	inline static void SetObjectName(T handle, const String& name) {
		SetObjectName(uint64((typename T::CType)handle), T::objectType, name);
	}
	static void SetObjectName(uint64 handle, vk::ObjectType type, const String& name);

	// Queue Info
	static void GetQueueInfo(vk::PhysicalDevice pdev, uint32* gFamily);
	inline static std::tuple<uint32, uint32> GraphicsQueueInfo() { return Device_.graphicsQueue; }

	// Device Checks
	static bool CheckDeviceValid(vk::PhysicalDevice pdev, DeviceFeatureChain& feats, DevicePropertyChain& props);

private:
	static const vk::DynamicLoader Loader_;
	static struct
	{
		vk::Instance handle{ };
		LayerSet layers{ };
		InstanceExtensionSet extensions{ };
		vk::DebugUtilsMessengerEXT debugUtils{ };
	} Instance_;
	static struct
	{
		vk::PhysicalDevice physicalDevice{ };
		DevicePropertyChain properties{ };
		DeviceExtensionSet extensions{ };
		vk::Device handle{ };
		std::tuple<uint32, uint32> graphicsQueue{ };
	} Device_;

#if defined(VK_USE_PLATFORM_WIN32_KHR)
	constexpr static const char PlatformSurfaceExtensionName_[] = VK_KHR_WIN32_SURFACE_EXTENSION_NAME;
#elif defined(VK_USE_PLATFORM_XLIB_KHR)
	constexpr static const char PlatformSurfaceExtensionName_[] = VK_KHR_XLIB_SURFACE_EXTENSION_NAME;
#endif // defined(VK_USE_PLATFORM_WIN32_KHR)
	constexpr static const char ValidationLayerName_[] = "VK_LAYER_KHRONOS_validation";

	VEGA_NO_COPY(Vulkan)
	VEGA_NO_MOVE(Vulkan)
	VEGA_NO_INIT(Vulkan)
}; // class Vulkan

} // namespace vega
