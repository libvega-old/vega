/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./GraphicsEngineImpl.hpp"
#include "./Managers/BindingManager.hpp"
#include "./Managers/CommandManager.hpp"
#include "./Managers/MemoryManager.hpp"
#include "./Managers/PipelineManager.hpp"
#include "./Managers/ResourceManager.hpp"
#include "./Managers/TransferManager.hpp"
#include "./Managers/UniformManager.hpp"

#include <atomic>


namespace vega
{

// ====================================================================================================================
GraphicsEngineImpl::ThreadMonitor::~ThreadMonitor()
{
	// Ideally, there are only two nominal ways for a graphics thread to be cleaned up:
	//   1. Normal cleanup by a call to unregister in AppThread when it is exiting
	//   2. Hard kill when the program is faced with std::terminate() or std::abort()
	// In the first case, the asset statement below is used to ensure that we haven't missed anything in the library,
	// while this dtor is never called if we are in the second case (since abort doesn't cleanup static objects).
	// 
	// There is addionally the case where the application calls std::exit(). But in this case we cannot be sure of what
	// other objects have been destroyed or the state of the program so its a moot point to try to handle it. The best
	// we can do here is strongly recommend that the application does not use std::exit, but instead terminates the
	// app normally, or uses std::terminate to indicate when there is a fatal error.
	assert(!this->isRegistered() && "Failed to deregister graphics thread in normal program execution");
}


// ====================================================================================================================
// ====================================================================================================================
bool GraphicsEngineImpl::Terminating_{ false };
thread_local GraphicsEngineImpl::ThreadMonitor GraphicsEngineImpl::ThreadMonitor_{ };
static std::atomic_uint32_t ThreadIndex_{ 1 };

// ====================================================================================================================
GraphicsEngineImpl::GraphicsEngineImpl(const GraphicsDevice* device)
	: GraphicsEngine()
	, device_{ device }
	, bManager_{ }
	, cManager_{ }
	, mManager_{ }
	, pManager_{ }
	, rManager_{ }
	, tManager_{ }
	, uManager_{ }
{
	// Create Vulkan device
	Vulkan::OpenDevice(VkPhysicalDevice(device->handle_));
	const auto vkdevice = Vulkan::Device();

	// Create the managers
	ThreadMonitor_.threadIndex = ThreadIndex_.fetch_add(1);
	bManager_ = std::make_unique<BindingManager>();
	cManager_ = std::make_unique<CommandManager>();
	mManager_ = std::make_unique<MemoryManager>();
	pManager_ = std::make_unique<PipelineManager>();
	rManager_ = std::make_unique<ResourceManager>();
	tManager_ = std::make_unique<TransferManager>();
	uManager_ = std::make_unique<UniformManager>();

	// Register the main thread
	registerThread();
}

// ====================================================================================================================
GraphicsEngineImpl::~GraphicsEngineImpl()
{
	Terminating_ = true;

	// Cleanup main thread resources
	unregisterThread();

	// Destroy the managers (order is important, since some managers rely on others)
	// Memory after Resource
	uManager_.reset();
	tManager_.reset();
	rManager_.reset();
	pManager_.reset();
	mManager_.reset();
	cManager_.reset();
	bManager_.reset();

	// Close Vulkan device
	Vulkan::CloseDevice();

	Terminating_ = false;
}

// ====================================================================================================================
void GraphicsEngineImpl::nextFrame()
{
	bManager_->update();
	cManager_->update();
	mManager_->update();
	pManager_->update();
	rManager_->update();
	tManager_->update();
	uManager_->update();
}

// ====================================================================================================================
void GraphicsEngineImpl::registerThread()
{
	if (ThreadMonitor_.threadIndex == 0) {
		ThreadMonitor_.threadIndex = ThreadIndex_.fetch_add(1);
	}

	bManager_->registerThread();
	cManager_->registerThread();
	mManager_->registerThread();
	pManager_->registerThread();
	rManager_->registerThread();
	tManager_->registerThread();
	uManager_->registerThread();
}

// ====================================================================================================================
void GraphicsEngineImpl::unregisterThread()
{
	uManager_->unregisterThread();
	tManager_->unregisterThread();
	rManager_->unregisterThread();
	pManager_->unregisterThread();
	mManager_->unregisterThread();
	cManager_->unregisterThread();
	bManager_->unregisterThread();

	ThreadMonitor_.threadIndex = 0;
}

} // namespace vega
