/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <Vega/Graphics/GraphicsEngine.hpp>
#include "./Vulkan.hpp"

#include <thread>


namespace vega
{

class BindingManager;
class CommandManager;
class MemoryManager;
class PipelineManager;
class ResourceManager;
class TransferManager;
class UniformManager;

// Dedicated implementation subtype for GraphicsEngine
class GraphicsEngineImpl final :
	public GraphicsEngine
{
	friend GraphicsEngine;

	// Used to perform thread_local tracking of graphics resources and state
	struct ThreadMonitor final
	{
		ThreadMonitor() : threadId{ std::this_thread::get_id() }, threadIndex{ 0 } { }
		~ThreadMonitor();

		inline bool isRegistered() const { return threadIndex != 0; }

		const std::thread::id threadId;
		uint32 threadIndex; // == 0 before registration, > 0 afterwards

		VEGA_NO_COPY(ThreadMonitor)
		VEGA_NO_MOVE(ThreadMonitor)
	}; // struct ThreadMonitor

public:
	GraphicsEngineImpl(const GraphicsDevice* device);
	~GraphicsEngineImpl();

	// Frame Functions
	void nextFrame();

	// Threading
	void registerThread();
	void unregisterThread();
	inline uint32 threadIndex() const { return ThreadMonitor_.threadIndex; }

	// Managers
	inline BindingManager* bindingManager() const { AssertThreadRegistered(); return bManager_.get(); }
	inline CommandManager* commandManager() const { AssertThreadRegistered(); return cManager_.get(); }
	inline MemoryManager* memoryManager() const { AssertThreadRegistered(); return mManager_.get(); }
	inline PipelineManager* pipelineManager() const { AssertThreadRegistered(); return pManager_.get(); }
	inline ResourceManager* resourceManager() const { AssertThreadRegistered(); return rManager_.get(); }
	inline TransferManager* transferManager() const { AssertThreadRegistered(); return tManager_.get(); }
	inline UniformManager* uniformManager() const { AssertThreadRegistered(); return uManager_.get(); }

	// Instance Access
	inline static GraphicsEngineImpl* Get() { return static_cast<GraphicsEngineImpl*>(GraphicsEngine::Get()); }

	// Threading
	inline static void AssertThreadRegistered() {
		if (!ThreadMonitor_.isRegistered() && !Terminating_) {
			// Terminating_ check will only be important after the main thread is the only remaining thread
			throw std::runtime_error("Attempted to perform graphics operations on a non-registered thread");
		}
	}

private:
	const GraphicsDevice* const device_;
	UPtr<BindingManager> bManager_;
	UPtr<CommandManager> cManager_;
	UPtr<MemoryManager> mManager_;
	UPtr<PipelineManager> pManager_;
	UPtr<ResourceManager> rManager_;
	UPtr<TransferManager> tManager_;
	UPtr<UniformManager> uManager_;

	static bool Terminating_;
	static thread_local ThreadMonitor ThreadMonitor_;

	VEGA_NO_COPY(GraphicsEngineImpl)
	VEGA_NO_MOVE(GraphicsEngineImpl)
}; // class GraphicsEngineImpl

DECLARE_IMPL_GETTER(GraphicsEngine)

} // namespace vega
