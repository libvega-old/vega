/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./TextureImpl.hpp"
#include "../Managers/BindingManager.hpp"
#include "../Managers/ResourceManager.hpp"

#include <array>


namespace vega
{

// ====================================================================================================================
static constexpr vk::ImageUsageFlags USAGE{ 
	vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eSampled
};
static constexpr std::array<vk::ImageViewType, uint32(Texture::Dims::D3) + 1> VIEW_TYPES{
	vk::ImageViewType::e1D, vk::ImageViewType::e2D, vk::ImageViewType::e3D
};

// ====================================================================================================================
TextureImpl::TextureImpl(Texture::Dims dims, const Vec3ui& size, TexelFormat format,
		const StagingBuffer* data, uint64 dataOffset)
	: Texture()
	, image_{ USAGE, VIEW_TYPES[uint32(dims)], vk::Format(format.formatId()), { size.x, size.y, size.z, 1, 1 }, 
		data, dataOffset }
	, bindings_{ }
	, bindingMutex_{ }
{
	bindings_.fill(UINT16_MAX);
}

// ====================================================================================================================
TextureImpl::~TextureImpl()
{
	const auto bm = BindingManager::Get();
	for (auto index : bindings_) {
		if (index != UINT16_MAX) {
			bm->freeSampler(index);
		}
	}
}

// ====================================================================================================================
uint16 TextureImpl::getBindingIndex(Sampler sampler) const
{
	auto& index = bindings_[uint32(sampler)];
	if (index == UINT16_MAX) {
		std::lock_guard lock{ bindingMutex_ };
		if (index != UINT16_MAX) {
			return index; // Check for data race
		}
		index = BindingManager::Get()->reserveSampler(image_.viewHandle(), ResourceManager::Get()->getSampler(sampler));
	}
	return index;
}

} // namespace vega
