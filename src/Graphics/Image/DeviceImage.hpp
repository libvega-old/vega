/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include "../ResourceBase.hpp"
#include "../Managers/MemoryManager.hpp"
#include "../Managers/TransferManager.hpp"
#include "../Vulkan.hpp"
#include <Vega/Math/Vec3.hpp>


namespace vega
{

class StagingBuffer;

// Implementation of device-side image objects
class DeviceImage final
	: public ResourceBase
{
public:
	// Extent struct
	struct Extent final
	{
		union {
			struct {
				uint32 width;
				uint32 height;
				uint32 depth;
			};
			Vec3ui size;
		};
		uint32 layers;
		uint32 mips;
	}; // struct Extent

	DeviceImage(vk::ImageUsageFlags usage, vk::ImageViewType type, vk::Format format, const Extent& extent,
		const StagingBuffer* data, uint64 dataOffset);
	~DeviceImage();

	inline vk::ImageUsageFlags usage() const { return usage_; }
	inline vk::ImageViewType type() const { return type_; }
	inline vk::Format format() const { return format_; }
	inline const Extent& imageExtent() const { return extent_; }

	inline vk::Image imageHandle() const { return image_; }
	inline vk::ImageView viewHandle() const { return view_; }
	inline const MemoryAllocation* memory() const { return memory_.get(); }

	inline bool hasData() const { return hasData_; }

	void setData(const void* data, const ImageRegion& reg);
	void setData(const StagingBuffer& data, const ImageRegion& reg, uint64 dataOffset);

protected:
	inline Opt<uint64> resourceHandle() const override {
		return { uint64(VkImage(image_)) };
	}

private:
	static vk::ImageType ViewToImageType(vk::ImageViewType type);

private:
	const vk::ImageUsageFlags usage_;
	const vk::ImageViewType type_;
	const vk::Format format_;
	const Extent extent_;
	vk::Image image_;
	vk::ImageView view_;
	MemoryHandle memory_;
	bool hasData_;

	VEGA_NO_COPY(DeviceImage)
	VEGA_NO_MOVE(DeviceImage)
}; // class DeviceImage

} // namespace vega
