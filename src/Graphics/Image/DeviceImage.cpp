/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./DeviceImage.hpp"
#include "../Buffer/StagingBufferImpl.hpp"
#include "../Managers/TransferManager.hpp"
#include <Vega/Graphics/TexelFormat.hpp>


namespace vega
{

// ====================================================================================================================
static constexpr vk::ComponentMapping IDENTITY_MAPPING{ };

// ====================================================================================================================
DeviceImage::DeviceImage(vk::ImageUsageFlags usage, vk::ImageViewType type, vk::Format format, const Extent& extent,
		const StagingBuffer* data, uint64 dataOffset)
	: ResourceBase(vk::ObjectType::eImage)
	, usage_{ usage }
	, type_{ type }
	, format_{ format }
	, extent_{ extent }
	, image_{ }
	, view_{ }
	, memory_{ }
	, hasData_{ false }
{
	const auto vkdevice = Vulkan::Device();

	// Create the image
	vk::ImageCreateInfo ici{};
	ici.flags = {}; // Will eventually need cube flags
	ici.imageType = ViewToImageType(type);
	ici.format = format;
	ici.extent = vk::Extent3D{ extent.width, extent.height, extent.depth };
	ici.mipLevels = extent.mips;
	ici.arrayLayers = extent.layers;
	ici.samples = vk::SampleCountFlagBits::e1;
	ici.tiling = vk::ImageTiling::eOptimal;
	ici.usage = usage;
	ici.sharingMode = vk::SharingMode::eExclusive;
	ici.initialLayout = vk::ImageLayout::eUndefined;
	image_ = vkdevice.createImage(ici);

	// Allocate and bind memory
	memory_ = MemoryManager::Get()->allocateDevice(image_);
	vkdevice.bindImageMemory(image_, memory_->memory(), memory_->offset());

	// Create the image view
	vk::ImageViewCreateInfo ivci{};
	ivci.image = image_;
	ivci.viewType = type;
	ivci.format = format;
	ivci.components = IDENTITY_MAPPING;
	ivci.subresourceRange = { vk::ImageAspectFlagBits::eColor, 0, extent.mips, 0, extent.layers };
	view_ = vkdevice.createImageView(ivci);

	// Set the initial image data
	if (data) {
		const ImageRegion reg{ 0, 0, 0, extent.width, extent.height, extent.depth, 0, 1 };
		TransferManager::Get()->setImageDataImpl(image_, reg, *get_impl(data), dataOffset, ici.usage, true);
		hasData_ = true;
	}
}

// ====================================================================================================================
DeviceImage::~DeviceImage()
{
	queueDestroy({ view_, image_, memory_.release() });
}

// ====================================================================================================================
void DeviceImage::setData(const void* data, const ImageRegion& reg)
{
	// TODO: Support layered images

	// Validate
	if (!data) {
		throw std::runtime_error("Cannot update Texture with null data");
	}
	if (reg.width == 0) {
		throw std::runtime_error("Cannot update a zero volume Texture region (width == 0)");
	}
	if (reg.height == 0) {
		throw std::runtime_error("Cannot update a zero volume Texture region (height == 0)");
	}
	if (reg.depth == 0) {
		throw std::runtime_error("Cannot update a zero volume Texture region (depth == 0)");
	}
	if ((reg.x + reg.width) > extent_.width) {
		throw std::runtime_error("Cannot update an out-of-bounds Texture region (x coordinate)");
	}
	if ((reg.y + reg.height) > extent_.height) {
		throw std::runtime_error("Cannot update an out-of-bounds Texture region (y coordinate)");
	}
	if ((reg.z + reg.depth) > extent_.depth) {
		throw std::runtime_error("Cannot update an out-of-bounds Texture region (z coordinate)");
	}

	// Update data
	const auto dataSize = TexelFormat::FromId(uint32(format_)).size() * reg.width * reg.height * reg.depth;
	const auto whole = (reg.width == extent_.width) && (reg.height == extent_.height) && (reg.depth == extent_.depth);
	TransferManager::Get()->setImageData(image_, usage_, reg, data, dataSize, whole);
}

// ====================================================================================================================
void DeviceImage::setData(const StagingBuffer& data, const ImageRegion& reg, uint64 dataOffset)
{
	// TODO: Support layered images

	// Validate
	const auto dataSize = TexelFormat::FromId(uint32(format_)).size() * reg.width * reg.height * reg.depth;
	if ((dataSize + dataOffset) > data.size()) {
		throw std::runtime_error("Source buffer not large enough to supply texture data");
	}
	if (reg.width == 0) {
		throw std::runtime_error("Cannot update a zero volume Texture region (width == 0)");
	}
	if (reg.height == 0) {
		throw std::runtime_error("Cannot update a zero volume Texture region (height == 0)");
	}
	if (reg.depth == 0) {
		throw std::runtime_error("Cannot update a zero volume Texture region (depth == 0)");
	}
	if ((reg.x + reg.width) > extent_.width) {
		throw std::runtime_error("Cannot update an out-of-bounds Texture region (x coordinate)");
	}
	if ((reg.y + reg.height) > extent_.height) {
		throw std::runtime_error("Cannot update an out-of-bounds Texture region (y coordinate)");
	}
	if ((reg.z + reg.depth) > extent_.depth) {
		throw std::runtime_error("Cannot update an out-of-bounds Texture region (z coordinate)");
	}

	// Update data
	const auto whole = (reg.width == extent_.width) && (reg.height == extent_.height) && (reg.depth == extent_.depth);
	TransferManager::Get()->setImageDataImpl(image_, reg, get_impl(data), dataOffset, usage_, whole);
}

// ====================================================================================================================
vk::ImageType DeviceImage::ViewToImageType(vk::ImageViewType type)
{
	switch (type)
	{
	case vk::ImageViewType::e1D: return vk::ImageType::e1D;
	case vk::ImageViewType::e2D: return vk::ImageType::e2D;
	case vk::ImageViewType::e3D: return vk::ImageType::e3D;
	default: assert(false && "Invalid image view type"); return { };
	}
}

} // namespace vega
