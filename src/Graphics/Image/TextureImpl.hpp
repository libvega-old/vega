/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include <Vega/Graphics/Texture.hpp>
#include <Vega/Render/Sampler.hpp>
#include "./DeviceImage.hpp"

#include <array>
#include <mutex>


namespace vega
{

// Implementation of Texture
class TextureImpl final
	: public Texture
{
	friend class Texture;

public:
	TextureImpl(Texture::Dims dims, const Vec3ui& size, TexelFormat format, const StagingBuffer* data, 
		uint64 dataOffset);
	~TextureImpl();

	inline const DeviceImage* image() const { return &image_; }

	uint16 getBindingIndex(Sampler sampler) const;

private:
	DeviceImage image_;
	mutable std::array<uint16, uint32(Sampler::LinearLinearTransparent) + 1> bindings_;
	mutable std::mutex bindingMutex_;

	VEGA_NO_COPY(TextureImpl)
	VEGA_NO_MOVE(TextureImpl)
}; // class TextureImpl

DECLARE_IMPL_GETTER(Texture)

} // namespace vega
