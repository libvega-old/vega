/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Core/Monitor.hpp>
#include <Vega/Core/Threading.hpp>
#include "./GLFW.hpp"

#include <algorithm>
#include <vector>


namespace vega
{

// ====================================================================================================================
const Monitor Monitor::Invalid{ };
std::vector<SPtr<Monitor::MonitorHandle>> Monitor::Handles_{ };
std::vector<Monitor> Monitor::Monitors_{ };

// ====================================================================================================================
bool Monitor::isPrimary() const
{
	const auto handle = handle_.lock();
	return handle && (handle->handle == glfwGetPrimaryMonitor());
}

// ====================================================================================================================
String Monitor::name() const
{
	const auto handle = handle_.lock();
	return { handle ? glfwGetMonitorName((GLFWmonitor*)handle->handle) : "" };
}

// ====================================================================================================================
Vec2ui Monitor::physicalSize() const
{
	if (const auto handle = handle_.lock()) {
		int w, h;
		glfwGetMonitorPhysicalSize((GLFWmonitor*)handle->handle, &w, &h);
		return { uint32(w), uint32(h) };
	}
	else {
		return { };
	}
}

// ====================================================================================================================
Vec2ui Monitor::size() const
{
	if (const auto handle = handle_.lock()) {
		const auto vid = glfwGetVideoMode((GLFWmonitor*)handle->handle);
		return { uint32(vid->width), uint32(vid->height) };
	}
	else {
		return { };
	}
}

// ====================================================================================================================
Vec2i Monitor::position() const
{
	if (const auto handle = handle_.lock()) {
		Vec2i pos{};
		glfwGetMonitorPos((GLFWmonitor*)handle->handle, &pos.x, &pos.y);
		return pos;
	}
	else {
		return { };
	}
}

// ====================================================================================================================
Vec2f Monitor::contentScale() const
{
	if (const auto handle = handle_.lock()) {
		Vec2f sc{};
		glfwGetMonitorContentScale((GLFWmonitor*)handle->handle, &sc.x, &sc.y);
		return sc;
	}
	else {
		return { };
	}
}

// ====================================================================================================================
Rect2i Monitor::workArea() const
{
	if (const auto handle = handle_.lock()) {
		int x, y, w, h;
		glfwGetMonitorWorkarea((GLFWmonitor*)handle->handle, &x, &y, &w, &h);
		return { x, y, uint32(w), uint32(h) };
	}
	else {
		return Rect2i::Empty;
	}
}

// ====================================================================================================================
Rect2i Monitor::displayArea() const
{
	if (const auto handle = handle_.lock()) {
		int x, y;
		glfwGetMonitorPos((GLFWmonitor*)handle->handle, &x, &y);
		const auto vid = glfwGetVideoMode((GLFWmonitor*)handle->handle);
		return { x, y, uint32(vid->width), uint32(vid->height) };
	}
	else {
		return Rect2i::Empty;
	}
}

// ====================================================================================================================
const std::vector<VideoMode>& Monitor::videoModes() const
{
	if (const auto handle = handle_.lock()) {
		return handle->modes;
	}
	else {
		static const std::vector<VideoMode> EMPTY{};
		return EMPTY;
	}
}

// ====================================================================================================================
VideoMode Monitor::currentVideoMode() const
{
	if (const auto handle = handle_.lock()) {
		const auto vidmode = glfwGetVideoMode((GLFWmonitor*)handle->handle);
		VideoMode mode{};
		mode.width_ = uint32(vidmode->width);
		mode.height_ = uint32(vidmode->height);
		mode.color_ = uint32(vidmode->redBits + vidmode->greenBits + vidmode->blueBits);
		mode.refresh_ = uint32(vidmode->refreshRate);
		return mode;
	}
	else {
		return {};
	}
}

// ====================================================================================================================
const std::vector<Monitor>& Monitor::Monitors()
{
	static bool Init_{ false };
	static const auto MakeVideoMode_ = [](const GLFWvidmode& mptr) -> VideoMode {
		VideoMode mode{};
		mode.width_ = uint32(mptr.width);
		mode.height_ = uint32(mptr.height);
		mode.color_ = uint32(mptr.redBits + mptr.greenBits + mptr.blueBits);
		mode.refresh_ = uint32(mptr.refreshRate);
		return mode;
	};

	Threading::AssertMainThread("Cannot enumerate system monitors");

	if (!Init_) {
		GLFW::Initialize();

		int count;
		const auto mons = glfwGetMonitors(&count);
		Handles_.resize(size_t(count), { });
		Monitors_.resize(size_t(count), { });
		std::transform(mons, mons + size_t(count), Handles_.begin(),
			[](GLFWmonitor* mon) { return std::make_shared<MonitorHandle>(mon); });
		std::transform(Handles_.begin(), Handles_.end(), Monitors_.begin(),
			[](const SPtr<MonitorHandle>& mon) { return Monitor{ mon }; });
		for (const auto& handle : Handles_) {
			const auto modes = glfwGetVideoModes((GLFWmonitor*)handle->handle, &count);
			handle->modes.resize(size_t(count));
			std::transform(modes, modes + size_t(count), handle->modes.begin(), MakeVideoMode_);
		}

		glfwSetMonitorCallback([](GLFWmonitor* mon, int evt) {
			if (evt == GLFW_CONNECTED) {
				const auto& handle = Handles_.emplace_back(std::make_shared<MonitorHandle>(mon));
				Monitors_.push_back(Monitor{ handle });
				int count;
				const auto modes = glfwGetVideoModes((GLFWmonitor*)handle->handle, &count);
				handle->modes.resize(size_t(count));
				std::transform(modes, modes + size_t(count), handle->modes.begin(), MakeVideoMode_);
			}
			else if (evt == GLFW_DISCONNECTED) {
				const auto mit = std::find_if(Monitors_.begin(), Monitors_.end(),
					[mon](Monitor monit) { return monit.handle_.lock()->handle == mon; });
				if (mit != Monitors_.end()) {
					Monitors_.erase(mit);
				}
				const auto hit = std::find_if(Handles_.begin(), Handles_.end(),
					[mon](const SPtr<MonitorHandle>& hand) { return hand->handle == mon; });
				if (hit != Handles_.end()) {
					Handles_.erase(hit);
				}
			}
		});

		Init_ = true;
	}

	return Monitors_;
}

// ====================================================================================================================
Monitor Monitor::Primary()
{
	for (const auto& mon : Monitors()) {
		if (mon.isPrimary()) {
			return mon;
		}
	}
	throw std::runtime_error("System has no connected monitors");
}

} // namespace vega
