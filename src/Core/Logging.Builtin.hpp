/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <Vega/Core/Logging.hpp>

#include <fstream>


namespace vega
{

// Default console log handler
class ConsoleHandler final 
	: public LogHandler
{
public:
	ConsoleHandler(LogLevel mask) : mask_{ mask } { }

	void handleMessage(const Logger& logger, LogLevel level, const TimeSpan& time, const StringView& message) override;

private:
	LogLevel mask_;

	VEGA_NO_COPY(ConsoleHandler)
	VEGA_NO_MOVE(ConsoleHandler)
}; // class ConsoleHandler

// Default file log handler
class FileHandler final 
	: public LogHandler
{
public:
	FileHandler(const StringView& path, bool history, LogLevel mask);
	~FileHandler();

	void handleMessage(const Logger& logger, LogLevel level, const TimeSpan& time, const StringView& message) override;

private:
	LogLevel mask_;
	std::ofstream file_;

	VEGA_NO_COPY(FileHandler)
	VEGA_NO_MOVE(FileHandler)
}; // class FileHandler

} // namespace vega
