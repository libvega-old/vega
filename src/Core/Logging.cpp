/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./Logging.Builtin.hpp"
#include <Vega/Core/Time.hpp>
#include <Vega/Math/Math.hpp>

#include <algorithm>
#include <list>
#include <mutex>
#include <unordered_map>


namespace vega
{

// ====================================================================================================================
String to_string(LogLevelMask mask)
{
	if (mask == LogLevel::Standard) {
		return "Standard";
	}
	else if (mask == LogLevel::All) {
		return "All";
	}
	else if (mask == LogLevel::None) {
		return "None";
	}

	std::stringstream ss{};
	uint32 maskCount{ 0 };
	if (mask & LogLevel::Verbose) {
		ss << "Verbose|";
		maskCount++;
	}
	if (mask & LogLevel::Info) {
		ss << "Info|";
		maskCount++;
	}
	if (mask & LogLevel::Warning) {
		ss << "Warning|";
		maskCount++;
	}
	if (mask & LogLevel::Error) {
		ss << "Error|";
		maskCount++;
	}
	if (mask & LogLevel::Fatal) {
		ss << "Fatal|";
		maskCount++;
	}
	const String res{ ss.str() };
	return (maskCount == 1) 
		? res.substr(0, res.size() - 1) 
		: ('[' + res.substr(0, res.size() - 1) + ']');
}


// ====================================================================================================================
// ====================================================================================================================
namespace loggers {
	static std::mutex Mutex_{ };
	static std::unordered_map<String, Logger*> Loggers_{ };
}

// ====================================================================================================================
Logger::Logger(const StringView& name, const StringView& tag)
	: name_{ name }
	, tag_{ }
{
	std::memset(tag_.str, ' ', TAG_SIZE);
	tag_.str[TAG_SIZE] = '\0';
	std::memcpy(tag_.str, tag.data(), math::min(tag.length(), TAG_SIZE));

	std::lock_guard lock{ loggers::Mutex_ };
	loggers::Loggers_.insert({ String{ name }, this });
}

// ====================================================================================================================
Logger::~Logger()
{
	std::lock_guard lock{ loggers::Mutex_ };
	loggers::Loggers_.erase(name_);
}

// ====================================================================================================================
SPtr<Logger> Logger::Get(const StringView& name, const Opt<StringView>& tag)
{
	if (const auto logger = GetExisting(name); logger) {
		return logger;
	}
	return SPtr<Logger>{ new Logger(name, tag.value_or(name)) }; // Ctor adds to the map
}

// ====================================================================================================================
SPtr<Logger> Logger::GetExisting(const StringView& name)
{
	std::lock_guard lock{ loggers::Mutex_ };
	const auto it = loggers::Loggers_.find(String{ name });
	return (it != loggers::Loggers_.end()) ? it->second->shared_from_this() : SPtr<Logger>{ };
}


// ====================================================================================================================
// ====================================================================================================================
namespace handlers {
	static std::mutex Mutex_{ };
	static std::list<LogHandler*> Handlers_{ };
}

// ====================================================================================================================
LogHandler::LogHandler()
	: installed_{ false }
{

}

// ====================================================================================================================
LogHandler::~LogHandler()
{
	if (isInstalled()) {
		std::lock_guard lock{ handlers::Mutex_ };
		const auto it = std::find_if(handlers::Handlers_.begin(), handlers::Handlers_.end(), [this](LogHandler* lh) {
			return lh == this;
		});
		handlers::Handlers_.erase(it);
	}
}

// ====================================================================================================================
void LogHandler::Install(LogHandler& handler)
{
	if (handler.isInstalled()) {
		return;
	}

	std::lock_guard lock{ handlers::Mutex_ };
	handlers::Handlers_.push_back(&handler);
	handler.installed_ = true;
}

// ====================================================================================================================
void LogHandler::PostMessage(const Logger& logger, LogLevel level, const StringView& message)
{
	const auto now = Time::Elapsed();
	std::lock_guard lock{ handlers::Mutex_ };
	for (const auto handler : handlers::Handlers_) {
		handler->handleMessage(logger, level, now, message);
	}
}


// ====================================================================================================================
// ====================================================================================================================
LogStream::LogStream(const Logger& logger, LogLevel level)
	: logger_{ &logger }
	, level_{ level }
	, buffer_{ }
	, dirty_{ false }
{

}

// ====================================================================================================================
LogStream::~LogStream()
{
	flush();
}

// ====================================================================================================================
void LogStream::flush()
{
	if (!dirty()) {
		return;
	}

	switch (level_)
	{
	case LogLevel::Verbose: logger_->verbose(buffer_.str()); break;
	case LogLevel::Info:    logger_->info(buffer_.str()); break;
	case LogLevel::Warning: logger_->warn(buffer_.str()); break;
	case LogLevel::Error:   logger_->error(buffer_.str()); break;
	case LogLevel::Fatal:   logger_->fatal(buffer_.str()); break;
	}
	buffer_.str("");
	dirty_ = false;
}


// ====================================================================================================================
// ====================================================================================================================
UPtr<Logger> Log::DefaultLogger_{ new Logger("default", "vega") };

// ====================================================================================================================
void Log::EnableDefaultConsoleHandler(LogLevel level)
{
	static UPtr<ConsoleHandler> ConsoleHandler_{ new ConsoleHandler(level) };
	static bool Initialized_{ false };
	if (!Initialized_) {
		LogHandler::Install(*ConsoleHandler_);
		Initialized_ = true;
	}
}

// ====================================================================================================================
void Log::EnableDefaultFileHandler(bool history, LogLevel level)
{
	static UPtr<FileHandler> FileHandler_{ new FileHandler("./logs", history, level) };
	static bool Initialized_{ false };
	if (!Initialized_) {
		LogHandler::Install(*FileHandler_);
		Initialized_ = true;
	}
}

} // namespace vega
