/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Logging.Builtin.hpp"

#include <filesystem>
#include <iostream>
namespace fs = std::filesystem;

#include <sys/types.h>
#include <sys/stat.h>
#ifndef VEGA_WIN32
#	include <unistd.h>
#elif !defined(stat)
#	define stat _stat
#endif


namespace vega
{

namespace impl
{

// Full default log prefix, format: "[HH:MM:SS.sss][logtag  ][L]\0"
using LogPrefix = char[28];

// Populate the logger prefix
static void populate_prefix(const TimeSpan& time, const Logger& logger, LogLevel level, LogPrefix& pre)
{
	// Fill out the time
	const auto
		hr  = uint32(time.hours()),
		min = uint32(time.minutes()) % 60,
		sec = uint32(time.seconds()) % 60,
		ms  = uint32(time.milliseconds()) % 1000;
	pre[0]  = '[';
	pre[1]  = char(hr / 10) + '0';
	pre[2]  = char(hr % 10) + '0';
	pre[3]  = ':';
	pre[4]  = char(min / 10) + '0';
	pre[5]  = char(min % 10) + '0';
	pre[6]  = ':';
	pre[7]  = char(sec / 10) + '0';
	pre[8]  = char(sec % 10) + '0';
	pre[9]  = '.';
	pre[10] = char(ms / 100) + '0';
	pre[11] = char((ms % 100) / 10) + '0';
	pre[12] = char(ms % 10) + '0';
	pre[13] = ']';

	// Logger tag
	pre[14] = '[';
	std::memcpy(&pre[15], logger.tag().str, Logger::TAG_SIZE);
	pre[23] = ']';

	// Log level
	pre[24] = '[';
	switch (level) {
	case LogLevel::Verbose: pre[25] = 'V'; break;
	case LogLevel::Info:    pre[25] = 'I'; break;
	case LogLevel::Warning: pre[25] = 'W'; break;
	case LogLevel::Error:   pre[25] = 'E'; break;
	case LogLevel::Fatal:   pre[25] = '!'; break;
	default:                pre[25] = '*'; break;
	}
	pre[26] = ']';
	pre[27] = '\0';
}

// Populate the date-time string
using DateTimeString = char[18]; // Format: "YY_MM_DD.HH_mm_ss\0"
inline static void populate_datetime(time_t time, DateTimeString& dts)
{
	const auto localtime = *std::localtime(&time);
	dts[0] = char((localtime.tm_year % 100) / 10) + '0';
	dts[1] = char(localtime.tm_year % 10) + '0';
	dts[2] = '_';
	dts[3] = char((localtime.tm_mon + 1) / 10) + '0';
	dts[4] = char((localtime.tm_mon + 1) % 10) + '0';
	dts[5] = '_';
	dts[6] = char(localtime.tm_mday / 10) + '0';
	dts[7] = char(localtime.tm_mday % 10) + '0';
	dts[8] = '.';
	dts[9] = char(localtime.tm_hour / 10) + '0';
	dts[10] = char(localtime.tm_hour % 10) + '0';
	dts[11] = '_';
	dts[12] = char(localtime.tm_min / 10) + '0';
	dts[13] = char(localtime.tm_min % 10) + '0';
	dts[14] = '_';
	dts[15] = char(localtime.tm_sec / 10) + '0';
	dts[16] = char(localtime.tm_sec % 10) + '0';
	dts[17] = '\0';
}

} // namespace impl


// ====================================================================================================================
// ====================================================================================================================
void ConsoleHandler::handleMessage(const Logger& logger, LogLevel level, const TimeSpan& time, 
	const StringView& message)
{
	if (!bool(mask_ & level)) {
		return;
	}

	impl::LogPrefix prefix;
	impl::populate_prefix(time, logger, level, prefix);
	auto& stream = (uint32(level) >= uint32(LogLevel::Warning)) ? std::cout : std::cerr;
	stream << prefix << ":  " << message << std::endl;
}


// ====================================================================================================================
// ====================================================================================================================
FileHandler::FileHandler(const StringView& path, bool history, LogLevel mask)
	: mask_{ mask }
	, file_ { }
{
	const auto filePath = fs::path{ path } / fs::path{ "latest.log" };

	// Create dir if needed
	if (!fs::exists(path)) {
		fs::create_directory(path);
	}

	// Open the file
	if (history && fs::is_regular_file(filePath)) {
		// Get create time
		struct stat result {};
		stat(filePath.string().c_str(), &result);
		impl::DateTimeString rawDTS{};
		impl::populate_datetime(result.st_ctime, rawDTS);

		// Rename old file
		const auto dstPath = fs::path{ path } / fs::path{ to_string("%s.log", rawDTS) };
		std::error_code ec{};
		fs::rename(filePath, dstPath, ec);
	}
	file_.open(filePath, std::ios::out | std::ios::trunc);
}

// ====================================================================================================================
FileHandler::~FileHandler()
{
	if (file_) {
		file_.flush();
		file_.close();
	}
}

// ====================================================================================================================
void FileHandler::handleMessage(const Logger& logger, LogLevel level, const TimeSpan& time, 
	const StringView& message)
{
	if (!file_ || !bool(mask_ & level)) {
		return;
	}

	impl::LogPrefix prefix;
	impl::populate_prefix(time, logger, level, prefix);
	file_ << prefix << ":  " << message << std::endl;
}

} // namespace vega
