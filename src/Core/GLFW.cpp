/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./GLFW.hpp"

#if defined(VEGA_WIN32)
#	define GLFW_EXPOSE_NATIVE_WIN32
#elif defined(VEGA_LINUX)
#	define GLFW_EXPOSE_NATIVE_X11
#endif // defined(VEGA_WIN32)
#include <GLFW/glfw3native.h>

#include <atomic>


namespace vega
{

// ====================================================================================================================
bool GLFW::Initialized_{ false };
int GLFW::LastErrorCode_{ GLFW_NO_ERROR };
String GLFW::LastErrorMsg_{ "" };

// ====================================================================================================================
void GLFW::Initialize()
{
	static std::atomic_flag Initializing_{ ATOMIC_FLAG_INIT };

	if (Initializing_.test_and_set()) {
		while (!Initialized_) { ; } // Already initializing, wait just in case its not fully initialized yet
	}
	else { // First initialization
		// Install error handler
		glfwSetErrorCallback([](int err, const char* msg) {
			LastErrorCode_ = err;
			LastErrorMsg_ = { msg };
		});

		// Attempt initialization
		if (!glfwInit()) {
			int errcode; String errmsg;
			GetError(&errcode, &errmsg);
			throw std::runtime_error(to_string("Failed to initialize GLFW (code %d): %s", errcode, errmsg.c_str()));
		}

		Initialized_ = true;
	}
}

// ====================================================================================================================
void GLFW::Terminate()
{
	if (Initialized_) {
		glfwTerminate();
		Initialized_ = false;
	}
}

// ====================================================================================================================
void GLFW::PollEvents()
{
	glfwPollEvents();
}

// ====================================================================================================================
GLFWwindow* GLFW::OpenWindow(const String& title, uint32 w, uint32 h)
{
	Initialize();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	return glfwCreateWindow(int(w), int(h), title.c_str(), nullptr, nullptr);
}

// ====================================================================================================================
void* GLFW::GetNativeWindowHandle(GLFWwindow* window)
{
#if defined(VEGA_WIN32)
	return static_cast<void*>(glfwGetWin32Window(window));
#else
	return nullptr;
#endif // defined(VEGA_WIN32)
}

// ====================================================================================================================
bool GLFW::GetError(int* code, String* msg)
{
	if (LastErrorCode_ != GLFW_NO_ERROR) {
		if (code) {
			*code = LastErrorCode_;
		}
		if (msg) {
			*msg = LastErrorMsg_;
		}
		LastErrorCode_ = GLFW_NO_ERROR;
		LastErrorMsg_ = { "" };
		return true;
	}
	return false;
}


} // namespace vega
