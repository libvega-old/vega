/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <Vega/Core/Window.hpp>
#include <Vega/Input/Keyboard.hpp>
#include <Vega/Input/Mouse.hpp>
#include "../Render/RenderLayoutImpl.hpp"
#include "../Render/Renderer/RendererImpl.hpp"
#include "./GLFW.hpp"


namespace vega
{

class Swapchain;

// Implementation for Window
class WindowImpl final
	: public Window
{
	friend class Window;

public:
	WindowImpl(const String& title, const Vec2ui& size, const SPtr<RenderLayout>& layout);
	~WindowImpl();

	inline GLFWwindow* handle() const { return handle_; }

	void beginFrame();
	void beginRender();
	void endFrame();

	// Called by the Swapchain when it rebuilds
	void onSwapchainRebuild(const Vec2ui& newSize);

private:
	GLFWwindow* const handle_;
	const uint32 index_;
	UPtr<Swapchain> swapchain_;
	UPtr<RendererImpl> renderer_;
	Opt<Vec2ui> rendererSize_;
	UPtr<Keyboard> keyboard_;
	UPtr<Mouse> mouse_;
	struct
	{
		String title;
		bool resizable;
		bool decorated;
		bool floating;
		vega::WindowMode mode;
	} props_;
	struct
	{
		Rect2i window;
		vega::Monitor monitor;
	} saved_;

	VEGA_NO_COPY(WindowImpl)
	VEGA_NO_MOVE(WindowImpl)
}; // class WindowImpl

DECLARE_IMPL_GETTER(Window)

} // namespace vega
