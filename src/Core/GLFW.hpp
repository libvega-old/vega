/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#ifdef APIENTRY
#	undef APIENTRY
#endif


namespace vega
{

// Manages the global GLFW instance
class GLFW final
{
	friend class AppBase;

public:
	// Perform global initialization, if needed
	static void Initialize();

	// Called from std::atexit
	static void Terminate();

	// Perform global event polling
	static void PollEvents();

	// Creates a window with the given params, performing global initialization if needed
	static GLFWwindow* OpenWindow(const String& title, uint32 w, uint32 h);

	// Gets the native handle (HWND on Windows, nullptr otherwise) for the window.
	static void* GetNativeWindowHandle(GLFWwindow* window);

	// Check if GLFW is globally initialized
	inline static bool IsInitialized() { return Initialized_; }

	// Loads the error, if any, into the arguments and clears the error, returning if there was an error
	static bool GetError(int* code, String* msg);

private:
	static bool Initialized_;
	static int LastErrorCode_;
	static String LastErrorMsg_;

	VEGA_NO_COPY(GLFW)
	VEGA_NO_MOVE(GLFW)
	VEGA_NO_INIT(GLFW)
}; // class GLFW

} // namespace vega
