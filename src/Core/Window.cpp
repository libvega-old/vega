/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./WindowImpl.hpp"
#include "../Render/Swapchain.hpp"

#include <array>


namespace vega
{

#define THIS_IMPL_TYPE WindowImpl

// ====================================================================================================================
const String& to_string(WindowMode mode)
{
	static const std::array<String, 3> NAMES{
		"Window", "FullscreenWindow", "FullscreenExclusive"
	};
	return NAMES.at(uint32(mode));
}


// ====================================================================================================================
// ====================================================================================================================
Window::Window()
{

}

// ====================================================================================================================
Window::~Window()
{

}

// ====================================================================================================================
uint32 Window::index() const
{
	return THIS_IMPL->index_;
}

// ====================================================================================================================
bool Window::isMain() const
{
	return THIS_IMPL->index_ == 0;
}

// ====================================================================================================================
const String& Window::title() const
{
	return THIS_IMPL->props_.title;
}

// ====================================================================================================================
void Window::title(const String& title)
{
	THIS_IMPL->props_.title = title;
	glfwSetWindowTitle(THIS_IMPL->handle_, title.c_str());
}

// ====================================================================================================================
bool Window::resizeable() const
{
	return THIS_IMPL->props_.resizable;
}

// ====================================================================================================================
void Window::resizeable(bool flag)
{
	THIS_IMPL->props_.resizable = flag;
	if (THIS_IMPL->props_.mode == WindowMode::Window) {
		glfwSetWindowAttrib(THIS_IMPL->handle_, GLFW_RESIZABLE, flag ? GLFW_TRUE : GLFW_FALSE);
	}
}

// ====================================================================================================================
bool Window::decorated() const
{
	return THIS_IMPL->props_.decorated;
}

// ====================================================================================================================
void Window::decorated(bool flag)
{
	THIS_IMPL->props_.decorated = flag;
	if (THIS_IMPL->props_.mode == WindowMode::Window) {
		glfwSetWindowAttrib(THIS_IMPL->handle_, GLFW_DECORATED, flag ? GLFW_TRUE : GLFW_FALSE);
	}
}

// ====================================================================================================================
bool Window::floating() const
{
	return THIS_IMPL->props_.floating;
}

// ====================================================================================================================
void Window::floating(bool flag)
{
	THIS_IMPL->props_.floating = flag;
	if (THIS_IMPL->props_.mode == WindowMode::Window) {
		glfwSetWindowAttrib(THIS_IMPL->handle_, GLFW_FLOATING, flag ? GLFW_TRUE : GLFW_FALSE);
	}
}

// ====================================================================================================================
WindowMode Window::windowMode() const
{
	return THIS_IMPL->props_.mode;
}

// ====================================================================================================================
void Window::windowMode(Opt<::vega::Monitor> monitor, Opt<VideoMode> vidMode)
{
	// Skip switch if possible
	const auto newMode = monitor.has_value()
		? (vidMode.has_value() ? WindowMode::FullscreenExclusive : WindowMode::FullscreenWindow)
		: WindowMode::Window;
	if (newMode == THIS_IMPL->props_.mode) {
		return;
	}

	const auto handle = THIS_IMPL->handle_;
	auto& saved = THIS_IMPL->saved_;

	// Perform the switch
	if (newMode == WindowMode::Window) {
		if (THIS_IMPL->props_.mode == WindowMode::FullscreenWindow) { // Window -> Window
			glfwSetWindowPos(handle, saved.window.x, saved.window.y);
			glfwSetWindowSize(handle, int(saved.window.w), int(saved.window.h));
		}
		else { // Fullscreen -> Window
			glfwSetWindowMonitor(handle, nullptr, saved.window.x, saved.window.y, int(saved.window.w),
				int(saved.window.h), GLFW_DONT_CARE);
		}

		// Update to windowed-mode attribs
		glfwSetWindowAttrib(handle, GLFW_RESIZABLE, THIS_IMPL->props_.resizable ? GLFW_TRUE : GLFW_FALSE);
		glfwSetWindowAttrib(handle, GLFW_DECORATED, THIS_IMPL->props_.decorated ? GLFW_TRUE : GLFW_FALSE);
		glfwSetWindowAttrib(handle, GLFW_FLOATING, THIS_IMPL->props_.floating ? GLFW_TRUE : GLFW_FALSE);
	}
	else if (newMode == WindowMode::FullscreenWindow) {
		if (THIS_IMPL->props_.mode == WindowMode::Window) { // Window -> Window
			saved.window = { position(), size() };
			const auto disp = (saved.monitor = this->monitor()).displayArea();
			glfwSetWindowAttrib(handle, GLFW_RESIZABLE, GLFW_FALSE);
			glfwSetWindowAttrib(handle, GLFW_DECORATED, GLFW_FALSE);
			glfwSetWindowAttrib(handle, GLFW_FLOATING, GLFW_FALSE);
			glfwSetWindowPos(handle, disp.x, disp.y);
			glfwSetWindowSize(handle, int(disp.w), int(disp.h));
		}
		else { // Fullscreen -> Window
			const auto disp = (saved.monitor.isValid() ? saved.monitor : Monitor::Primary()).displayArea();
			glfwSetWindowMonitor(handle, nullptr, disp.x, disp.y, int(disp.w), int(disp.h), GLFW_DONT_CARE);
			glfwSetWindowAttrib(handle, GLFW_RESIZABLE, GLFW_FALSE);
			glfwSetWindowAttrib(handle, GLFW_DECORATED, GLFW_FALSE);
			glfwSetWindowAttrib(handle, GLFW_FLOATING, GLFW_FALSE);
		}

		glfwFocusWindow(handle);
	}
	else { // Exclusive Fullscreen (Window -> Fullscreen)
		saved.monitor = Monitor();
		if (THIS_IMPL->props_.mode == WindowMode::Window) {
			saved.window = { position(), size() };
		}

		glfwSetWindowMonitor(handle, reinterpret_cast<GLFWmonitor*>(saved.monitor.handle_.lock()->handle),
			0, 0, int(vidMode.value().width()), int(vidMode.value().height()), int(vidMode.value().refresh()));
	}

	// Update props
	THIS_IMPL->props_.mode = newMode;
}

// ====================================================================================================================
Vec2i Window::position() const
{
	Vec2i pos{};
	glfwGetWindowPos(THIS_IMPL->handle_, &pos.x, &pos.y);
	return pos;
}

// ====================================================================================================================
Vec2ui Window::size() const
{
	int w, h;
	glfwGetWindowSize(THIS_IMPL->handle_, &w, &h);
	return { uint32(w), uint32(h) };
}

// ====================================================================================================================
Vec2ui Window::rendererSize() const
{
	return THIS_IMPL->renderer_->extent();
}

// ====================================================================================================================
void Window::size(const Vec2ui& size)
{
	if (THIS_IMPL->handle_) {
		if (size.x == 0 || size.y == 0) {
			this->minimize();
			return;
		}
		glfwSetWindowSize(THIS_IMPL->handle_, int(size.x), int(size.y));
	}
}

// ====================================================================================================================
void Window::lockRendererSize(const Vec2ui& size)
{
	THIS_IMPL->renderer_->setSurfaceParams(size, std::nullopt);
	THIS_IMPL->rendererSize_ = size;
}

// ====================================================================================================================
void Window::unlockRendererSize(bool immediate)
{
	if (THIS_IMPL->rendererSize_.has_value()) {
		THIS_IMPL->rendererSize_ = std::nullopt;
		if (immediate) {
			THIS_IMPL->renderer_->setSurfaceParams(size(), std::nullopt);
		}
	}
}

// ====================================================================================================================
bool Window::isRendererLocked() const
{
	return THIS_IMPL->rendererSize_.has_value();
}

// ====================================================================================================================
const Monitor& Window::monitor() const
{
	if (!isVisible()) {
		return Monitor::Invalid;
	}

	uint32 bestSize{ 0 };
	const vega::Monitor* bestMon{ nullptr };

	const auto content = contentArea();
	for (auto& mon : vega::Monitor::Monitors()) {
		const auto area = mon.workArea().overlap(content).area();
		if (area > bestSize) {
			bestSize = area;
			bestMon = &mon;
		}
	}

	return bestMon ? *bestMon : Monitor::Invalid;
}

// ====================================================================================================================
bool Window::isCloseRequested() const
{
	return glfwWindowShouldClose(THIS_IMPL->handle_) == GLFW_TRUE;
}

// ====================================================================================================================
void Window::close()
{
	glfwSetWindowShouldClose(THIS_IMPL->handle_, GLFW_TRUE);
}

// ====================================================================================================================
void Window::hide()
{
	glfwHideWindow(THIS_IMPL->handle_);
}

// ====================================================================================================================
void Window::show()
{
	glfwShowWindow(THIS_IMPL->handle_);
}

// ====================================================================================================================
bool Window::isVisible() const
{
	return glfwGetWindowAttrib(THIS_IMPL->handle_, GLFW_VISIBLE) == GLFW_TRUE;
}

// ====================================================================================================================
void Window::focus()
{
	glfwFocusWindow(THIS_IMPL->handle_);
}

// ====================================================================================================================
bool Window::isFocused() const
{
	return glfwGetWindowAttrib(THIS_IMPL->handle_, GLFW_FOCUSED) == GLFW_TRUE;
}

// ====================================================================================================================
void Window::requestAttention()
{
	glfwRequestWindowAttention(THIS_IMPL->handle_);
}

// ====================================================================================================================
void Window::minimize()
{
	glfwIconifyWindow(THIS_IMPL->handle_);
}

// ====================================================================================================================
void Window::restore()
{
	glfwRestoreWindow(THIS_IMPL->handle_);
}

// ====================================================================================================================
void Window::maximize()
{
	glfwMaximizeWindow(THIS_IMPL->handle_);
}

// ====================================================================================================================
bool Window::isMinimized() const
{
	return glfwGetWindowAttrib(THIS_IMPL->handle_, GLFW_ICONIFIED) == GLFW_TRUE;
}

// ====================================================================================================================
bool Window::isMaximized() const
{
	return glfwGetWindowAttrib(THIS_IMPL->handle_, GLFW_MAXIMIZED) == GLFW_TRUE;
}

// ====================================================================================================================
bool Window::vsync() const
{
	return THIS_IMPL->swapchain_->vsync();
}

// ====================================================================================================================
void Window::vsync(bool vsync)
{
	THIS_IMPL->swapchain_->vsync(vsync);
}

// ====================================================================================================================
Keyboard* Window::keyboard() const
{
	return THIS_IMPL->keyboard_.get();
}

// ====================================================================================================================
Mouse* Window::mouse() const
{
	return THIS_IMPL->mouse_.get();
}

// ====================================================================================================================
Renderer* Window::renderer() const
{
	return THIS_IMPL->renderer_.get();
}

} // namespace vega
