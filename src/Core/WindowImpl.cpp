/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./WindowImpl.hpp"
#include "../Render/Swapchain.hpp"


namespace vega
{

// ====================================================================================================================
static uint32 WindowCounter_{ 0 };

// ====================================================================================================================
WindowImpl::WindowImpl(const String& title, const Vec2ui& size, const SPtr<RenderLayout>& layout)
	: Window()
	, handle_{ GLFW::OpenWindow(title, size.x, size.y) }
	, swapchain_{ }
	, renderer_{ }
	, rendererSize_{ std::nullopt }
	, index_{ WindowCounter_++ }
	, keyboard_{ }
	, mouse_{ }
	, props_{ }
	, saved_{ { }, Monitor::Invalid }
{
	// Check creation
	if (!handle_) {
		int errcode; String errmsg;
		GLFW::GetError(&errcode, &errmsg);
		throw std::runtime_error(to_string("Failed to create window (code %d): %s", errcode, errmsg.c_str()));
	}
	glfwSetWindowUserPointer(handle_, this);

	// Set initial position
	auto pos = (Monitor::Primary().size() / 2) - (size / 2);
	glfwSetWindowPos(handle_, int(pos.x), int(pos.y));

	// Set initial properties
	props_.title = title;
	props_.resizable = true;
	props_.decorated = true;
	props_.floating = false;
	props_.mode = WindowMode::Window;
	glfwSetWindowAttrib(handle_, GLFW_RESIZABLE, GLFW_TRUE);
	glfwSetWindowAttrib(handle_, GLFW_DECORATED, GLFW_TRUE);
	glfwSetWindowAttrib(handle_, GLFW_FLOATING, GLFW_FALSE);

	// Create the swapchain
	swapchain_ = std::make_unique<Swapchain>(this);

	// Create the renderer
	const auto& target0 = get_impl(*layout).targets()[0];
	if (!target0.format.isColor() || !target0.flags.preserve) {
		throw std::runtime_error("RenderLayouts used in Windows must have a preserved color target for target 0");
	}
	renderer_ = std::make_unique<RendererImpl>(layout, size, MSAA::X1, true);

	// Create input objects
	keyboard_.reset(new Keyboard(this));
	mouse_.reset(new Mouse(this));
}

// ====================================================================================================================
WindowImpl::~WindowImpl()
{
	if (!handle_) {
		return;
	}

	keyboard_.reset();
	mouse_.reset();

	renderer_.reset();
	swapchain_.reset();

	glfwHideWindow(handle_);
	glfwDestroyWindow(handle_);
}

// ====================================================================================================================
void WindowImpl::beginFrame()
{
	mouse_->nextFrame();
	keyboard_->nextFrame();
}

// ====================================================================================================================
void WindowImpl::beginRender()
{
	renderer_->beginImpl();
}

// ====================================================================================================================
void WindowImpl::endFrame()
{
	renderer_->endImpl();
	swapchain_->present(*renderer_->getRenderTarget(0));
}

// ====================================================================================================================
void WindowImpl::onSwapchainRebuild(const Vec2ui& newSize)
{
	if (!renderer_) {
		return; // This will happen in the ctor
	}

	if (!rendererSize_.has_value()) { // Only update renderer if its not locked to a size
		renderer_->setSurfaceParams(newSize, std::nullopt);
	}
}

} // namespace vega
