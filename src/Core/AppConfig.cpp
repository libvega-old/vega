/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Core/AppConfig.hpp>


namespace vega
{

// ====================================================================================================================
String ApplicationInfo::AppName_{ "VegaApp" };
Version ApplicationInfo::AppVersion_{ };

// ====================================================================================================================
ApplicationInfo::ApplicationInfo(const String& appName, Version appVersion)
{
	AppName_ = appName;
	AppVersion_ = appVersion;
}


// ====================================================================================================================
// ====================================================================================================================
AppConfig::AppConfig()
	: windowSize_{ 1280, 720 }
	, gDevice_{ nullptr }
	, aDevice_{ nullptr }
	, assetsPath_{ "./Data" }
{

}

// ====================================================================================================================
AppConfig::~AppConfig()
{

}

} // namespace vega
