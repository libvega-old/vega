/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Core/AppThread.hpp>
#include "../Graphics/GraphicsEngineImpl.hpp"

#include <chrono>

#if defined(VEGA_WIN32)
#	include <Windows.h>
#	include <processthreadsapi.h>
#elif defined(VEGA_POSIX)
#	include <pthread.h>
#endif // defined(VEGA_WIN32)


namespace vega
{

// ====================================================================================================================
uint32 AppThread::ThreadIndex_{ 0 };
String AppThread::ExceptionWhat_{ };
String AppThread::ExceptionThreadName_{ };

// ====================================================================================================================
AppThread::AppThread()
	: index_{ ThreadIndex_++ }
	, name_{ }
	, nameMutex_{ }
	, thread_{ nullptr }
	, threadId_{ }
	, shouldExit_{ false }
	, ended_{ false }
	, causedTimeout_{ false }
	, signals_{ }
{
	/// MAIN THREAD
	///

	// Set default thread name
	name(to_string("App_Thread_%u", index_));
}

// ====================================================================================================================
AppThread::~AppThread()
{
	/// MAIN THREAD
	///

	if (!causedTimeout_) {
		assert((!thread_ || ended_.load()) && "AppThread should be exited or terminated before being destroyed");
		if (thread_) {
			thread_->join();
		}
		thread_.reset();
	}
	else {
		thread_->detach(); // Normally bad, but this will only happen if the application is actively terminating
	}
}

// ====================================================================================================================
void AppThread::name(const String& name)
{
	/// MAIN OR APP THREAD
	/// 

	std::lock_guard lock{ nameMutex_ };
	name_ = name;

	if (!thread_) {
		return;
	}

#if defined(VEGA_WIN32)
	const HANDLE handle{ static_cast<HANDLE>(thread_->native_handle()) };
	const std::wstring wideName{ name.begin(), name.end() };
	::SetThreadDescription(handle, wideName.c_str());

#elif defined(VEGA_POSIX) && defined(VEGA_GCC)
	static constexpr size_t MAX_THREAD_NAME_LENGTH{ 15 }; // Limit set by POSIX, not supported on Clang
	const pthread_t handle{ static_cast<pthread_t>(thread_->native_handle()) };
	const String realName{ name.substr(0, std::min(name.length(), MAX_THREAD_NAME_LENGTH)) };
	pthread_setname_np(handle, realName.c_str());

#endif // defined(VEGA_WIN32)
}

// ====================================================================================================================
void AppThread::exit()
{
	/// MAIN OR APP THREAD
	///

	if (!thread_) {
		return;
	}

	shouldExit_.store(true);

	// Trigger wait signals to allow quick detection of exit flag (only if not self-exit)
	if (std::this_thread::get_id() != threadId_) {
		signals_.updateStart.notify();
		signals_.renderStart.notify();
	}
}

// ====================================================================================================================
void AppThread::startThread(TimeSpan timeLimit)
{
	/// MAIN THREAD
	///

	assert(!thread_ && "Cannot double-start an application thread");

	thread_ = std::make_unique<std::thread>([this]() {
		/// APP THREAD
		///

		this->threadId_ = std::this_thread::get_id();

		bool registered{ false };
		try {
			GraphicsEngineImpl::Get()->registerThread();
			registered = true;

			this->thread_func();
		}
		catch (const std::exception& ex) {
#if VEGA_DEBUG
			const String errStr{ ex.what() }; // Allows the error string to be examined by debug step-throughs
			throw;
#else
			// Unblock the main thread to allow for terminate reporting
			OnThreadException(ex, name_);
			signals_.updateEnd.notify();
			signals_.renderEnd.notify();
			while (true) {
				// Sleep until the main window kills us
				// On windows, the reporting message box gets killed if this thread exits, for some reason
				std::this_thread::sleep_for(std::chrono::milliseconds(100));
			}
#endif // VEGA_DEBUG
		}

		if (registered) {
			GraphicsEngineImpl::Get()->unregisterThread();
		}
		this->ended_.store(true);
		signals_.exitComplete.notify();
	});

	// Update the name to catch pre-start name sets
	name(name_);

	// Wait for initialization to finish before returning
	if (!signals_.startComplete.wait(timeLimit)) {
		causedTimeout_ = true;
		throw std::runtime_error(to_string("Application thread '%s' caused frame timeout in OnStart()", name_.c_str()));
	}
}


// ====================================================================================================================
void AppThread::startUpdate()
{
	/// MAIN THREAD
	///

	signals_.updateStart.notify();
}

// ====================================================================================================================
void AppThread::waitEndUpdate(TimeSpan timeLimit)
{
	/// MAIN THREAD
	///

	if (!signals_.updateEnd.wait(timeLimit)) {
		causedTimeout_ = true;
		throw std::runtime_error(to_string("Application thread '%s' caused frame timeout in Update()", name_.c_str()));
	}
}

// ====================================================================================================================
void AppThread::startRender()
{
	/// MAIN THREAD
	///

	signals_.renderStart.notify();
}

// ====================================================================================================================
void AppThread::waitEndRender(TimeSpan timeLimit)
{
	/// MAIN THREAD
	///

	if (!signals_.renderEnd.wait(timeLimit)) {
		causedTimeout_ = true;
		throw std::runtime_error(to_string("Application thread '%s' caused frame timeout in Render()", name_.c_str()));
	}
}

// ====================================================================================================================
void AppThread::waitForExit(TimeSpan timeLimit)
{
	/// MAIN THREAD
	///

	// Check if thread was never started
	if (!thread_) {
		return;
	}

	// Wait with timeout
	if (!signals_.exitComplete.wait(timeLimit)) {
		causedTimeout_ = true;
		throw std::runtime_error(to_string("Application thread '%s' caused frame timeout in OnStop()", name_.c_str()));
	}
	thread_->join();
	thread_.reset();
}

// ====================================================================================================================
void AppThread::thread_func()
{
	/// APP THREAD
	///

	// Mark as started
	this->onStart();
	signals_.startComplete.notify();

	// Frame loop
	while (!shouldExit_.load()) {
		// Wait for update
		signals_.updateStart.wait(shouldExit_);
		if (shouldExit_.load()) {
			break;
		}

		// Perform update
		this->update();

		// Signal update complete
		signals_.updateEnd.notify();
		if (shouldExit_.load()) {
			signals_.renderEnd.notify();
			break;
		}

		// Wait for render
		signals_.renderStart.wait(shouldExit_);
		if (shouldExit_.load()) {
			signals_.renderEnd.notify();
			break;
		}

		// Perform render
		this->render();

		// Signal render complete
		signals_.renderEnd.notify();
		if (shouldExit_.load()) {
			break;
		}
	}

	// Perform cleanup
	this->onStop();
}

// ====================================================================================================================
void AppThread::OnThreadException(const std::exception& ex, const String& name)
{
	/// APP THREAD
	///

	ExceptionWhat_ = ex.what();
	ExceptionThreadName_ = name;
}


// ====================================================================================================================
// ====================================================================================================================
AppThread::Signal::Signal()
	: cond_{ }
	, mutex_{ }
	, waiting_{ false }
	, triggered_{ false }
{

}

// ====================================================================================================================
bool AppThread::Signal::notify()
{
	std::lock_guard lock{ mutex_ };
	assert(!triggered_ && "Double-notify on application thread signal");
	triggered_ = true;
	cond_.notify_one();
	return waiting_;
}

// ====================================================================================================================
bool AppThread::Signal::wait(TimeSpan timeout)
{
	/// MAIN THREAD
	///

	std::unique_lock lock{ mutex_ };
	assert(!waiting_ && "Double-wait on application thread signal");
	waiting_ = true;

	// Wait for the signal, thread needs to exit, or a timeout occured
	const std::chrono::microseconds waitTime{ int64(timeout.microseconds()) };
	const bool retVal{ cond_.wait_for(lock, waitTime, [this]() { return this->triggered_; }) };

	waiting_ = false;
	triggered_ = false;
	return retVal;
}

// ====================================================================================================================
void AppThread::Signal::wait(const std::atomic_bool& exitFlag)
{
	/// APP THREAD
	/// 

	std::unique_lock lock{ mutex_ };
	assert(!waiting_ && "Double-wait on application thread signal");
	waiting_ = true;
	cond_.wait(lock, [this, &exitFlag]() { return this->triggered_ || exitFlag.load(); });
	waiting_ = false;
	triggered_ = false;
}

} // namespace vega
