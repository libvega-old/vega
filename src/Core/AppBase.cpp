/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Core/AppBase.hpp>
#include <Vega/Core/Threading.hpp>
#include <Vega/Core/Time.hpp>
//#include <Vega/Core/Logging.hpp> // TODO
#include "./GLFW.hpp"
#include "./WindowImpl.hpp"
#include "../Asset/AssetTypeImpl.hpp"
#include "../Graphics/GraphicsEngineImpl.hpp"
#include "../Audio/AudioEngineImpl.hpp"

#include <iostream>

#if defined(VEGA_WIN32)
#	include <Windows.h>
#endif


namespace vega
{

// ====================================================================================================================
AppBase* AppBase::Instance_{ nullptr };

// ====================================================================================================================
AppBase::AppBase(const AppConfig& config)
	: config_{ config }
	, aEngine_{ }
	, gEngine_{ }
	, assets_{ }
	, mainWindow_{ }
	, state_{ }
	, thread_{ }
{
	Threading::AssertMainThread("cannot construct AppBase instance");

	// Setup instance
	if (Instance_) {
		throw std::runtime_error("Cannot create more than one instance of AppBase");
	}
	Instance_ = this;

	// Initialize Vulkan if not yet done
	Vulkan::Initialize();

	state_.stage = AppStage::Created;
}

// ====================================================================================================================
AppBase::~AppBase()
{
	// Global cleanup of Vulkan
	Vulkan::Terminate();

	Instance_ = nullptr;
}

// ====================================================================================================================
void AppBase::run()
{
	Threading::AssertMainThread("cannot call AppBase::Run()");

	try {
		state_.stage = AppStage::Initialize;

		// Create the audio engine
		{
			const AudioDevice* device{ config_.audioDevice() };
			if (!device) {
				device = AudioDevice::DefaultDevice();
			}
			aEngine_ = std::make_unique<AudioEngineImpl>(*device);
		}

		// Create the graphics engine
		{
			const GraphicsDevice* device{ config_.graphicsDevice() }; 
			if (!device) {
				device = GraphicsDevice::DefaultDevice();
			}
			if (!device) {
				throw std::runtime_error("No graphics devices on the system support requirements for Vega");
			}
			gEngine_ = std::make_unique<GraphicsEngineImpl>(device);
		}

		// Create the main window
		{
			auto layout = createDefaultRenderLayout();
			if (!layout) {
				throw std::runtime_error("A null RenderLayout cannot be used for the main window");
			}
			mainWindow_.reset(new WindowImpl(ApplicationInfo::AppName(), config_.windowSize(), layout));
		}

		// Perform user initialization
		initialize();

		// User asset loading
		state_.stage = AppStage::AssetLoad;
		assets_.reset(AssetManager::OpenFolder(config_.assetsPath()));
		loadAssets();

		// Main loop
		state_.stage = AppStage::InterFrame;
		Time::Initialize();
		beforeStart();
		while (!state_.shouldExit && !mainWindow_->isCloseRequested()) {
			// Frame functions
			doBeginFrame();
			doUpdate();
			doMidFrame();
			if (state_.shouldExit) {
				break; // Break early when exiting to avoid final render
			}
			doRender();
			doEndFrame();
			state_.stage = AppStage::InterFrame;
		}

		// Do termination
		state_.stage = AppStage::Terminate;
		doTerminate();

		// Cleanup app objects
		aEngine_->stopAll();
		mainWindow_.reset();

		// Destroy assets
		assets_.reset();
		for (auto& pair : AssetTypeImpl::AllTypes()) {
			pair.second->unloadDefaultAsset();
		}

		// Cleanup engines
		gEngine_.reset();
		aEngine_.reset();
	}
	catch (const std::exception& ex) {
#if defined(VEGA_DEBUG)
		std::cerr << "UNHANDLED RUNTIME EXCEPTION:  " << ex.what() << std::endl;
		throw;
#else
		onRuntimeException(ex);
		std::terminate();
#endif
	}
}

// ====================================================================================================================
void AppBase::exit()
{
	state_.shouldExit = true;
}

// ====================================================================================================================
SPtr<RenderLayout> AppBase::createDefaultRenderLayout()
{
	return
		(*graphics()->startNewRenderLayout(1, Subpass::Pass0))
		.addTarget(TexelFormats::UNorm4, true, { RenderUse::Color0 })
		.addTarget(TexelFormats::Depth32, false, { RenderUse::Depth })
		.build();
}

// ====================================================================================================================
void AppBase::doBeginFrame()
{
	state_.stage = AppStage::BeginFrame;

	// Frame start functions
	Time::NextFrame();
	get_impl(mainWindow_.get())->beginFrame();
	get_impl(gEngine_.get())->nextFrame();
	GLFW::PollEvents();

	// User function
	beginFrame();

	// Start any new application threads
	{
		std::lock_guard lock{ thread_.threadMutex };
		if (!thread_.newThreads.empty()) {
			for (auto& ptr : thread_.newThreads) {
				ptr->startThread();
				thread_.currentThreads.push_back(std::move(ptr));
			}
			thread_.newThreads.clear();
		}
	}
}

// ====================================================================================================================
void AppBase::doUpdate()
{
	state_.stage = AppStage::Update;

	// Start thread updates and remove old completed threads
	for (uint32 ti = 0; ti < thread_.currentThreads.size(); ++ti) {
		auto& threadPtr = thread_.currentThreads[ti];
		if (threadPtr->ended()) {
			thread_.currentThreads.erase(thread_.currentThreads.begin() + ti); // Destroy thread
			ti -= 1; // Adjust for new indices
		}
		else {
			threadPtr->startUpdate();
		}
	}

	// User update
	update();

	// Wait for thread updates
	for (const auto& threadPtr : thread_.currentThreads) {
		threadPtr->waitEndUpdate();
	}
	if (AppThread::HasThreadException()) {
		throw std::runtime_error(to_string("Application thread error in Update() ('%s') - %s",
			AppThread::ExceptionThreadName_.c_str(), AppThread::ExceptionWhat_.c_str()
		));
	}
}

// ====================================================================================================================
void AppBase::doMidFrame()
{
	state_.stage = AppStage::MidFrame;

	// User mid-frame
	midFrame();
}

// ====================================================================================================================
void AppBase::doRender()
{
	state_.stage = AppStage::Render;

	// Start window renderers
	get_impl(mainWindow_.get())->beginRender();

	// Launch thread renders
	for (const auto& threadPtr : thread_.currentThreads) {
		threadPtr->startRender();
	}

	// User render
	render();

	// Wait for thread render
	for (const auto& threadPtr : thread_.currentThreads) {
		threadPtr->waitEndRender();
	}
	if (AppThread::HasThreadException()) {
		throw std::runtime_error(to_string("Application thread error in Render() ('%s') - %s",
			AppThread::ExceptionThreadName_.c_str(), AppThread::ExceptionWhat_.c_str()
		));
	}
}

// ====================================================================================================================
void AppBase::doEndFrame()
{
	state_.stage = AppStage::EndFrame;

	// User end frame
	endFrame();

	// Ensure no incomplete offscreen renderers
	if (RendererImpl::ActiveOffscreenCount() > 0) {
		throw std::runtime_error("Cannot exit AppBase::endFrame() with active offscreen Renderer instances");
	}

	// End window frames
	get_impl(mainWindow_.get())->endFrame();
}

// ====================================================================================================================
void AppBase::doTerminate()
{
	// Wait idle on graphics engine
	Vulkan::Device().waitIdle();

	// Cleanup the threads
	thread_.newThreads.clear(); // No threading yet in these, so no exiting or waiting required
	for (const auto& threadPtr : thread_.currentThreads) {
		threadPtr->exit();
	}
	for (const auto& threadPtr : thread_.currentThreads) {
		threadPtr->waitForExit();
	}
	thread_.currentThreads.clear();

	// User terminate
	terminate();
}

// ====================================================================================================================
AppThread* AppBase::addApplicationThread(UPtr<AppThread>&& thread)
{
	Threading::AssertMainThread("cannot launch a new AppThread instance from any thread except the main one");
	std::lock_guard lock{ thread_.threadMutex };
	thread_.newThreads.push_back(std::move(thread));
	return thread_.newThreads.rbegin()->get();
}


// ====================================================================================================================
// ====================================================================================================================
// Manages static registration of std::set_terminate() and std::atexit()
static const struct AppBase::_EXIT_HANDLER {
	_EXIT_HANDLER() {
		std::set_terminate(HandleTerminate);
		std::atexit(HandleAtExit);
	}

	static void HandleTerminate()
	{
		// No exception data is available, nothing to handle
		if (!std::current_exception()) {
			std::abort();
		}

		try {
			std::rethrow_exception(std::current_exception());
		}
		catch (const std::exception& ex) {
#if defined(VEGA_WIN32)
			// Show a message box
			const String message{ to_string("Unhandled runtime exception\r\rwhat(): \"%s\"", ex.what()) };
			MessageBox(
				NULL,
				std::wstring{ message.begin(), message.end() }.c_str(),
				L"Vega - Unhandled Exception",
				MB_OK | MB_ICONERROR | MB_TASKMODAL
			);

#else
			// TODO: Have some obvious way to express the exception on non-windows platforms

#endif // defined(VEGA_WIN32)

			std::cerr << "Fatal runtime error caused abort: " << ex.what() << std::endl;
			std::abort();
		}
	}

	static void HandleAtExit()
	{
		GLFW::Terminate();
		//Log::AtExit(); // TODO
	}
} _EXIT_HANDLER{};

} // namespace vega
