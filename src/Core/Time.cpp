/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Core/Time.hpp>


namespace vega
{


namespace impl
{
using Clock = std::conditional<
	std::chrono::high_resolution_clock::is_steady,
	std::chrono::high_resolution_clock,
	std::chrono::steady_clock
>::type;
using ClockPoint = Clock::time_point;

static const ClockPoint StartTime_{ Clock::now() };
}


// ====================================================================================================================
TimeSpan   Time::Delta_{ };
TimeSpan   Time::RealDelta_{ };
TimeSpan   Time::Elapsed_{ };
TimeSpan   Time::LastElapsed_{ };
uint64     Time::FrameCount_{ 0 };
float      Time::Scale_{ 1 };
Opt<float> Time::NewScale_{ std::nullopt };
std::array<float, 10> Time::FpsHistory_{ };
uint32     Time::FpsIndex_{ 0 };
float      Time::Fps_{ 0 };
TimeSpan   Time::LastTime_{ };

// ====================================================================================================================
TimeSpan Time::ElapsedRaw()
{
	const auto now = impl::Clock::now();
	const auto delta = now - impl::StartTime_;
	return { delta };
}

// ====================================================================================================================
float Time::Scale(float newScale)
{
	if (newScale < 0) {
		throw std::out_of_range("Negative time scales not supported");
	}
	NewScale_ = newScale;
	return Scale_;
}

// ====================================================================================================================
void Time::Initialize()
{
	// Initialize values
	Delta_ = { };
	RealDelta_ = { };
	Elapsed_ = { };
	LastElapsed_ = { };
	FrameCount_ = 0;
	Scale_ = 1;
	NewScale_ = std::nullopt;

	// Reset FPS
	FpsHistory_.fill(0);
	FpsIndex_ = 0;
	Fps_ = 0;

	LastTime_ = { impl::Clock::now().time_since_epoch() };
}

// ====================================================================================================================
void Time::NextFrame()
{
	FrameCount_ += 1;

	// Update time scale
	if (NewScale_.has_value()) {
		Scale_ = NewScale_.value();
		NewScale_ = std::nullopt;
	}

	// Update time values
	const TimeSpan now{ impl::Clock::now().time_since_epoch() };
	RealDelta_ = now - LastTime_;
	Delta_ = RealDelta_ * Scale_;
	LastElapsed_ = Elapsed_;
	Elapsed_ += RealDelta_;
	LastTime_ = now;

	// Update fps
	FpsIndex_ = (FpsIndex_ + 1ull) % FpsHistory_.size();
	FpsHistory_[FpsIndex_] = float(1 / RealDelta_.seconds());
	Fps_ = 0;
	for (auto fps : FpsHistory_) {
		Fps_ += fps;
	}
	Fps_ /= std::min(FrameCount_, FpsHistory_.size());
}

} // namespace vega
