/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Math/Bitset.hpp>


namespace vega
{

// ====================================================================================================================
Opt<size_t> Bitset::firstSet() const
{
	size_t index{ 0 };
	const auto end = data_.data() + data_.size();
	for (auto it = data_.data(); it != end; ++it) {
		if (*it == WORD_NONE) {
			index += WORD_SIZE;
			continue;
		}
		for (uint64 copy = ~*it; copy & 0x1; copy >>= 1, ++index) {}
		break;
	}
	return (index < size_) ? index : Opt<size_t>{ };
}

// ====================================================================================================================
Opt<size_t> Bitset::firstClear() const
{
	size_t index{ 0 };
	const auto end = data_.data() + data_.size();
	for (auto it = data_.data(); it != end; ++it) {
		if (~*it == WORD_NONE) {
			index += WORD_SIZE;
			continue;
		}
		for (uint64 copy = *it; copy & 0x1; copy >>= 1, ++index) {}
		break;
	}
	return (index < size_) ? index : Opt<size_t>{ };
}

} // namespace vega
