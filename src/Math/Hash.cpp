/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Math/Hash.hpp>


namespace vega
{

// ====================================================================================================================
constexpr uint32 mh3_C1{ 0xCC9E2D51 }, mh3_C2{ 0x1B873593 }, mh3_C3{ 0xE6546B64 };

// ====================================================================================================================
Hash32 Hash32::MurmurHash3(const void* data, uint32 length, uint32 seed)
{
	// Adapted from the MurmurHash3 reference implementation
	
	if (!data || (length == 0)) return { 0 };

	const uint32 nBlocks{ length / 4 };
	uint32 h1{ seed };

	// Hash body
	const uint32* const blocksPtr{ (const uint32*)data };
	for (uint32 bi = 0; bi < nBlocks; ++bi) {
		uint32 k1{ blocksPtr[bi] };
		k1 *= mh3_C1;
		k1 = math::rot32(k1, 15);
		k1 *= mh3_C2;

		h1 ^= k1;
		h1 = math::rot32(h1, 13);
		h1 = (h1 * 5) + mh3_C3;
	}

	// Hash tail
	if (length & 3) {
		const uint8* const tailPtr{ (const uint8*)data + (nBlocks * 4ull) };
		uint32 k1{ 0 };

		switch (length & 3)
		{
		case 3: k1 ^= (tailPtr[2] << 16); [[fallthrough]];
		case 2: k1 ^= (tailPtr[1] << 8); [[fallthrough]];
		case 1: k1 ^= tailPtr[0];
		}

		k1 *= mh3_C1;
		k1 = math::rot32(k1, 15);
		k1 *= mh3_C2;
		h1 ^= k1;
	}

	return { math::avalanche32(h1 ^ length) };
}


// ====================================================================================================================
// ====================================================================================================================
constexpr uint64 xxh_C1{ 0x9E3779B185EBCA87ull }, xxh_C2{ 0xC2B2AE3D27D4EB4Full }, xxh_C3{ 0x165667B19E3779F9ull },
	xxh_C4{ 0x85EBCA77C2B2AE63ull }, xxh_C5{ 0x27D4EB2F165667C5ull };

// ====================================================================================================================
inline constexpr uint64 xxh64_single(uint64 prev, uint64 input) {
	return math::rot64(prev + input * xxh_C2, 31) * xxh_C1;
}

// ====================================================================================================================
inline constexpr void xxh64_block(const void* data, uint64& state0, uint64& state1, uint64& state2, uint64& state3) {
	const uint64* const blocks{ (const uint64*)data };
	state0 = xxh64_single(state0, blocks[0]);
	state1 = xxh64_single(state1, blocks[1]);
	state2 = xxh64_single(state2, blocks[2]);
	state3 = xxh64_single(state3, blocks[3]);
}

// ====================================================================================================================
Hash64 Hash64::XXHash64(const void* data, uint32 length, uint32 seed)
{
	// Adapted from simple implementation at https://github.com/stbrumme/xxhash/blob/master/xxhash64.h
	constexpr uint64 BLOCK_SIZE{ sizeof(uint64) * 4 };

	if (!data || (length == 0)) return { 0 };

	const uint8* curr{ (const uint8*)data };
	const uint8* const end{ curr + length };

	// Skip block processing if possible
	uint64 hash{ seed + xxh_C5 };
	if (length >= BLOCK_SIZE) {
		// Hash body
		const uint8* const endBlock{ end - (length % BLOCK_SIZE) };
		uint64 state0{ seed + xxh_C1 + xxh_C2 }, state1{ seed + xxh_C2 }, state2{ seed }, state3{ seed - xxh_C1 };
		while (curr < endBlock) {
			xxh64_block(curr, state0, state1, state2, state3);
			curr += BLOCK_SIZE;
		}
		
		// Fold state
		hash = math::rot64(state0, 1) + math::rot64(state1, 7) + math::rot64(state2, 12) + math::rot64(state3, 18);
		hash ^= ((xxh64_single(0, state0) * xxh_C1) + xxh_C4);
		hash ^= ((xxh64_single(0, state1) * xxh_C1) + xxh_C4);
		hash ^= ((xxh64_single(0, state2) * xxh_C1) + xxh_C4);
		hash ^= ((xxh64_single(0, state3) * xxh_C1) + xxh_C4);
	}
	hash += length;

	// Process remaining 8 byte steps
	for (; (curr + 8) <= end; curr += 8) {
		hash = (math::rot64(hash ^ xxh64_single(0, *(const uint64*)curr), 27) * xxh_C1) + xxh_C4;
	}

	// Process last possible 4-byte step
	if ((curr + 4) <= end) {
		hash = (math::rot64(hash ^ (*(const uint32*)curr * xxh_C1), 23) * xxh_C2) + xxh_C3;
		curr += 4;
	}

	// Process last [1, 3] bytes, one at a time
	while (curr != end) {
		hash = math::rot64(hash ^ (*curr++ * xxh_C5), 11) * xxh_C1;
	}

	return { math::avalanche64(hash) };
}

} // namespace vega
