/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Math/Quat.hpp>


namespace vega
{

// ====================================================================================================================
const Quat Quat::Identity{ 0, 0, 0, 1 };

// ====================================================================================================================
Quat Quat::FromRotationMatrix(const Mat4f& m)
{
	const float scale{ m.m00 + m.m11 + m.m22 };
	if (scale > 0) {
		const float sqrt{ std::sqrt(scale + 1) }, half{ 0.5f / sqrt };
		return { (m.m12 - m.m21) * half, (m.m20 - m.m02) * half, (m.m01 - m.m10) * half, sqrt / 2 };
	}
	else if ((m.m00 >= m.m11) && (m.m00 >= m.m22)) {
		const float sqrt{ std::sqrt(1 + m.m00 - m.m11 - m.m22) }, half{ 0.5f / sqrt };
		return { sqrt / 2, (m.m01 + m.m10) * half, (m.m02 + m.m20) * half, (m.m12 - m.m21) * half };
	}
	else if (m.m11 > m.m22) {
		const float sqrt{ std::sqrt(1 + m.m11 - m.m00 - m.m22) }, half{ 0.5f / sqrt };
		return { (m.m01 + m.m10) * half, sqrt / 2, (m.m12 + m.m21) * half, (m.m20 - m.m02) * half };
	}
	else {
		const float sqrt{ std::sqrt(1 + m.m22 - m.m00 - m.m11) }, half{ 0.5f / sqrt };
		return { (m.m20 + m.m02) * half, (m.m21 + m.m12) * half, sqrt / 2, (m.m01 - m.m10) * half };
	}
}

} // namespace vega
