/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Math/Frustum.hpp>


namespace vega
{

// ====================================================================================================================
Frustum::Frustum(const Mat4f& viewProj)
	: matrix_{ viewProj }
	, planes_{ }
	, corners_{ }
{
	rebuild();
}

// ====================================================================================================================
void Frustum::matrix(const Mat4f& viewProj)
{
	matrix_ = viewProj;
	rebuild();
}

// ====================================================================================================================
void Frustum::rebuild()
{
	static const auto calcCorner = [](const Plane3& p1, const Plane3& p2, const Plane3& p3, Vec3f* corner) {
		const Vec3f c12{ p1.normal.cross(p2.normal) }, c23{ p2.normal.cross(p3.normal) },
			c31{ p3.normal.cross(p1.normal) };
		const Vec3f v1{ -p1.d * c23 }, v2{ -p2.d * c31 }, v3{ -p3.d * c12 };
		const float scale{ 1.f / p1.normal.dot(c23) };
		*corner = (v1 + v2 + v3) * scale;
	};

	// Calc the planes
	const auto& m = matrix_;
	planes_[0] = {         -m.m20,         -m.m21,         -m.m22,          m.m23 };
	planes_[1] = {  m.m20 - m.m30,  m.m21 - m.m31,  m.m22 - m.m32, -m.m23 + m.m33 };
	planes_[2] = {  m.m10 - m.m30,  m.m11 - m.m31,  m.m12 - m.m32, -m.m13 + m.m33 };
	planes_[3] = { -m.m30 - m.m10, -m.m31 - m.m11, -m.m32 - m.m12,  m.m33 + m.m13 };
	planes_[4] = { -m.m30 - m.m00, -m.m31 - m.m01, -m.m32 - m.m02,  m.m33 + m.m03 };
	planes_[5] = {  m.m00 - m.m30,  m.m01 - m.m31,  m.m02 - m.m32, -m.m03 + m.m33 };

	// Calc the corners
	calcCorner(near(), top(),    left(),  &corners_[0]);
	calcCorner(near(), top(),    right(), &corners_[1]);
	calcCorner(near(), bottom(), left(),  &corners_[2]);
	calcCorner(near(), bottom(), right(), &corners_[3]);
	calcCorner(far(),  top(),    left(),  &corners_[4]);
	calcCorner(far(),  top(),    right(), &corners_[5]);
	calcCorner(far(),  bottom(), left(),  &corners_[6]);
	calcCorner(far(),  bottom(), right(), &corners_[7]);
}

} // namespace vega
