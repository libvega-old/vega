/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <Vega/Asset/AssetType.hpp>

#include <typeindex>
#include <unordered_map>


namespace vega
{

// Implementation of AssetType
class AssetTypeImpl final
	: public AssetType
{
	friend class AssetType;

public:
	AssetTypeImpl(const String& name, const std::type_info& type, LoaderFactory lf, AssetFactory af);
	~AssetTypeImpl();

	// Per-Thread asset loader instance
	AssetLoaderBase* loader() const;

	// Asset functions
	inline const AssetBase* defaultAsset() const { return defaultAsset_.get(); }
	void createDefaultAsset();
	void unloadDefaultAsset();

	inline static std::unordered_map<std::type_index, UPtr<AssetTypeImpl>>& AllTypes() { return TypeMap_; }

private:
	const String typeName_;
	const std::type_info& type_;
	const LoaderFactory loaderFactory_;
	const AssetFactory assetFactory_;
	UPtr<AssetBase> defaultAsset_;

	static std::unordered_map<std::type_index, UPtr<AssetTypeImpl>> TypeMap_;

	VEGA_NO_COPY(AssetTypeImpl)
	VEGA_NO_MOVE(AssetTypeImpl)
}; // class AssetTypeImpl


DECLARE_IMPL_GETTER(AssetType)

} // namespace vega
