/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Asset/AssetManager.hpp>
#include "./AssetTypeImpl.hpp"

#include <filesystem>
#include <fstream>
namespace fs = std::filesystem;


namespace vega
{

// ====================================================================================================================
AssetManager::AssetManager(const String& path)
	: rootPath_{ path }
	, assets_{ }
	, assetMutex_{ }
	, unloading_{ false }
{

}

// ====================================================================================================================
AssetManager::~AssetManager()
{
	// Destroy all assets
	unloadAll();
}

// ====================================================================================================================
bool AssetManager::stopManaging(const AssetBase& asset)
{
	// Check manager pointer
	if (asset.control_->manager != this) {
		return false;
	}
	asset.control_->manager = nullptr;
	const auto key = asset.control_->key;
	asset.control_->key = 0;

	// If unloading, can't change the asset map
	if (unloading_) {
		return true;
	}

	// Remove from the name map
	std::lock_guard lock{ assetMutex_ };
	const auto it = assets_.find(Hash64{ key });
	if ((it != assets_.end()) && (it->second.get() == &asset)) {
		it->second.release(); // Release ownership, do not delete
		assets_.erase(it);
		return true;
	}
	else {
		return false;
	}
}

// ====================================================================================================================
void AssetManager::unloadAll()
{
	std::lock_guard lock{ assetMutex_ };
	unloading_ = true;

	// Delete all assets (UPtr<> takes care of delete)
	assets_.clear();

	unloading_ = false;
}

// ====================================================================================================================
AssetBase* AssetManager::getOrLoadAsset(const std::type_info& assetType, const AssetKey& key)
{
	namespace fs = std::filesystem;

	// Check the cache first
	if (const auto cached = getTypedAssetFromCache(assetType, key); cached) {
		return cached;
	}

	// Perform loader lookup
	const auto typeObj = get_impl(AssetType::GetType(assetType));
	if (!typeObj) {
		throw std::runtime_error("Attempted to load unregistered asset type");
	}
	if (!typeObj->hasLoader()) {
		throw std::runtime_error("Attempted to load asset type without registered loader type");
	}
	const auto loader = typeObj->loader();

	// Create the asset stream
	const auto fullPath = fs::path{ rootPath_ } / fs::path{ key.name() };
	std::ifstream stream{ fullPath.string() };
	if (!stream) {
		throw std::runtime_error(to_string("Failed to open asset file '%s'", key.name().c_str()));
	}

	// Execute the loader
	const AssetLoadInfo loadInfo{ assetType, key.name(), fullPath.string() };
	auto asset = loader->loadAsset(loadInfo, stream);
	if (!asset) {
		throw std::runtime_error(to_string("Loader returned null object for asset '%s'", key.name().c_str()));
	}
	asset->control_->manager = this;
	asset->control_->key = key.value();

	// Add to the cache and return
	std::lock_guard lock{ assetMutex_ };
	return assets_.emplace(key.hash_, std::move(asset)).first->second.get();
}

// ====================================================================================================================
AssetBase* AssetManager::getTypedAssetFromCache(const std::type_info& assetType, const AssetKey& key) const
{
	std::lock_guard lock{ assetMutex_ };
	const auto cit = assets_.find(key.hash_);
	if (cit != assets_.end()) {
		if (cit->second->assetType() != assetType) {
			throw std::runtime_error(to_string("Type mismatch for cached asset '%s'", key.name().c_str()));
		}
		return cit->second.get();
	}
	return nullptr;
}

// ====================================================================================================================
AssetManager* AssetManager::OpenFolder(const String& path)
{
	namespace fs = std::filesystem;

	// Check path
	std::error_code ecode{};
	fs::path folderPath{ path };
	if (fs::exists(folderPath) && (!fs::is_directory(folderPath, ecode) || ecode)) {
		throw std::runtime_error(to_string("Path does not point to a extant directory: '%s'", path.c_str()));
	}
	folderPath = fs::absolute(folderPath);

	// Create and return
	return new AssetManager(folderPath.string());
}

} // namespace vega
