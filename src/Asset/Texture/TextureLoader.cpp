/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./TextureLoader.hpp"
#include "../../Graphics/Image/TextureImpl.hpp"
#include "../../Graphics/GraphicsEngineImpl.hpp"
#include <Vega/Asset/AssetType.hpp>

#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#define STBI_NO_PSD // Disable all loaders except for JPEG, PNG, and BMP (for now)
#define STBI_NO_TGA
#define STBI_NO_GIF
#define STBI_NO_HDR
#define STBI_NO_PIC
#define STBI_NO_PNM
#include <stb/stb_image.h>


namespace vega
{

// ====================================================================================================================
///// TEXTURE ASSET TYPE DECLARATION /////
static const AssetType::Declaration<Texture, TextureLoader> TextureLoaderDecl_{
	"Texture",
	[]() -> UPtr<TextureLoader> { return std::make_unique<TextureLoader>(); },
	{ nullptr } // TODO: Make default texture
};

// ====================================================================================================================
TextureLoader::TextureLoader()
{

}

// ====================================================================================================================
TextureLoader::~TextureLoader()
{

}

// ====================================================================================================================
UPtr<AssetBase> TextureLoader::loadAsset(const AssetLoadInfo& assetInfo, std::istream& assetStream)
{
	// Load the file info
	int rawx, rawy, rawc;
	if (!stbi_info(assetInfo.fullPath().c_str(), &rawx, &rawy, &rawc)) {
		throw std::runtime_error(to_string("Failed to load image file info: %s", stbi_failure_reason()));
	}

	// Validate file
	const auto maxSize = GraphicsEngine::Get()->properties()->maxImageSize2D;
	if (uint32(rawx) > maxSize) {
		throw std::runtime_error(
			to_string("Image file is larger than supported limit (%u > %u)", uint32(rawx), maxSize));
	}
	if (uint32(rawy) > maxSize) {
		throw std::runtime_error(
			to_string("Image file is larger than supported limit (%u > %u)", uint32(rawy), maxSize));
	}
	if ((rawc < 1) || (rawc > 4)) {
		throw std::runtime_error(to_string("Image file has invalid number of channels (%d)", rawc));
	}

	// Calculate load values
	const int channels = (rawc == STBI_grey) ? STBI_grey : STBI_rgb_alpha;
	const auto format = (rawc == STBI_grey) ? TexelFormats::UNorm : TexelFormats::UNorm4;

	// Load the data
	auto* data = stbi_load(assetInfo.fullPath().c_str(), &rawx, &rawy, &rawc, channels);
	if (!data) {
		throw std::runtime_error(to_string("Failed to load image file data: %s", stbi_failure_reason()));
	}

	// Create and return the texture
	try {
		auto texture = GraphicsEngine::Get()->create2DTexture(format, uint32(rawx), uint32(rawy));
		texture->setData(data, 0, 0, uint32(rawx), uint32(rawy));
		stbi_image_free(data);
		return texture;
	}
	catch (...) {
		stbi_image_free(data);
		throw;
	}
}

} // namespace vega
