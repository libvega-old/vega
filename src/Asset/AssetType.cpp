/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./AssetTypeImpl.hpp"

#include <algorithm>


namespace vega
{

#define THIS_IMPL_TYPE AssetTypeImpl

// ====================================================================================================================
AssetType::AssetType()
{

}

// ====================================================================================================================
AssetType::~AssetType()
{

}

// ====================================================================================================================
const String& AssetType::typeName() const
{
	return THIS_IMPL->typeName_;
}

// ====================================================================================================================
bool AssetType::hasLoader() const
{
	return !!THIS_IMPL->loaderFactory_;
}

// ====================================================================================================================
bool AssetType::hasDefaultAsset() const
{
	return !!THIS_IMPL->assetFactory_;
}

// ====================================================================================================================
const AssetType* AssetType::GetType(const String& typeName)
{
	const auto& types = AssetTypeImpl::TypeMap_;
	const auto it = std::find_if(types.begin(), types.end(), 
		[&typeName](const decltype(AssetTypeImpl::TypeMap_)::value_type& pair) 
		{ return pair.second->typeName() == typeName; });
	return (it != types.end()) ? it->second.get() : nullptr;
}

// ====================================================================================================================
const AssetType* AssetType::GetType(const std::type_info& typeInfo)
{
	const auto& types = AssetTypeImpl::TypeMap_;
	const auto it = types.find(std::type_index{ typeInfo });
	return (it != types.end()) ? it->second.get() : nullptr;
}

// ====================================================================================================================
void AssetType::RegisterType(const String& name, const std::type_info& type, LoaderFactory lFactory, 
	AssetFactory aFactory)
{
	AssetTypeImpl::TypeMap_.emplace( 
		std::type_index{ type }, 
		std::move(std::make_unique<AssetTypeImpl>(name, type, lFactory, aFactory))
	);
}

} // namespace vega
