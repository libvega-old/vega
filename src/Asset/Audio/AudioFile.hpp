/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include <Vega/Audio/AudioFormat.hpp>


namespace vega
{

// Format-agnostic loader/decoder for audio files
class AudioFile final
{
public:
	enum class AudioType { UNKNOWN = 0, WAV, OGG, FLAC };

	AudioFile(const String& fileName);
	~AudioFile();

	// File Info
	inline uint64 frameCount() const { return info_.frames; }
	inline uint32 sampleRate() const { return info_.rate; }
	inline uint32 channelCount() const { return info_.channels; }
	inline uint32 sampleSize() const { return info_.sampleSize; }
	Opt<AudioFormat> nativeFormat() const;

	// Stream Info
	inline uint64 frameOffset() const { return stream_.offset; }
	inline uint64 remainingFrames() const { return info_.frames - stream_.offset; }

	// Streaming Functions
	uint64 readFrames(uint64 frameCount, AudioFormat format, void* data);
	void resetStream();

private:
	const String fileName_;
	const AudioType type_;
	void* context_;
	struct {
		uint64 frames;
		uint32 rate;
		uint32 channels;
		uint32 sampleSize;
	} info_;
	struct {
		uint64 offset;
	} stream_;

	VEGA_NO_COPY(AudioFile)
	VEGA_NO_MOVE(AudioFile)
}; // class AudioFile

} // namespace vega
