/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./SoundLoader.hpp"
#include "../../Audio/AudioEngineImpl.hpp"
#include "../../Audio/SoundImpl.hpp"
#include "./AudioFile.hpp"
#include <Vega/Asset/AssetType.hpp>


namespace vega
{

// ====================================================================================================================
///// SHADER ASSET TYPE DECLARATION /////
static const AssetType::Declaration<Sound, SoundLoader> ShaderLoaderDecl_{
	"Sound",
	[]() -> UPtr<SoundLoader> { return std::make_unique<SoundLoader>(); }, 
	{ nullptr }
};

// ====================================================================================================================
SoundLoader::SoundLoader()
{

}

// ====================================================================================================================
SoundLoader::~SoundLoader()
{

}

// ====================================================================================================================
UPtr<AssetBase> SoundLoader::loadAsset(const AssetLoadInfo& assetInfo, std::istream& assetStream)
{
	// Open and validate audio file
	AudioFile file{ assetInfo.fullPath() }; // Won't work if we do non-file assets
	if (file.channelCount() > 2) {
		throw std::runtime_error("Cannot load audio files with >2 channels");
	}
	const auto format = file.nativeFormat();
	if (!format.has_value()) {
		throw std::runtime_error("Unsupported audio sample format");
	}

	// Read in the audio data
	std::vector<char> data{};
	data.reserve(file.frameCount() * file.channelCount() * file.sampleSize());
	if (file.readFrames(file.frameCount(), format.value(), data.data()) != file.frameCount()) {
		throw std::runtime_error("Failed to fully read audio file");
	}

	// Create buffer and load into sound
	return AudioEngine::Get()->createSound(format.value(), file.sampleRate(), uint32(file.frameCount()), data.data());
}

} // namespace vega
