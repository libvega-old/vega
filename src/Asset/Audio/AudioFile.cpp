/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./AudioFile.hpp"

#define DR_WAV_IMPLEMENTATION
#include <dr_libs/dr_wav.h>
#define DR_FLAC_IMPLEMENTATION
#include <dr_libs/dr_flac.h>
#define STB_VORBIS_HEADER_ONLY
#include "./stb_vorbis.c"

#include <filesystem>
#include <algorithm>
namespace fs = std::filesystem;

#define WAV_CTX (reinterpret_cast<drwav*>(context_))
#define OGG_CTX (reinterpret_cast<stb_vorbis*>(context_))
#define FLAC_CTX (reinterpret_cast<drflac*>(context_))


namespace vega
{

// ====================================================================================================================
static AudioFile::AudioType DetectType(const String& fileName)
{
	namespace fs = std::filesystem;

	// Get the extension
	const fs::path path{ fileName };
	if (!path.has_extension()) {
		return AudioFile::AudioType::UNKNOWN;
	}
	const auto ext = path.extension();

	// Standardized extension
	auto extStr = ext.string().substr(1); // without leading '.'
	std::transform(extStr.begin(), extStr.end(), extStr.begin(), [](unsigned char c) { return ::tolower(c); });

	// Switch on extension
	if ((extStr == "wav") || (extStr == "wave")) {
		return AudioFile::AudioType::WAV;
	}
	else if (extStr == "ogg") {
		return AudioFile::AudioType::OGG;
	}
	else if (extStr == "flac") {
		return AudioFile::AudioType::FLAC;
	}
	else {
		return AudioFile::AudioType::UNKNOWN;
	}
}

// ====================================================================================================================
AudioFile::AudioFile(const String& fileName)
	: fileName_{ fileName }
	, type_{ DetectType(fileName) }
	, context_{ nullptr }
	, info_{ 0, 0, 0 }
	, stream_{ 0 }
{
	// Create the context based on file type
	switch (type_)
	{
	case AudioType::WAV: {
		context_ = new drwav;
		if (!drwav_init_file(WAV_CTX, fileName.c_str(), nullptr)) {
			delete (WAV_CTX);
			throw std::runtime_error("Failed to open WAV file");
		}
	} break;
	case AudioType::OGG: {
		int err;
		context_ = stb_vorbis_open_filename(fileName.c_str(), &err, nullptr);
		if (!context_) {
			throw std::runtime_error("Failed to open OGG/Vorbis file");
		}
	} break;
	case AudioType::FLAC: {
		context_ = drflac_open_file(fileName.c_str(), nullptr);
		if (!context_) {
			throw std::runtime_error("Failed to open FLAC file");
		}
	} break;
	case AudioType::UNKNOWN:
	default:
		throw std::runtime_error("Unsupported audio file type");
	}

	// Get the file info
	switch (type_)
	{
	case AudioType::WAV: {
		info_.frames = WAV_CTX->totalPCMFrameCount;
		info_.rate = WAV_CTX->sampleRate;
		info_.channels = WAV_CTX->channels;
		info_.sampleSize = WAV_CTX->bitsPerSample / 8;
	} break;
	case AudioType::OGG: {
		const auto info = stb_vorbis_get_info(OGG_CTX);
		const auto length = stb_vorbis_stream_length_in_samples(OGG_CTX);
		info_.frames = length;
		info_.rate = info.sample_rate;
		info_.channels = info.channels;
		info_.sampleSize = 2; // Vorbis does not have a concept of BPS, so we always use 16 / 8 == 2
	} break;
	case AudioType::FLAC: {
		info_.frames = FLAC_CTX->totalPCMFrameCount;
		info_.rate = FLAC_CTX->sampleRate;
		info_.channels = FLAC_CTX->channels;
		info_.sampleSize = FLAC_CTX->bitsPerSample / 8;
	} break;
	}
}

// ====================================================================================================================
AudioFile::~AudioFile()
{
	if (context_) {
		switch (type_)
		{
		case AudioType::WAV: {
			drwav_uninit(WAV_CTX);
			delete (WAV_CTX);
		} break;
		case AudioType::OGG: stb_vorbis_close(OGG_CTX); break;
		case AudioType::FLAC: drflac_close(FLAC_CTX); break;
		}
	}
}

// ====================================================================================================================
Opt<AudioFormat> AudioFile::nativeFormat() const
{
	if (info_.channels > 2) {
		return std::nullopt;
	}

	switch (info_.sampleSize)
	{
		// For case 1: none of the loaders can handle 8-bit data natively so we just promote to 16-bit
	case 1: return (info_.channels == 1) ? AudioFormat::Mono16 : AudioFormat::Stereo16;
	case 2: return (info_.channels == 1) ? AudioFormat::Mono16 : AudioFormat::Stereo16;
	case 4: return (info_.channels == 1) ? AudioFormat::MonoFloat : AudioFormat::StereoFloat;
	default: return std::nullopt;
	}
}

// ====================================================================================================================
uint64 AudioFile::readFrames(uint64 frameCount, AudioFormat format, void* data)
{
	// Validate
	if (!context_) {
		return 0;
	}
	if (remainingFrames() == 0) {
		return 0;
	}

	// Calculate actual read size
	const auto fCount = std::min(frameCount, remainingFrames());
	const auto sCount = fCount * info_.channels;

	// Dispatch read command
	uint64 actual{ 0 };
	switch (type_)
	{
	case AudioType::WAV: {
		if (format.sampleSize() == 1) {
			std::vector<int16> tmpData{};
			tmpData.reserve(sCount);
			actual = drwav_read_pcm_frames_s16(WAV_CTX, fCount, tmpData.data());
			for (uint64 ii = 0; ii < sCount; ++ii) {
				((int8*)data)[ii] = int8(tmpData.data()[ii] / 256); // Slow, but in general 8-bit formats are rare
			}
		}
		else if (format.sampleSize() == 2) {
			actual = drwav_read_pcm_frames_s16(WAV_CTX, fCount, (int16*)data);
		}
		else if (format.sampleSize() == 4) {
			actual = drwav_read_pcm_frames_f32(WAV_CTX, fCount, (float*)data);
		}
	} break;
	case AudioType::OGG: {
		if (format.sampleSize() == 1) {
			std::vector<int16> tmpData{};
			tmpData.reserve(sCount);
			actual = uint64(
				stb_vorbis_get_samples_short_interleaved(OGG_CTX, int(info_.channels), tmpData.data(), int(sCount))
			);
			for (uint64 ii = 0; ii < sCount; ++ii) {
				((int8*)data)[ii] = int8(tmpData.data()[ii] / 256); // Slow, but in general 8-bit formats are rare
			}
		}
		else if (format.sampleSize() == 2) {
			actual = uint64(
				stb_vorbis_get_samples_short_interleaved(OGG_CTX, int(info_.channels), (int16*)data, int(sCount))
			);
		}
		else if (format.sampleSize() == 4) {
			actual = uint64(
				stb_vorbis_get_samples_float_interleaved(OGG_CTX, int(info_.channels), (float*)data, int(sCount))
			);
		}
	} break;
	case AudioType::FLAC: {
		if (format.sampleSize() == 1) {
			std::vector<int16> tmpData{};
			tmpData.reserve(sCount);
			actual = drflac_read_pcm_frames_s16(FLAC_CTX, fCount, tmpData.data());
			for (uint64 ii = 0; ii < sCount; ++ii) {
				((int8*)data)[ii] = int8(tmpData.data()[ii] / 256); // Slow, but in general 8-bit formats are rare
			}
		}
		else if (format.sampleSize() == 2) {
			actual = drflac_read_pcm_frames_s16(FLAC_CTX, fCount, (int16*)data);
		}
		else if (format.sampleSize() == 4) {
			actual = drflac_read_pcm_frames_f32(FLAC_CTX, fCount, (float*)data);
		}
	} break;
	}
	
	// Update and report
	stream_.offset += actual;
	return actual;
}

// ====================================================================================================================
void AudioFile::resetStream()
{
	// Validate
	if (!context_) {
		return;
	}

	// Dispatch reset command
	if (type_ == AudioType::WAV) {
		if (!drwav_seek_to_pcm_frame(WAV_CTX, 0)) {
			throw std::runtime_error("Failed to seek WAV stream to start");
		}
	}
	else if (type_ == AudioType::OGG) {
		if (!stb_vorbis_seek_start(OGG_CTX)) {
			throw std::runtime_error("Failed to seek OGG stream to start");
		}
	}
	else if (type_ == AudioType::FLAC) {
		if (!drflac_seek_to_pcm_frame(FLAC_CTX, 0)) {
			throw std::runtime_error("Failed to seek FLAC stream to start");
		}
	}
}

} // namespace vega
