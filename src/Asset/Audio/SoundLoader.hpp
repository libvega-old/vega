/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include <Vega/Asset/AssetLoaderBase.hpp>
#include <Vega/Asset/AssetBase.hpp>


namespace vega
{

// Asset loader type for audio files into Sound
class SoundLoader final
	: public AssetLoaderBase
{
public:
	SoundLoader();
	~SoundLoader();

	UPtr<AssetBase> loadAsset(
		const AssetLoadInfo& assetInfo,
		std::istream& assetStream) override;

	VEGA_NO_COPY(SoundLoader)
	VEGA_NO_MOVE(SoundLoader)
}; // class SoundLoader

} // namespace vega
