/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../../Common.Impl.hpp"
#include <Vega/Asset/AssetLoaderBase.hpp>


namespace vega
{

// Loader type for VSL shaders
class ShaderLoader final
	: public AssetLoaderBase
{
public:
	ShaderLoader();
	~ShaderLoader();

	UPtr<AssetBase> loadAsset(
		const AssetLoadInfo& assetInfo,
		std::istream& assetStream) override;

	VEGA_NO_COPY(ShaderLoader)
	VEGA_NO_MOVE(ShaderLoader)
}; // class ShaderLoader

} // namespace vega
