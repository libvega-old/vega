/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./ShaderLoader.hpp"
#include <Vega/Asset/AssetType.hpp>
#include "../../Graphics/ShaderImpl.hpp"

#include <VSL/Type.hpp>
#include <VSL/Shader.hpp>


namespace vega
{

// ====================================================================================================================
///// SHADER ASSET TYPE DECLARATION /////
static const AssetType::Declaration<Shader, ShaderLoader> ShaderLoaderDecl_{
	"Shader",
	[]() -> UPtr<ShaderLoader> { return std::make_unique<ShaderLoader>(); },
	{ nullptr }
};

// ====================================================================================================================
static void ConvertInput(const vsl::InputRecord& vsl, ShaderInput& input);
static void ConvertOutput(const vsl::OutputRecord& vsl, ShaderOutput& output);
static void ConvertBinding(const vsl::BindingRecord& vsl, ShaderBinding& binding);
static void ConvertSubpassInput(const vsl::SubpassInputRecord& vsl, ShaderSubpassInput& input);
static void ConvertMember(const vsl::UniformMemberRecord& vsl, const String& name, ShaderUniformMember& member);
static ColorBlend ConvertColorBlend(const vsl::OutputRecord& vsl);
static VertexFormat ConvertNumericType(uint32 baseType, uint32 size, uint32 dim0, uint32 dim1);
static TexelFormat ConvertTexelType(uint32 format);
static TexelFormat ConvertTexelType(uint32 baseType, uint32 size, uint32 dim0);
static BindingType ConvertBindingType(uint32 baseType, uint32 rank);
static vk::Result TryCreateModule(const ShaderImpl::Bytecode& bytecode, vk::ShaderModule* mod);


// ====================================================================================================================
// ====================================================================================================================
ShaderLoader::ShaderLoader()
{

}

// ====================================================================================================================
ShaderLoader::~ShaderLoader()
{

}

// ====================================================================================================================
UPtr<AssetBase> ShaderLoader::loadAsset(const AssetLoadInfo& assetInfo, std::istream& assetStream)
{
	// Load the shader using VSL
	const auto [shader, shaderError] = vsl::Shader::LoadFromFile(assetInfo.fullPath());
	if (!shader) {
		throw std::runtime_error(to_string("Failed to load shader - %s", shaderError.c_str()));
	}

	// Create the reflection info
	ShaderImpl::Reflection refl{};
	refl.stageMask = ShaderStage(shader->Stages());

	// Translate the reflection components
	refl.inputs.resize(shader->Inputs().size());
	for (uint32 ii = 0; ii < shader->Inputs().size(); ++ii) {
		ConvertInput(shader->Inputs()[ii], refl.inputs[ii]);
	}
	refl.outputs.resize(shader->Outputs().size());
	for (uint32 oi = 0; oi < shader->Outputs().size(); ++oi) {
		ConvertOutput(shader->Outputs()[oi], refl.outputs[oi]);
	}
	refl.bindings.resize(shader->Bindings().size());
	for (uint32 bi = 0; bi < shader->Bindings().size(); ++bi) {
		ConvertBinding(shader->Bindings()[bi], refl.bindings[bi]);
	}
	refl.subpassInputs.resize(shader->SubpassInputs().size());
	for (uint32 si = 0; si < shader->SubpassInputs().size(); ++si) {
		ConvertSubpassInput(shader->SubpassInputs()[si], refl.subpassInputs[si]);
	}

	// Fill out additional input information
	refl.vertexMap.fill(UINT8_MAX);
	for (const auto& input : refl.inputs) {
		refl.vertexMask |= input.use();
		refl.vertexMap[input.use().value()] = uint8(input.location());
	}

	// Reflect the uniform information
	if (shader->Uniform().has_value()) {
		const uint32 ustages{ shader->Uniform().value().stages };
		refl.uniform.size = shader->Uniform().value().size;
		refl.uniform.stageMask = ShaderStage(ustages);

		refl.uniform.members.resize(shader->UniformMembers().size());
		for (uint32 ui = 0; ui < shader->UniformMembers().size(); ++ui) {
			const auto& mempair = shader->UniformMembers()[ui];
			ConvertMember(mempair.second, mempair.first, refl.uniform.members[ui]);
		}
	}

	// Create the program
	const auto& bc = shader->Bytecodes();
	ShaderImpl::Modules modules{};
	assert(
		(TryCreateModule(bc.at(vsl::ShaderStages::Vertex), &modules.vert) == vk::Result::eSuccess) &&
		"Bad VSL vertex shader bytecode"
	);
	assert(
		(TryCreateModule(bc.at(vsl::ShaderStages::Fragment), &modules.frag) == vk::Result::eSuccess) &&
		"Bad VSL fragment shader bytecode"
	);

	// Return the loaded shader
	return std::make_unique<ShaderImpl>(std::move(modules), std::move(refl));
}


// ====================================================================================================================
// ====================================================================================================================
static void ConvertInput(const vsl::InputRecord& vsl, ShaderInput& input)
{
	const uint32 useVal{ vsl.semantic };
	input = {
		*(VertexUse*)&useVal, vsl.location, ConvertNumericType(vsl.baseType, 4, vsl.dims[0], vsl.dims[1]), vsl.arraySize
	};
}

// ====================================================================================================================
static void ConvertOutput(const vsl::OutputRecord& vsl, ShaderOutput& output)
{
	output = { ConvertTexelType(vsl.baseType, 4, vsl.dim0), ConvertColorBlend(vsl) };
}

// ====================================================================================================================
static void ConvertBinding(const vsl::BindingRecord& vsl, ShaderBinding& binding)
{
	uint32 stageMask{ vsl.stageMask };
	binding = { vsl.slot, ConvertBindingType(vsl.baseType, vsl.image.rank), *(ShaderStage*)&stageMask };
}

// ====================================================================================================================
static void ConvertSubpassInput(const vsl::SubpassInputRecord& vsl, ShaderSubpassInput& input)
{
	input = { vsl.index, ConvertTexelType(vsl.format) };
}

// ====================================================================================================================
static void ConvertMember(const vsl::UniformMemberRecord& vsl, const String& name, ShaderUniformMember& member)
{
	member = { name, vsl.offset, ConvertNumericType(vsl.baseType, 4, vsl.dims[0], vsl.dims[1]), vsl.arraySize };
}

// ====================================================================================================================
static ColorBlend ConvertColorBlend(const vsl::OutputRecord& vsl)
{
	ColorBlend blend{};
	blend.srcColor(BlendFactor(vsl.srcColorFactor));
	blend.dstColor(BlendFactor(vsl.dstColorFactor));
	blend.srcAlpha(BlendFactor(vsl.srcAlphaFactor));
	blend.dstAlpha(BlendFactor(vsl.dstAlphaFactor));
	blend.colorOp(BlendOp(vsl.colorOp));
	blend.alphaOp(BlendOp(vsl.alphaOp));
	blend.writeMask(ColorChannel::All);
	return blend;
}

// ====================================================================================================================
static VertexFormat ConvertNumericType(uint32 baseType, uint32 size, uint32 dim0, uint32 dim1)
{
	assert((size == 4) && "Invalid numeric type size in shader reflection (!= 4)");

	switch (vsl::BaseType(baseType))
	{
	case vsl::BaseType::Signed: {
		assert((dim0 >= 1) && (dim0 <= 4) && (dim1 == 1) && "Invalid dimensions for signed vertex format");
		return (dim0 == 1) ? VertexFormats::Int : (dim0 == 2) ? VertexFormats::Int2 :
			(dim0 == 3) ? VertexFormats::Int3 : VertexFormats::Int4;
	}
	case vsl::BaseType::Unsigned: {
		assert((dim0 >= 1) && (dim0 <= 4) && (dim1 == 1) && "Invalid dimensions for unsigned vertex format");
		return (dim0 == 1) ? VertexFormats::UInt : (dim0 == 2) ? VertexFormats::UInt2 :
			(dim0 == 3) ? VertexFormats::UInt3 : VertexFormats::UInt4;
	}
	case vsl::BaseType::Float: {
		assert((dim0 >= 1) && (dim0 <= 4) && (dim1 >= 1) && (dim1 <= 4) && "Invalid dimensions for float vertex format");
		switch (dim1)
		{
		case 1: return (dim0 == 1) ? VertexFormats::Float : (dim0 == 2) ? VertexFormats::Float2 :
			(dim0 == 3) ? VertexFormats::Float3 : VertexFormats::Float4;
		case 2: return (dim0 == 2) ? VertexFormats::Float2x2 : (dim0 == 3) ? VertexFormats::Float3x2 :
			VertexFormats::Float4x2;
		case 3: return (dim0 == 2) ? VertexFormats::Float2x3 : (dim0 == 3) ? VertexFormats::Float3x3 :
			VertexFormats::Float4x3;
		case 4: return (dim0 == 2) ? VertexFormats::Float2x4 : (dim0 == 3) ? VertexFormats::Float3x4 :
			VertexFormats::Float4x4;
		}
	}
	}

	assert(false && "Invalid numeric type in shader info");
	return { };
}

// ====================================================================================================================
static TexelFormat ConvertTexelType(uint32 format)
{
	switch (format)
	{
	case vsl::TexelFormat::Int.Value(): return TexelFormats::SInt;
	case vsl::TexelFormat::Int2.Value(): return TexelFormats::SInt2;
	case vsl::TexelFormat::Int4.Value(): return TexelFormats::SInt4;
	case vsl::TexelFormat::UInt.Value(): return TexelFormats::UInt;
	case vsl::TexelFormat::UInt2.Value(): return TexelFormats::UInt2;
	case vsl::TexelFormat::UInt4.Value(): return TexelFormats::UInt4;
	case vsl::TexelFormat::Float.Value(): return TexelFormats::Float;
	case vsl::TexelFormat::Float2.Value(): return TexelFormats::Float2;
	case vsl::TexelFormat::Float4.Value(): return TexelFormats::Float4;
	}

	assert(false && "Invalid texel type in shader info");
	return { };
}

// ====================================================================================================================
static TexelFormat ConvertTexelType(uint32 baseType, uint32 size, uint32 dim0)
{
	assert((size == 4) && "Invalid texel type size in shader reflection (!= 4)");
	assert(((dim0 == 1) || (dim0 == 2) || (dim0 <= 4)) && "Invalid dimensions for signed texel format");

	switch (vsl::BaseType(baseType))
	{
	case vsl::BaseType::Signed: {
		return (dim0 == 1) ? TexelFormats::SInt : (dim0 == 2) ? TexelFormats::SInt2 : TexelFormats::SInt4;
	}
	case vsl::BaseType::Unsigned: {
		return (dim0 == 1) ? TexelFormats::UInt : (dim0 == 2) ? TexelFormats::UInt2 : TexelFormats::UInt4;
	}
	case vsl::BaseType::Float: {
		return (dim0 == 1) ? TexelFormats::Float : (dim0 == 2) ? TexelFormats::Float2 : TexelFormats::Float4;
	}
	}

	assert(false && "Invalid texel type in shader info");
	return { };
}

// ====================================================================================================================
static BindingType ConvertBindingType(uint32 baseType, uint32 rank)
{
	const auto base = vsl::BaseType(baseType);
	if (base == vsl::BaseType::Sampler) {
		switch (rank)
		{
		case vsl::TexelRank::E1D.Value(): return BindingType::Sampler1D;
		case vsl::TexelRank::E2D.Value(): return BindingType::Sampler2D;
		case vsl::TexelRank::E3D.Value(): return BindingType::Sampler3D;
		}
		assert(false && "Invalid sampler type in shader");
	}
	else if (base == vsl::BaseType::Image) {
		assert(false && "Invalid image type in shader (images not supported)");
	}
	else if (base == vsl::BaseType::ROBuffer) {
		return BindingType::ROBuffer;
	}
	else if (base == vsl::BaseType::RWBuffer) {
		assert(false && "Invalid RWBuffer type in shader (RWBuffers not supported)");
	}
	else if (base == vsl::BaseType::ROTexels) {
		return BindingType::ROTexels;
	}
	else if (base == vsl::BaseType::RWTexels) {
		assert(false && "Invalid RWTexels type in shader (RWTexels not supported)");
	}

	assert(false && "Invalid binding type");
	return { };
}

// ====================================================================================================================
static vk::Result TryCreateModule(const ShaderImpl::Bytecode& bytecode, vk::ShaderModule* mod)
{
	vk::ShaderModuleCreateInfo ci{};
	ci.codeSize = size_t(bytecode.size() * sizeof(uint32_t));
	ci.pCode = bytecode.data();
	return Vulkan::Device().createShaderModule(&ci, nullptr, mod);
}

} // namespace vega
