/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Asset/AssetBase.hpp>
#include <Vega/Asset/AssetManager.hpp>
#include "./AssetTypeImpl.hpp"


namespace vega
{

// ====================================================================================================================
AssetBase::AssetBase(const std::type_info& assetType)
	: control_{ std::make_shared<Control>(this, assetType) }
{

}

// ====================================================================================================================
AssetBase::~AssetBase()
{
	if (control_->manager) {
		control_->manager->stopManaging(*this);
	}

	if (const auto defaultAsset = get_impl(AssetType::GetType(control_->type))->defaultAsset(); defaultAsset) {
		control_->asset = const_cast<AssetBase*>(defaultAsset);
		control_->state = AssetState::Defaulted;
	}
	else {
		control_->asset = nullptr;
		control_->state = AssetState::Unloaded;
	}

	control_.reset();
}

} // namespace vega
