/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./AssetTypeImpl.hpp"


namespace vega
{

// ====================================================================================================================
std::unordered_map<std::type_index, UPtr<AssetTypeImpl>> AssetTypeImpl::TypeMap_{ };

// ====================================================================================================================
AssetTypeImpl::AssetTypeImpl(const String& name, const std::type_info& type, LoaderFactory lf, AssetFactory af)
	: AssetType()
	, typeName_{ name }
	, type_{ type }
	, loaderFactory_{ lf }
	, assetFactory_{ af }
	, defaultAsset_{ nullptr }
{

}

// ====================================================================================================================
AssetTypeImpl::~AssetTypeImpl()
{
	// Should be destroyed by now, but try anyway
	defaultAsset_.reset();
}

// ====================================================================================================================
AssetLoaderBase* AssetTypeImpl::loader() const
{
	static thread_local std::unordered_map<std::type_index, UPtr<AssetLoaderBase>> ThreadLoaders_{ };

	if (!hasLoader()) {
		return nullptr;
	}

	// Find the loader
	const auto it = ThreadLoaders_.find(std::type_index{ type_ });
	if (it != ThreadLoaders_.end()) {
		return it->second.get();
	}
	else {
		auto loader = loaderFactory_();
		return ThreadLoaders_.emplace(std::type_index{ type_ }, std::move(loader)).first->second.get();
	}
}

// ====================================================================================================================
void AssetTypeImpl::createDefaultAsset()
{
	if (!hasDefaultAsset()) {
		return;
	}
	defaultAsset_ = assetFactory_();
}

// ====================================================================================================================
void AssetTypeImpl::unloadDefaultAsset()
{
	defaultAsset_.reset();
}

} // namespace vega
