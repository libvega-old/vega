/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <Vega/Audio/AudioEngine.hpp>
#include <Vega/Audio/AudioWorld.hpp>
#include "./OpenAL.hpp"

#include <atomic>
#include <mutex>
#include <stack>
#include <unordered_map>
#include <vector>


namespace vega
{

class SoundInstanceImpl;

// Implementation of AudioEngine
class AudioEngineImpl final
	: public AudioEngine
{
	friend class AudioEngine;

public:
	AudioEngineImpl(const AudioDevice& device);
	~AudioEngineImpl();

	Opt<uint32> reserveSource(SoundInstanceImpl* inst);
	void onSoundDestroyed(SoundInstanceImpl* inst);

private:
	void onSourceStop(uint32 source);

private:
	const AudioDevice* device_;
	struct
	{
		std::vector<uint32> all{ };
		std::stack<uint32> available{ };
		std::mutex mutex{ };
	} sources_;
	struct
	{
		std::unordered_map<uint32, SoundInstanceImpl*> sounds{ };
		std::mutex mutex{ };
		std::atomic_bool stopAll{ };
	} playback_;
	UPtr<AudioWorld> world_;

	VEGA_NO_COPY(AudioEngineImpl)
	VEGA_NO_MOVE(AudioEngineImpl)
}; // class AudioEngineImpl

DECLARE_IMPL_GETTER(AudioEngine)

} // namespace vega
