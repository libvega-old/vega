/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./AudioEmitterImpl.hpp"


namespace vega
{

// ====================================================================================================================
AudioEmitterImpl::AudioEmitterImpl(const SoundImpl& sound)
	: AudioEmitter()
	, sound_{ sound }
	, source_{ }
	, props_{ }
	, pos_{ }
	, vel_{ }
{

}

// ====================================================================================================================
AudioEmitterImpl::~AudioEmitterImpl()
{

}

} // namespace vega
