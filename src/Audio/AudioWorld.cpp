/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Audio/AudioWorld.hpp>
#include <Vega/Math/Mat4.hpp>
#include "./AudioEmitterImpl.hpp"
#include "./OpenAL.hpp"


namespace vega
{

// ====================================================================================================================
AudioWorld::AudioWorld()
{

}

// ====================================================================================================================
AudioWorld::~AudioWorld()
{

}

// ====================================================================================================================
Vec3f AudioWorld::listenerPosition() const
{
	Vec3f pos;
	AL_CHECK(alGetListener3f(AL_POSITION, &pos.x, &pos.y, &pos.z));
	return pos;
}

// ====================================================================================================================
void AudioWorld::listenerPosition(const Vec3f& pos)
{
	AL_CHECK(alListener3f(AL_POSITION, pos.x, pos.y, pos.z));
}

// ====================================================================================================================
Vec3f AudioWorld::listenerVelocity() const
{
	Vec3f vel;
	AL_CHECK(alGetListener3f(AL_VELOCITY, &vel.x, &vel.y, &vel.z));
	return vel;
}

// ====================================================================================================================
void AudioWorld::listenerVelocity(const Vec3f& vel)
{
	AL_CHECK(alListener3f(AL_VELOCITY, vel.x, vel.y, vel.z));
}

// ====================================================================================================================
std::tuple<Vec3f, Vec3f> AudioWorld::listenerOrientation() const
{
	float buffer[6];
	AL_CHECK(alGetListenerfv(AL_ORIENTATION, buffer));
	return { { buffer[0], buffer[1], buffer[2] }, { buffer[3], buffer[4], buffer[5] } };
}

// ====================================================================================================================
void AudioWorld::listenerOrientation(const Vec3f& forward, const Vec3f& up)
{
	const auto normFwd = forward.normalized();
	const auto right = forward.cross(up.normalized());
	const auto trueUp = right.cross(normFwd);

	const float data[6]{ normFwd.x, normFwd.y, normFwd.z, trueUp.x, trueUp.y, trueUp.z };
	AL_CHECK(alListenerfv(AL_ORIENTATION, data));
}

// ====================================================================================================================
void AudioWorld::listenerOrientation(const Vec3f& forward, Rad<float> roll)
{
	const auto world = Mat4f::RotationAxis(forward.normalized(), roll);
	const auto trueFwd = world.forward();
	const auto up = world.up();

	const float data[6]{ trueFwd.x, trueFwd.y, trueFwd.z, up.x, up.y, up.z };
	AL_CHECK(alListenerfv(AL_ORIENTATION, data));
}

// ====================================================================================================================
UPtr<AudioEmitter> AudioWorld::createEmitter(const Sound& sound)
{
	return std::make_unique<AudioEmitterImpl>(get_impl(sound));
}

} // namespace vega
