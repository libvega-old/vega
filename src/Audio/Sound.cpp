/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./SoundImpl.hpp"
#include "./AudioEngineImpl.hpp"
#include "./AudioBuffer.hpp"
#include "./OpenAL.hpp"
#include <Vega/Math/Math.hpp>

#include <algorithm>
#include <cmath>


namespace vega
{

// ====================================================================================================================
static Vec3f _calc_pan_position(float pan)
{
	pan *= 0.5f; // Change [-1, 1] to [-.5, .5]
	return { pan, 0, -std::sqrt(1.0f - pan * pan) }; // Calculate a 60deg arc to emulate panning in relative mode
}


#define THIS_IMPL_TYPE SoundImpl
// ====================================================================================================================
// ====================================================================================================================
Sound::Sound()
	: AssetBase(typeid(Sound))
{

}

// ====================================================================================================================
Sound::~Sound()
{
	
}

// ====================================================================================================================
UPtr<SoundInstance> Sound::createInstance() const
{
	return std::make_unique<SoundInstanceImpl>(THIS_IMPL->buffer_, false);
}

// ====================================================================================================================
bool Sound::play(float volume, float speed) const
{
	auto inst = std::make_unique<SoundInstanceImpl>(THIS_IMPL->buffer_, true);
	inst->volume(volume);
	inst->speed(speed);

	if (inst->play()) {
		inst.release();
		return true;
	}
	else {
		inst.reset();
		return false;
	}
}


#undef THIS_IMPL_TYPE
#define THIS_IMPL_TYPE SoundInstanceImpl
// ====================================================================================================================
// ====================================================================================================================
SoundInstance::SoundInstance()
{

}

// ====================================================================================================================
SoundInstance::~SoundInstance()
{

}

// ====================================================================================================================
AudioState SoundInstance::state() const
{
	if (!THIS_IMPL->source_.has_value()) {
		return AudioState::Stopped;
	}

	// Get the state and translate
	int32 state{ AL_STOPPED };
	AL_CHECK(alGetSourcei(THIS_IMPL->source_.value(), AL_SOURCE_STATE, &state));
	switch (state)
	{
	case AL_PLAYING: return AudioState::Playing;
	case AL_PAUSED: return AudioState::Paused;
	case AL_STOPPED:
	case AL_INITIAL:
	default: return AudioState::Stopped;
	}
}

// ====================================================================================================================
void SoundInstance::volume(float volume)
{
	const auto impl = THIS_IMPL;
	impl->props_.gain = std::clamp(volume, 0.f, 1.f);
	if (impl->source_.has_value()) {
		AL_CHECK(alSourcef(impl->source_.value(), AL_GAIN, impl->props_.gain));
	}
}

// ====================================================================================================================
float SoundInstance::volume() const
{
	return THIS_IMPL->props_.gain;
}

// ====================================================================================================================
void SoundInstance::speed(float speed)
{
	const auto impl = THIS_IMPL;
	impl->props_.speed = std::max(0.0f, speed);
	if (impl->source_.has_value()) {
		AL_CHECK(alSourcef(impl->source_.value(), AL_PITCH, impl->props_.speed));
	}
}

// ====================================================================================================================
float SoundInstance::speed() const
{
	return THIS_IMPL->props_.speed;
}

// ====================================================================================================================
void SoundInstance::looping(bool looping)
{
	const auto impl = THIS_IMPL;
	impl->props_.loop = looping;
	if (impl->source_.has_value()) {
		AL_CHECK(alSourcei(impl->source_.value(), AL_LOOPING, looping ? AL_TRUE : AL_FALSE));
	}
}

// ====================================================================================================================
bool SoundInstance::looping() const
{
	return THIS_IMPL->props_.loop;
}

// ====================================================================================================================
void SoundInstance::pan(float pan)
{
	const auto impl = THIS_IMPL;
	impl->props_.pan = math::clamp(pan, -1.0f, 1.0f);
	if (impl->source_.has_value()) {
		const auto pos = _calc_pan_position(impl->props_.pan);
		AL_CHECK(alSource3f(impl->source_.value(), AL_POSITION, pos.x, pos.y, pos.z));
	}
}

// ====================================================================================================================
float SoundInstance::pan() const
{
	return THIS_IMPL->props_.pan;
}

// ====================================================================================================================
bool SoundInstance::play()
{
	const auto impl = THIS_IMPL;

	// Check current state
	const auto curr = state();
	if (curr == AudioState::Playing) {
		return true;
	}

	// Grab a new source if needed
	if (!impl->source_.has_value()) {
		// Reserve the new source
		const auto src = get_impl(AudioEngine::Get())->reserveSource(impl);
		if (!src.has_value()) {
			return false;
		}
		impl->source_ = src.value();
		AL_CHECK(alSourcei(src.value(), AL_BUFFER, impl->buffer_->handle()));

		// Update source properties
		AL_CHECK(alSourcef(src.value(), AL_GAIN, impl->props_.gain));
		AL_CHECK(alSourcef(src.value(), AL_PITCH, impl->props_.speed));
		AL_CHECK(alSourcei(src.value(), AL_LOOPING, impl->props_.loop ? AL_TRUE : AL_FALSE));
		const auto panPos = _calc_pan_position(impl->props_.pan);
		AL_CHECK(alSource3f(src.value(), AL_POSITION, panPos.x, panPos.y, panPos.z));

		// Setup properties unique to globally playing sound effects
		AL_CHECK(alSourcei(src.value(), AL_SOURCE_RELATIVE, AL_TRUE));
		if (impl->buffer_->format().channelCount() != 1) {
			AL_CHECK(alSourcei(src.value(), AL_SOURCE_SPATIALIZE_SOFT, AL_TRUE));
		}
		AL_CHECK(alSourcei(src.value(), AL_DISTANCE_MODEL, AL_NONE));
	}

	// Play
	AL_CHECK(alSourcePlay(impl->source_.value()));
	return true;
}

// ====================================================================================================================
void SoundInstance::pause()
{
	// Check current state
	if (!isPlaying()) {
		return;
	}

	// Pause
	AL_CHECK(alSourcePause(THIS_IMPL->source_.value()));
}

// ====================================================================================================================
void SoundInstance::stop()
{
	// Check current state
	if (isStopped()) {
		return;
	}

	// Stop and release source (this calls onStop() through a callback)
	AL_CHECK(alSourceStop(THIS_IMPL->source_.value()));
	AL_CHECK(alSourcei(THIS_IMPL->source_.value(), AL_BUFFER, 0));
}

} // namespace vega
