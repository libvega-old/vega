/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./AudioEmitterImpl.hpp"

#include <algorithm>


namespace vega
{

#define THIS_IMPL_TYPE AudioEmitterImpl

// ====================================================================================================================
AudioEmitter::AudioEmitter()
{

}

// ====================================================================================================================
AudioEmitter::~AudioEmitter()
{
	
}

// ====================================================================================================================
const Sound* AudioEmitter::sound() const
{
	return THIS_IMPL->sound_.get();
}

// ====================================================================================================================
void AudioEmitter::volume(float volume)
{
	const auto impl = THIS_IMPL;
	impl->props_.gain = std::clamp(volume, 0.f, 1.f);
	if (impl->source_.has_value()) {
		AL_CHECK(alSourcef(impl->source_.value(), AL_GAIN, impl->props_.gain));
	}
}

// ====================================================================================================================
float AudioEmitter::volume() const
{
	return THIS_IMPL->props_.gain;
}

// ====================================================================================================================
void AudioEmitter::speed(float speed)
{
	const auto impl = THIS_IMPL;
	impl->props_.speed = std::max(0.0f, speed);
	if (impl->source_.has_value()) {
		AL_CHECK(alSourcef(impl->source_.value(), AL_PITCH, impl->props_.speed));
	}
}

// ====================================================================================================================
float AudioEmitter::speed() const
{
	return THIS_IMPL->props_.speed;
}

// ====================================================================================================================
void AudioEmitter::looping(bool looping)
{
	const auto impl = THIS_IMPL;
	impl->props_.loop = looping;
	if (impl->source_.has_value()) {
		AL_CHECK(alSourcei(impl->source_.value(), AL_LOOPING, looping ? AL_TRUE : AL_FALSE));
	}
}

// ====================================================================================================================
bool AudioEmitter::looping() const
{
	return THIS_IMPL->props_.loop;
}

// ====================================================================================================================
void AudioEmitter::position(const Vec3f& pos)
{
	const auto impl = THIS_IMPL;
	impl->pos_ = pos;
	if (impl->source_.has_value()) {
		AL_CHECK(alSource3f(impl->source_.value(), AL_POSITION, pos.x, pos.y, pos.z));
	}
}

// ====================================================================================================================
Vec3f AudioEmitter::position() const
{
	return THIS_IMPL->pos_;
}

// ====================================================================================================================
void AudioEmitter::velocity(const Vec3f& vel)
{
	const auto impl = THIS_IMPL;
	impl->vel_ = vel;
	if (impl->source_.has_value()) {
		AL_CHECK(alSource3f(impl->source_.value(), AL_VELOCITY, vel.x, vel.y, vel.z));
	}
}

// ====================================================================================================================
Vec3f AudioEmitter::velocity() const
{
	return THIS_IMPL->vel_;
}

} // namespace vega
