/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <Vega/Audio/Sound.hpp>
#include "./AudioBuffer.hpp"


namespace vega
{

// Implementation of Sound
class SoundImpl final
	: public Sound
{
	friend class Sound;

public:
	SoundImpl(AudioFormat format, uint32 freq, uint32 frames, const void* data);
	~SoundImpl();

	inline const AudioBuffer* buffer() const { return buffer_.get(); }

private:
	SPtr<AudioBuffer> buffer_;

	VEGA_NO_COPY(SoundImpl)
	VEGA_NO_MOVE(SoundImpl)
}; // class SoundImpl

DECLARE_IMPL_GETTER(Sound)


// Implementation of SoundInstance
class SoundInstanceImpl final
	: public SoundInstance
{
	friend class SoundInstance;

public:
	SoundInstanceImpl(const SPtr<AudioBuffer>& buffer, bool transient);
	~SoundInstanceImpl();

	inline Opt<uint32> source() const { return source_; }
	inline bool transient() const { return trans_; }

	bool onStop();

private:
	const SPtr<AudioBuffer> buffer_;
	const bool trans_;
	Opt<uint32> source_;
	struct
	{
		float gain{ 1 };
		float speed{ 1 };
		float pan{ 0 };
		bool loop{ false };
	} props_;

	VEGA_NO_COPY(SoundInstanceImpl)
	VEGA_NO_MOVE(SoundInstanceImpl)
}; // class SoundInstanceImpl

DECLARE_IMPL_GETTER(SoundInstance)

} // namespace vega
