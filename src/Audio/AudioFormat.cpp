/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Audio/AudioFormat.hpp>
#include "./OpenAL.hpp"

#include <array>


namespace vega
{

// ====================================================================================================================
const String& AudioFormat::str() const
{
	static const std::array<String, AudioFormat::StereoFloat.value() + 1> NAMES{
		"Mono8", "Stereo8", "Mono16", "Stereo16", "MonoFloat", "StereoFloat"
	};
	return NAMES.at(value_);
}

// ====================================================================================================================
const AudioFormat::FormatInfo& AudioFormat::info() const
{
	static const std::array<FormatInfo, AudioFormat::StereoFloat.value() + 1> INFOS{{
		{ AL_FORMAT_MONO8,          1, 1 },
		{ AL_FORMAT_STEREO8,        2, 1 },
		{ AL_FORMAT_MONO16,         1, 2 },
		{ AL_FORMAT_STEREO16,       2, 2 },
		{ AL_FORMAT_MONO_FLOAT32,   1, 4 },
		{ AL_FORMAT_STEREO_FLOAT32, 2, 4 }
	}};
	return INFOS.at(value_);
}

} // namespace vega
