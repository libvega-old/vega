/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Audio/AudioDevice.hpp>
#include <Vega/Core/Threading.hpp>
#include "./OpenAL.hpp"

#include <mutex>


namespace vega
{

// ====================================================================================================================
String AudioDevice::DefaultDeviceName_{ };

// ====================================================================================================================
AudioDevice::AudioDevice(const String& name)
	: name_{ name }
{

}

// ====================================================================================================================
bool AudioDevice::isDefault() const
{
	return DefaultDeviceName_.empty() ? (this == AllDevices()[0].get()) : (name_ == DefaultDeviceName_);
}

// ====================================================================================================================
const std::vector<UPtr<const AudioDevice>>& AudioDevice::AllDevices()
{
	static std::vector<UPtr<const AudioDevice>> AllDevices_{ };
	static std::mutex DeviceMutex_{ };
	static const auto _UpdateDevices = []() {
		// Get run-on string with double-null terminator
		auto names = alcGetString(nullptr, ALC_ALL_DEVICES_SPECIFIER);

		// Get the default device name in the most accurate way (using ALC_DEFAULT_DEVICE_SPECIFIER does not work)
		const auto defaultDevice = alcOpenDevice(nullptr);
		if (defaultDevice) {
			// This is a known bug in OpenAL Soft that the developer does not intend to fix (back compat concerns).
			// We must use ALC_ALL_DEVICES_SPECIFIER, not ALC_DEVICE_SPECIFIER, in this call to get the device name.
			// See http://openal.org/pipermail/openal/2016-September/000540.html
			const String defaultName{ alcGetString(defaultDevice, ALC_ALL_DEVICES_SPECIFIER) };
			if (defaultName.find(OpenAL::OPENAL_SOFT_DEVICE_PREFIX) == 0) {
				DefaultDeviceName_ = defaultName.substr(OpenAL::OPENAL_SOFT_DEVICE_PREFIX.length());
			}
			else {
				DefaultDeviceName_.clear();
			}
			alcCloseDevice(defaultDevice);
		}
		else {
			DefaultDeviceName_.clear();
		}

		// Free pointers from old list
		AllDevices_.clear();

		// Iterate over null terminators until double-null reached
		while (names[0]) {
			const String deviceName{ names }; // Constructs to first null character
			if (deviceName.find(OpenAL::OPENAL_SOFT_DEVICE_PREFIX) == 0) { // Only add OpenAL Soft managed devices
				AllDevices_.emplace_back(
					new AudioDevice(deviceName.substr(OpenAL::OPENAL_SOFT_DEVICE_PREFIX.length()))
				);
			}
			names += (deviceName.length() + 1); // Advance past null terminator
		}
	};

	Threading::AssertMainThread("Must call AudioDevice::AllDevices() on main thread");

	std::lock_guard lock{ DeviceMutex_ };
	if (AllDevices_.empty()) {
		_UpdateDevices();
	}
	return AllDevices_;
}

// ====================================================================================================================
const AudioDevice* AudioDevice::DefaultDevice()
{
	Threading::AssertMainThread("Must call AudioDevice::DefaultDevice() on main thread");

	const auto& allDevices = AllDevices();
	for (const auto& device : allDevices) {
		if (device->isDefault()) {
			return device.get();
		}
	}
	return allDevices[0].get();
}

} // namespace vega
