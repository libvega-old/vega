/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"

#define AL_ALEXT_PROTOTYPES
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>


namespace vega
{

// Global objects and interface to OpenAL 
class OpenAL final
{
public:
	// AL Error Functions
	static void ALCheck(const StringView& message = "");
	static String ALErrorString(int error);

	// ALC Error Functions
	static void ALCCheck(ALCdevice* device, const StringView& message = "");
	static String ALCErrorString(int error);

	// Initialize/Terminate
	static void Initialize(const String& deviceName);
	static void Terminate();

	// Objects
	inline static ALCdevice* Device() { return Device_; }
	inline static ALCcontext* Context() { return Context_; }

public:
	// Known prefix for device names that are managed by OpenAL Soft
	static const String OPENAL_SOFT_DEVICE_PREFIX;

private:
	static ALCdevice* Device_;
	static ALCcontext* Context_;

	VEGA_NO_COPY(OpenAL)
	VEGA_NO_MOVE(OpenAL)
	VEGA_NO_INIT(OpenAL)
}; // class OpenAL

} // namespace vega


#if defined(VEGA_DEBUG)
#	define AL_CHECK(statement)           { (statement); ::vega::OpenAL::ALCheck(#statement); }
#	define ALC_CHECK(device, statement)  { (statement); ::vega::OpenAL::ALCCheck(device, #statement); }
#else
#	define AL_CHECK(statement)           { (statement); }
#	define ALC_CHECK(device, statement)  { (statement); }
#endif // defined(VEGA_DEBUG)
