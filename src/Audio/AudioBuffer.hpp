/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <Vega/Core/Time.hpp>
#include <Vega/Audio/AudioFormat.hpp>
#include "./OpenAL.hpp"


namespace vega
{

// Contains a collection of playable audio data.
class AudioBuffer final
{
public:
	// The minumum supported frequency for audio data.
	inline static constexpr uint32 MIN_FREQUENCY{ 4'000 };
	// The maximum supported frequency for audio data.
	inline static constexpr uint32 MAX_FREQUENCY{ 48'000 };

	AudioBuffer();
	~AudioBuffer();

	void setData(AudioFormat format, uint32 freq, uint32 frameCount, const void* data);

	inline uint32 handle() const { return handle_; }
	inline AudioFormat format() const { return format_; }
	inline uint32 frequency() const { return freq_; }
	inline uint32 frameCount() const { return frameCount_; }
	inline uint64 dataSize() const {
		return uint64(frameCount_) * format_.frameSize();
	}
	TimeSpan duration() const {
		return TimeSpan::Seconds(double(frameCount_) / freq_);
	}

private:
	const uint32 handle_;
	AudioFormat format_;
	uint32 freq_;
	uint32 frameCount_;

	VEGA_NO_COPY(AudioBuffer)
	VEGA_NO_MOVE(AudioBuffer)
}; // class AudioBuffer

} // namespace vega
