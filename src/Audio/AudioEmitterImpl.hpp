/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <Vega/Audio/AudioEmitter.hpp>
#include "./SoundImpl.hpp"


namespace vega
{

// Implementation of AudioEmitter
class AudioEmitterImpl final
	: public AudioEmitter
{
	friend class AudioEmitter;

public:
	AudioEmitterImpl(const SoundImpl& sound);
	~AudioEmitterImpl();

private:
	AssetHandle<SoundImpl> sound_;
	Opt<uint32> source_;
	struct
	{
		float gain{ 1 };
		float speed{ 1 };
		bool loop{ false };
	} props_;
	Vec3f pos_;
	Vec3f vel_;

	VEGA_NO_COPY(AudioEmitterImpl)
	VEGA_NO_MOVE(AudioEmitterImpl)
}; // class AudioEmitterImpl

DECLARE_IMPL_GETTER(AudioEmitter)

} // namespace vega
