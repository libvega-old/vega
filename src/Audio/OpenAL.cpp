/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./OpenAL.hpp"


namespace vega
{

// ====================================================================================================================
const String OpenAL::OPENAL_SOFT_DEVICE_PREFIX{ "OpenAL Soft on " };
ALCdevice* OpenAL::Device_{ nullptr };
ALCcontext* OpenAL::Context_{ nullptr };

// ====================================================================================================================
void OpenAL::ALCheck(const StringView& message)
{
	const auto err = alGetError();
	if (err != AL_NO_ERROR) {
		const auto estr = ALErrorString(err);
		throw std::runtime_error(to_string("AL error (%s) - \"%s\"", estr.c_str(), message.data()));
	}
}

// ====================================================================================================================
String OpenAL::ALErrorString(int error)
{
	switch (error) {
	case AL_NO_ERROR: return "NO_ERROR";
	case AL_INVALID_NAME: return "INVALID_NAME";
	case AL_INVALID_ENUM: return "INVALID_ENUM";
	case AL_INVALID_VALUE: return "INVALID_VALUE";
	case AL_INVALID_OPERATION: return "INVALID_OPERATION";
	case AL_OUT_OF_MEMORY: return "OUT_OF_MEMORY";
	default: return {};
	}
}

// ====================================================================================================================
void OpenAL::ALCCheck(ALCdevice* device, const StringView& message)
{
	const auto err = alcGetError(device);
	if (err != ALC_NO_ERROR) {
		const auto estr = ALCErrorString(err);
		throw std::runtime_error(to_string("ALC error (%s) - \"%s\"", estr.c_str(), message.data()));
	}
}

// ====================================================================================================================
String OpenAL::ALCErrorString(int error)
{
	switch (error)
	{
	case ALC_NO_ERROR: return "NO_ERROR";
	case ALC_INVALID_DEVICE: return "INVALID_DEVICE";
	case ALC_INVALID_CONTEXT: return "INVALID_CONTEXT";
	case ALC_INVALID_ENUM: return "INVALID_ENUM";
	case ALC_INVALID_VALUE: return "INVALID_VALUE";
	case ALC_OUT_OF_MEMORY: return "OUT_OF_MEMORY";
	default: return {};
	}
}

// ====================================================================================================================
void OpenAL::Initialize(const String& deviceName)
{
	// Open the device
	const auto fullDeviceName = OPENAL_SOFT_DEVICE_PREFIX + deviceName;
	const auto deviceHandle = alcOpenDevice((const ALchar*)fullDeviceName.c_str());
	ALCCheck(deviceHandle, "Failed to open audio playback device");
	Device_ = deviceHandle;

	// Open the context
	static const int CONTEXT_ATTR[2]{ 0, 0 }; // No additional attributes
	const auto contextHandle = alcCreateContext(deviceHandle, CONTEXT_ATTR);
	ALCCheck(deviceHandle, "Failed to create audio playback context");
	Context_ = contextHandle;

	// Make context current
	if (alcMakeContextCurrent(Context_) != ALC_TRUE) {
		throw std::runtime_error("Failed to activate audio playback context");
	}
	ALCCheck(deviceHandle, "Failed to activate audio playback context");

	// Enable features
	alEnable(AL_SOURCE_DISTANCE_MODEL);
	ALCheck("Failed to enable SOURCE_DISTANCE_MODEL feature");
}

// ====================================================================================================================
void OpenAL::Terminate()
{
	// Destroy context
	if (Context_) {
		alcMakeContextCurrent(nullptr);
		alcDestroyContext(Context_);
		Context_ = nullptr;
	}

	// Close device handle
	if (Device_) {
		alcCloseDevice(Device_);
		Device_ = nullptr;
	}
}

} // namespace vega
