/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./AudioEngineImpl.hpp"
#include "./SoundImpl.hpp"
#include "./AudioEmitterImpl.hpp"
#include <Vega/Math/Mat4.hpp>


namespace vega
{

#define THIS_IMPL_TYPE AudioEngineImpl

// ====================================================================================================================
AudioEngine* AudioEngine::Instance_{ nullptr };

// ====================================================================================================================
AudioEngine::AudioEngine()
{
	Instance_ = this;
}

// ====================================================================================================================
AudioEngine::~AudioEngine()
{
	Instance_ = nullptr;
}

// ====================================================================================================================
const AudioDevice* AudioEngine::playbackDevice() const
{
	return THIS_IMPL->device_;
}

// ====================================================================================================================
void AudioEngine::stopAll()
{
	const auto impl = THIS_IMPL;
	impl->playback_.stopAll.store(true);

	// Stop all playing instances
	try {
		std::lock_guard lock{ impl->playback_.mutex };
		std::lock_guard lock2{ impl->sources_.mutex };
		for (const auto& pair : impl->playback_.sounds) {
			pair.second->stop();
			impl->sources_.available.push(pair.first);
			if (get_impl(pair.second)->transient()) {
				delete pair.second;
			}
		}
		impl->playback_.sounds.clear();
		impl->playback_.stopAll.store(false);
	}
	catch (...) {
		impl->playback_.stopAll.store(false);
		throw;
	}
}

// ====================================================================================================================
AudioWorld* AudioEngine::world()
{
	return THIS_IMPL->world_.get();
}

// ====================================================================================================================
UPtr<Sound> AudioEngine::createSound(AudioFormat format, uint32 frequency, uint32 frameCount, const void* data)
{
	// Validate args
	if ((frequency < AudioBuffer::MIN_FREQUENCY) || (frequency > AudioBuffer::MAX_FREQUENCY)) {
		throw std::runtime_error(
			to_string("Invalid Sound frequency %u (must be in [%u, %u])", frequency,
				AudioBuffer::MIN_FREQUENCY, AudioBuffer::MAX_FREQUENCY)
		);
	}
	if (frameCount == 0) {
		throw std::runtime_error("Cannot create Sound with zero frames");
	}
	if (!data) {
		throw std::runtime_error("Cannot create a Sound from null data");
	}

	// Create sound
	return std::make_unique<SoundImpl>(format, frequency, frameCount, data);
}

} // namespace vega
