/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./AudioBuffer.hpp"


namespace vega
{

// ====================================================================================================================
AudioBuffer::AudioBuffer()
	: handle_{ ([]() { uint32 handle; AL_CHECK(alGenBuffers(1, &handle)); return handle; })() }
	, format_{ }
	, freq_{ }
	, frameCount_{ }
{

}

// ====================================================================================================================
AudioBuffer::~AudioBuffer()
{
	AL_CHECK(alDeleteBuffers(1, &handle_));
}

// ====================================================================================================================
void AudioBuffer::setData(AudioFormat format, uint32 freq, uint32 frameCount, const void* data)
{
	// Set the data
	const uint64 dataSize{ uint64(frameCount) * format.frameSize() };
	AL_CHECK(alBufferData(handle_, ALenum(format.formatId()), (const ALvoid*)data, ALsizei(dataSize), ALsizei(freq)));

	// Get the real buffer info
	int32 abits, achannels, asize, afreq;
	AL_CHECK(alGetBufferi(handle_, AL_BITS, &abits));
	AL_CHECK(alGetBufferi(handle_, AL_CHANNELS, &achannels));
	AL_CHECK(alGetBufferi(handle_, AL_SIZE, &asize));
	AL_CHECK(alGetBufferi(handle_, AL_FREQUENCY, &afreq));

	// Update the fields
	format_ = format;
	freq_ = uint32(afreq);
	frameCount_ = uint32(asize) / uint32((abits / 8) * achannels);
}

} // namespace vega
