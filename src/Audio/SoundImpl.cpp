/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./SoundImpl.hpp"
#include "./AudioEngineImpl.hpp"


namespace vega
{

// ====================================================================================================================
SoundImpl::SoundImpl(AudioFormat format, uint32 freq, uint32 frames, const void* data)
	: buffer_{ std::make_shared<AudioBuffer>() }
{
	buffer_->setData(format, freq, frames, data);
}

// ====================================================================================================================
SoundImpl::~SoundImpl()
{
	buffer_.reset();
}


// ====================================================================================================================
// ====================================================================================================================
SoundInstanceImpl::SoundInstanceImpl(const SPtr<AudioBuffer>& buffer, bool transient)
	: SoundInstance()
	, buffer_{ buffer }
	, trans_{ transient }
	, source_{ std::nullopt }
	, props_{ }
{

}

// ====================================================================================================================
SoundInstanceImpl::~SoundInstanceImpl()
{
	get_impl(AudioEngine::Get())->onSoundDestroyed(this);
	stop();
}

// ====================================================================================================================
bool SoundInstanceImpl::onStop()
{
	// Clear source (source is released by calling code)
	if (source_.has_value()) {
		AL_CHECK(alSourcei(source_.value(), AL_BUFFER, 0));
	}
	source_ = std::nullopt;

	// Return transient status, if true, the instance is deleted
	return trans_;
}

} // namespace vega
