/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./AudioEngineImpl.hpp"
#include "./SoundImpl.hpp"

#include <array>
#include <atomic>


namespace vega
{

// ====================================================================================================================
static std::atomic_bool Destroying_{ false }; // Needed to prevent the async AL event cb from accessing destroyed eng
static const std::array<ALenum, 3> AL_EVENT_TYPES{
	AL_EVENT_TYPE_SOURCE_STATE_CHANGED_SOFT, AL_EVENT_TYPE_BUFFER_COMPLETED_SOFT, AL_EVENT_TYPE_DISCONNECTED_SOFT
};

// ====================================================================================================================
AudioEngineImpl::AudioEngineImpl(const AudioDevice& device)
	: AudioEngine()
	, device_{ &device }
	, sources_{ }
	, playback_{ }
	, world_{ }
{
	OpenAL::Initialize(device.name());

	// Gen sources
	sources_.all.resize(AudioEngine::MAX_SOURCES);
	alGenSources(ALsizei(sources_.all.size()), (ALuint*)sources_.all.data());
	OpenAL::ALCheck("Failed to generate audio playback sources");
	for (const auto src : sources_.all) {
		sources_.available.push(src);
	}

	// Register event callbacks (AL_SOFT_events)
	alEventControlSOFT(ALsizei(AL_EVENT_TYPES.size()), AL_EVENT_TYPES.data(), AL_TRUE);
	alEventCallbackSOFT(
		[](ALenum eventType, ALuint object, ALuint param, ALsizei, const ALchar*, ALvoid* userParam) {
			const auto engine = reinterpret_cast<AudioEngineImpl*>(userParam);
			if (!engine || Destroying_.load()) {
				return;
			}

			switch (eventType)
			{
			case AL_EVENT_TYPE_SOURCE_STATE_CHANGED_SOFT: {
				if (param == AL_STOPPED) {
					engine->onSourceStop(object);
				}
			} break;
			case AL_EVENT_TYPE_BUFFER_COMPLETED_SOFT: {
				// TODO: Will be used to implement streaming
			} break;
			case AL_EVENT_TYPE_DISCONNECTED_SOFT: {
				// TODO: Will be used to implement playback device switching
			} break;
			}
		},
		this
	);

	// Create the audio world
	world_.reset(new AudioWorld);
}

// ====================================================================================================================
AudioEngineImpl::~AudioEngineImpl()
{
	Destroying_.store(true);
	alEventCallbackSOFT(nullptr, nullptr);

	// Call StopAll to cleanup existing sources
	stopAll();

	world_.reset();

	alDeleteSources(ALsizei(sources_.all.size()), (const ALuint*)sources_.all.data());
	OpenAL::Terminate();
}

// ====================================================================================================================
Opt<uint32> AudioEngineImpl::reserveSource(SoundInstanceImpl* inst)
{
	// Wait if we are stopping all to prevent infinite lock
	while (playback_.stopAll.load()) { ; }

	// Get next available
	uint32 source{};
	{
		std::lock_guard lock{ sources_.mutex };
		if (sources_.available.empty()) {
			return std::nullopt;
		}
		source = sources_.available.top();
		sources_.available.pop();
	}

	// Associate the instance with the source
	{
		std::lock_guard lock{ playback_.mutex };
		playback_.sounds[source] = inst;
	}

	return source;
}

// ====================================================================================================================
void AudioEngineImpl::onSoundDestroyed(SoundInstanceImpl* inst)
{
	if (!playback_.stopAll.load() && inst->source().has_value()) {
		std::lock_guard lock{ playback_.mutex };
		const auto it = playback_.sounds.find(inst->source().value());
		if ((it != playback_.sounds.end()) && (it->second == inst)) {
			std::lock_guard lock2{ sources_.mutex };
			sources_.available.push(it->first);
			playback_.sounds.erase(it);
		}
	}
}

// ====================================================================================================================
void AudioEngineImpl::onSourceStop(uint32 source)
{
	// Lookup sound
	SoundInstanceImpl* inst{ nullptr };
	{
		std::lock_guard lock{ playback_.mutex };
		const auto it = playback_.sounds.find(source);
		if (it != playback_.sounds.end()) {
			inst = it->second;
			playback_.sounds.erase(it);
		}
	}

	// Free sound if needed
	if (inst) {
		if (inst->onStop()) {
			delete inst;
		}
		std::lock_guard lock{ sources_.mutex };
		sources_.available.push(source);
	}
}

} // namespace vega
