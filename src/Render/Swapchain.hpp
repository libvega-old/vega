/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include "../Graphics/Vulkan.hpp"
#include "../Graphics/Managers/CommandManager.hpp"
#include <Vega/Math/Vec2.hpp>


namespace vega
{

class WindowImpl;
class RenderTarget;

// Implements surface and swapchain management for a specific window
class Swapchain final
{
	struct SwapchainImage final
	{
		vk::Image image{ };
		CommandFence::Wait mappedFence{ };
	}; // struct SwapchainImage
	struct SurfaceInfo final
	{
		Vec2ui extent{ };
		uint32 minImageCount{ };
		vk::SurfaceTransformFlagBitsKHR transform{ };
	}; // struct SurfaceInfo

public:
	Swapchain(WindowImpl* window);
	~Swapchain();

	inline const Vec2ui& extent() const { return swapchain_.extent; }

	inline bool vsyncOnly() const { return !surface_.hasImmediate && !surface_.hasMailbox; }
	inline bool vsync() const { return surface_.mode == vk::PresentModeKHR::eFifo; }
	void vsync(bool vsync);

	void present(const RenderTarget& target);

private:
	void getSurfaceInfo(SurfaceInfo& info) const;

	void rebuild();
	bool acquire(vk::Result& result);

	void destroySwapchain();

private:
	WindowImpl* const window_;
	struct
	{
		vk::SurfaceKHR handle{ };
		vk::SurfaceFormatKHR format{ };
		vk::PresentModeKHR mode{ };
		bool hasImmediate{ };
		bool hasMailbox{ };
	} surface_;
	struct
	{
		vk::SwapchainKHR handle{ };
		std::vector<SwapchainImage> images{ };
		Vec2ui extent{ };
		uint32 imageIndex{ };
		bool minimized{ };
		bool dirty{ };
	} swapchain_;
	struct
	{
		std::array<vk::Semaphore, Vulkan::MAX_PARALLEL_FRAMES> acquireSemaphores{ };
		std::array<vk::Semaphore, Vulkan::MAX_PARALLEL_FRAMES> renderSemaphores{ };
		CommandFence::Wait lastFence{ };
		uint32 syncIndex{ };
	} sync_;

	VEGA_NO_COPY(Swapchain)
	VEGA_NO_MOVE(Swapchain)
}; // class Swapchain

} // namespace vega
