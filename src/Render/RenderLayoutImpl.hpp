/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <Vega/Render/RenderLayout.hpp>
#include <Vega/Core/InlineVector.hpp>
#include <Vega/Graphics/Multisampling.hpp>
#include "../Graphics/Vulkan.hpp"

#include <array>
#include <set>


namespace vega
{

// Implementation of RenderLayout
class RenderLayoutImpl final
	: public RenderLayout
{
	friend class RenderLayout;

public:
	inline static constexpr uint32 MAX_SUBPASS_TARGETS{ 4u };

	struct Target final
	{
		TexelFormat format{ };
		struct
		{
			uint8 preserve : 1; // Preserved at end of Renderer
			uint8 input    : 1; // Used as input
			uint8 output   : 1; // Used as output
			uint8 internal : 1; // Special internal attachment that exists only to support MSAA ops
			uint8 msaa     : 1; // If the target is MSAA
		} flags{ };
		uint32 partnerIndex{ }; // The index of the partner attachment for (real, internal) pairs
		RenderUse lastUse{ };
	}; // struct Target

	struct Subpass final
	{
		bool msaa{ };
		bool resolve{ };
		uint32 colorCount{ 0 };
		std::array<uint32, MAX_SUBPASS_TARGETS> colorTargets{ };
		std::array<uint32, MAX_SUBPASS_TARGETS> resolveTargets{ };
		uint32 inputCount{ 0 };
		std::array<uint32, MAX_SUBPASS_TARGETS> inputTargets{ };
		uint32 depthTarget{ VK_ATTACHMENT_UNUSED };
		uint32 depthResolveTarget{ VK_ATTACHMENT_UNUSED };
		InlineVector<uint32, MAX_TARGETS> preserveTargets{ };
	}; // struct Subpass

	struct Dependency final
	{
		uint8 srcSubpass{ UINT8_MAX };
		uint8 dstSubpass{ UINT8_MAX };
		enum : uint8 {
			Color, Depth, Input
		} srcUse{}, dstUse{};

		inline constexpr bool operator < (const Dependency& r) const { 
			return *(const uint32*)this < *(const uint32*)&r;
		}
	}; // struct Dependency
	static_assert(sizeof(Dependency) == sizeof(uint32), "Invalid RenderLayout::Dependency size");

	using TargetVector = InlineVector<Target, MAX_TARGETS>;
	using MSAATargetVector = InlineVector<Target, MAX_TARGETS * 2>;
	using SubpassVector = InlineVector<Subpass, MAX_SUBPASSES>;
	using DependencySet = std::set<Dependency, std::less<Dependency>>;

	RenderLayoutImpl(
		TargetVector&& targets,
		MSAATargetVector&& msaaTargets,
		SubpassVector&& subpasses,
		SubpassVector&& msaaSubpasses,
		DependencySet&& dependencies
	);
	~RenderLayoutImpl();

	inline const TargetVector& targets() const { return targets_; }
	inline const MSAATargetVector& msaaTargets() const { return msaaTargets_; }
	inline const SubpassVector& subpasses() const { return subpasses_; }
	inline const SubpassVector& msaaSubpasses() const { return msaaSubpasses_; }
	inline const DependencySet& dependencies() const { return dependencies_; }

	vk::RenderPass createRenderPass(MSAA msaa) const;

private:
	bool supportsMSAA_;
	TargetVector targets_;
	MSAATargetVector msaaTargets_;
	SubpassVector subpasses_;
	SubpassVector msaaSubpasses_;
	DependencySet dependencies_;

	VEGA_NO_COPY(RenderLayoutImpl)
	VEGA_NO_MOVE(RenderLayoutImpl)
}; // class RenderLayoutImpl

DECLARE_IMPL_GETTER(RenderLayout)


// Implementation of RenderLayoutBuilder
class RenderLayoutBuilderImpl final
	: public RenderLayoutBuilder
{
	friend class RenderLayoutBuilder;

	inline static constexpr uint32 MAX_SUBPASSES{ RenderLayout::MAX_SUBPASSES };
	inline static constexpr uint32 MAX_TARGETS{ RenderLayout::MAX_TARGETS };
	inline static constexpr uint32 MAX_SUBPASS_TARGETS{ 4u };

	struct Target final
	{
		uint32 index{ };
		TexelFormat format{ };
		struct
		{
			uint8 preserve : 1; // Preserved at end of Renderer
			uint8 input    : 1; // Used as input
			uint8 color    : 1; // Used as color output
			uint8 depth    : 1; // Used as depth output
			uint8 msaa     : 1; // Used as an output in a resolve subpass -> must be MSAA
			uint8 resolve  : 1; // Used after a resolve pass -> needs a separate resolve attachment
		} flags{ };
		uint32 resolvePass{ };
		uint32 firstUseIndex{ };
		uint32 lastUseIndex{ };
		InlineVector<RenderUse, MAX_SUBPASSES> uses{ };
	}; // struct Target

	struct Subpass final
	{
		uint32 index{ };
		RenderUseMask useMask{ };
		bool msaa{ false };
		std::array<Opt<uint32>, MAX_SUBPASS_TARGETS> colorTargets{ };
		std::array<Opt<uint32>, MAX_SUBPASS_TARGETS> inputTargets{ };
		Opt<uint32> depthTarget{ std::nullopt };
	}; // struct Subpass

public:
	RenderLayoutBuilderImpl(uint32 spCount, SubpassMask msaaMask);
	~RenderLayoutBuilderImpl();

private:
	InlineVector<Subpass, MAX_SUBPASSES> subpasses_;
	InlineVector<Target, MAX_TARGETS> targets_;

	VEGA_NO_COPY(RenderLayoutBuilderImpl)
	VEGA_NO_MOVE(RenderLayoutBuilderImpl)
}; // class RenderLayoutBuilderImpl

DECLARE_IMPL_GETTER(RenderLayoutBuilder)

} // namespace vega
