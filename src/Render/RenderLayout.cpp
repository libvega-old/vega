/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include "./RenderLayoutImpl.hpp"

#include <sstream>


namespace vega
{

// ====================================================================================================================
static inline bool RenderUse_isColor(RenderUse use) { 
	return (use >= RenderUse::Color0) && (use <= RenderUse::Color3); 
}
static inline bool RenderUse_isInput(RenderUse use) {
	return (use >= RenderUse::Input0) && (use <= RenderUse::Input3);
}


// ====================================================================================================================
// ====================================================================================================================
String to_string(RenderUseMask mask)
{
	if (mask.rawValue() == 0) {
		return "None";
	}

	std::stringstream ss{};
	uint32 maskCount{ 0 };
	if (bool(mask & RenderUse::Color0)) { ss << "Color0|"; ++maskCount; }
	if (bool(mask & RenderUse::Color1)) { ss << "Color1|"; ++maskCount; }
	if (bool(mask & RenderUse::Color2)) { ss << "Color2|"; ++maskCount; }
	if (bool(mask & RenderUse::Color3)) { ss << "Color3|"; ++maskCount; }
	if (bool(mask & RenderUse::Input0)) { ss << "Input0|"; ++maskCount; }
	if (bool(mask & RenderUse::Input1)) { ss << "Input1|"; ++maskCount; }
	if (bool(mask & RenderUse::Input2)) { ss << "Input2|"; ++maskCount; }
	if (bool(mask & RenderUse::Input3)) { ss << "Input3|"; ++maskCount; }
	if (bool(mask & RenderUse::Depth))  { ss << "Depth|";  ++maskCount; }
	const auto res = ss.str();
	return (maskCount == 1)
		? res.substr(0, res.size() - 1)
		: '[' + res.substr(0, res.size() - 1) + ']';
}


// ====================================================================================================================
// ====================================================================================================================
String to_string(SubpassMask mask)
{
	if (mask.rawValue() == 0) {
		return "None";
	}

	std::stringstream ss{};
	uint32 maskCount{ 0 };
	if (bool(mask & Subpass::Pass0)) { ss << "Pass0|"; ++maskCount; }
	if (bool(mask & Subpass::Pass1)) { ss << "Pass1|"; ++maskCount; }
	if (bool(mask & Subpass::Pass2)) { ss << "Pass2|"; ++maskCount; }
	if (bool(mask & Subpass::Pass3)) { ss << "Pass3|"; ++maskCount; }
	if (bool(mask & Subpass::Pass4)) { ss << "Pass4|"; ++maskCount; }
	if (bool(mask & Subpass::Pass5)) { ss << "Pass5|"; ++maskCount; }
	if (bool(mask & Subpass::Pass6)) { ss << "Pass6|"; ++maskCount; }
	if (bool(mask & Subpass::Pass7)) { ss << "Pass7|"; ++maskCount; }
	const auto res = ss.str();
	return (maskCount == 1)
		? res.substr(0, res.size() - 1)
		: '[' + res.substr(0, res.size() - 1) + ']';
}


#define THIS_IMPL_TYPE RenderLayoutImpl
// ====================================================================================================================
// ====================================================================================================================
RenderLayout::RenderLayout()
{

}

// ====================================================================================================================
RenderLayout::~RenderLayout()
{

}

// ====================================================================================================================
bool RenderLayout::supportsMSAA() const
{
	return THIS_IMPL->supportsMSAA_;
}

// ====================================================================================================================
uint32 RenderLayout::subpassCount() const
{
	return uint32(THIS_IMPL->subpasses_.size());
}


#undef THIS_IMPL_TYPE
#define THIS_IMPL_TYPE RenderLayoutBuilderImpl
// ====================================================================================================================
// ====================================================================================================================
RenderLayoutBuilder::RenderLayoutBuilder()
{

}

// ====================================================================================================================
RenderLayoutBuilder::~RenderLayoutBuilder()
{

}

// ====================================================================================================================
uint32 RenderLayoutBuilder::subpassCount() const
{
	return uint32(THIS_IMPL->subpasses_.size());
}

// ====================================================================================================================
uint32 RenderLayoutBuilder::targetCount() const
{
	return uint32(THIS_IMPL->targets_.size());
}

// ====================================================================================================================
RenderLayoutBuilder& RenderLayoutBuilder::addTarget(TexelFormat format, bool preserve,
	const std::initializer_list<RenderUse>& uses)
{
	using Target = RenderLayoutBuilderImpl::Target;
	using Subpass = RenderLayoutBuilderImpl::Subpass;

	auto impl = THIS_IMPL;

	Target target{};
	target.index = uint32(impl->targets_.size());
	target.format = format;
	target.flags.preserve = preserve ? 1 : 0;

	// Simple validation
	if (uses.size() != impl->subpasses_.size()) {
		throw BadLayoutError(to_string("Target %u has an invalid subpass count", target.index));
	}

	// Validate the timeline
	Opt<uint32> firstUse{ std::nullopt };
	Opt<uint32> lastUse{ std::nullopt };
	for (uint32 si = 0; si < subpassCount(); ++si) {
		const auto& subpass = impl->subpasses_[si];
		const auto use = *(uses.begin() + si);
		const auto lastPass = (si == (subpassCount() - 1));
		if (use == RenderUse::None) {
			continue;
		}

		// Check: duplicate target use
		if (bool(subpass.useMask & use)) {
			throw BadLayoutError(to_string("Target %u has duplicate use '%s' with other target in subpass %u", 
				target.index, to_string(use).c_str(), si));
		}

		// Validate Uses
		if (RenderUse_isColor(use)) {
			if (!format.isColor()) { // Check: format is a color format
				throw BadLayoutError(to_string(
					"Target %u is depth/stencil and cannot be used as a color attachment in subpass %u", target.index, si));
			}
			if (subpass.msaa) {
				if (target.flags.resolve) { // Check: MSAA after resolve or other non-MSAA use
					throw BadLayoutError(to_string(
						"Target %u cannot be used in MSAA subpass %u after non-MSAA use", target.index, si));
				}
				target.flags.msaa = 1; // Update: target will use MSAA
			}
			else {
				if (!target.flags.resolve) {
					target.resolvePass = lastUse.value_or(0); // Update: target resolve at last use, or zero
				}
				target.flags.resolve = 1; // Update: target will be resolved (if MSAA)
			}

			target.flags.color = 1; // Update: target is used as color attachment
		}
		else if (RenderUse_isInput(use)) {
			if (!target.flags.color && !target.flags.depth) { // Check: Input use before being written to
				throw BadLayoutError(to_string(
					"Target %u cannot be used as input in subpass %u before being used as output", target.index, si));
			}

			if (!target.flags.resolve) {
				target.resolvePass = lastUse.value(); // Update: target resolve at last use
			}
			target.flags.resolve = 1; // Update: target will be resolved (if MSAA)
			target.flags.input = 1; // Update: target is used as input attachment
		}
		else if (use == RenderUse::Depth) {
			if (!format.isDepth()) { // Check: format is a depth/stencil format
				throw BadLayoutError(to_string(
					"Target %u is color and cannot be used as a depth/stencil attachment in subpass %u", target.index, si));
			}
			if (subpass.msaa) {
				if (target.flags.resolve) { // Check: MSAA after resolve or other non-MSAA use
					throw BadLayoutError(to_string(
						"Target %u cannot be used in MSAA subpass %u after non-MSAA use", target.index, si));
				}
				target.flags.msaa = 1; // Update: target will use MSAA
			}
			else {
				if (!target.flags.resolve) {
					target.resolvePass = lastUse.value_or(0); // Update: target resolve at last use, or zero
				}
				target.flags.resolve = 1; // Update: target will be resolved (if MSAA)
			}

			target.flags.depth = 1; // Update: target is used as depth attachment
		}
		else {
			assert(false && "Invalid RenderUse for target");
		}

		lastUse = si;
		if (!firstUse.has_value()) {
			firstUse = lastUse.value();
		}
	}

	// Check: the target is never used
	if (!firstUse.has_value()) {
		throw BadLayoutError(to_string("Target %u is never used by any subpass", target.index));
	}
	target.firstUseIndex = firstUse.value();
	target.lastUseIndex = lastUse.value();

	// Mark resolve if preserved
	if (preserve && !target.flags.resolve) {
		target.resolvePass = lastUse.value();
		target.flags.resolve = 1;
	}

	// Update the state now that validation is complete
	for (uint32 si = 0; si < subpassCount(); ++si) {
		auto& subpass = impl->subpasses_[si];
		const auto use = *(uses.begin() + si);
		target.uses.push_back(use);
		if (use == RenderUse::None) {
			continue;
		}

		subpass.useMask |= use;

		switch (use)
		{
		case RenderUse::Color0: subpass.colorTargets[0] = target.index; break;
		case RenderUse::Color1: subpass.colorTargets[1] = target.index; break;
		case RenderUse::Color2: subpass.colorTargets[2] = target.index; break;
		case RenderUse::Color3: subpass.colorTargets[3] = target.index; break;
		case RenderUse::Input0: subpass.inputTargets[0] = target.index; break;
		case RenderUse::Input1: subpass.inputTargets[1] = target.index; break;
		case RenderUse::Input2: subpass.inputTargets[2] = target.index; break;
		case RenderUse::Input3: subpass.inputTargets[3] = target.index; break;
		case RenderUse::Depth:  subpass.depthTarget = target.index; break;
		}
	}

	// Add the target and return
	impl->targets_.push_back(target);
	return *this;
}

// ====================================================================================================================
SPtr<RenderLayout> RenderLayoutBuilder::build() const
{
	using RLTarget = RenderLayoutImpl::Target;
	using RLSubpass = RenderLayoutImpl::Subpass;
	using RLDep = RenderLayoutImpl::Dependency;

	auto impl = THIS_IMPL;

	// Resolve independent checks
	if (impl->targets_.empty()) { // Check: at least one target is specified
		throw BadLayoutError("Cannot build a RenderLayout with zero targets");
	}
	uint32 preserveCount{ 0 };
	for (const auto& target : impl->targets_) {
		if (target.flags.preserve) {
			preserveCount++;
		}
	}
	if (preserveCount == 0) { // Check: at least one target is preserved
		throw BadLayoutError("Each RenderLayout must have at least one preserved target");
	}

	// Create the RenderLayout subpasses after validation
	InlineVector<RLSubpass, RenderLayout::MAX_SUBPASSES> normalSubpasses{ };
	InlineVector<RLSubpass, RenderLayout::MAX_SUBPASSES> msaaSubpasses{ };
	for (const auto& subpass : impl->subpasses_) {
		// Check: color attachments are contiguous
		uint32 colorCount{ 0 }, maxColor{ 0 };
		for (uint32 ti = 0; ti < subpass.colorTargets.size(); ++ti) {
			if (subpass.colorTargets[ti].has_value()) {
				colorCount++; maxColor = ti + 1;
			}
		}
		if (colorCount != maxColor) {
			throw BadLayoutError(to_string(
				"Subpass %u does not have contiguously bound color attachments", subpass.index));
		}

		// Check: input attachments are contiguous
		uint32 inputCount{ 0 }, maxInput{ 0 };
		for (uint32 ti = 0; ti < subpass.inputTargets.size(); ++ti) {
			if (subpass.inputTargets[ti].has_value()) {
				inputCount++; maxInput = ti + 1;
			}
		}
		if (inputCount != maxInput) {
			throw BadLayoutError(to_string(
				"Subpass %u does not have contiguously bound input attachments", subpass.index));
		}

		// Initialize the subpasses
		auto& rlSubpass = normalSubpasses.emplace_back();
		rlSubpass.msaa = false;
		rlSubpass.resolveTargets.fill(VK_ATTACHMENT_UNUSED);
		rlSubpass.colorCount = colorCount;
		rlSubpass.inputCount = inputCount;
		auto& msaaSubpass = msaaSubpasses.emplace_back();
		msaaSubpass.msaa = subpass.msaa;
		msaaSubpass.resolveTargets.fill(VK_ATTACHMENT_UNUSED);
		msaaSubpass.colorCount = colorCount;
		msaaSubpass.inputCount = inputCount;
	}

	// Create the RenderLayout targets and dependencies
	InlineVector<RLTarget, RenderLayout::MAX_TARGETS> normalTargets{ };
	InlineVector<RLTarget, RenderLayout::MAX_TARGETS> normalMSAATargets{ };
	InlineVector<RLTarget, RenderLayout::MAX_TARGETS> internalTargets{ };
	RenderLayoutImpl::DependencySet dependencies{ };
	for (const auto& target : impl->targets_) {
		// Initialize the layout targets
		auto rlTarget = &normalTargets.emplace_back();
		rlTarget->format = target.format;
		rlTarget->flags.preserve = target.flags.preserve;
		rlTarget->flags.internal = 0;
		rlTarget->flags.msaa = 0;
		auto msaaTarget = &normalMSAATargets.emplace_back();
		msaaTarget->format = target.format;
		msaaTarget->flags.preserve = target.flags.preserve;
		msaaTarget->flags.internal = 0;
		msaaTarget->flags.msaa = (target.flags.msaa && !target.flags.resolve) ? 1 : 0;

		// Get if the msaa target needs a partner
		RLTarget* intTarget{ nullptr };
		if (target.flags.msaa && target.flags.resolve) {
			intTarget = &internalTargets.emplace_back();
			intTarget->format = target.format;
			intTarget->flags.preserve = 0;
			intTarget->flags.internal = 1;
			intTarget->flags.msaa = 1;
			intTarget->partnerIndex = target.index;
			msaaTarget->partnerIndex = targetCount() + uint32(internalTargets.size() - 1);
		}

		// Populate the subpasses
		uint32 lastUseIndex{ 0 };
		RenderUse lastUseType{ RenderUse::None };
		for (uint32 si = 0; si < subpassCount(); ++si) {
			const auto use = target.uses[si];
			const auto& subpass = impl->subpasses_[si];
			auto& rlSubpass = normalSubpasses[si];
			auto& msaaSubpass = msaaSubpasses[si];
			const bool 
				inNormUse     = (si >= target.firstUseIndex) && ((si <= target.lastUseIndex) || target.flags.preserve),
				inResolveUse  = (si >= target.resolvePass) && ((si <= target.lastUseIndex) || target.flags.preserve),
				inInternalUse = (si >= target.firstUseIndex) && (si <= target.resolvePass),
				isResolving   = intTarget && (target.resolvePass == si);

			// Check: if not used, we need to check if the target is preserved
			if (use == RenderUse::None) {
				if (inNormUse) {
					rlSubpass.preserveTargets.push_back(target.index);
					if (!intTarget) {
						msaaSubpass.preserveTargets.push_back(target.index);
					}
				}
				if (intTarget && inInternalUse) {
					msaaSubpass.preserveTargets.push_back(msaaTarget->partnerIndex);
				}
				if (intTarget && inResolveUse) {
					msaaSubpass.preserveTargets.push_back(target.index);
				}
				continue;
			}

			// Switch on the usage
			if (RenderUse_isColor(use)) {
				const uint32 attIndex = (use == RenderUse::Color0) ? 0 : (use == RenderUse::Color1) ? 1 
					: (use == RenderUse::Color2) ? 2 : 3;
				rlSubpass.colorTargets[attIndex] = target.index;
				rlTarget->flags.output = 1;
				if (intTarget && inInternalUse) {
					msaaSubpass.colorTargets[attIndex] = msaaTarget->partnerIndex;
					intTarget->flags.output = 1;
				}
				else {
					msaaSubpass.colorTargets[attIndex] = target.index;
					msaaTarget->flags.output = 1;
				}
				if (isResolving) {
					msaaSubpass.resolveTargets[attIndex] = target.index;
					msaaSubpass.resolve = true;
					msaaTarget->flags.output = 1;
				}
			}
			else if (RenderUse_isInput(use)) {
				const uint32 attIndex = (use == RenderUse::Input0) ? 0 : (use == RenderUse::Input1) ? 1
					: (use == RenderUse::Input2) ? 2 : 3;
				rlSubpass.inputTargets[attIndex] = target.index;
				rlTarget->flags.input = 1;
				msaaSubpass.inputTargets[attIndex] = target.index; // Never uses partner as input, so no check needed
				msaaTarget->flags.input = 1;
			}
			else if (use == RenderUse::Depth) {
				rlSubpass.depthTarget = target.index;
				rlTarget->flags.output = 1;
				if (intTarget && inInternalUse) {
					msaaSubpass.depthTarget = msaaTarget->partnerIndex;
					intTarget->flags.output = 1;
				}
				else {
					msaaSubpass.depthTarget = target.index;
					msaaTarget->flags.output = 1;
				}
				if (isResolving) {
					msaaSubpass.depthResolveTarget = target.index;
					msaaSubpass.resolve = true;
				}
			}
			else {
				assert(false && "Invalid RenderUse for target in build()");
			}

			// Generate dependency
			if (lastUseType != RenderUse::None) {
				RLDep dep{};
				dep.srcSubpass = uint8(lastUseIndex);
				dep.dstSubpass = uint8(si);
				dep.srcUse = RenderUse_isColor(lastUseType) ? RLDep::Color : 
					RenderUse_isInput(lastUseType) ? RLDep::Input : RLDep::Depth;
				dep.dstUse = RenderUse_isColor(use) ? RLDep::Color : 
					RenderUse_isInput(use) ? RLDep::Input : RLDep::Depth;
				if (dep.srcUse != dep.dstUse) { // Back-to-back identical uses do not need a dependency
					dependencies.insert(dep);
				}
			}
			lastUseIndex = si;
			lastUseType = use;
		}

		// Set the last use
		rlTarget->lastUse = lastUseType;
		msaaTarget->lastUse = lastUseType;
		if (intTarget) {
			intTarget->lastUse = target.uses[target.resolvePass];
		}

		// Outgoing dependency for preserved targets
		if (target.flags.preserve) {
			RLDep dep{};
			dep.srcSubpass = uint8(lastUseIndex);
			dep.dstSubpass = UINT8_MAX; // Will be VK_SUBPASS_EXTERNAL
			dep.srcUse = RenderUse_isColor(lastUseType) ? RLDep::Color :
				RenderUse_isInput(lastUseType) ? RLDep::Input : RLDep::Depth;
			dep.dstUse = RLDep::Input;
			dependencies.insert(dep);
		}
	}

	// Construct and return the layout
	RenderLayoutImpl::MSAATargetVector msaaTargets{ };
	msaaTargets.assign(normalMSAATargets.begin(), normalMSAATargets.end());
	for (const auto& intTarget : internalTargets) {
		msaaTargets.push_back(intTarget);
	}
	return std::make_shared<RenderLayoutImpl>(
		std::move(normalTargets),
		std::move(msaaTargets),
		std::move(normalSubpasses),
		std::move(msaaSubpasses),
		std::move(dependencies)
	);
}

} // namespace vega
