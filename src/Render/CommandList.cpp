/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./CommandListImpl.hpp"
#include "./PipelineImpl.hpp"
#include "./Renderer/RendererImpl.hpp"
#include "./Renderer/RenderTargetImpl.hpp"
#include "../Graphics/Managers/PipelineManager.hpp"
#include "../Graphics/Managers/ResourceManager.hpp"
#include "../Graphics/Managers/BindingManager.hpp"
#include "../Graphics/ShaderImpl.hpp"
#include "../Graphics/Image/TextureImpl.hpp"
#include "../Graphics/Buffer/TexelBufferImpl.hpp"
#include "../Graphics/Buffer/StorageBufferImpl.hpp"
#include "../Graphics/Buffer/IndexBufferImpl.hpp"

#define ASSERT_RECORDING(func) \
	if (!isRecording()) { throw std::runtime_error("Cannot call " #func "() on an inactive CommandList"); }
#define ASSERT_PIPELINE(func) \
	if (!hasPipeline()) { throw std::runtime_error("Cannot call " #func "() without a bound pipeline"); }


namespace vega
{

#define THIS_IMPL_TYPE CommandListImpl

// ====================================================================================================================
CommandList::CommandList()
{

}

// ====================================================================================================================
CommandList::~CommandList()
{

}

// ====================================================================================================================
const Renderer* CommandList::renderer() const
{
	return THIS_IMPL->renderer_;
}

// ====================================================================================================================
uint32 CommandList::subpass() const
{
	return THIS_IMPL->subpass_;
}

// ====================================================================================================================
uint64 CommandList::frame() const
{
	return THIS_IMPL->frame_;
}

// ====================================================================================================================
bool CommandList::isRecording() const
{
	return !!THIS_IMPL->cmd_;
}

// ====================================================================================================================
void CommandList::usePipeline(const Pipeline& pipeline)
{
	ASSERT_RECORDING(usePipeline);

	const auto thisimpl = THIS_IMPL;

	// Check for duplicate
	const bool firstPipeline = thisimpl->state_.pipelineHash.value() == 0;
	if (get_impl(pipeline).hash() == thisimpl->state_.pipelineHash) {
		return; // Same pipeline already bound
	}

	// Lookup the pipeline handle
	const auto vkpipeline = PipelineManager::Get()->getPipeline(
		get_impl(pipeline), *thisimpl->renderer_, thisimpl->subpass_
	);
	vkpipeline->markUsed();

	// Bind pipeline handle
	thisimpl->cmd_.bindPipeline(vk::PipelineBindPoint::eGraphics, vkpipeline->handle());
	thisimpl->state_.pipelineHash = get_impl(pipeline).hash();
	thisimpl->state_.shader = get_impl(get_impl(pipeline).shader().get());

	// If binding the first pipeline, setup the initial dynamic state
	if (firstPipeline) {
		const auto& defvp = thisimpl->state_.viewport;
		vk::Viewport vkvp{ defvp.left(), defvp.top(), defvp.width(), defvp.height(), defvp.minDepth(), defvp.maxDepth() };
		const auto& defsc = thisimpl->state_.scissor;
		vk::Rect2D vksc{ { defsc.x, defsc.y }, { defsc.w, defsc.h } };
		thisimpl->cmd_.setViewport(0, 1, &vkvp);
		thisimpl->cmd_.setScissor(0, 1, &vksc);
		thisimpl->cmd_.setStencilReference(vk::StencilFaceFlagBits::eFront, thisimpl->state_.stencilFront);
		thisimpl->cmd_.setStencilReference(vk::StencilFaceFlagBits::eBack, thisimpl->state_.stencilBack);
		thisimpl->cmd_.bindDescriptorSets(
			vk::PipelineBindPoint::eGraphics,
			thisimpl->state_.shader->pipelineLayout(),
			0,
			BindingManager::Get()->globalTable(),
			{ }
		);
	}
}

// ====================================================================================================================
bool CommandList::hasPipeline() const
{
	return THIS_IMPL->state_.pipelineHash.value() != 0;
}

// ====================================================================================================================
void CommandList::setViewport(const Viewport& viewport)
{
	ASSERT_RECORDING(setViewport);

	THIS_IMPL->state_.viewport = viewport;
	if (hasPipeline()) {
		const vk::Viewport vkvp{
			viewport.left(), viewport.top(), viewport.width(), viewport.height(), viewport.minDepth(), 
			viewport.maxDepth()
		};
		THIS_IMPL->cmd_.setViewport(0, 1, &vkvp);
	}
}

// ====================================================================================================================
void CommandList::setScissor(const Scissor& scissor)
{
	ASSERT_RECORDING(setScissor);

	THIS_IMPL->state_.scissor = scissor;
	if (hasPipeline()) {
		const vk::Rect2D vksc{ { scissor.x, scissor.y }, { scissor.w, scissor.h } };
		THIS_IMPL->cmd_.setScissor(0, 1, &vksc);
	}
}

// ====================================================================================================================
void CommandList::setStencilReference(uint8 ref, bool front, bool back)
{
	ASSERT_RECORDING(setStencilReference);

	if (front) { 
		THIS_IMPL->state_.stencilFront = ref;
		if (hasPipeline()) {
			THIS_IMPL->cmd_.setStencilReference(vk::StencilFaceFlagBits::eFront, ref);
		}
	}
	if (back) { 
		THIS_IMPL->state_.stencilBack = ref; 
		if (hasPipeline()) {
			THIS_IMPL->cmd_.setStencilReference(vk::StencilFaceFlagBits::eBack, ref);
		}
	}
}

// ====================================================================================================================
void CommandList::bind(uint32 slot, const Texture& texture, Sampler sampler)
{
	ASSERT_RECORDING(bind(Texture));
	ASSERT_PIPELINE(bind(Texture));

	// Check the binding
	const auto bind = THIS_IMPL->state_.shader->findBinding(slot);
	if (!bind) {
		throw std::runtime_error(to_string("Cannot bind texture to unused slot %u", slot));
	}
	switch (texture.dimensions())
	{
	case Texture::Dims::D1: {
		if (bind->type() != BindingType::Sampler1D) {
			throw std::runtime_error(
				to_string("Invalid binding type, expected = %s, actual = Sampler1D", to_string(bind->type()).c_str())
			);
		}
	} break;
	case Texture::Dims::D2: {
		if (bind->type() != BindingType::Sampler2D) {
			throw std::runtime_error(
				to_string("Invalid binding type, expected = %s, actual = Sampler2D", to_string(bind->type()).c_str())
			);
		}
	} break;
	case Texture::Dims::D3: {
		if (bind->type() != BindingType::Sampler3D) {
			throw std::runtime_error(
				to_string("Invalid binding type, expected = %s, actual = Sampler3D", to_string(bind->type()).c_str())
			);
		}
	} break;
	default: assert(false && "Unhandled texture dimension in bindTexture()");
	}

	// Update the binding
	const auto index = get_impl(texture).getBindingIndex(sampler);
	if (index != THIS_IMPL->bindings_.indices[slot]) {
		THIS_IMPL->bindings_.indices[slot] = index;
		THIS_IMPL->bindings_.dirty = true;
	}

	// Mark used
	get_impl(texture).image()->resourceMarkUsed();
}

// ====================================================================================================================
void CommandList::bind(uint32 slot, const RenderTarget& target, Sampler sampler)
{
	ASSERT_RECORDING(bind(RenderTarget));
	ASSERT_PIPELINE(bind(RenderTarget));

	// Check the binding
	const auto bind = THIS_IMPL->state_.shader->findBinding(slot);
	if (!bind) {
		throw std::runtime_error(to_string("Cannot bind render target to unused slot %u", slot));
	}
	if (bind->type() != BindingType::Sampler2D) {
		throw std::runtime_error(
			to_string("Invalid binding type, expected = %s, actual = Sampler2D(RT)", to_string(bind->type()).c_str())
		);
	}

	// Update the binding
	const auto [index, wait] = get_impl(target).getSamplerResult(sampler);
	if (index != THIS_IMPL->bindings_.indices[slot]) {
		THIS_IMPL->bindings_.indices[slot] = index;
		THIS_IMPL->bindings_.dirty = true;

		if (target.renderer()->isRecording()) {
			THIS_IMPL->renderer_->addDependent(target.renderer());
		}
	}
}

// ====================================================================================================================
void CommandList::bind(uint32 slot, const TexelBuffer& buffer)
{
	ASSERT_RECORDING(bind(TexelBuffer));
	ASSERT_PIPELINE(bind(TexelBuffer));

	// Check the binding
	const auto bind = THIS_IMPL->state_.shader->findBinding(slot);
	if (!bind) {
		throw std::runtime_error(to_string("Cannot bind texel buffer to unused slot %u", slot));
	}
	if (bind->type() != BindingType::ROTexels) {
		throw std::runtime_error(
			to_string("Invalid binding type, expected = %s, actual = ROTexels", to_string(bind->type()).c_str())
		);
	}

	// Update the binding
	const auto index = get_impl(buffer).bindingIndex();
	if (index != THIS_IMPL->bindings_.indices[slot]) {
		THIS_IMPL->bindings_.indices[slot] = index;
		THIS_IMPL->bindings_.dirty = true;
	}

	// Mark used
	get_impl(buffer).buffer()->resourceMarkUsed();
}

// ====================================================================================================================
void CommandList::bind(uint32 slot, const StorageBuffer& buffer)
{
	ASSERT_RECORDING(bind(StorageBuffer));
	ASSERT_PIPELINE(bind(StorageBuffer));

	// Check the binding
	const auto bind = THIS_IMPL->state_.shader->findBinding(slot);
	if (!bind) {
		throw std::runtime_error(to_string("Cannot bind storage buffer to unused slot %u", slot));
	}
	if (bind->type() != BindingType::ROBuffer) {
		throw std::runtime_error(
			to_string("Invalid binding type, expected = %s, actual = ROBuffer", to_string(bind->type()).c_str())
		);
	}

	// Update the binding
	const auto index = get_impl(buffer).bindingIndex();
	if (index != THIS_IMPL->bindings_.indices[slot]) {
		THIS_IMPL->bindings_.indices[slot] = index;
		THIS_IMPL->bindings_.dirty = true;
	}

	// Mark used
	get_impl(buffer).buffer()->resourceMarkUsed();
}

// ====================================================================================================================
void* CommandList::allocateUniformData(uint32 size, bool zero)
{
	ASSERT_RECORDING(allocateUniformData);
	ASSERT_PIPELINE(allocateUniformData);

	auto& uniforms = get_impl(this)->uniform_;

	// Validate
	if (size == 0) {
		throw std::runtime_error("Cannot request uniform data of size zero");
	}
	if (size > MAX_UNIFORM_SIZE) {
		throw std::runtime_error(to_string("Cannot request uniform data larger than %u", MAX_UNIFORM_SIZE));
	}
	if (uniforms.userDirty && (size <= uniforms.userSlot.size)) {
		// No draw call since the last uniform slot, so we don't need to allocate a new one
		return uniforms.userSlot.dataPtr;
	}

	// Get new slot, store, and return
	UniformManager::Get()->allocateDataSlot(uniforms.userSlot, size);
	uniforms.userDirty = true;
	if (zero) {
		std::memset(uniforms.userSlot.dataPtr, 0, uniforms.userSlot.size);
	}
	return uniforms.userSlot.dataPtr;
}

// ====================================================================================================================
void CommandList::setCameraParams(const Camera3D& camera)
{
	ASSERT_RECORDING(setCameraParams);

	get_impl(this)->updateGlobals(&camera);
}

// ====================================================================================================================
void CommandList::bindVertexBuffers(const std::initializer_list<const VertexBuffer*> buffers, bool unbindAll)
{
	ASSERT_RECORDING(bindVertexBuffers);
	ASSERT_PIPELINE(bindVertexBuffers);

	auto& verts = THIS_IMPL->vertex_;

	// size == 0 will unbind all buffers
	if (unbindAll || (buffers.size() == 0)) {
		const CommandListImpl::VertexAttrib ZERO_ATTR{ ResourceManager::Get()->getZeroBuffer(), 0, 0 };
		verts.attribs.fill(ZERO_ATTR);
		verts.enabled = { };
		verts.dirty = true;
		if (!unbindAll) {
			return; // size == 0 -> unbind and return
		}
	}

	// Update the buffer bindings
	const auto shader = THIS_IMPL->state_.shader;
	for (const auto buf : buffers) {
		if (!buf) {
			throw std::runtime_error("Cannot bind a null vertex buffer to a CommandList");
		}
		const auto bufimpl = get_impl(buf);
		bufimpl->buffer()->resourceMarkUsed();

		// Loop over each element in the buffer
		for (const auto& elem : buf->layout().elements()) {
			const auto input = shader->findInput(elem.use());
			if (!input) {
				continue; // Skip elements that are unused
			}

			auto& attrib = verts.attribs[input->location()];
			attrib.buffer = bufimpl->buffer()->handle();
			attrib.offset = elem.offset();
			attrib.stride = buf->layout().stride();
			verts.enabled |= elem.use();
		}
	}

	// Mark dirty
	verts.dirty = true;
}

// ====================================================================================================================
void CommandList::unbindVertexUse(VertexUse use)
{
	ASSERT_RECORDING(unbindVertexUse);
	ASSERT_PIPELINE(unbindVertexUse);

	// Get the input
	const auto input = THIS_IMPL->state_.shader->findInput(use);
	if (input) {
		auto& verts = THIS_IMPL->vertex_;
		verts.enabled.clear(use);
		verts.dirty = true;

		auto& attrib = verts.attribs[input->location()];
		attrib.buffer = ResourceManager::Get()->getZeroBuffer();
		attrib.offset = 0;
		attrib.stride = 0;
	}
}

// ====================================================================================================================
void CommandList::draw(uint32 vertCount, uint32 instCount, uint32 firstVert, uint32 firstInst)
{
	ASSERT_RECORDING(draw);
	ASSERT_PIPELINE(draw);

	const auto impl = THIS_IMPL;

	// Update bindings and resources if needed
	if (impl->bindings_.dirty) {
		impl->updateBindings();
	}
	if (impl->vertex_.dirty) {
		impl->updateVertexBuffers();
	}
	if (impl->uniform_.globalDirty || impl->uniform_.userDirty) {
		impl->updateUniforms();
	}

	// Submit the draw command
	impl->cmd_.draw(vertCount, instCount, firstVert, firstInst);
}

// ====================================================================================================================
void CommandList::drawIndexed(const IndexBuffer& buffer, uint32 indexCount, uint32 instCount, uint32 firstIndex, 
	int32 vertOffset, uint32 firstInst)
{
	ASSERT_RECORDING(drawIndexed);
	ASSERT_PIPELINE(drawIndexed);

	const auto impl = THIS_IMPL;

	// Update bindings and resources if needed
	if (impl->bindings_.dirty) {
		impl->updateBindings();
	}
	if (impl->vertex_.dirty) {
		impl->updateVertexBuffers();
	}
	if (impl->uniform_.globalDirty || impl->uniform_.userDirty) {
		impl->updateUniforms();
	}

	// Bind the index buffer and submit the draw command
	get_impl(buffer).buffer()->resourceMarkUsed();
	impl->cmd_.bindIndexBuffer(get_impl(buffer).buffer()->handle(), 0, vk::IndexType(buffer.indexType().value()));
	impl->cmd_.drawIndexed(indexCount, instCount, firstIndex, vertOffset, firstInst);
}

} // namespace vega
