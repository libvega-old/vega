/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <Vega/Render/Pipeline.hpp>
#include <Vega/Math/Hash.hpp>


namespace vega
{

// Implementation of Pipeline
class PipelineImpl final
	: public Pipeline
{
	friend class Pipeline;

public:
	PipelineImpl(AssetHandle<Shader> shader, const RenderState& state);
	~PipelineImpl();

	inline Hash64 hash() const { return hash_; }

private:
	const AssetHandle<Shader> shader_;
	const RenderState state_;
	const Hash64 hash_;

	VEGA_NO_COPY(PipelineImpl)
	VEGA_NO_MOVE(PipelineImpl)
}; // class PipelineImpl

DECLARE_IMPL_GETTER(Pipeline)

} // namespace vega
