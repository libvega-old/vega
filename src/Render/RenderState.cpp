/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Render/RenderState.hpp>

#include <array>


namespace vega
{

// ====================================================================================================================
const String& to_string(Topology topo)
{
	static const std::array<String, uint32(Topology::TriangleFan) + 1> NAMES{
		"PointList", "LineList", "LineStrip", "TriangleList", "TriangleStrip", "TriangleFan"
	};
	return NAMES.at(uint32(topo));
}


// ====================================================================================================================
// ====================================================================================================================
const String& to_string(FillMode mode)
{
	static const std::array<String, uint32(FillMode::Points) + 1> NAMES{
		"Solid", "Lines", "Points"
	};
	return NAMES.at(uint32(mode));
}


// ====================================================================================================================
// ====================================================================================================================
const String& to_string(CullMode mode)
{
	static const std::array<String, uint32(CullMode::FrontAndBack) + 1> NAMES{
		"None", "Front", "Back", "FrontAndBack"
	};
	return NAMES.at(uint32(mode));
}


// ====================================================================================================================
// ====================================================================================================================
// Compact collection of render pipeline states
union RenderStateSet final
{
public:
	RenderStateSet() : _raw_{ 0, 0, 0 } { }

	struct State
	{
		ColorBlend color0;        // off: 0.0
		ColorBlend color1;        // off: 0.32
		ColorBlend color2;        // off: 1.0
		ColorBlend color3;        // off: 1.32
		StencilMode stencilFront; // off: 2.0
		StencilMode stencilBack;  // off: 2.16

		uint32 color0Enable : 1;  // off: 2.32
		uint32 color1Enable : 1;  // off: 2.33
		uint32 color2Enable : 1;  // off: 2.34
		uint32 color3Enable : 1;  // off: 2.35

		uint32 depthMode : 2;     // off: 2.36
		uint32 depthOp : 3;       // off: 2.38
		uint32 topology : 3;      // off: 2.41
		uint32 primRestart : 1;   // off: 2.44
		uint32 fill : 2;          // off: 2.45
		uint32 cull : 2;          // off: 2.47
	} state;
	uint64 _raw_[3];
}; // union RenderStateSet
static_assert(sizeof(ColorBlend) == 4, "Invalid ColorBlend size for RenderStateSet");
static_assert(sizeof(StencilMode) == 2, "Invalid StencilMode size for RenderStateSet");
static_assert(sizeof(RenderStateSet) == 24, "Unexpected RenderStateSet size");


#define STATE_SET (std::launder(reinterpret_cast<RenderStateSet*>(&state_)))
#define CSTATE_SET (std::launder(reinterpret_cast<const RenderStateSet*>(&state_)))
// ====================================================================================================================
// ====================================================================================================================
RenderState::RenderState()
	: state_{ }
{
	static_assert(sizeof(StateStorage) == sizeof(RenderStateSet), "Mismatch of render state storage and type size");
	static_assert(alignof(StateStorage) == alignof(RenderStateSet), "Mismatch of render state storage and type align");

	// Setup defaults
	auto set = STATE_SET;
	set->state.color0 = ColorBlends::None;
	set->state.color1 = ColorBlends::None;
	set->state.color2 = ColorBlends::None;
	set->state.color3 = ColorBlends::None;
	set->state.stencilFront = StencilModes::None;
	set->state.stencilBack = StencilModes::None;
	set->state.color0Enable = 0;
	set->state.color1Enable = 0;
	set->state.color2Enable = 0;
	set->state.color3Enable = 0;
	set->state.depthMode = uint32(DepthMode::ReadWrite);
	set->state.depthOp = uint32(CompareOp::Less);
	set->state.topology = uint32(Topology::TriangleList);
	set->state.primRestart = 0;
	set->state.fill = uint32(FillMode::Solid);
	set->state.cull = uint32(CullMode::None);
}

// ====================================================================================================================
RenderState::~RenderState()
{

}

// ====================================================================================================================
Opt<ColorBlend> RenderState::color0() const
{
	return CSTATE_SET->state.color0Enable ? CSTATE_SET->state.color0 : Opt<ColorBlend>{ };
}

// ====================================================================================================================
Opt<ColorBlend> RenderState::color1() const
{
	return CSTATE_SET->state.color1Enable ? CSTATE_SET->state.color1 : Opt<ColorBlend>{ };
}

// ====================================================================================================================
Opt<ColorBlend> RenderState::color2() const
{
	return CSTATE_SET->state.color2Enable ? CSTATE_SET->state.color2 : Opt<ColorBlend>{ };
}

// ====================================================================================================================
Opt<ColorBlend> RenderState::color3() const
{
	return CSTATE_SET->state.color3Enable ? CSTATE_SET->state.color3 : Opt<ColorBlend>{ };
}

// ====================================================================================================================
StencilMode RenderState::stencilFront() const
{
	return CSTATE_SET->state.stencilFront;
}

// ====================================================================================================================
StencilMode RenderState::stencilBack() const
{
	return CSTATE_SET->state.stencilBack;
}

// ====================================================================================================================
DepthMode RenderState::depthMode() const
{
	return DepthMode(CSTATE_SET->state.depthMode);
}

// ====================================================================================================================
CompareOp RenderState::depthOp() const
{
	return CompareOp(CSTATE_SET->state.depthOp);
}

// ====================================================================================================================
Topology RenderState::topology() const
{
	return Topology(CSTATE_SET->state.topology);
}

// ====================================================================================================================
bool RenderState::primitiveRestart() const
{
	return CSTATE_SET->state.primRestart != 0;
}

// ====================================================================================================================
FillMode RenderState::fillMode() const
{
	return FillMode(CSTATE_SET->state.fill);
}

// ====================================================================================================================
CullMode RenderState::cullMode() const
{
	return CullMode(CSTATE_SET->state.cull);
}

// ====================================================================================================================
RenderState& RenderState::color0(Opt<ColorBlend> blend)
{
	STATE_SET->state.color0 = blend.value_or(ColorBlends::None);
	STATE_SET->state.color0Enable = blend.has_value() ? 1 : 0;
	return *this;
}

// ====================================================================================================================
RenderState& RenderState::color1(Opt<ColorBlend> blend)
{
	STATE_SET->state.color1 = blend.value_or(ColorBlends::None);
	STATE_SET->state.color1Enable = blend.has_value() ? 1 : 0;
	return *this;
}

// ====================================================================================================================
RenderState& RenderState::color2(Opt<ColorBlend> blend)
{
	STATE_SET->state.color2 = blend.value_or(ColorBlends::None);
	STATE_SET->state.color2Enable = blend.has_value() ? 1 : 0;
	return *this;
}

// ====================================================================================================================
RenderState& RenderState::color3(Opt<ColorBlend> blend)
{
	STATE_SET->state.color3 = blend.value_or(ColorBlends::None);
	STATE_SET->state.color3Enable = blend.has_value() ? 1 : 0;
	return *this;
}

// ====================================================================================================================
RenderState& RenderState::stencilFront(StencilMode mode)
{
	STATE_SET->state.stencilFront = mode;
	return *this;
}

// ====================================================================================================================
RenderState& RenderState::stencilBack(StencilMode mode)
{
	STATE_SET->state.stencilBack = mode;
	return *this;
}

// ====================================================================================================================
RenderState& RenderState::depthMode(DepthMode mode)
{
	STATE_SET->state.depthMode = uint32(mode);
	return *this;
}

// ====================================================================================================================
RenderState& RenderState::depthOp(CompareOp compare)
{
	STATE_SET->state.depthOp = uint32(compare);
	return *this;
}

// ====================================================================================================================
RenderState& RenderState::topology(Topology topology)
{
	STATE_SET->state.topology = uint32(topology);
	return *this;
}

// ====================================================================================================================
RenderState& RenderState::primitiveRestart(bool restart)
{
	STATE_SET->state.primRestart = restart ? 1 : 0;
	return *this;
}

// ====================================================================================================================
RenderState& RenderState::fillMode(FillMode mode)
{
	STATE_SET->state.fill = uint32(mode);
	return *this;
}

// ====================================================================================================================
RenderState& RenderState::cullMode(CullMode mode)
{
	STATE_SET->state.cull = uint32(mode);
	return *this;
}

// ====================================================================================================================
uint64 RenderState::raw0() const
{
	return CSTATE_SET->_raw_[0];
}

// ====================================================================================================================
uint64 RenderState::raw1() const
{
	return CSTATE_SET->_raw_[1];
}

// ====================================================================================================================
uint64 RenderState::raw2() const
{
	return CSTATE_SET->_raw_[2];
}

} // namespace vega
