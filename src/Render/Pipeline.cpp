/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */


#include "../Common.Impl.hpp"
#include "./PipelineImpl.hpp"


namespace vega
{

#define THIS_IMPL_TYPE PipelineImpl

// ====================================================================================================================
Pipeline::Pipeline()
{

}

// ====================================================================================================================
Pipeline::~Pipeline()
{

}

// ====================================================================================================================
AssetHandle<Shader> Pipeline::shader() const
{
	return THIS_IMPL->shader_;
}

// ====================================================================================================================
const RenderState& Pipeline::renderState() const
{
	return THIS_IMPL->state_;
}

} // namespace vega
