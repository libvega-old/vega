/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Framebuffer.hpp"
#include "../../Graphics/Managers/BindingManager.hpp"


namespace vega
{

// ====================================================================================================================
Framebuffer::Framebuffer(const SPtr<RenderLayout>& layout, MSAA msaa, const Vec2ui& extent, vk::RenderPass renderPass)
	: layout_{ layout }
	, msaa_{ msaa }
	, extent_{ extent }
	, frameIndex_{ 0 }
	, handles_{ }
	, targets_{ }
	, bindingMutex_{ }
{
	constexpr vk::ComponentMapping IDENTITY_MAPPING{ };
	constexpr vk::ImageSubresourceRange COLOR_SUBRESOURCE{ vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1 };
	constexpr vk::ImageSubresourceRange DEPTH_SUBRESOURCE{ vk::ImageAspectFlagBits::eDepth, 0, 1, 0, 1 };
	constexpr vk::ImageSubresourceRange DEPTH_STENCIL_SUBRESOURCE{ 
		vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil, 0, 1, 0, 1 
	};

	const auto vkdevice = Vulkan::Device();
	const bool isMSAA = msaa != MSAA::X1;
	const auto& limpl = get_impl(*layout);
	const auto targetCount = isMSAA ? limpl.msaaTargets().size() : limpl.targets().size();

	// Create the targets
	for (uint32 ti = 0; ti < targetCount; ++ti) {
		const auto& rltarget = isMSAA ? limpl.msaaTargets()[ti] : limpl.targets()[ti];

		// Describe the image
		vk::ImageCreateInfo ici{};
		ici.imageType = vk::ImageType::e2D;
		ici.format = vk::Format(rltarget.format.formatId());
		ici.extent = vk::Extent3D{ extent.x, extent.y, 1 };
		ici.mipLevels = 1;
		ici.arrayLayers = 1;
		ici.samples = rltarget.flags.msaa ? vk::SampleCountFlagBits(msaa) : vk::SampleCountFlagBits::e1;
		ici.tiling = vk::ImageTiling::eOptimal;
		ici.usage = GetImageUsage(rltarget);
		ici.sharingMode = vk::SharingMode::eExclusive;
		ici.initialLayout = vk::ImageLayout::eUndefined;

		// Describe the image view
		vk::ImageViewCreateInfo ivci{};
		ivci.image = nullptr; // Populated in loop
		ivci.viewType = vk::ImageViewType::e2D;
		ivci.format = ici.format;
		ivci.components = IDENTITY_MAPPING;
		ivci.subresourceRange =
			rltarget.format.isColor() ? COLOR_SUBRESOURCE :
			rltarget.format.hasStencil() ? DEPTH_STENCIL_SUBRESOURCE : DEPTH_SUBRESOURCE;

		// Create an image for each frame (if preserved), or just once (if not preserved)
		targets_[ti].count = uint32(rltarget.flags.preserve ? Vulkan::MAX_PARALLEL_FRAMES : 1);
		for (uint32 fi = 0; fi < targets_[ti].count; ++fi) {
			// Create the objects and bind the memory
			const auto image = vkdevice.createImage(ici);
			auto memory = bool(ici.usage & vk::ImageUsageFlagBits::eTransientAttachment)
				? MemoryManager::Get()->allocateTransient(image)
				: MemoryManager::Get()->allocateDevice(image);
			vkdevice.bindImageMemory(image, memory->memory(), memory->offset());
			ivci.image = image;
			const auto view = vkdevice.createImageView(ivci);

			// Store to target
			targets_[ti].images[fi].image = image;
			targets_[ti].images[fi].view = view;
			targets_[ti].images[fi].memory = std::move(memory);
			targets_[ti].images[fi].bindings.fill(UINT16_MAX);
		}
	}

	// Create the framebuffers
	std::array<vk::ImageView, MAX_TARGETS> views{ };
	for (uint32 fi = 0; fi < Vulkan::MAX_PARALLEL_FRAMES; ++fi) {
		// Get the views for the frame
		for (uint32 ti = 0; ti < targetCount; ++ti) {
			const auto ii = fi % targets_[ti].count;
			views[ti] = targets_[ti].images[ii].view;
		}

		// Create the object
		vk::FramebufferCreateInfo fci{};
		fci.renderPass = renderPass;
		fci.attachmentCount = uint32(targetCount);
		fci.pAttachments = views.data();
		fci.width = extent.x;
		fci.height = extent.y;
		fci.layers = 1;
		handles_[fi] = vkdevice.createFramebuffer(fci);
	}
}

// ====================================================================================================================
Framebuffer::~Framebuffer()
{
	const auto vkdev = Vulkan::Device();
	vkdev.waitIdle();

	// Clean the framebuffers
	for (auto fb : handles_) {
		if (fb) {
			vkdev.destroyFramebuffer(fb);
		}
	}

	// Clean the targets
	const auto bindmgr = BindingManager::Get();
	for (auto& targ : targets_) {
		for (auto& img : targ.images) {
			if (img.view) {
				vkdev.destroyImageView(img.view);
			}
			if (img.image) {
				vkdev.destroyImage(img.image);
			}
			if (img.memory) {
				img.memory.reset();
				for (const auto binding : img.bindings) {
					if (binding != UINT16_MAX) {
						bindmgr->freeSampler(binding);
					}
				}
			}
		}
	}
}

// ====================================================================================================================
vk::Framebuffer Framebuffer::nextFrame()
{
	frameIndex_ = (frameIndex_ + 1u) % handles_.size();
	return handles_[frameIndex_];
}

// ====================================================================================================================
const Framebuffer::Image* Framebuffer::currentImage(uint32 index, Opt<Sampler> sampler) const
{
	auto& target = targets_[index];
	auto& image = target.images[frameIndex_ % target.count];

	if (sampler.has_value()) {
		const auto sampidx = uint32(sampler.value());

		std::lock_guard lock{ bindingMutex_ };
		if (image.bindings[sampidx] == UINT16_MAX) {
			const auto samp = ResourceManager::Get()->getSampler(sampler.value());
			image.bindings[sampidx] = BindingManager::Get()->reserveSampler(image.view, samp);
		}
	}

	return &image;
}

// ====================================================================================================================
vk::Framebuffer Framebuffer::currentFramebuffer() const
{
	return handles_[frameIndex_];
}

// ====================================================================================================================
vk::ImageUsageFlags Framebuffer::GetImageUsage(const RenderLayoutImpl::Target& target)
{
	vk::ImageUsageFlags flags{ };
	if (target.flags.output) {
		flags |= (target.format.isColor() ? 
			vk::ImageUsageFlagBits::eColorAttachment : vk::ImageUsageFlagBits::eDepthStencilAttachment);
	}
	if (target.flags.input) {
		flags |= vk::ImageUsageFlagBits::eInputAttachment;
	}
	if (target.flags.preserve) {
		flags |= (vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eTransferSrc);
	}
	else if (MemoryManager::Get()->hasTransient()) { // && !preserve (implied)
		flags |= vk::ImageUsageFlagBits::eTransientAttachment;
	}
	return flags;
}

} // namespace vega
