/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./RenderTargetImpl.hpp"
#include "./RendererImpl.hpp"


namespace vega
{

// ====================================================================================================================
RenderTargetImpl::RenderTargetImpl(const RendererImpl* renderer, uint32 target)
	: RenderTarget()
	, renderer_{ renderer }
	, target_{ target }
{

}

// ====================================================================================================================
RenderTargetImpl::~RenderTargetImpl()
{

}

// ====================================================================================================================
std::tuple<vk::Image, CommandFence::Wait> RenderTargetImpl::getWindowResult() const
{
	const auto image = renderer_->framebuffer()->currentImage(target_, std::nullopt);
	return { image->image, renderer_->activeWait() };
}

// ====================================================================================================================
std::tuple<uint16, CommandFence::Wait> RenderTargetImpl::getSamplerResult(Sampler sampler) const
{
	const auto image = renderer_->framebuffer()->currentImage(target_, sampler);
	return { image->bindings[uint32(sampler)], renderer_->activeWait() };
}

} // namespace vega
