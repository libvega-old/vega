/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include <Vega/Render/RenderTarget.hpp>
#include <Vega/Render/Sampler.hpp>
#include "../../Graphics/Vulkan.hpp"
#include "../../Graphics/Managers/CommandManager.hpp"


namespace vega
{

class RendererImpl;

// Implementation of RenderTarget
class RenderTargetImpl final
	: public RenderTarget
{
	friend class RenderTarget;

public:
	RenderTargetImpl(const RendererImpl* renderer, uint32 target);
	~RenderTargetImpl();

	// Used by swapchain for window presentation operations
	std::tuple<vk::Image, CommandFence::Wait> getWindowResult() const;
	// Used by command lists for offscreen rendering
	std::tuple<uint16, CommandFence::Wait> getSamplerResult(Sampler sampler) const;

private:
	const RendererImpl* const renderer_;
	const uint32 target_;

	VEGA_NO_COPY(RenderTargetImpl)
	VEGA_NO_MOVE(RenderTargetImpl)
}; // class RenderTargetImpl

DECLARE_IMPL_GETTER(RenderTarget)

} // namespace vega
