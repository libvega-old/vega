/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include <Vega/Render/Renderer.hpp>
#include <Vega/Math/Hash.hpp>
#include <Vega/Core/InlineVector.hpp>
#include "./Framebuffer.hpp"
#include "./RenderTargetImpl.hpp"
#include "../../Graphics/Managers/CommandManager.hpp"
#include "../../Graphics/Vulkan.hpp"

#include <atomic>
#include <array>
#include <thread>
#include <set>


namespace vega
{

// Implementation of Renderer
class RendererImpl final
	: public Renderer
{
	friend class Renderer;

public:
	RendererImpl(const SPtr<RenderLayout>& layout, const Vec2ui& extent, MSAA msaa, bool window);
	~RendererImpl();

	inline uint32 uid() const { return uid_; }
	inline bool isWindow() const { return window_; }
	inline vk::RenderPass renderPass() const { return renderPass_; }
	inline const Framebuffer* framebuffer() const { return framebuffer_.get(); }

	inline Hash64 subpassHash(uint32 subpassIndex) const { return subpassHashes_[subpassIndex]; }

	void beginImpl();
	void endImpl();
	void addDependent(const Renderer* renderer);

	CommandFence::Wait activeWait() const;

	inline static uint32 ActiveOffscreenCount() { return ActiveCount_.load(); }

protected:
	void rebuildHash();

private:
	const uint32 uid_;
	const SPtr<RenderLayout> layout_;
	const bool window_;
	vk::RenderPass renderPass_;
	UPtr<Framebuffer> framebuffer_;
	std::array<UPtr<RenderTargetImpl>, RenderLayout::MAX_SUBPASSES> renderTargets_;
	std::array<Hash64, RenderLayout::MAX_SUBPASSES> subpassHashes_;
	InlineVector<vk::ClearValue, RenderLayout::MAX_SUBPASSES * 2> clearValues_;
	std::set<const Renderer*> depRenderers_;
	struct
	{
		std::thread::id thread{ };
		vk::CommandBuffer buffer{ };
		uint32 subpass{ 0 };
		uint64 lastBeginFrame{ 0 };
		SPtr<CommandFence> signalFence{ };
		CommandFence::Wait lastFenceWait{ };
	} cmd_;

	static std::atomic_uint32_t ActiveCount_;
	static std::atomic_uint32_t UID_;

	VEGA_NO_COPY(RendererImpl)
	VEGA_NO_MOVE(RendererImpl)
}; // class RendererImpl

DECLARE_IMPL_GETTER(Renderer)

} // namespace vega
