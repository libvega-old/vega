/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./RendererImpl.hpp"
#include <Vega/Core/Time.hpp>


namespace vega
{

// ====================================================================================================================
std::atomic_uint32_t RendererImpl::ActiveCount_{ 0 };
std::atomic_uint32_t RendererImpl::UID_{ 0 };

// ====================================================================================================================
RendererImpl::RendererImpl(const SPtr<RenderLayout>& layout, const Vec2ui& extent, MSAA msaa, bool window)
	: Renderer()
	, uid_{ UID_.fetch_add(1) }
	, layout_{ layout }
	, window_{ window }
	, renderPass_{ }
	, framebuffer_{ }
	, renderTargets_{ }
	, subpassHashes_{ }
	, clearValues_{ }
	, depRenderers_{ }
	, cmd_{ }
{
	constexpr std::array<float, 4> COLOR_BLACK{ 0, 0, 0, 1 };

	// Create the initial render pass and framebuffer
	renderPass_ = get_impl(*layout).createRenderPass(msaa);
	framebuffer_ = std::make_unique<Framebuffer>(layout, msaa, extent, renderPass_);

	// Create the render targets
	uint32 ti{ 0 };
	for (const auto& target : get_impl(*layout).targets()) {
		if (target.flags.preserve) {
			renderTargets_[ti] = std::make_unique<RenderTargetImpl>(this, ti);
		}
		++ti;
	}

	// Default clear values
	for (const auto& target : get_impl(*layout).msaaTargets()) {
		if (target.format.isColor()) {
			clearValues_.push_back(vk::ClearColorValue{ COLOR_BLACK });
		}
		else {
			clearValues_.push_back(vk::ClearDepthStencilValue{ 1.0f, 0 });
		}
	}

	// Initial command objects
	cmd_.signalFence = std::make_shared<CommandFence>();

	// Setup the initial hash
	rebuildHash();
}

// ====================================================================================================================
RendererImpl::~RendererImpl()
{
	const auto vkdev = Vulkan::Device();
	vkdev.waitIdle();

	// Cleanup command objects
	cmd_.signalFence.reset();

	// Cleanup render targets
	for (auto& target : renderTargets_) {
		target.reset();
	}

	// Cleanup the renderpass and framebuffer
	framebuffer_.reset();
	if (renderPass_) {
		vkdev.destroyRenderPass(renderPass_);
	}
}

// ====================================================================================================================
void RendererImpl::beginImpl()
{
	// Check state
	if (isRecording()) {
		throw std::runtime_error("Cannot call begin() on a Renderer that is already recording");
	}
	if (cmd_.lastBeginFrame == Time::FrameCount()) {
		throw std::runtime_error("Cannot call begin() on a Renderer more than once per frame");
	}

	// Prepare command values
	cmd_.thread = std::this_thread::get_id();
	cmd_.buffer = CommandManager::Get()->allocateFrameCommand(vk::CommandBufferLevel::ePrimary);
	cmd_.subpass = 0;
	cmd_.lastBeginFrame = Time::FrameCount();

	// Start the commands
	vk::CommandBufferBeginInfo cbbi{};
	cbbi.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;
	cmd_.buffer.begin(cbbi);

	// Start the render pass
	vk::RenderPassBeginInfo rpbi{};
	rpbi.renderPass = renderPass_;
	rpbi.framebuffer = framebuffer_->nextFrame();
	rpbi.renderArea = vk::Rect2D{ { 0, 0 }, { framebuffer_->extent().x, framebuffer_->extent().y } };
	rpbi.clearValueCount = framebuffer_->targetCount();
	rpbi.pClearValues = clearValues_.data();
	cmd_.buffer.beginRenderPass(rpbi, vk::SubpassContents::eSecondaryCommandBuffers);

	// Update global counter
	if (!window_) {
		ActiveCount_.fetch_add(1);
	}
}

// ====================================================================================================================
void RendererImpl::endImpl()
{
	// Check state
	if (!isRecording()) {
		throw std::runtime_error("Cannot call end() on a Renderer that is not recording");
	}
	if (cmd_.subpass != (layout_->subpassCount() - 1)) {
		throw std::runtime_error("Cannot call end() on a Renderer that has not processed all of its subpasses");
	}
	if (cmd_.thread != std::this_thread::get_id()) {
		throw std::runtime_error("Cannot call end() on a Renderer from a different thread than start()");
	}

	// Check dependent renderers
	for (const auto dep : depRenderers_) {
		if (dep->isRecording()) {
			throw std::runtime_error("Cannot end a Renderer while dependent offscreen renderers are recording");
		}
	}
	depRenderers_.clear();

	// End render pass and buffer
	cmd_.buffer.endRenderPass();
	cmd_.buffer.end();

	// Submit the commands for execution
	CommandManager::Get()->submitGraphics(
		{ cmd_.buffer }, { }, { CommandFence::Signal{ *cmd_.signalFence, &cmd_.lastFenceWait } }
	);

	// Clear command values
	cmd_.buffer = nullptr;
	cmd_.subpass = 0;

	// Update global counter
	if (!window_) {
		ActiveCount_.fetch_sub(1);
	}
}

// ====================================================================================================================
void RendererImpl::addDependent(const Renderer* renderer)
{
	depRenderers_.insert(renderer);
}

// ====================================================================================================================
CommandFence::Wait RendererImpl::activeWait() const
{
	if (isRecording()) {
		return { *cmd_.signalFence, cmd_.signalFence->useCount() + 1 }; // Need to look ahead to what the wait will be
	}
	else {
		return cmd_.lastFenceWait;
	}
}

// ====================================================================================================================
void RendererImpl::rebuildHash()
{
	const auto& layout = get_impl(*layout_);
	const auto isMsaa = msaa() != MSAA::X1;

	// Recalculate the hash for each subpass
	for (uint32 si = 0; si < layout_->subpassCount(); ++si) {
		const auto& subpass = isMsaa ? layout.msaaSubpasses()[si] : layout.subpasses()[si];
		const auto msaaVal = subpass.msaa ? uint64(msaa()) : 1;
		InlineVector<uint64, RenderLayoutImpl::MAX_TARGETS * 2> data{ }; // * 2 is more than worst case scenario

		// Use the hash data     (index << 56) | (msaa << 48) | (use << 32) | u64(format)
		uint64 ti{ 0 };
		for (uint32 ii = 0; ii < subpass.inputCount; ++ii, ++ti) {
			const auto targetIndex = subpass.inputTargets[ii];
			const auto& target = isMsaa ? layout.msaaTargets()[targetIndex] : layout.targets()[targetIndex];
			data.push_back(
				(ti << 56) | (1ull << 48) | (uint64(RenderUse::Input0) << 32) | target.format.formatId()
			);
		}
		for (uint32 ci = 0; ci < subpass.colorCount; ++ci, ++ti) {
			const auto targetIndex = subpass.colorTargets[ci];
			const auto& target = isMsaa ? layout.msaaTargets()[targetIndex] : layout.targets()[targetIndex];
			data.push_back(
				(ti << 56) | (msaaVal << 48) | (uint64(RenderUse::Color0) << 32) | target.format.formatId()
			);
		}
		if (subpass.depthTarget != VK_ATTACHMENT_UNUSED) {
			const auto& target = 
				isMsaa ? layout.msaaTargets()[subpass.depthTarget] : layout.targets()[subpass.depthTarget];
			data.push_back(
				(ti++ << 56) | (msaaVal << 48) | (uint64(RenderUse::Depth) << 32) | target.format.formatId()
			);
		}
		if (subpass.resolve) {
			for (uint32 ri = 0; ri < subpass.colorCount; ++ri, ++ti) {
				const auto targetIndex = subpass.resolveTargets[ri];
				if (targetIndex == VK_ATTACHMENT_UNUSED) {
					continue;
				}
				const auto& target = isMsaa ? layout.msaaTargets()[targetIndex] : layout.targets()[targetIndex];
				data.push_back(
					(ti << 56) | (1ull << 48) | (uint64(RenderUse::Color0) << 32) | target.format.formatId()
				);
			}
			if (subpass.depthResolveTarget != VK_ATTACHMENT_UNUSED) {
				const auto& target = isMsaa ? 
					layout.msaaTargets()[subpass.depthResolveTarget] : layout.targets()[subpass.depthResolveTarget];
				data.push_back(
					(ti++ << 56) | (1ull << 48) | (uint64(RenderUse::Depth) << 32) | target.format.formatId()
				);
			}
		}

		// Build the hash for the subpass
		subpassHashes_[si] = Hash64::XXHash64(data.data(), uint32(data.size() * sizeof(uint64)));
	}
}

} // namespace vega
