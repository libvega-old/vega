/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../../Common.Impl.hpp"
#include "../RenderLayoutImpl.hpp"
#include "../../Graphics/Managers/MemoryManager.hpp"
#include "../../Graphics/Managers/ResourceManager.hpp"
#include <Vega/Graphics/Multisampling.hpp>
#include <Vega/Math/Vec2.hpp>

#include <array>
#include <mutex>


namespace vega
{

// Manages a set of images for a Renderer instance, with double-buffering as needed
class Framebuffer final
{
public:
	// Max targets per framebuffer (max per Renderer * 2 for internal attachments)
	inline static constexpr uint32 MAX_TARGETS{ RenderLayout::MAX_TARGETS * 2 };

	struct Image final
	{
		vk::Image image{ };
		vk::ImageView view{ };
		MemoryHandle memory{ };
		mutable std::array<uint16, ResourceManager::SAMPLER_COUNT> bindings{ };
	}; // struct Image

	struct Target final
	{
		uint32 count{ 0 };
		std::array<Image, Vulkan::MAX_PARALLEL_FRAMES> images{ };
	}; // struct Target

	Framebuffer(const SPtr<RenderLayout>& layout, MSAA msaa, const Vec2ui& extent, vk::RenderPass renderPass);
	~Framebuffer();

	inline MSAA msaa() const { return msaa_; }
	inline const Vec2ui& extent() const { return extent_; }
	inline uint32 targetCount() const {
		return uint32((msaa_ != MSAA::X1) 
			? get_impl(*layout_).msaaTargets().size() 
			: get_impl(*layout_).targets().size());
	}

	vk::Framebuffer nextFrame();
	const Image* currentImage(uint32 index, Opt<Sampler> sampler) const;
	vk::Framebuffer currentFramebuffer() const;

	static vk::ImageUsageFlags GetImageUsage(const RenderLayoutImpl::Target& target);

private:
	const SPtr<RenderLayout> layout_;
	const MSAA msaa_;
	const Vec2ui extent_;
	uint32 frameIndex_;
	std::array<vk::Framebuffer, Vulkan::MAX_PARALLEL_FRAMES> handles_;
	std::array<Target, MAX_TARGETS> targets_;
	mutable std::mutex bindingMutex_;

	VEGA_NO_COPY(Framebuffer)
	VEGA_NO_MOVE(Framebuffer)
}; // class Framebuffer

} // namespace vega
