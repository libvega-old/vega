/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */


#include "./PipelineImpl.hpp"
#include "../Graphics/ShaderImpl.hpp"


namespace vega
{

// ====================================================================================================================
PipelineImpl::PipelineImpl(AssetHandle<Shader> shader, const RenderState& state)
	: Pipeline()
	, shader_{ shader }
	, state_{ state }
	, hash_{ Hash64::XXHash64({ get_impl(*shader).resourceId(), state.raw0(), state.raw1(), state.raw2() }) }
{

}

// ====================================================================================================================
PipelineImpl::~PipelineImpl()
{

}

} // namespace vega
