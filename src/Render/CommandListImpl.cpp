/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./CommandListImpl.hpp"
#include "../Graphics/Managers/CommandManager.hpp"
#include "../Graphics/Managers/ResourceManager.hpp"
#include "./Renderer/RendererImpl.hpp"
#include <Vega/Core/Time.hpp>


namespace vega
{

// ====================================================================================================================
CommandListImpl::CommandListImpl(RendererImpl* renderer, uint32 subpass)
	: CommandList()
	, renderer_{ renderer }
	, subpass_{ subpass }
	, frame_{ Time::FrameCount() }
	, cmd_{ CommandManager::Get()->allocateFrameCommand(vk::CommandBufferLevel::eSecondary) }
	, state_{ }
	, bindings_{ }
	, vertex_{ }
	, uniform_{ }
{
	// Begin the commands
	vk::CommandBufferInheritanceInfo cbii{};
	cbii.renderPass = renderer->renderPass();
	cbii.subpass = subpass;
	cbii.framebuffer = renderer->framebuffer()->currentFramebuffer();
	vk::CommandBufferBeginInfo cbbi{};
	cbbi.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit | vk::CommandBufferUsageFlagBits::eRenderPassContinue;
	cbbi.pInheritanceInfo = &cbii;
	cmd_.begin(cbbi);

	// Default state
	state_.pipelineHash = { 0 };
	state_.shader = nullptr;
	state_.scissor = renderer->defaultScissor();
	state_.viewport = renderer->defaultViewport();
	state_.stencilFront = 0;
	state_.stencilBack = 0;
	bindings_.indices.fill(UINT16_MAX);
	bindings_.dirty = false;
	const VertexAttrib ZERO_ATTR{ ResourceManager::Get()->getZeroBuffer(), 0, 0 };
	vertex_.attribs.fill(ZERO_ATTR);
	vertex_.enabled = { };
	vertex_.dirty = false;
	updateGlobals(nullptr);
}

// ====================================================================================================================
CommandListImpl::~CommandListImpl()
{

}

// ====================================================================================================================
void CommandListImpl::updateBindings()
{
	// Set the push constants for the bindings
	const uint32 bindCount = uint32(state_.shader->bindings().size());
	const uint32 pushSize = (((bindCount - 1) / 2) + 1) * 4;
	cmd_.pushConstants(
		state_.shader->pipelineLayout(), vk::ShaderStageFlags(state_.shader->stageMask().rawValue()), 
		0, pushSize, &bindings_.indices[0]
	);
	bindings_.dirty = false;
}

// ====================================================================================================================
void CommandListImpl::updateVertexBuffers()
{
	// Populate the vertex info
	std::array<vk::Buffer, ShaderImpl::MAX_VERTEX_ATTRIB_COUNT> buffers{};
	std::array<uint64, ShaderImpl::MAX_VERTEX_ATTRIB_COUNT> offsets{};
	std::array<uint64, ShaderImpl::MAX_VERTEX_ATTRIB_COUNT> strides{};
	for (uint32 bi = 0; bi < state_.shader->inputs().size(); ++bi) {
		const auto& attr = vertex_.attribs[bi];
		buffers[bi] = attr.buffer;
		offsets[bi] = attr.offset;
		strides[bi] = attr.stride;
	}

	// Bind the vertex buffers
	cmd_.bindVertexBuffers2EXT(
		0, uint32(state_.shader->inputs().size()),
		buffers.data(),
		offsets.data(),
		nullptr,
		strides.data()
	);
	vertex_.dirty = false;
}

// ====================================================================================================================
void CommandListImpl::updateUniforms()
{
	// Bind the updated set
	cmd_.bindDescriptorSets(
		vk::PipelineBindPoint::eGraphics,
		state_.shader->pipelineLayout(),
		1,
		uniform_.globalSlot.set,
		{ uniform_.globalSlot.offset, uniform_.userSlot.offset }
	);
	uniform_.globalDirty = false;
	uniform_.userDirty = false;
}

// ====================================================================================================================
void CommandListImpl::updateGlobals(const Camera3D* camera)
{
	// Allocate new global slot if needed
	if (!uniform_.globalDirty) {
		UniformManager::Get()->allocateDataSlot(uniform_.globalSlot, sizeof(GlobalShaderData));
	}

	// Update global uniform data
	auto glob = reinterpret_cast<GlobalShaderData*>(uniform_.globalSlot.dataPtr);
	glob->time = float(Time::Elapsed().seconds());
	glob->deltaTime = { float(Time::Delta().seconds()), float(Time::RealDelta().seconds()) };
	glob->cameraPos = camera ? camera->position() : Vec3f::Zero;
	glob->cameraDir = camera ? (camera->target() - camera->position()).normalized() : Vec3f::Zero;
	glob->viewProj = camera ? camera->viewProjection() : Mat4f::Identity;
	glob->target = {
		renderer_->extent().x, renderer_->extent().y, 1.f / renderer_->extent().x, 1.f / renderer_->extent().y
	};
	if (camera) {
		const auto [near, far] = camera->depth();
		glob->depth = { near, far };
	}
	else {
		glob->depth = { 0, 1 };
	}

	uniform_.globalDirty = true;
}

} // namespace vega
