/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Renderer/RendererImpl.hpp"
#include "../Graphics/GraphicsEngineImpl.hpp"
#include "./CommandListImpl.hpp"


namespace vega
{

#define THIS_IMPL_TYPE RendererImpl

// ====================================================================================================================
Renderer::Renderer()
{

}

// ====================================================================================================================
Renderer::~Renderer()
{

}

// ====================================================================================================================
const SPtr<RenderLayout>& Renderer::layout() const
{
	return THIS_IMPL->layout_;
}

// ====================================================================================================================
bool Renderer::isWindow() const
{
	return THIS_IMPL->window_;
}
 
// ====================================================================================================================
MSAA Renderer::msaa() const
{
	return THIS_IMPL->framebuffer_->msaa();
}

// ====================================================================================================================
const Vec2ui& Renderer::extent() const
{
	return THIS_IMPL->framebuffer_->extent();
}

// ====================================================================================================================
void Renderer::setSurfaceParams(Opt<Vec2ui> extent, Opt<MSAA> msaa)
{
	const bool extentChanged = extent.has_value() && (extent.value() != this->extent());
	const bool msaaChanged = msaa.has_value() && (msaa.value() != this->msaa());
	if (!extentChanged && !msaaChanged) {
		return;
	}

	// Validate parameters
	const auto props = GraphicsEngine::Get()->device()->properties();
	if (msaaChanged) {
		if ((msaa.value() != MSAA::X1) && !THIS_IMPL->layout_->supportsMSAA()) {
			throw std::runtime_error("Cannot use MSAA operations on a Renderer that does not support MSAA");
		}
		if (!bool(props->supportedMSAA & msaa.value())) {
			throw std::runtime_error(
				to_string("Cannot use unsupported MSAA '%s' on Renderer", to_string(msaa.value()).c_str()));
		}
	}
	if (extentChanged) {
		if (extent.value().x > props->maxFramebufferWidth || extent.value().x == 0) {
			throw std::runtime_error(to_string("Invalid Renderer width %u, valid range: [1, %u]",
				extent.value().x, props->maxFramebufferWidth));
		}
		if (extent.value().y > props->maxFramebufferHeight || extent.value().y == 0) {
			throw std::runtime_error(to_string("Invalid Renderer height %u, valid range: [1, %u]",
				extent.value().y, props->maxFramebufferHeight));
		}
	}

	// Update the RenderPass only if there is an MSAA change
	const auto vkdev = Vulkan::Device();
	vkdev.waitIdle();
	if (msaaChanged) {
		vkdev.destroyRenderPass(THIS_IMPL->renderPass_);
		THIS_IMPL->renderPass_ = get_impl(*layout()).createRenderPass(msaa.value());
	}

	// Update the framebuffer if either changed
	const auto newMSAA = msaa.value_or(THIS_IMPL->framebuffer_->msaa());
	const auto newExtent = extent.value_or(THIS_IMPL->framebuffer_->extent());
	THIS_IMPL->framebuffer_.reset();
	THIS_IMPL->framebuffer_ = std::make_unique<Framebuffer>(layout(), newMSAA, newExtent, THIS_IMPL->renderPass_);

	// Rebuild the subpass hash for pipelines
	THIS_IMPL->rebuildHash();
}

// ====================================================================================================================
void Renderer::begin()
{
	if (isWindow()) {
		throw std::runtime_error("Cannot call begin() directly on a window Renderer");
	}
	THIS_IMPL->beginImpl();
}

// ====================================================================================================================
void Renderer::end()
{
	if (isWindow()) {
		throw std::runtime_error("Cannot call end() directly on a window Renderer");
	}
	THIS_IMPL->endImpl();
}

// ====================================================================================================================
void Renderer::nextSubpass()
{
	// Validate
	if (!isRecording()) {
		throw std::runtime_error("Cannot call nextSubpass() on a Renderer that is not recording");
	}
	if (currentSubpass() == (layout()->subpassCount() - 1)) {
		throw std::runtime_error("Cannot call nextSubpass() on a Renderer that is on the last subpass");
	}
	if (THIS_IMPL->cmd_.thread != std::this_thread::get_id()) {
		throw std::runtime_error("Cannot call nextSubpass() on a Renderer from a different thread than start()");
	}

	// Move to next pass
	THIS_IMPL->cmd_.subpass += 1;
	THIS_IMPL->cmd_.buffer.nextSubpass(vk::SubpassContents::eSecondaryCommandBuffers);
}

// ====================================================================================================================
bool Renderer::isRecording() const
{
	return !!THIS_IMPL->cmd_.buffer;
}

// ====================================================================================================================
uint32 Renderer::currentSubpass() const
{
	return THIS_IMPL->cmd_.subpass;
}

// ====================================================================================================================
UPtr<CommandList> Renderer::startCommandList(uint32 subpass)
{
	// Validate
	if (!isRecording()) {
		throw std::runtime_error("Cannot call startCommandList() on a Renderer that is not recording");
	}
	if (subpass >= layout()->subpassCount()) {
		throw std::runtime_error("Cannot call startCommandList() on an invalid subpass index");
	}
	if (currentSubpass() > subpass) {
		throw std::runtime_error("Cannot call startCommandList() for a subpass that has already been recorded");
	}

	// Create (todo: maybe cache in the future?)
	return std::make_unique<CommandListImpl>(THIS_IMPL, subpass);
}

// ====================================================================================================================
void Renderer::commit(CommandList& list)
{
	// Validate
	if (!isRecording()) {
		throw std::runtime_error("Cannot call commit() on a Renderer that is not recording");
	}
	if (list.renderer() != this) {
		throw std::runtime_error("Cannot call commit() with a CommandList from a different Renderer");
	}
	if (list.subpass() != currentSubpass()) {
		throw std::runtime_error("Cannot call commit() with a CommandList from a different subpass than the active one");
	}
	if (!list.isRecording()) {
		throw std::runtime_error("Cannot call commit() with a CommandList that has already been submitted");
	}

	// Submit the list
	auto& impl = get_impl(list);
	impl.cmd_.end();
	THIS_IMPL->cmd_.buffer.executeCommands(impl.cmd_);

	// Mark the list as used
	impl.cmd_ = nullptr;
}

// ====================================================================================================================
const RenderTarget* Renderer::getRenderTarget(uint32 targetIndex) const
{
	const auto& layout = get_impl(*this->layout());
	if (targetIndex >= layout.targets().size()) {
		throw std::runtime_error("Invalid render target index (out of range)");
	}
	if (!layout.targets()[targetIndex].flags.preserve) {
		throw std::runtime_error("Invalid render target index (target not preserved)");
	}
	return THIS_IMPL->renderTargets_[targetIndex].get();
}

// ====================================================================================================================
void Renderer::setClearColor(uint32 targetIndex, Color color)
{
	const auto& layout = get_impl(*this->layout());
	const auto& targets = layout.msaaTargets();
	if (targetIndex < layout.targets().size() && targets[targetIndex].format.isColor()) {
		vk::ClearColorValue colorValue{ std::array<float, 4>{ color.rf(), color.gf(), color.bf(), color.af() }};
		THIS_IMPL->clearValues_[targetIndex] = colorValue;
		if (targets[targetIndex].partnerIndex != 0) {
			THIS_IMPL->clearValues_[targets[targetIndex].partnerIndex] = colorValue;
		}
	}
}

// ====================================================================================================================
void Renderer::setClearDepthStencilValue(uint32 targetIndex, float depth, uint32 stencil)
{
	const auto& layout = get_impl(*this->layout());
	const auto& targets = layout.msaaTargets();
	if (targetIndex < layout.targets().size() && targets[targetIndex].format.isDepth()) {
		vk::ClearDepthStencilValue dsValue{ depth, stencil };
		THIS_IMPL->clearValues_[targetIndex] = dsValue;
		if (targets[targetIndex].partnerIndex != 0) {
			THIS_IMPL->clearValues_[targets[targetIndex].partnerIndex] = dsValue;
		}
	}
}

} // namespace vega
