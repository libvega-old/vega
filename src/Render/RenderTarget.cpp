/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Renderer/RenderTargetImpl.hpp"
#include "./Renderer/RendererImpl.hpp"


namespace vega
{

#define THIS_IMPL_TYPE RenderTargetImpl

// ====================================================================================================================
RenderTarget::RenderTarget()
{

}

// ====================================================================================================================
RenderTarget::~RenderTarget()
{

}

// ====================================================================================================================
const Renderer* RenderTarget::renderer() const
{
	return static_cast<const Renderer*>(THIS_IMPL->renderer_);
}

// ====================================================================================================================
uint32 RenderTarget::target() const
{
	return THIS_IMPL->target_;
}

} // namespace vega
