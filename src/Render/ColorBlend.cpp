/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Render/ColorBlend.hpp>

#include <array>


namespace vega
{

// ====================================================================================================================
const String& to_string(BlendOp value)
{
	static const std::array<String, uint32(BlendOp::Max) + 1> NAMES{
		"Add", "Subtract", "InvSubtract", "Min", "Max"
	};
	return NAMES.at(uint32(value));
}


// ====================================================================================================================
// ====================================================================================================================
const String& to_string(BlendFactor value)
{
	static const std::array<String, uint32(BlendFactor::AlphaSaturate) + 1> NAMES{
		"Zero", "One", "SrcColor", "InvSrcColor", "DstColor", "InvDstColor", "SrcAlpha", "InvSrcAlpha", "DstAlpha",
		"InvDstAlpha", "ConstColor", "InvConstColor", "ConstAlpha", "InvConstAlpha", "AlphaSaturate"
	};
	return NAMES.at(uint32(value));
}


// ====================================================================================================================
// ====================================================================================================================
String to_string(ColorChannelMask value)
{
	char str[4];
	uint32 len{ 0 };
	if (bool(value & ColorChannel::R)) str[len++] = 'R';
	if (bool(value & ColorChannel::G)) str[len++] = 'G';
	if (bool(value & ColorChannel::B)) str[len++] = 'B';
	if (bool(value & ColorChannel::A)) str[len++] = 'A';
	return String(str, len);

}

} // namespace vega
