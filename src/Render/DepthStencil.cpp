/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Render/DepthStencil.hpp>

#include <array>


namespace vega
{

// ====================================================================================================================
const String& to_string(CompareOp op)
{
	static const std::array<String, uint32(CompareOp::Always) + 1> NAMES{
		"Never", "Less", "Equal", "LessEqual", "Greater", "NotEqual", "GreaterEqual", "Always"
	};
	return NAMES.at(uint32(op));
}


// ====================================================================================================================
// ====================================================================================================================
const String& to_string(DepthMode mode)
{
	static const std::array<String, uint32(DepthMode::ReadWrite) + 1> NAMES{
		"Disabled", "ReadOnly", "ReadWrite"
	};
	return NAMES.at(uint32(mode));
}


// ====================================================================================================================
// ====================================================================================================================
const String& to_string(StencilOp op)
{
	static const std::array<String, uint32(StencilOp::DecWrap) + 1> NAMES{
		"Keep", "Zero", "Replace", "IncClamp", "DecClamp", "Invert", "IncWrap", "DecWrap"
	};
	return NAMES.at(uint32(op));
}

} // namespace vega
