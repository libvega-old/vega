/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Render/Camera3D.hpp>


namespace vega
{

// ====================================================================================================================
Camera3D::Camera3D()
	: view_{ }
	, proj_{ }
	, viewProj_{ }
{

}

// ====================================================================================================================
const Mat4f& Camera3D::view() const
{
	if (view_.dirty) {
		if (math::abs(view_.roll) <= std::numeric_limits<float>::epsilon()) {
			view_.matrix = Mat4f::LookAt(view_.position, view_.target, Vec3f::Up);
		}
		else {
			const auto rotMat = Mat4f::RotationAxis(view_.target - view_.position, view_.roll);
			view_.matrix = Mat4f::LookAt(view_.position, view_.target, rotMat * Vec3f::Up);
		}
		view_.dirty = false;
	}
	return view_.matrix;
}

// ====================================================================================================================
const Mat4f& Camera3D::projection() const
{
	if (proj_.dirty) {
		if (isPerspective()) {
			const auto params = std::get_if<PerspectiveParams>(&proj_.params);
			proj_.matrix = Mat4f::Perspective(params->fov, params->aspect, params->near, params->far);
		}
		else {
			const auto params = std::get_if<OrthographicParams>(&proj_.params);
			proj_.matrix = Mat4f::Orthographic(params->width, params->height, params->near, params->far);
		}
		proj_.dirty = false;
	}
	return proj_.matrix;
}

// ====================================================================================================================
const Mat4f& Camera3D::viewProjection() const
{
	if (viewProj_.dirty) {
		viewProj_.matrix = view() * projection();
		viewProj_.frustum.matrix(viewProj_.matrix);
		viewProj_.dirty = false;
	}
	return viewProj_.matrix;
}

// ====================================================================================================================
const Frustum& Camera3D::frustum() const
{
	if (viewProj_.dirty) {
		viewProj_.matrix = view() * projection();
		viewProj_.frustum.matrix(viewProj_.matrix);
		viewProj_.dirty = false;
	}
	return viewProj_.frustum;
}

// ====================================================================================================================
std::tuple<float, float> Camera3D::depth() const
{
	if (isPerspective()) {
		const auto params = std::get_if<PerspectiveParams>(&proj_.params);
		return { params->near, params->far };
	}
	else {
		const auto params = std::get_if<OrthographicParams>(&proj_.params);
		return { params->near, params->far };
	}
}


// ====================================================================================================================
// ====================================================================================================================
ArcBallController::ArcBallController(const Vec3f& target)
	: Camera3DController()
	, params_{ }
	, min_{ }
	, max_{ }
{
	static constexpr Rad<float> ALT_LIMIT{ 89.5f * math::deg_2_rad<float> };
	static constexpr float LOWEST{ std::numeric_limits<float>::lowest() };
	static constexpr float HIGHEST{ std::numeric_limits<float>::max() };

	// Setup default values
	params_.dist = 1;
	min_ = { -ALT_LIMIT, LOWEST, LOWEST, 0.001f };
	max_ = { ALT_LIMIT, HIGHEST, HIGHEST, HIGHEST };
}

// ====================================================================================================================
void ArcBallController::updateCamera()
{
	const Vec3f diff{
		float(double(params_.dist) * math::cos(params_.altitude) * math::cos(params_.azimuth)),
		float(double(params_.dist) * math::sin(params_.altitude)),
		float(double(params_.dist) * math::cos(params_.altitude) * math::sin(params_.azimuth))
	};
	mutCamera().position(camera().target() + diff);
	mutCamera().roll(params_.roll);
}


// ====================================================================================================================
// ====================================================================================================================
FPSController::FPSController(Rad<float> yaw, Rad<float> pitch)
	: Camera3DController()
	, pitch_{ math::clamp(pitch, -PITCH_LIMIT, PITCH_LIMIT), -PITCH_LIMIT, PITCH_LIMIT }
	, yaw_{ math::angle_wrap(yaw) }
{

}

// ====================================================================================================================
void FPSController::updateCamera()
{
	const Vec3f dir{
		float(math::cos(pitch_.value) * math::cos(yaw_)),
		float(math::sin(pitch_.value)),
		float(math::cos(pitch_.value) * math::sin(yaw_))
	};
	mutCamera().target(camera().position() + dir);
}

} // namespace vega
