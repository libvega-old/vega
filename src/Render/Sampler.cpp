/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Render/Sampler.hpp>

#include <array>


namespace vega
{

// ====================================================================================================================
const String& to_string(Sampler sampler)
{
	static const std::array<String, uint32(Sampler::LinearLinearTransparent) + 1> NAMES{
		"NearNearRepeat", "NearNearMirror", "NearNearEdge", "NearNearBlack", "NearNearTransparent",
		"NearLinearRepeat", "NearLinearMirror", "NearLinearEdge", "NearLinearBlack", "NearLinearTransparent",
		"LinearNearRepeat", "LinearNearMirror", "LinearNearEdge", "LinearNearBlack", "LinearNearTransparent",
		"LinearLinearRepeat", "LinearLinearMirror", "LinearLinearEdge", "LinearLinearBlack", "LinearLinearTransparent"
	};
	return NAMES.at(uint32(sampler));
}

} // namespace vega
