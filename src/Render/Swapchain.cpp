/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./Swapchain.hpp"
#include "../Core/WindowImpl.hpp"
#include "../Core/GLFW.hpp"
#include "./Renderer/RenderTargetImpl.hpp"

#include <algorithm>


namespace vega
{

// ====================================================================================================================
static constexpr std::array<vk::SurfaceFormatKHR, 2> BEST_FORMATS{
	vk::SurfaceFormatKHR{ vk::Format::eB8G8R8A8Unorm, vk::ColorSpaceKHR::eSrgbNonlinear },
	vk::SurfaceFormatKHR{ vk::Format::eR8G8B8A8Unorm, vk::ColorSpaceKHR::eSrgbNonlinear }
};
static constexpr vk::ComponentMapping IDENTITY_MAPPING{ };
static const vk::ClearColorValue DEFAULT_CLEAR_COLOR{ std::array<float, 4>{ 0.15f, 0.15f, 0.15f, 1.0f } };
static constexpr vk::ImageSubresourceRange COLOR_SUBRESOURCE{ vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1 };


// ====================================================================================================================
Swapchain::Swapchain(WindowImpl* window)
	: window_{ window }
	, surface_{ }
	, swapchain_{ }
	, sync_{ }
{
	// Create and check the surface
	VkSurfaceKHR handleRaw{};
	GLFWwindow* windowHandle{ window->handle() };
	const vk::Result res{ glfwCreateWindowSurface(Vulkan::Instance(), windowHandle, nullptr, &handleRaw) };
	if (res != vk::Result::eSuccess) {
		throw std::runtime_error(to_string("Failed to create window surface - %s", vk::to_string(res).c_str()));
	}
	surface_.handle = { handleRaw };
	if (!Vulkan::PhysicalDevice().getSurfaceSupportKHR(CommandManager::Get()->graphicsFamily(), surface_.handle)) {
		throw std::runtime_error("Selected device does not support surface presentation");
	}
	Vulkan::SetObjectName(surface_.handle, to_string("Surface%u", window->index()));

	// Get the surface information
	const auto formats = Vulkan::PhysicalDevice().getSurfaceFormatsKHR(surface_.handle);
	const auto modes = Vulkan::PhysicalDevice().getSurfacePresentModesKHR(surface_.handle);
	surface_.format = formats[0];
	for (const auto& pfmt : BEST_FORMATS) {
		if (std::find(formats.begin(), formats.end(), pfmt) != formats.end()) {
			surface_.format = pfmt;
			break;
		}
	}
	surface_.mode = vk::PresentModeKHR::eFifo;
	surface_.hasImmediate = std::find(modes.begin(), modes.end(), vk::PresentModeKHR::eImmediate) != modes.end();
	surface_.hasMailbox = std::find(modes.begin(), modes.end(), vk::PresentModeKHR::eMailbox) != modes.end();

	// Initial build
	rebuild();
}

// ====================================================================================================================
Swapchain::~Swapchain()
{
	destroySwapchain();

	// Cleanup surface
	if (surface_.handle) {
		Vulkan::Device().waitIdle();
		Vulkan::Instance().destroySurfaceKHR(surface_.handle);
	}
}

// ====================================================================================================================
void Swapchain::vsync(bool vsync)
{
	if (vsync == this->vsync()) {
		return;
	}

	const auto old = surface_.mode;
	surface_.mode =
		(vsync || vsyncOnly()) ? vk::PresentModeKHR::eFifo :
		surface_.hasMailbox ? vk::PresentModeKHR::eMailbox : vk::PresentModeKHR::eImmediate;
	swapchain_.dirty = swapchain_.dirty || (old != surface_.mode);
}

// ====================================================================================================================
void Swapchain::present(const RenderTarget& target)
{
	static constexpr vk::PipelineStageFlags WAIT_STAGE{ vk::PipelineStageFlagBits::eTransfer };
	static constexpr vk::ImageSubresourceLayers COLOR_LAYERS{ vk::ImageAspectFlagBits::eColor, 0, 0, 1 };

	// Get submit objects
	const auto renderSem = sync_.renderSemaphores[sync_.syncIndex];
	const auto acquireSem = sync_.acquireSemaphores[sync_.syncIndex];

	// Switch on minimized state
	if (swapchain_.minimized) {
		// If minimized, just process one frame at a time
		// Use a no-work command buffer to track frame progression
		sync_.lastFence.wait();
		sync_.lastFence = CommandManager::Get()->submitGraphicsCommand([](vk::CommandBuffer cmd) { 
			cmd.pipelineBarrier(
				vk::PipelineStageFlagBits::eBottomOfPipe,
				vk::PipelineStageFlagBits::eTopOfPipe,
				{ },
				{ }, { }, { }
			);
			return true; 
		});

		// Rebuild if no longer minimized
		SurfaceInfo info{};
		getSurfaceInfo(info);
		swapchain_.minimized = (info.extent == Vec2ui::Zero);
		if (!swapchain_.minimized) {
			rebuild();
		}
		return;
	}
	else {
		const auto currImg = swapchain_.images[swapchain_.imageIndex].image;
		const auto [srcImg, renderWait] = get_impl(target).getWindowResult();

		sync_.lastFence = CommandManager::Get()->submitGraphicsCommand(
			[blitSrc=srcImg, blitDst=currImg, &target, this](vk::CommandBuffer cmd) {
				vk::ImageMemoryBarrier targetBar{}; // Barrier for render target image
				targetBar.srcAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
				targetBar.dstAccessMask = vk::AccessFlagBits::eTransferRead;
				targetBar.oldLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
				targetBar.newLayout = vk::ImageLayout::eTransferSrcOptimal;
				targetBar.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				targetBar.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				targetBar.image = blitSrc;
				targetBar.subresourceRange = COLOR_SUBRESOURCE;
				vk::ImageMemoryBarrier swapBar{}; // Barrier for swapchain image
				swapBar.srcAccessMask = { };
				swapBar.dstAccessMask = vk::AccessFlagBits::eTransferWrite;
				swapBar.oldLayout = vk::ImageLayout::eUndefined;
				swapBar.newLayout = vk::ImageLayout::eTransferDstOptimal;
				swapBar.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				swapBar.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
				swapBar.image = blitDst;
				swapBar.subresourceRange = COLOR_SUBRESOURCE;

				// Src barrier
				cmd.pipelineBarrier(
					vk::PipelineStageFlagBits::eColorAttachmentOutput,
					vk::PipelineStageFlagBits::eTransfer,
					{ },
					{ }, { }, { targetBar, swapBar }
				);

				// Blit command
				vk::ImageBlit blit{};
				blit.srcSubresource = COLOR_LAYERS;
				blit.srcOffsets[0] = vk::Offset3D{ 0, 0, 0 };
				blit.srcOffsets[1] = 
					vk::Offset3D{ int32(target.renderer()->extent().x), int32(target.renderer()->extent().y), 1 };
				blit.dstSubresource = COLOR_LAYERS;
				blit.dstOffsets[0] = vk::Offset3D{ 0, 0, 0 };
				blit.dstOffsets[1] = vk::Offset3D{ int32(swapchain_.extent.x), int32(swapchain_.extent.y), 1 };
				cmd.blitImage(
					blitSrc,
					vk::ImageLayout::eTransferSrcOptimal,
					blitDst,
					vk::ImageLayout::eTransferDstOptimal,
					{ blit },
					vk::Filter::eLinear
				);

				// Dst barrier (render target cannot be used again, so we don't worry about swapping back)
				swapBar.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
				swapBar.dstAccessMask = { };
				swapBar.oldLayout = vk::ImageLayout::eTransferDstOptimal;
				swapBar.newLayout = vk::ImageLayout::ePresentSrcKHR;
				cmd.pipelineBarrier(
					vk::PipelineStageFlagBits::eTransfer,
					vk::PipelineStageFlagBits::eBottomOfPipe,
					{ },
					{ }, { }, { swapBar }
				);

				return true;
			},
			{ CommandManager::SemaphoreWait{ acquireSem, WAIT_STAGE }, CommandFence::DeviceWait{ renderWait, WAIT_STAGE } },
			{ renderSem }
		);

		swapchain_.images[swapchain_.imageIndex].mappedFence = sync_.lastFence;
	}

	// Describe presentation
	vk::PresentInfoKHR pi{};
	pi.waitSemaphoreCount = 1;
	pi.pWaitSemaphores = &renderSem;
	pi.swapchainCount = 1;
	pi.pSwapchains = &swapchain_.handle;
	pi.pImageIndices = &(swapchain_.imageIndex);

	// Present and handle result
	const auto res = CommandManager::Get()->present(pi);
	sync_.syncIndex = uint32((sync_.syncIndex + 1ull) % sync_.acquireSemaphores.size());
	if (swapchain_.dirty || (res == vk::Result::eSuboptimalKHR) || (res == vk::Result::eErrorOutOfDateKHR)) {
		rebuild(); // Also acquires
	}
	else if (res != vk::Result::eSuccess) {
		throw std::runtime_error(
			to_string("Failed to present swapchain to window surface (%s)", vk::to_string(res).c_str()));
	}
	else if (vk::Result acqRes{}; !acquire(acqRes)) {
		throw std::runtime_error(
			to_string("Failed to acquire window surface after presentation (%s)", vk::to_string(acqRes).c_str()));
	}
}

// ====================================================================================================================
void Swapchain::getSurfaceInfo(SurfaceInfo& info) const
{
	const auto caps = Vulkan::PhysicalDevice().getSurfaceCapabilitiesKHR(surface_.handle);
	if (caps.currentExtent.width != UINT32_MAX) {
		info.extent = { caps.currentExtent.width, caps.currentExtent.height };
	}
	else {
		info.extent = window_->size().clamp(
			Vec2ui{ caps.minImageExtent.width, caps.minImageExtent.height },
			Vec2ui{ caps.maxImageExtent.width, caps.maxImageExtent.height }
		);
	}
	info.minImageCount = caps.minImageCount;
	info.transform = caps.currentTransform;
}

// ====================================================================================================================
void Swapchain::rebuild()
{
	const auto vkdevice = Vulkan::Device();

	// Get surface info
	SurfaceInfo info{};
	getSurfaceInfo(info);

	// Cancel rebuild if window is minimized
	swapchain_.minimized = (info.extent == Vec2ui::Zero);
	if (swapchain_.minimized) {
		sync_.lastFence.wait(); // Prevents the swapchain from immediately being used in an invalid state
		return;
	}

	// Prepare new swapchain info
	vk::SwapchainCreateInfoKHR scci{};
	scci.surface = surface_.handle;
	scci.minImageCount = info.minImageCount; // Look into if count + 1 might be better in some cases
	scci.imageFormat = surface_.format.format;
	scci.imageColorSpace = surface_.format.colorSpace;
	scci.imageExtent = vk::Extent2D{ info.extent.x, info.extent.y };
	scci.imageArrayLayers = 1;
	scci.imageUsage = vk::ImageUsageFlagBits::eTransferDst;
	scci.imageSharingMode = vk::SharingMode::eExclusive;
	scci.preTransform = info.transform;
	scci.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
	scci.presentMode = surface_.mode;
	scci.oldSwapchain = swapchain_.handle;

	// Wait idle before rebuild
	vkdevice.waitIdle();
	sync_.lastFence = { };

	// Create new swapchain and get images, then destroy old objects
	const auto newHandle = vkdevice.createSwapchainKHR(scci);
	const auto newImages = vkdevice.getSwapchainImagesKHR(newHandle);
	Vulkan::SetObjectName(newHandle, to_string("Swapchain%u", window_->index()));
	destroySwapchain();

	// Setup new images
	vk::ImageViewCreateInfo ivci{};
	ivci.viewType = vk::ImageViewType::e2D;
	ivci.format = scci.imageFormat;
	ivci.components = IDENTITY_MAPPING;
	ivci.subresourceRange = COLOR_SUBRESOURCE;
	swapchain_.images.resize(newImages.size());
	for (uint32 ii = 0; ii < swapchain_.images.size(); ++ii) {
		auto& scimage = swapchain_.images[ii];
		scimage.image = (ivci.image = newImages[ii]);
		scimage.mappedFence = { };

		Vulkan::SetObjectName(scimage.image, to_string("Swapchain%u.Image%u", window_->index(), ii));
	}

	// Setup new sync objects
	for (uint32 si = 0; si < sync_.acquireSemaphores.size(); ++si) {
		sync_.acquireSemaphores[si] = vkdevice.createSemaphore({ });
		Vulkan::SetObjectName(sync_.acquireSemaphores[si], to_string("Swapchain%u.ASem%u", window_->index(), si));
	}
	for (uint32 si = 0; si < sync_.renderSemaphores.size(); ++si) {
		sync_.renderSemaphores[si] = vkdevice.createSemaphore({ });
		Vulkan::SetObjectName(sync_.renderSemaphores[si], to_string("Swapchain%u.RSem%u", window_->index(), si));
	}
	sync_.lastFence = { };
	sync_.syncIndex = 0;

	// Update swapchain info
	swapchain_.handle = newHandle;
	swapchain_.extent = info.extent;
	swapchain_.imageIndex = 0;
	swapchain_.dirty = false;

	// Acquire the new swapchain
	vk::Result acqRes{};
	if (!acquire(acqRes)) {
		throw std::runtime_error(
			to_string("Failed to acquire window swapchain after rebuild (%s)", vk::to_string(acqRes).c_str()));
	}

	// Inform window
	window_->onSwapchainRebuild(info.extent);
}

// ====================================================================================================================
bool Swapchain::acquire(vk::Result& result)
{
	// Acquire the next image
	uint32 iidx{};
	auto asem = sync_.acquireSemaphores[sync_.syncIndex];
	result = Vulkan::Device().acquireNextImageKHR(swapchain_.handle, UINT64_MAX, asem, { }, &iidx);
	if (result != vk::Result::eSuccess) {
		return false;
	}
	swapchain_.imageIndex = iidx;

	// Wait for out-of-order fences
	swapchain_.images[iidx].mappedFence.wait();
	swapchain_.images[iidx].mappedFence = sync_.lastFence;

	return true;
}

// ====================================================================================================================
void Swapchain::destroySwapchain()
{
	const auto vkdevice = Vulkan::Device();
	vkdevice.waitIdle();

	// Sync objects
	for (auto& sem : sync_.acquireSemaphores) {
		if (sem) {
			vkdevice.destroySemaphore(sem);
		}
		sem = { nullptr };
	}
	for (auto& sem : sync_.renderSemaphores) {
		if (sem) {
			vkdevice.destroySemaphore(sem);
		}
		sem = { nullptr };
	}

	// Swapchain objects
	if (swapchain_.handle) {
		vkdevice.destroySwapchainKHR(swapchain_.handle);
		swapchain_.handle = { nullptr };
	}
}

} // namespace vega
