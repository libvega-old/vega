/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "../Common.Impl.hpp"
#include <Vega/Render/CommandList.hpp>
#include <Vega/Render/Viewport.hpp>
#include <Vega/Math/Rect2.hpp>
#include "../Graphics/ShaderImpl.hpp"
#include "../Graphics/Vulkan.hpp"
#include "../Graphics/Buffer/VertexBufferImpl.hpp"
#include "../Graphics/Managers/UniformManager.hpp"

#include <array>


namespace vega
{

class RendererImpl;
class ShaderImpl;

// Implementation of CommandList
class CommandListImpl final
	: public CommandList
{
	friend class CommandList;
	friend class Renderer;

	struct VertexAttrib final
	{
		vk::Buffer buffer{ nullptr };
		uint32 offset{ 0 };
		uint32 stride{ 0 };
	}; // struct VertexAttrib

public:
	CommandListImpl(RendererImpl* renderer, uint32 subpass);
	~CommandListImpl();

	inline vk::CommandBuffer commandBuffer() const { return cmd_; }

protected:
	void updateBindings();
	void updateVertexBuffers();
	void updateUniforms();
	void updateGlobals(const Camera3D* camera);

private:
	RendererImpl* const renderer_;
	const uint32 subpass_;
	const uint64 frame_;
	vk::CommandBuffer cmd_;
	struct
	{
		Hash64 pipelineHash{ 0 };
		ShaderImpl* shader{ nullptr };
		Viewport viewport{ };
		Rect2i scissor{ };
		uint8 stencilFront{ };
		uint8 stencilBack{ };
	} state_;
	struct
	{
		std::array<uint16, Shader::MAX_BINDING_COUNT> indices{ };
		bool dirty{ false };
	} bindings_;
	struct
	{
		std::array<VertexAttrib, Shader::MAX_VERTEX_ATTRIB_COUNT> attribs{ };
		VertexUseMask enabled{ };
		bool dirty{ false };
	} vertex_;
	struct
	{
		UniformManager::DataSlot globalSlot{ };
		UniformManager::DataSlot userSlot{ };
		bool globalDirty{ false };
		bool userDirty{ false };
	} uniform_;

	VEGA_NO_COPY(CommandListImpl)
	VEGA_NO_MOVE(CommandListImpl)
}; // class CommandListImpl

DECLARE_IMPL_GETTER(CommandList)

} // namepsace vega
