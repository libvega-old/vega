/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./RenderLayoutImpl.hpp"


namespace vega
{

// ====================================================================================================================
static inline bool RenderUse_isColor(RenderUse use) {
	return (use >= RenderUse::Color0) && (use <= RenderUse::Color3);
}
static inline bool RenderUse_isInput(RenderUse use) {
	return (use >= RenderUse::Input0) && (use <= RenderUse::Input3);
}


// ====================================================================================================================
// ====================================================================================================================
RenderLayoutImpl::RenderLayoutImpl(TargetVector&& targets, MSAATargetVector&& msaaTargets, SubpassVector&& subpasses,
		SubpassVector&& msaaSubpasses, DependencySet&& dependencies)
	: RenderLayout()
	, supportsMSAA_{ false }
	, targets_{ std::move(targets) }
	, msaaTargets_{ std::move(msaaTargets) }
	, subpasses_{ std::move(subpasses) }
	, msaaSubpasses_{ std::move(msaaSubpasses) }
	, dependencies_{ std::move(dependencies) }
{
	for (const auto& subpass : msaaSubpasses_) {
		if (subpass.msaa) {
			supportsMSAA_ = true;
			break;
		}
	}
}

// ====================================================================================================================
RenderLayoutImpl::~RenderLayoutImpl()
{

}

// ====================================================================================================================
vk::RenderPass RenderLayoutImpl::createRenderPass(MSAA msaa) const
{
	const bool isMSAA = (msaa != MSAA::X1);

	// Create attachments
	const auto attCount = isMSAA ? msaaTargets_.size() : targets_.size();
	std::vector<vk::AttachmentDescription2> vkatts{ }; vkatts.resize(attCount);
	for (uint32 ti = 0; ti < attCount; ++ti) {
		const auto& target = isMSAA ? msaaTargets_[ti] : targets_[ti];
		auto& vkatt = vkatts[ti];
		const bool targClear = // Clear iff: not msaa renderer, or target is msaa, or target has no msaa partner
			!isMSAA || (target.flags.msaa || (target.partnerIndex == 0));

		vkatt.flags = { };
		vkatt.format = vk::Format(target.format.formatId());
		vkatt.samples = target.flags.msaa ? vk::SampleCountFlagBits(msaa) : vk::SampleCountFlagBits::e1;
		vkatt.loadOp = targClear ? vk::AttachmentLoadOp::eClear : vk::AttachmentLoadOp::eDontCare;
		vkatt.storeOp = target.flags.preserve ? vk::AttachmentStoreOp::eStore : vk::AttachmentStoreOp::eDontCare;
		vkatt.stencilLoadOp = target.format.hasStencil() ? vkatt.loadOp : vk::AttachmentLoadOp::eDontCare;
		vkatt.stencilStoreOp = target.format.hasStencil() ? vkatt.storeOp : vk::AttachmentStoreOp::eDontCare;
		vkatt.initialLayout = vk::ImageLayout::eUndefined;
		vkatt.finalLayout =
			target.flags.preserve ? vk::ImageLayout::eShaderReadOnlyOptimal :
			RenderUse_isColor(target.lastUse) ? vk::ImageLayout::eColorAttachmentOptimal :
			RenderUse_isInput(target.lastUse) ? vk::ImageLayout::eShaderReadOnlyOptimal :
			vk::ImageLayout::eDepthStencilAttachmentOptimal;
	}

	// Create the subpasses
	std::vector<vk::SubpassDescription2> vkpasses{ }; vkpasses.resize(subpasses_.size());
	std::vector<vk::AttachmentReference2> vkrefs{ }; vkrefs.resize(subpasses_.size() * attCount);
	std::vector<vk::SubpassDescriptionDepthStencilResolve> vkdsresolves{ }; vkdsresolves.resize(vkpasses.size());
	auto refPtr = vkrefs.data();
	auto dsresPtr = vkdsresolves.data();
	for (uint32 si = 0; si < subpasses_.size(); ++si) {
		const auto& subpass = isMSAA ? msaaSubpasses_[si] : subpasses_[si];
		auto& vkpass = vkpasses[si];
		vkpass.flags = { };
		vkpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
		vkpass.viewMask = 0;

		// Color
		vkpass.colorAttachmentCount = subpass.colorCount;
		vkpass.pColorAttachments = refPtr;
		for (uint32 ri = 0; ri < subpass.colorCount; ++ri, ++refPtr) {
			refPtr->attachment = subpass.colorTargets[ri];
			refPtr->layout = vk::ImageLayout::eColorAttachmentOptimal;
		}

		// Resolve
		if (subpass.resolve) {
			vkpass.pResolveAttachments = refPtr;
			for (uint32 ri = 0; ri < subpass.colorCount; ++ri, ++refPtr) {
				refPtr->attachment = subpass.resolveTargets[ri]; // VK_ATTACHMENT_UNUSED already present if needed
				refPtr->layout = vk::ImageLayout::eColorAttachmentOptimal;
			}
		}

		// Depth + Depth Resolve
		if (subpass.depthTarget != VK_ATTACHMENT_UNUSED) {
			vkpass.pDepthStencilAttachment = refPtr;
			refPtr->attachment = subpass.depthTarget;
			refPtr->layout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
			++refPtr;
		}
		if (subpass.depthResolveTarget != VK_ATTACHMENT_UNUSED) {
			dsresPtr->depthResolveMode = vk::ResolveModeFlagBits::eMin;
			dsresPtr->stencilResolveMode = vk::ResolveModeFlagBits::eMin;
			dsresPtr->pDepthStencilResolveAttachment = refPtr;
			refPtr->attachment = subpass.depthResolveTarget;
			refPtr->layout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
			vkpass.pNext = dsresPtr++;
			++refPtr;
		}

		// Input
		vkpass.inputAttachmentCount = subpass.inputCount;
		vkpass.pInputAttachments = refPtr;
		for (uint32 ri = 0; ri < subpass.inputCount; ++ri, ++refPtr) {
			refPtr->attachment = subpass.inputTargets[ri];
			refPtr->layout = vk::ImageLayout::eShaderReadOnlyOptimal;
			const auto format = targets_[refPtr->attachment].format;
			refPtr->aspectMask =
				format.isColor() ? vk::ImageAspectFlagBits::eColor :
				format.hasStencil() ? (vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil) :
				vk::ImageAspectFlagBits::eDepth;
		}

		// Preserve
		vkpass.preserveAttachmentCount = uint32(subpass.preserveTargets.size());
		vkpass.pPreserveAttachments = subpass.preserveTargets.data();
	}

	// Create dependencies
	std::vector<vk::SubpassDependency2> vkdeps{}; vkdeps.reserve(dependencies_.size());
	for (const auto& dep : dependencies_) {
		auto& vkdep = vkdeps.emplace_back();
		vkdep.srcSubpass = (dep.srcSubpass == UINT8_MAX) ? VK_SUBPASS_EXTERNAL : dep.srcSubpass;
		vkdep.dstSubpass = (dep.dstSubpass == UINT8_MAX) ? VK_SUBPASS_EXTERNAL : dep.dstSubpass;
		vkdep.srcStageMask =
			(dep.srcUse == Dependency::Color) ? vk::PipelineStageFlagBits::eColorAttachmentOutput :
			(dep.srcUse == Dependency::Input) ? vk::PipelineStageFlagBits::eFragmentShader :
			vk::PipelineStageFlagBits::eLateFragmentTests;
		vkdep.dstStageMask =
			(dep.dstUse == Dependency::Color) ? vk::PipelineStageFlagBits::eColorAttachmentOutput :
			(dep.dstUse == Dependency::Input) ? vk::PipelineStageFlagBits::eFragmentShader :
			vk::PipelineStageFlagBits::eEarlyFragmentTests;
		vkdep.srcAccessMask =
			(dep.srcUse == Dependency::Color) ? vk::AccessFlagBits::eColorAttachmentWrite :
			(dep.srcUse == Dependency::Input) ? vk::AccessFlagBits::eInputAttachmentRead :
			vk::AccessFlagBits::eDepthStencilAttachmentWrite;
		vkdep.dstAccessMask =
			(dep.dstUse == Dependency::Color) ? vk::AccessFlagBits::eColorAttachmentRead :
			(dep.dstUse == Dependency::Input) ? vk::AccessFlagBits::eInputAttachmentRead :
			vk::AccessFlagBits::eDepthStencilAttachmentRead;
		if (vkdep.dstSubpass == VK_SUBPASS_EXTERNAL) {
			vkdep.dstAccessMask = vk::AccessFlagBits::eShaderRead;
		}
		else {
			vkdep.dependencyFlags = vk::DependencyFlagBits::eByRegion;
		}
	}

	// Create the new render pass
	vk::RenderPassCreateInfo2 rpci{};
	rpci.attachmentCount = uint32(vkatts.size());
	rpci.pAttachments = vkatts.data();
	rpci.subpassCount = uint32(vkpasses.size());
	rpci.pSubpasses = vkpasses.data();
	rpci.dependencyCount = uint32(vkdeps.size());
	rpci.pDependencies = vkdeps.data();
	return Vulkan::Device().createRenderPass2(rpci);
}


// ====================================================================================================================
// ====================================================================================================================
RenderLayoutBuilderImpl::RenderLayoutBuilderImpl(uint32 spCount, SubpassMask msaaMask)
	: RenderLayoutBuilder()
	, subpasses_{ }
	, targets_{ }
{
	for (uint32 si = 0; si < spCount; ++si) {
		auto& sp = subpasses_.emplace_back();
		sp.index = si;
		sp.msaa = bool(msaaMask.rawValue() & (1 << si));
		sp.colorTargets.fill(std::nullopt);
		sp.inputTargets.fill(std::nullopt);
	}
}

// ====================================================================================================================
RenderLayoutBuilderImpl::~RenderLayoutBuilderImpl()
{

}

} // namespace vega
