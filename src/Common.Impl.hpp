/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include <Vega/Common.hpp>


namespace vega
{

// Casts type T to dedicated impl type (selected by THIS_IMPL_TYPE) from within T while preserving const-ness
#define THIS_IMPL (static_cast<std::conditional_t<std::is_const_v<std::remove_pointer_t<decltype(this)>>, \
	const THIS_IMPL_TYPE*, THIS_IMPL_TYPE*>>(this))

// Declares functions for getting the impl type from a type base pointer
#define DECLARE_IMPL_GETTER(baseType) \
	inline baseType##Impl* get_impl(baseType* baseptr_) { return static_cast<baseType##Impl*>(baseptr_); } \
	inline const baseType##Impl* get_impl(const baseType* baseptr_) { return static_cast<const baseType##Impl*>(baseptr_); } \
	inline baseType##Impl& get_impl(baseType& baseref_) { return static_cast<baseType##Impl&>(baseref_); } \
	inline const baseType##Impl& get_impl(const baseType& baseref_) { return static_cast<const baseType##Impl&>(baseref_); }

} // namespace vega
