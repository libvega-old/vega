/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Input/Keyboard.hpp>
#include "../Core/WindowImpl.hpp"
#include "../Core/GLFW.hpp"

#include <sstream>


namespace vega
{

// ====================================================================================================================
// ====================================================================================================================
const String& Key::name() const
{
	static const std::array<String, Keys::KEY_COUNT> NAMES{
		"UNKNOWN", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
		"U", "V", "W", "X", "Y", "Z", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "D0", "KP1", "KP2", "KP3",
		"KP4", "KP5", "KP6", "KP7", "KP8", "KP9", "KP0", "KP/", "KP*", "KP-", "KP+", "KP.", "~", "-", "=", "[",
		"]", "\\", ";", "'", ",", ".", "/", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12",
		"Up", "Down", "Left", "Right", "Tab", "CapsLock", "Backspace", "Enter", "Insert", "Home", "PageUp", "Delete",
		"End", "PageDown", "Return", "Space", "Escape", "LSuper", "RSuper", "Menu", "PrintScreen", "ScrollLock",
		"Pause", "NumLock", "LShift", "LControl", "LAlt", "RShift", "RControl", "RAlt"
	};
	return NAMES.at(index_);
}


// ====================================================================================================================
// ====================================================================================================================
String to_string(ModKeyMask mask)
{
	if (!bool(mask)) {
		return "None";
	}

	std::stringstream ss{};
	if (bool(mask & ModKey::LShift)) {
		ss << "|LShift";
	}
	if (bool(mask & ModKey::LCtrl)) {
		ss << "|LCtrl";
	}
	if (bool(mask & ModKey::LAlt)) {
		ss << "|LAlt";
	}
	if (bool(mask & ModKey::RShift)) {
		ss << "|RShift";
	}
	if (bool(mask & ModKey::RCtrl)) {
		ss << "|RCtrl";
	}
	if (bool(mask & ModKey::RAlt)) {
		ss << "|RAlt";
	}
	return ss.str().substr(1);
}


// ====================================================================================================================
// ====================================================================================================================
Keyboard::Keyboard(Window* window)
	: window_{ window }
	, keys_{ }
	, mods_{ }
	, time_{ }
{
	// Set callback
	glfwSetKeyCallback(get_impl(window)->handle(),
		[](GLFWwindow* win, int key, int scancode, int action, int mods) {
			auto winptr = glfwGetWindowUserPointer(win);
			reinterpret_cast<::vega::Window*>(winptr)->keyboard()->handleKey(key, scancode, action, mods);
		});

	// Default settings
	time_.hold = 0.5f;
	time_.tap = 0.2f;
}

// ====================================================================================================================
Keyboard::~Keyboard()
{
	glfwSetKeyCallback(get_impl(window_)->handle(), nullptr);
}

// ====================================================================================================================
void Keyboard::nextFrame()
{
	std::memcpy(keys_.previous.data(), keys_.current.data(), keys_.current.size() * sizeof(bool));
	mods_.previous = mods_.current;
	time_.last = float(Time::Elapsed().seconds());
}

// ====================================================================================================================
void Keyboard::handleKey(int keycode, int scancode, int action, int mods)
{
	if (action == GLFW_REPEAT) {
		return; // Don't handle repeat events
	}
	const auto key = KeyFromSystemValue(keycode);
	if (key == Keys::Unknown) {
		return;
	}
	const auto keyval = key.index();

	// Update on action
	if (action == GLFW_PRESS) {
		keys_.current[keyval] = true;
		time_.press[keyval] = time_.last;
		keys_.pressed.insert(keys_.pressed.end(), key);
	}
	else if (action == GLFW_RELEASE) {
		keys_.current[keyval] = false;
		time_.release[keyval] = time_.last;
		keys_.pressed.erase(key);
	}

	// Update modifiers
	if (keycode == GLFW_KEY_LEFT_SHIFT) {
		if (action == GLFW_PRESS) mods_.current.set(ModKey::LShift);
		else mods_.current.clear(ModKey::LShift);
	}
	else if (keycode == GLFW_KEY_LEFT_CONTROL) {
		if (action == GLFW_PRESS) mods_.current.set(ModKey::LCtrl);
		else mods_.current.clear(ModKey::LCtrl);
	}
	else if (keycode == GLFW_KEY_LEFT_ALT) {
		if (action == GLFW_PRESS) mods_.current.set(ModKey::LAlt);
		else mods_.current.clear(ModKey::LAlt);
	}
	else if (keycode == GLFW_KEY_RIGHT_SHIFT) {
		if (action == GLFW_PRESS) mods_.current.set(ModKey::RShift);
		else mods_.current.clear(ModKey::RShift);
	}
	else if (keycode == GLFW_KEY_RIGHT_CONTROL) {
		if (action == GLFW_PRESS) mods_.current.set(ModKey::RCtrl);
		else mods_.current.clear(ModKey::RCtrl);
	}
	else if (keycode == GLFW_KEY_RIGHT_ALT) {
		if (action == GLFW_PRESS) mods_.current.set(ModKey::RAlt);
		else mods_.current.clear(ModKey::RAlt);
	}
}

// ====================================================================================================================
Key Keyboard::KeyFromSystemValue(int value)
{
	static constexpr Key KEYMAP0[]{ // Key 32 - 96
		Keys::Space, { }, { }, { }, { }, { }, { }, Keys::Apostrophe, { }, { }, { }, { }, Keys::Comma, Keys::Minus,
		Keys::Period, Keys::Slash, Keys::D0, Keys::D1, Keys::D2, Keys::D3, Keys::D4, Keys::D5, Keys::D6, Keys::D7,
		Keys::D8, Keys::D9, { }, Keys::Semicolon, { }, Keys::Equals, { }, { }, { }, Keys::A, Keys::B, Keys::C, Keys::D,
		Keys::E, Keys::F, Keys::G, Keys::H, Keys::I, Keys::J, Keys::K, Keys::L, Keys::M, Keys::N, Keys::O, Keys::P,
		Keys::Q, Keys::R, Keys::S, Keys::T, Keys::U, Keys::V, Keys::W, Keys::X, Keys::Y, Keys::Z, Keys::LeftBracket,
		Keys::Backslash, Keys::RightBracket, { }, { }, Keys::Grave
	};
	static constexpr Key KEYMAP1[]{ // Key 161, 162 (not used)
		{ }, { }
	};
	static constexpr Key KEYMAP2[]{ // Key 256 - 269
		Keys::Escape, Keys::Enter, Keys::Tab, Keys::Backspace, Keys::Insert, Keys::Delete, Keys::Right, Keys::Left,
		Keys::Down, Keys::Up, Keys::PageUp, Keys::PageDown, Keys::Home, Keys::End
	};
	static constexpr Key KEYMAP3[]{ // Key 280 - 348
		Keys::CapsLock, Keys::ScrollLock, Keys::NumLock, Keys::PrintScreen, Keys::Pause, { }, { }, { }, { }, { },
		Keys::F1, Keys::F2, Keys::F3, Keys::F4, Keys::F5, Keys::F6, Keys::F7, Keys::F8, Keys::F9, Keys::F10, Keys::F11,
		Keys::F12, { }, { }, { }, { }, { }, { }, { }, { }, { }, { }, { }, { }, { }, { }, { }, { }, { }, { }, Keys::KP0,
		Keys::KP1, Keys::KP2, Keys::KP3, Keys::KP4, Keys::KP5, Keys::KP6, Keys::KP7, Keys::KP8, Keys::KP9,
		Keys::KPDecimal, Keys::KPDivide, Keys::KPMultiply, Keys::KPSubtract, Keys::KPAdd, Keys::KPEnter, { }, { }, { },
		{ }, Keys::LeftShift, Keys::LeftControl, Keys::LeftAlt, Keys::LeftSuper, Keys::RightShift, Keys::RightControl,
		Keys::RightAlt, Keys::RightSuper, Keys::Menu
	};

	return (value == -1) ? Keys::Unknown :
		(value <= GLFW_KEY_GRAVE_ACCENT) ? KEYMAP0[value - GLFW_KEY_SPACE] :
		(value <= GLFW_KEY_WORLD_2) ? KEYMAP1[value - GLFW_KEY_WORLD_1] :
		(value <= GLFW_KEY_END) ? KEYMAP2[value - GLFW_KEY_ESCAPE] :
		(value <= GLFW_KEY_MENU) ? KEYMAP3[value - GLFW_KEY_CAPS_LOCK] :
		Keys::Unknown;
}

} // namespace vega
