/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "../Common.Impl.hpp"
#include <Vega/Input/Mouse.hpp>
#include "../Core/WindowImpl.hpp"
#include "../Core/GLFW.hpp"

#include <array>
#include <sstream>


namespace vega
{

// ====================================================================================================================
const String& to_string(CursorMode mode)
{
	static const std::array<String, uint32(CursorMode::Locked) + 1> NAMES{
		"Normal", "Hidden", "Locked"
	};
	return NAMES.at(uint32(mode));
}


// ====================================================================================================================
// ====================================================================================================================
String to_string(MouseButtonMask mask)
{
	if (!bool(mask)) {
		return "None";
	}

	std::stringstream ss{};
	if (bool(mask & MouseButton::Left)) {
		ss << "|Left";
	}
	if (bool(mask & MouseButton::Middle)) {
		ss << "|Middle";
	}
	if (bool(mask & MouseButton::Right)) {
		ss << "|Right";
	}
	if (bool(mask & MouseButton::X1)) {
		ss << "|X1";
	}
	if (bool(mask & MouseButton::X2)) {
		ss << "|X2";
	}
	if (bool(mask & MouseButton::X3)) {
		ss << "|X3";
	}
	if (bool(mask & MouseButton::X4)) {
		ss << "|X4";
	}
	if (bool(mask & MouseButton::X5)) {
		ss << "|X5";
	}
	return ss.str().substr(1);
}


// ====================================================================================================================
// ====================================================================================================================
Mouse::Mouse(Window* window)
	: window_{ window }
	, cursorMode_{ CursorMode::Normal }
	, buttons_{ }
	, click_{ }
	, position_{ }
	, scroll_{ }
	, hover_{ }
{
	const auto winptr = get_impl(window)->handle();

	// Register callbacks
	glfwSetMouseButtonCallback(winptr,
		[](GLFWwindow* window, int button, int action, int mods) {
			auto winptr = glfwGetWindowUserPointer(window);
			reinterpret_cast<WindowImpl*>(winptr)->mouse()->handleButton(button, action, mods);
		});
	glfwSetCursorPosCallback(winptr,
		[](GLFWwindow* window, double xpos, double ypos) {
			auto winptr = glfwGetWindowUserPointer(window);
			reinterpret_cast<WindowImpl*>(winptr)->mouse()->handleCursorPos(xpos, ypos);
		});
	glfwSetCursorEnterCallback(winptr,
		[](GLFWwindow* window, int entered) {
			auto winptr = glfwGetWindowUserPointer(window);
			reinterpret_cast<WindowImpl*>(winptr)->mouse()->handleCursorEnter(entered);
		});
	glfwSetScrollCallback(winptr,
		[](GLFWwindow* window, double xoffset, double yoffset) {
			auto winptr = glfwGetWindowUserPointer(window);
			reinterpret_cast<WindowImpl*>(winptr)->mouse()->handleScroll(xoffset, yoffset);
		});

	// Default settings
	click_.clickDistance = 25;
	click_.clickTime = 0.5f;
	click_.doubleClickTime = 0.5f;
}

// ====================================================================================================================
Mouse::~Mouse()
{
	const auto winptr = get_impl(window_)->handle();
	glfwSetMouseButtonCallback(winptr, nullptr);
	glfwSetCursorPosCallback(winptr, nullptr);
	glfwSetCursorEnterCallback(winptr, nullptr);
	glfwSetScrollCallback(winptr, nullptr);
}

// ====================================================================================================================
void Mouse::cursorMode(vega::CursorMode mode)
{
	const auto winptr = get_impl(window_)->handle();
	const auto mval =
		(mode == CursorMode::Locked) ? GLFW_CURSOR_DISABLED :
		(mode == CursorMode::Hidden) ? GLFW_CURSOR_HIDDEN : GLFW_CURSOR_NORMAL;
	glfwSetInputMode(winptr, GLFW_CURSOR, mval);
}

// ====================================================================================================================
void Mouse::handleButton(int button, int action, int mods)
{
	const bool press = (action == GLFW_PRESS);
	const auto bmask =
		(button == GLFW_MOUSE_BUTTON_LEFT) ? MouseButton::Left :
		(button == GLFW_MOUSE_BUTTON_RIGHT) ? MouseButton::Right :
		(button == GLFW_MOUSE_BUTTON_MIDDLE) ? MouseButton::Middle :
		(button == GLFW_MOUSE_BUTTON_4) ? MouseButton::X1 :
		(button == GLFW_MOUSE_BUTTON_5) ? MouseButton::X2 :
		(button == GLFW_MOUSE_BUTTON_6) ? MouseButton::X3 :
		(button == GLFW_MOUSE_BUTTON_7) ? MouseButton::X4 : MouseButton::X5;

	// Update mask
	if (press) {
		buttons_.current.set(bmask);
	}
	else {
		buttons_.current.clear(bmask);
	}

	// Perform click detection
	const auto now = float(Time::Elapsed().seconds());
	const auto bindex = ButtonIndex(bmask);
	double cx, cy;
	glfwGetCursorPos(get_impl(window_)->handle(), &cx, &cy);
	const Vec2i cpos{ int32(cx), int32(cy) };
	if (press) {
		click_.lastPressTime[bindex] = now;
		click_.lastPressPos[bindex] = cpos;
	}
	else {
		click_.lastReleaseTime[bindex] = now;
		click_.lastReleasePos[bindex] = cpos;

		const auto isClickTime = (now - click_.lastPressTime[bindex]) <= click_.clickTime;
		const auto isClickPos = (cpos - click_.lastPressPos[bindex]).length() <= click_.clickDistance;

		if (isClickTime && isClickPos) {
			if (click_.nextClickIsDouble[bindex]) {
				const auto isDClickTime = (now - click_.lastClickTime[bindex]) <= click_.doubleClickTime;
				const auto isDClickPos = (cpos - click_.lastClickPos[bindex]).length() <= click_.clickDistance;

				if (isDClickTime && isDClickPos) {
					click_.nextClickIsDouble[bindex] = false;
				}
			}
			else {
				click_.nextClickIsDouble[bindex] = true;
			}

			click_.clickInLastFrame[bindex] = true;
			click_.lastClickTime[bindex] = now;
			click_.lastClickPos[bindex] = cpos;
		}
		else {
			click_.nextClickIsDouble[bindex] = false;
		}
	}
}

// ====================================================================================================================
void Mouse::handleCursorPos(double xpos, double ypos)
{
	position_.current = { int32(xpos), int32(ypos) };
}

// ====================================================================================================================
void Mouse::handleCursorEnter(int entered)
{
	hover_.current = (entered == GLFW_TRUE);
}

// ====================================================================================================================
void Mouse::handleScroll(double xoffset, double yoffset)
{
	scroll_.current += Vec2i{ int32(xoffset), int32(yoffset) };
}

// ====================================================================================================================
void Mouse::nextFrame()
{
	// Transfer frame state
	buttons_.previous = buttons_.current;
	position_.previous = position_.current;
	scroll_.previous = scroll_.current;
	hover_.previous = hover_.current;

	// Update timing
	const auto delta = float(Time::Delta().seconds());
	uint32 mask = buttons_.current.rawValue();
	for (auto& time : buttons_.holdTimes) {
		if (mask & 0x1) {
			time += delta;
		}
		else {
			time = 0;
		}
		mask >>= 1;
	}

	// Update new state
	click_.clickInLastFrame.fill(false);
}

// ====================================================================================================================
uint32 Mouse::ButtonIndex(MouseButton mb)
{
	static const std::array<uint8, 16> NIBBLE{
		0, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0
	};
	return uint32(mb) ? ((uint32(mb) & 0x0F) ? NIBBLE[uint32(mb) & 0x0F] : NIBBLE[uint32(mb) >> 4] + 4) : 0;
}

} // namespace vega
