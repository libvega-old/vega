# Vega

A C++17 framework for multimedia applications, powered by Vulkan.

Vega is designed to be a middleware platform for graphics/audio/input, not a full fledged game engine. It takes inspiration from projects like [MonoGame](https://www.monogame.net/) - giving the programmer a set of powerful components, but relying on them to tie together the logic for the app to fit their specific needs.

Vega shaders are written in a [custom language](https://gitlab.com/libvega/vsl).

## Acknowledgements

A huge thanks to the following projects for making Vega possible:

* [Premake](https://premake.github.io/) - Project configuration generator.
* [GLFW](https://github.com/glfw/glfw) - Windowing and peripheral input.
* [OpenAL Soft](https://openal-soft.org/) - Audio playback, recording, and effects.
* [stb_vorbis](https://github.com/nothings/stb/blob/master/stb_vorbis.c) - OGG/Vorbis audio loading.
* [dr_libs](https://github.com/mackron/dr_libs) - WAV and Flac audio loading.
* [stb_image](https://github.com/nothings/stb/blob/master/stb_image.h) - Image file loading.

## Legal

Vega is licensed under the [Microsoft Public License (Ms-PL)](https://choosealicense.com/licenses/ms-pl/). This is a permissive OSS license close to the MIT license, but with an extra clause that modifications to the Vega source (*if also made open source*) need to be under a license compatible with the Ms-PL. The authors feel that this encourages code sharing without punishing closed source and commercial projects. For full details, please refer to the full text of the license.

All third-party libraries and projects are utilized within their original licenses, and, where applicible, are rehosted under the original licenses as well. These licenses are availble in at the links above, and are additionally available in the `licenses/` folder in this repo. These third party libraries belong solely to their original authors - the Vega authors make no ownership claims over them.
