## Test Project 

Source files placed in this directory will be ignored by Git, but will be built into a console application that can be used for testing the Vega library. The only file in this directory that is tracked by Git is this README.
