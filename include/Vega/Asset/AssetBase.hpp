/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AssetBase.hpp Base types for managing and accessing asset data.

#include "../Common.hpp"


namespace vega
{

class AssetManager;

//! @brief The load states of an AssetBase instance.
enum class AssetState : uint8
{
	//! @brief The asset is currently being loaded.
	Pending   = 0,
	//! @brief The asset is loaded and ready for regular use.
	Ready     = 1,
	//! @brief The original asset has been unloaded, and the default asset is now in use.
	Defaulted = 2,
	//! @brief The original asset has been unloaded and there is no default asset to use in its place.
	Unloaded  = 3
}; // enum class AssetState


//! @brief Base class for asset/content types that can be loaded, managed, and cached by an AssetManager.
class VEGA_API AssetBase
{
	friend AssetManager;
	template<typename T> friend struct AssetHandle;

	// Manages a shared asset reference state with shared_ptr
	struct Control final :
		public std::enable_shared_from_this<Control>
	{
		Control(AssetBase* asset, const std::type_info& type) 
			: asset{ asset }, type{ type }, manager{ nullptr }, key{ 0 }, state{ AssetState::Pending }
		{ }
		
		inline uint64 refCount() const { return uint64(weak_from_this().use_count()); }
		inline SPtr<Control> sptr() { return shared_from_this(); }
		inline SPtr<const Control> sptr() const { return shared_from_this(); }

		AssetBase* asset;
		const std::type_info& type;
		mutable AssetManager* manager;
		mutable uint64 key;
		AssetState state;
	}; // struct Control

public:
	virtual ~AssetBase();

	//! @brief The runtime type of the asset.
	inline const std::type_info& assetType() const { return control_->type; }
	//! @brief The number of AssetHandle instances referencing the asset.
	inline uint64 assetRefCount() const { return control_->refCount() - 1; }
	//! @brief If this asset instance is managed by an AssetManager.
	inline bool isManaged() const { return !!control_->manager; }
	//! @brief The AssetManager, if any, that is currently managing this asset.
	inline vega::AssetManager* assetManager() const { return control_->manager; }
	//! @brief The state of the asset, either AssetState::Pending or AssetState::Ready.
	inline AssetState assetState() const { return control_->state; }

protected:
	//! @brief Performs base construction for the asset type.
	//! @param assetType The runtime type of the asset.
	AssetBase(const std::type_info& assetType);

private:
	SPtr<Control> control_;

	VEGA_NO_COPY(AssetBase)
	VEGA_NO_MOVE(AssetBase)
}; // class AssetBase


//! @brief Safe asset handle which can automatically see changes to the referenced asset.
//! @tparam AssetT The asset type referenced by the handle.
template<typename AssetT>
struct AssetHandle final
{
	static_assert(std::is_base_of_v<AssetBase, AssetT>,
		"AssetHandle type parameter must be child type of AssetBase.");

public:
	//! @brief Create a new handle with a null reference.
	AssetHandle() : control_{ nullptr } { }
	//! @brief Create a new handle from an existing asset instance.
	AssetHandle(const AssetT& asset) : control_{ asset.control_ } { }
	~AssetHandle() { control_.reset(); }

	//! @brief If this handle is referencing a valid asset instance.
	inline bool valid() const { return control_ && control_->asset; }
	//! @brief If the referenced asset has been destroyed, and replaced with the default asset instance.
	inline bool defaulted() const { return control_ && (control_->state == AssetState::Defaulted); }
	//! @brief The number of AssetHandle instances referencing the asset.
	inline uint64 refCount() const { return control_ ? control_->refCount() : 0; }
	//! @brief If the asset instance is managed by an AssetManager.
	inline bool isManaged() const { return control_ && !!control_->manager; }
	//! @brief The AssetManager, if any, that is currently managing the asset.
	inline vega::AssetManager* manager() const { return control_ ? control_->manager : nullptr; }
	//! @brief The state of the asset, either AssetState::Pending or AssetState::Ready.
	//! @return If the handle has no asset, then AssetState::Pending is returned.
	inline AssetState state() const { return control_ ? control_->state : AssetState::Pending; }

	//! @brief Resets the handle to reference the default asset, or null.
	inline void reset() { control_.reset(); }

	//! @brief The asset that this handle is referencing, which may be the default asset if the original expired.
	inline AssetT* get() const { return control_ ? static_cast<AssetT*>(control_->asset) : nullptr; }

	//! @brief Implicit cast to the referenced asset pointer. Same as get().
	inline operator AssetT* () const { return get(); }
	//! @brief The referenced asset as an asset reference. Throws an exception for invalid assets.
	inline AssetT& operator * () const {
		if (!valid()) {
			throw std::runtime_error("Attempted to dereference expired or invalid asset");
		}
		return *static_cast<AssetT*>(control_->asset);
	}
	//! @brief The referenced asset pointer. Same as get().
	inline AssetT* operator -> () const { return get(); }

private:
	SPtr<AssetBase::Control> control_;

	VEGA_DEFAULT_COPY(AssetHandle)
	VEGA_DEFAULT_MOVE(AssetHandle)
}; // struct AssetHandle

} // namespace vega
