/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AssetType.hpp Management of runtime asset types.

#include "../Common.hpp"
#include "./AssetBase.hpp"
#include "./AssetLoaderBase.hpp"


namespace vega
{

//! @brief Contains runtime information about a specific asset type.
class VEGA_API AssetType
{
	friend class AssetTypeImpl;

public:
	//! @brief Functional callback for creating an AssetLoaderBase instance.
	using LoaderFactory = std::function<UPtr<AssetLoaderBase>()>;
	//! @brief Functional callback for creating an AssetBase default instance.
	using AssetFactory = std::function<UPtr<AssetBase>()>;

	//! @brief Allows compile-time declaration of asset types and their loader functions.
	//! @tparam AssetT The asset type to declare. Must be unique across all translation units.
	//! @tparam LoaderT The runtime asset loader type, or \c void to disable runtime loading.
	template<typename AssetT, typename LoaderT>
	struct Declaration final 
	{
		static_assert(std::is_base_of_v<AssetBase, AssetT> && !std::is_abstract_v<AssetT>,
			"AssetType::Declaration asset type must be a concrete child type of AssetBase");
		static_assert(std::is_same_v<LoaderT, void> || 
			(std::is_base_of_v<AssetLoaderBase, LoaderT> && !std::is_abstract_v<LoaderT>),
			"AssetType::Declaration loader type must be void, or a concrete child type of AssetLoaderBase");

		//! @brief Version of AssetType::LoaderFactory specific to `LoaderT`.
		using LoaderFactory = std::conditional_t<std::is_same_v<LoaderT, void>, void*, std::function<UPtr<LoaderT>()>>;
		//! @brief Version of AssetType::AssetFactory specific to `AssetT`.
		using AssetFactory = std::function<UPtr<AssetT>()>;

		//! @brief Initialize a new asset declaration (should have static lifetime - initialized at binary load).
		//! @param name The human-readable asset type name.
		//! @param loaderFactory The function for creating loader instance, or \c nullptr to disable runtime loading.
		//! @param assetFactory The function for creating the default asset instance, or \c nullptr to disable.
		explicit Declaration(const String& name, LoaderFactory loaderFactory, AssetFactory assetFactory);

		VEGA_NO_COPY(Declaration)
		VEGA_NO_MOVE(Declaration)
	}; // struct Declaration

	virtual ~AssetType();

	//! @brief The human-readable name of the asset type.
	const String& typeName() const;
	//! @brief If there is a runtime loader type associated with the asset type.
	bool hasLoader() const;
	//! @brief If the asset type has a default asset instance.
	bool hasDefaultAsset() const;

	//! @brief Gets the registered asset type with the given name, or \c nullptr if no such type exists.
	static const AssetType* GetType(const String& typeName);
	//! @brief Gets the registered asset type with the given type info, or \c nullptr if no such type exists.
	static const AssetType* GetType(const std::type_info& typeInfo);

private:
	AssetType();

	static void RegisterType(const String& name, const std::type_info& type, 
		LoaderFactory lFactory, AssetFactory aFactory);

	template<typename AssetT>
	struct AssetTypeRegisterCheck final {
		inline static bool Check() { static uint32 Count_{ 0 }; return Count_++ == 0; }
	};

	VEGA_NO_COPY(AssetType)
	VEGA_NO_MOVE(AssetType)
}; // class AssetType


template<typename AssetT, typename LoaderT>
AssetType::Declaration<AssetT, LoaderT>::Declaration(const String& name, LoaderFactory loaderFactory, 
	AssetFactory assetFactory)
{
	if (!loaderFactory && !std::is_same_v<LoaderT, void>) {
		throw std::runtime_error(to_string(
			"A non-null loader factory must be provided for asset type '%s'", name.c_str()));
	}
	if (loaderFactory && std::is_same_v<LoaderT, void>) {
		throw std::runtime_error(to_string(
			"A null loader factory must be provided to unloadable asset type '%s'", name.c_str()));
	}
	if (!AssetType::AssetTypeRegisterCheck<AssetT>::Check()) {
		throw std::runtime_error(to_string(
			"Duplicate asset type registration for asset type '%s'", name.c_str()));
	}
	AssetType::RegisterType(name, typeid(AssetT),
		loaderFactory 
			? ([loaderFactory]() -> UPtr<AssetLoaderBase> { return loaderFactory(); })
			: AssetType::LoaderFactory(nullptr),
		assetFactory
			? ([assetFactory]() -> UPtr<AssetBase> { return assetFactory(); })
			: AssetType::AssetFactory(nullptr)
	);
}

} // namespace vega
