/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AssetManager.hpp Asset management and caching.

#include "../Common.hpp"
#include "./AssetBase.hpp"
#include "../Math/Hash.hpp"

#include <mutex>
#include <unordered_map>


namespace vega
{

//! @brief Uniquely identifies an asset file and supports fast cache lookups in AssetManager instance.
struct VEGA_API AssetKey final
{
	friend class AssetManager;

public:
	//! @brief Construct a key from a string view.
	AssetKey(const StringView& name) : name_{ name }, hash_{ Hash64::XXHash64(name, 0) } { }
	//! @brief Construct a key from a string.
	AssetKey(const String& name) : AssetKey(StringView{ name }) { }
	//! @brief Construct a key from a c-string.
	AssetKey(const char* const name) : AssetKey(StringView{ name }) { }

	//! @brief The asset name or path.
	inline const String& name() const { return name_; }
	//! @brief The raw hashed key.
	inline uint64 value() const { return hash_.value(); }

private:
	String name_;
	Hash64 hash_;
}; // struct AssetKey


//! @brief Manages the loading, caching, and lifetime of a collection of AssetBase instances.
class VEGA_API AssetManager final
{
	using AssetMap = std::unordered_map<Hash64, UPtr<AssetBase>>;

public:
	~AssetManager();

	//! @brief Returns a handle to a cached or loaded asset object. 
	//! @tparam AssetT The asset type to get or load. Exception if cached object has type mismatch.
	//! @param key The asset key to first search the cache for, and then load if not found.
	template<class AssetT>
	inline AssetHandle<AssetT> load(const AssetKey& key) {
		// The type restrictions on AssetT are handled by AssetHandle<>
		const auto asset = getOrLoadAsset(typeid(AssetT), key);
		return AssetHandle<AssetT>(*dynamic_cast<AssetT*>(asset));
	}
	//! @brief Performs a cache lookup for the asset with the given key.
	//! @tparam AssetT The asset type to get from the cache. Exception if cached object has type mismatch.
	//! @param key The asset key to lookup in the cache.
	template<class AssetT>
	inline AssetHandle<AssetT> get(const AssetKey& key) const {
		// The type restrictions on AssetT are handled by AssetHandle<>
		return getTypedAssetFromCache(typeid(AssetT), key);
	}

	//! @brief Stops the manager from managing the asset. The asset lifetime becomes the responsibility of the caller.
	//! @param asset The asset to stop managing.
	//! @return If the asset was found in this manager, and was removed from the list of managed assets.
	bool stopManaging(const AssetBase& asset);

	//! @brief Destroys all managed assets in this manager. 
	void unloadAll();

	//! @brief The number of assets currently being tracked by the manager.
	inline uint32 assetCount() const { return uint32(assets_.size()); }
	//! @brief If the manager is currently unloading all assets.
	inline bool isUnloading() const { return unloading_; }

	//! @brief Creates a new AssetManager that loads normal unprocessed asset files with the given root folder path.
	//! @param path The path to the root asset folder, must be a folder path and must exist, otherwise an error.
	//! @return The new AssetManager instance.
	static AssetManager* OpenFolder(const String& path);

private:
	AssetManager(const String& folderPath);

	AssetBase* getOrLoadAsset(const std::type_info& assetType, const AssetKey& key);
	AssetBase* getTypedAssetFromCache(const std::type_info& assetType, const AssetKey& key) const;

private:
	const String rootPath_;
	AssetMap assets_;
	mutable std::mutex assetMutex_;
	bool unloading_;

	VEGA_NO_COPY(AssetManager)
	VEGA_NO_MOVE(AssetManager)
}; // class AssetManager

} // namespace vega
