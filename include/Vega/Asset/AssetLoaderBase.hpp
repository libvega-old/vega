/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AssetLoaderBase.hpp Extensible types for declaring and implementing Asset loading.

#include "../Common.hpp"
#include "./AssetBase.hpp"


namespace vega
{

//! @brief Information about an asset being loaded, made available to AssetLoaderBase instances.
class VEGA_API AssetLoadInfo final
{
public:
	AssetLoadInfo(const std::type_info& type, const String& name, const String& fullPath)
		: type_{ type }, name_{ name }, fullPath_{ fullPath }
	{ }

	//! @brief The runtime type of the asset being loaded.
	inline const std::type_info& assetType() const { return type_; }
	//! @brief The base name of the asset (usually file name without extension).
	inline const String& name() const { return name_; }
	//! @brief The full (absolute) path to the asset file.
	inline const String& fullPath() const { return fullPath_; }

private:
	const std::type_info& type_;
	String name_;
	String fullPath_;

	VEGA_NO_COPY(AssetLoadInfo)
	VEGA_NO_MOVE(AssetLoadInfo)
}; // class AssetLoadInfo


//! @brief Base class for implementing asset loading for a spcific asset type.
class VEGA_API AssetLoaderBase
{
	using LoaderFactory = std::function<UPtr<AssetLoaderBase>()>;
	using AssetFactory = std::function<UPtr<AssetBase>()>;

public:
	virtual ~AssetLoaderBase();

	//! @brief Virtual function for implementing asset loading logic.
	//! @param assetInfo Additional information about the asset being loaded.
	//! @param assetStream The stream open to the asset data to read and load the asset from.
	//! @return The loaded asset instance, which must match the expected runtime asset type.
	virtual UPtr<AssetBase> loadAsset(
		const AssetLoadInfo& assetInfo,
		std::istream& assetStream
	) = 0;

protected:
	//! @brief Base constructor for asset loaders.
	AssetLoaderBase();

	VEGA_NO_COPY(AssetLoaderBase)
	VEGA_NO_MOVE(AssetLoaderBase)
}; // class AssetLoaderBase

} // namespace vega
