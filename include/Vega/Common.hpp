/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Common.hpp Headers, macros, typedefs, and functions widely used by the library.

/* Macro Documentation */
#ifdef DOXYGEN_
#	define VEGA_WIN32 //!< Defined when Vega is targeting desktop Microsoft Windows.
#	define VEGA_LINUX //!< Defined when Vega is targeting a Linux flavor.
#	define VEGA_MSVC  //!< Defined when the current compiler is MSVC.
#	define VEGA_GCC   //!< Defined when the current compiler is GNU GCC.
#	define VEGA_CLANG //!< Defined when the current compiler is Clang.
#	define VEGA_API   //!< Shared library import/export symbol attribute.
#	define VEGA_C_API //!< Shared library C-linkage import/export symbol attribute.
#endif // DOXYGEN_

/* Ensure 64-bit */
#if !defined(_M_X64) && !defined(__x86_64__)
#	error Vega must be built to a 64-bit target.
#endif

/* OS Detection */
#if defined(_WIN32)
#	define VEGA_WIN32
#	ifndef NOMINMAX
#		define NOMINMAX
#	endif
#	ifndef WIN32_LEAN_AND_MEAN
#		define WIN32_LEAN_AND_MEAN
#	endif
#elif defined(__APPLE__)
#	error Cannot compile Vega for Apple platforms.
#elif defined(__ANDROID__)
#	error Cannot compile Vega for Android platforms.
#elif defined(__linux__)
	// Technically android is linux too, but that case is caught above
#	define VEGA_LINUX
#else
#	error Supported OS not detected - please use Windows or desktop Linux.
#endif // defined(_WIN32)

/* Compiler Detection */
#if defined(_MSC_VER)
#	define VEGA_MSVC
#elif defined(__MINGW32__)
#	error Cannot use MinGW to compile Vega.
#elif defined(__clang__)
#	define VEGA_CLANG
#elif defined(__GNUC__)
#	define VEGA_GCC
#else
#	error Unsupported compiler detected - please use MSVC, GNU GCC, or Clang.
#endif // defined(_MSC_VER)

/* Import/Export Macros */
#if defined(VEGA_MSVC)
#	if defined(VEGA_BUILD)
#		define VEGA_API __declspec(dllexport)
#		define VEGA_C_API extern "C" __declspec(dllexport)
#	else
#		define VEGA_API __declspec(dllimport)
#		define VEGA_C_API extern "C" __declspec(dllimport)
#	endif // defined(VEGA_BUILD)
#else
#	define VEGA_API __attribute__((__visibility__("default")))
#	define VEGA_C_API extern "C" __attribute__((__visibility__("default")))
#endif // defined(VEGA_MSVC)

/* Library Version */
//! @brief The Vega library major version.
#define VEGA_VERSION_MAJOR 0
//! @brief The Vega library minor version.
#define VEGA_VERSION_MINOR 1
//! @brief The Vega library patch version.
#define VEGA_VERSION_PATCH 0
//! @brief Creates a 32-bit packed integer version from the major, minor, and patch components.
#define VEGA_MAKE_VERSION(_maj,_min,_pat) (((_maj)<<22)|((_min)<<12)|(_pat))
//! @brief The 32-bit packed integer version of the library.
#define VEGA_VERSION VEGA_MAKE_VERSION(VEGA_VERSION_MAJOR,VEGA_VERSION_MINOR,VEGA_VERSION_PATCH)

/* Common Standard Headers */
#include <cassert>
#include <cstdarg>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <functional>
#include <memory>
#include <new>
#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>
#include <utility>
#include <variant>


/* Common Types and Functions */
namespace vega
{

/* Integer Types */
//! @brief Unsigned 8-bit integer.
using uint8  = std::uint8_t;
//! @brief Unsigned 16-bit integer.
using uint16 = std::uint16_t;
//! @brief Unsigned 32-bit integer.
using uint32 = std::uint32_t;
//! @brief Unsigned 64-bit integer.
using uint64 = std::uint64_t;
//! @brief Signed 8-bit integer.
using int8   = std::int8_t;
//! @brief Signed 16-bit integer.
using int16  = std::int16_t;
//! @brief Signed 32-bit integer.
using int32  = std::int32_t;
//! @brief Signed 64-bit integer.
using int64  = std::int64_t;

/* Strings */
//! @brief std::string
using String = std::string;
//! @brief std::string_view
using StringView = std::string_view;

/* Pointer Types */
//! @brief std::unique_ptr<T, Deleter>
template<typename T, typename Deleter = std::default_delete<T>>
using UPtr = std::unique_ptr<T, Deleter>;
//! @brief std::shared_ptr<T>
template<typename T>
using SPtr = std::shared_ptr<T>;
//! @brief std::weak_ptr<T>
template<typename T>
using WPtr = std::weak_ptr<T>;

/* Other Utility Types */
//! @brief std::optional<T>
template<typename T>
using Opt = std::optional<T>;
//! @brief std::variant<Types...>
template<typename... Types>
using Var = std::variant<Types...>;

/* String Formatting */
//! @brief Creates a formatted string from the given arguments.
//! @tparam ...Args The types for the format arguments.
//! @param fmt The format string.
//! @param ...args The format arguments.
//! @return A formatted string.
template<typename... Args>
inline String to_string(const StringView& fmt, Args&&... args)
{
	static constexpr size_t BUFSIZE{ 512 };
	char buf[BUFSIZE];
	int len = snprintf(buf, BUFSIZE, fmt.data(), std::forward<Args>(args)...);
	return (len < 0) ? String() : String(buf, (len < BUFSIZE) ? len : BUFSIZE);
}

} // namespace vega


/* Class Macros */
//! @brief Use within a type declaration to make the type non-copyable and default non-moveable.
#define VEGA_NO_COPY(cName) public: cName(const cName&) = delete; cName& operator = (const cName&) = delete;
//! @brief Use within a type declaration to make the type non-moveable and default non-copyable.
#define VEGA_NO_MOVE(cName) public: cName(cName&&) = delete; cName& operator = (cName&&) = delete;
//! @brief Use within a type declaration to make the type default copyable.
#define VEGA_DEFAULT_COPY(cName) public: cName(const cName&) = default; cName& operator = (const cName&) = default;
//! @brief Use within a type declaration to make the type default moveable.
#define VEGA_DEFAULT_MOVE(cName) public: cName(cName&&) = default; cName& operator = (cName&&) = default;
//! @brief Use within a type declaration to make the type non-initializable (static).
#define VEGA_NO_INIT(cName) \
	public: cName() = delete; \
		void* operator new (size_t) = delete; void* operator new[] (size_t) = delete; \
		void* operator new (size_t, const std::nothrow_t&) = delete; \
		void* operator new[] (size_t, const std::nothrow_t&) = delete; \
		void* operator new (size_t, void*) = delete; void* operator new[] (size_t, void*) = delete;
