/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AppThread.hpp Support for frame-based application multi-threading.

#include "../Common.hpp"
#include "./Time.hpp"

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>


namespace vega
{

//! @brief Base type for objects that run the Update/Render loop on a separate thread, synchronized to the application.
//! 
//! Once started, application threads will start looping at the beginning of the next application frame, unless added
//! in the ApplicationBase::BeginFrame() function. Threads must complete before a set amount of time, or the 
//! application will abort with an error.
class VEGA_API AppThread
{
	friend class AppBase;

	// Implementation of 1:1 wait/signal condition variable
	class Signal final
	{
	public:
		Signal();
		~Signal() { }

		// Notify the waiting thread on the signal, returns if a thread was waiting
		bool notify();
		// Wait for the signal to be triggered, returns if the signal occured before the timeout was reached
		// Designed for main thread to wait on dedicated thread
		bool wait(TimeSpan timeout);
		// Wait for the signal to be triggered, or the passed exit flag to be set to true
		// Designed for dedicated thread to wait on main thread
		void wait(const std::atomic_bool& exitFlag);

	private:
		std::condition_variable cond_;
		std::mutex mutex_;
		bool waiting_;   // Signals if a thread is waiting on the signal
		bool triggered_; // Detect spurious wakeups
	}; // class Signal

public:
	//! @brief The default timeout for considering application threads to be unresponsive and generating an exception.
	inline static constexpr TimeSpan DEFAULT_THREAD_TIMEOUT{ TimeSpan::Seconds(5) };

	virtual ~AppThread();

	//! @brief Gets the name of the thread, which may not be set, and is not guaranteed to be unique.
	inline const String& name() const { return name_; }
	//! @brief Sets the name of the thread.
	void name(const String& name);

	//! @brief Gets if the thread has been requested to close.
	inline bool shouldExit() const { return shouldExit_.load(); }
	//! @brief Gets if the thread is currently running.
	inline bool running() const { return !!thread_ && !ended_.load(); }
	//! @brief Gets if the thread has ended execution.
	inline bool ended() const { return ended_.load(); }

	//! @brief Marks that the thread should exit. The exit will occur the next time the exit flag is checked, which
	//! happens before updating, before rendering, and at the end of each frame.
	void exit();

	//! @brief Gets if one of the application threads has encountered a fatal exception.
	inline static bool HasThreadException() { return !ExceptionWhat_.empty(); }

protected:
	AppThread();

	//! @brief Called to setup the thread object immediately before it starts to execute alongside the application.
	//! This is the first function called on the separate dedicated system thread.
	virtual void onStart() { }
	//! @brief Called synchronously with the main application Update() function, but on its own thread. 
	//! The main thread will block at the end of its Update function until this function returns on all running threads.
	virtual void update() = 0;
	//! @brief Called synchronously with the main application Render() function, but on its own thread. 
	//! The main thread will block at the end of its Render function until this function returns on all running threads.
	virtual void render() = 0;
	//! @brief Called after the thread has started to exit to perform cleanup. 
	//! This is the last function called on the separate dedicated system thread.
	virtual void onStop() { }

private:
	// Called by ApplicationBase to start the thread, with a timeout limit to generate an error
	void startThread(TimeSpan timeLimit = DEFAULT_THREAD_TIMEOUT);

	// Frame function start/waits
	void startUpdate();
	void waitEndUpdate(TimeSpan timeLimit = DEFAULT_THREAD_TIMEOUT);
	void startRender();
	void waitEndRender(TimeSpan timeLimit = DEFAULT_THREAD_TIMEOUT);

	// Called by ApplicationBase to wait for the thread to exit
	void waitForExit(TimeSpan timeLimit = DEFAULT_THREAD_TIMEOUT);

	// Impementation thread function
	void thread_func();

	// Called when a thread has an exception, to inform the main thread and allow termination
	static void OnThreadException(const std::exception& ex, const String& name);

private:
	const uint32 index_;          // Unique index for the thread
	String name_;                 // Debug name identifying this thread
	std::mutex nameMutex_;
	UPtr<std::thread> thread_;    // Thread handle for this thread
	std::thread::id threadId_;    // Id of the app thread
	std::atomic_bool shouldExit_; // Flag for marking thread exit
	std::atomic_bool ended_;      // Set in the thread the indicate when execution is complete
	bool causedTimeout_;          // Marks the thread has caused a timeout
	struct
	{
		Signal startComplete{ }; // Dedicated -> Main
		Signal updateStart{ };   // Main -> Dedicated
		Signal updateEnd{ };     // Dedicated -> Main
		Signal renderStart{ };   // Main -> Dedicated
		Signal renderEnd{ };     // Dedicated -> Main
		Signal exitComplete{ };  // Dedicated -> Main
	} signals_;

	static uint32 ThreadIndex_;
	static String ExceptionWhat_;
	static String ExceptionThreadName_;

	VEGA_NO_COPY(AppThread)
	VEGA_NO_MOVE(AppThread)
}; // class AppThread

} // namespace vega
