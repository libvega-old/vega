/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file InlineVector.hpp Sized inline storage type.

#include "../Common.hpp"

#include <iterator>


namespace vega
{

//! @brief Acts like an inline std::vector<T> with maximum size N, or as an std::array<T, N> with an adjustable size.
//! @tparam T The type contained in the container.
//! @tparam N The maximum number of items in the container.
template<typename T, size_t N>
class InlineVector final
{
	static_assert(sizeof(T) > 0, "Cannot declare an InlineVector with an incomplete type");
	static_assert(N > 0, "Cannot declare an InlineVector with a capacity of zero");

	using StorageType = std::aligned_storage_t<sizeof(T), alignof(T)>;
	using DataType = T;

public:
	//! @brief The capacity of the specialized vector type.
	inline static constexpr size_t Capacity{ N };

	InlineVector() = default;
	InlineVector(const std::initializer_list<T>& list) : InlineVector() {
		assign(list.begin(), list.end());
	}
	InlineVector(const InlineVector& o) : InlineVector() {
		assign(o.begin(), o.end());
	}
	InlineVector& operator = (const InlineVector& o) {
		assign(o.begin(), o.end());
		return *this;
	}
	InlineVector(InlineVector&& o) : InlineVector() {
		static_assert(std::is_move_constructible_v<T>, "T must be move-constructable to use InlineVector move ctor");
		auto iter = o.begin();
		for (size_ = 0; iter != o.end(); ++iter, ++size_) {
			new (&(data()[size_])) T(std::move(*iter));
		}
		o.size_ = 0;
	}
	InlineVector& operator = (InlineVector&& o) {
		static_assert(std::is_move_constructible_v<T>, "T must be move-constructable to use InlineVector move assign");
		static_assert(std::is_move_assignable_v<T>, "T must be move-assignable to use InlineVector move assign");
		auto iter = o.begin();
		size_t index{ 0 };
		while (iter != o.end()) {
			if (index < size_) {
				data()[index] = std::move(*iter);
			}
			else {
				new (&(data()[index])) T(std::move(*iter));
			}
			++index; ++iter;
		}
		while (index < size_) {
			data()[index++].~T();
		}
		size_ = o.size_;
		o.size_ = 0;
		return *this;
	}
	~InlineVector() {
		for (size_t ii = 0; ii < size_; ++ii) {
			std::launder(reinterpret_cast<T*>(&data_[ii]))->~T();
		}
	}

	//! @brief Gets the value at the given index. Undefined if the index is outside the range of valid elements.
	//! @param index The element index.
	inline T& operator [] (size_t index) { return data()[index]; }
	//! @brief Gets the value at the given index. Undefined if the index is outside the range of valid elements.
	//! @param index The element index.
	inline const T& operator [] (size_t index) const { return data()[index]; }

	//! @brief The number of initialized elements in the vector.
	inline size_t size() const { return size_; }
	//! @brief The capacity of the vector.
	inline size_t capacity() const { return N; }
	//! @brief Gets if there are no objects in the vector (size() == 0).
	inline bool empty() const { return size_ == 0; }

	//! @brief The raw pointer to the contained data, only guaranteed to be initialized out to size() elements.
	inline T* data() { return std::launder(reinterpret_cast<T*>(&data_[0])); }
	//! @brief The raw pointer to the contained data, only guaranteed to be initialized out to size() elements.
	inline const T* data() const { return std::launder(reinterpret_cast<const T*>(&data_[0])); }
	//! @brief Gets a mutable iterator to the beginning of the data.
	inline T* begin() { return data(); }
	//! @brief Gets an immutable iterator to the beginning of the data.
	inline const T* begin() const { return data(); }
	//! @brief Gets a mutable iterator to the end of the data.
	inline T* end() { return data() + size_; }
	//! @brief Gets an immutable iterator to the end of the data.
	inline const T* end() const { return data() + size_; }
	//! @brief Gets a mutable iterator to the beginning of the reversed data.
	inline auto rbegin() { return std::make_reverse_iterator(end()); }
	//! @brief Gets an immutable iterator to the beginning of the reversed data.
	inline auto rbegin() const { return std::make_reverse_iterator(end()); }
	//! @brief Gets a mutable iterator to the end of the reversed data.
	inline auto rend() { return std::make_reverse_iterator(begin()); }
	//! @brief Gets an immutable iterator to the end of the reversed data.
	inline auto rend() const { return std::make_reverse_iterator(begin()); }

	//! @brief Copies a value to the end of the vector. Throws \c std::bad_alloc if the vector is out of space.
	//! @param val The value to copy into the vector.
	inline void push_back(const T& val) {
		static_assert(std::is_copy_constructible_v<T>, "T must be copy-constructable to use InlineVector::push_back");
		if (size_ == capacity()) {
			throw std::bad_alloc{};
		}
		new (&(data()[size_++])) T(val);
	}
	//! @brief Constructs a value in-place at the vector end. Throws \c std::bad_alloc if the vector is out of space.
	//! @tparam ...Args The in-place constructor argument types.
	//! @param ...args The in-place constructor arguments.
	template<typename... Args>
	inline T& emplace_back(Args&&... args) {
		if (size_ == capacity()) {
			throw std::bad_alloc{};
		}
		return *(new (&(data()[size_++])) T(std::forward<Args>(args)...));
	}
	//! @brief Removes the final element from the vector. No-op on empty vectors.
	inline void pop_back() {
		if (size_ > 0) {
			data()[--size_].~T();
		}
	}

	//! @brief Sets the contents of the vector equal to the range provided by the iterators through copy semantics.
	//!			Ignores iterator range values outside of the maximum capacity of the vector.
	//! @param begin The beginning of the copy range.
	//! @param end The end of the copy range.
	inline void assign(const T* begin, const T* end) {
		static_assert(std::is_copy_constructible_v<T>, "T must be copy-constructable to use InlineVector::assign");
		static_assert(std::is_copy_assignable_v<T>, "T must be copy-assignable to use InlineVector::assign");
		size_t index{ 0 };
		size_t newSize{ 0 };
		while ((index < N) && (begin != end)) {
			if (index < size_) {
				data()[index] = *begin;
			}
			else {
				new (&(data()[index])) T(*begin);
			}
			++begin; ++index; ++newSize;
		}
		while (index < size_) {
			data()[index++].~T();
		}
		size_ = newSize;
	}

	//! @brief Destroys all objects in the vector and sets the size to zero.
	inline void clear() {
		for (uint32 ii = 0; ii < size_; ++ii) {
			data()[ii].~T();
		}
		size_ = 0;
	}

private:
	size_t size_{ 0 };
	StorageType data_[N];
}; // class InlineVector

} // namespace vega
