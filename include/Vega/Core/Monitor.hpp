/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Monitor.hpp Types and operations for monitor peripherals.

#include "../Common.hpp"
#include "../Math/Vec2.hpp"
#include "../Math/Rect2.hpp"

#include <vector>


namespace vega
{

//! @brief Describes a supported hardware video mode for a Monitor.
struct VEGA_API VideoMode final
{
	friend class Monitor;

public:
	//! @brief The width of the video mode in logical pixels.
	inline uint32 width() const { return width_; }
	//! @brief The height of the video mode in logical pixels.
	inline uint32 height() const { return height_; }
	//! @brief The bit-depth of the color (24 is full color).
	inline uint32 colorDepth() const { return color_; }
	//! @brief The refresh rate in Hz.
	inline uint32 refresh() const { return refresh_; }

	inline bool operator == (const VideoMode& r) const {
		return width_ == r.width_ && height_ == r.height_ && color_ == r.color_ && refresh_ == r.refresh_;
	}
	inline bool operator != (const VideoMode& r) const {
		return width_ != r.width_ || height_ != r.height_ || color_ != r.color_ || refresh_ != r.refresh_;
	}

private:
	uint32 width_;
	uint32 height_;
	uint32 color_;
	uint32 refresh_;
}; // struct VideoMode


//! @brief A handle to a video monitor.
class VEGA_API Monitor final
{
	friend class Window;

	struct MonitorHandle final
	{
		void* handle;
		std::vector<VideoMode> modes;
		MonitorHandle(void* handle) : handle{ handle }, modes{ } { }
	}; // struct MonitorHandle

public:
	//! @brief Returns if the monitor handle is valid (not null, and still connected).
	inline bool isValid() const { return handle_.use_count() > 0; }
	//! @brief Returns if the monitor is considered the primary monitor on the system.
	bool isPrimary() const;

	//! @brief The reported name of the monitor, not guaranteed to be unique in the system.
	String name() const;
	//! @brief The physical size of the monitor screen, in millimeters.
	Vec2ui physicalSize() const;

	//! @brief The size of the monitor, in logical pixels (which may not match physical pixels in hDPI displays).
	Vec2ui size() const;
	//! @brief The position of the monitor, in global screen space in logical pixels.
	Vec2i position() const;
	//! @brief The logical content scale for the monitor, which provides the upscaling or downscaling in effect.
	Vec2f contentScale() const;
	//! @brief The area in which content can be displayed, which does not include system task bars, in logical pixels.
	Rect2i workArea() const;
	//! @brief The total display area in global screen space, including any system task bars.
	Rect2i displayArea() const;
	//! @brief The set of supported hardware video modes.
	const std::vector<VideoMode>& videoModes() const;
	//! @brief The current video mode of the monitor.
	VideoMode currentVideoMode() const;

	//! @brief Gets if the monitors are the same.
	inline bool operator == (Monitor r) const {
		return !handle_.owner_before(r.handle_) && !r.handle_.owner_before(handle_);
	}
	//! @brief Gets if the monitors are different.
	inline bool operator != (Monitor r) const {
		return handle_.owner_before(r.handle_) || r.handle_.owner_before(handle_);
	}

	//! @brief Gets the list of currently connected monitors. Must be called from main thread only.
	static const std::vector<Monitor>& Monitors();
	//! @brief Gets a handle to the primary system monitor. Must be called from main thread only.
	static Monitor Primary();

private:
	Monitor() : handle_{ } { }
	explicit Monitor(const SPtr<MonitorHandle>& handle) : handle_{ handle } { }

public:
	//! @brief Constant handle representing an invalid or non-extant monitor.
	static const Monitor Invalid;

private:
	WPtr<MonitorHandle> handle_;

	static std::vector<SPtr<MonitorHandle>> Handles_; // Logically UPtr
	static std::vector<Monitor> Monitors_;
}; // class Monitor

} // namespace vega
