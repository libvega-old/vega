/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Logging.hpp Message logging functionality.

#include "../Common.hpp"
#include "./Flags.hpp"
#include "./Time.hpp"

#include <sstream>
#include <ostream>


namespace vega
{

class Logger;

//! @brief Represents logging message importance levels.
enum class LogLevel : uint32
{
	//! @brief Indicates low-level, non-important debug messages.
	Verbose = 1 << 0,
	//! @brief Indicates normal execution informative messages.
	Info    = 1 << 1,
	//! @brief Indicates an unexpected but non-error state.
	Warning = 1 << 2,
	//! @brief Indicates a recoverable error state.
	Error   = 1 << 3,
	//! @brief Very high importance message indicating an unrecoverable error state.
	Fatal   = 1 << 4,

	//! @brief Bitmask of logging levels representing no levels.
	None = 0,
	//! @brief Bitmask representing all logging levels except verbose.
	Standard = Info | Warning | Error | Fatal,
	//! @brief Bitmask representing all logging levels.
	All = Verbose | Info | Warning | Error | Fatal
}; // enum class LogLevel

//! @brief Mask of LogLevel values.
using LogLevelMask = Flags<LogLevel>;
VEGA_DEFINE_FLAGS_OPERATORS(LogLevel)

//! @brief Gets the string representation 
VEGA_API String to_string(LogLevelMask mask);


//! @brief Base type for handling log messages as they are logged.
class VEGA_API LogHandler
{
	friend class Logger;

public:
	virtual ~LogHandler();

	//! @brief If the handler instance has been installed and is active in handling messages.
	inline bool isInstalled() const { return installed_; }

	//! @brief Virtual function to implement handling for log messages.
	//! @param logger The logger that produced the message.
	//! @param level The logging level of the message.
	//! @param time The total elapsed runtime of the application when the message was logged.
	//! @param message The pre-formatted message string.
	virtual void handleMessage(const Logger& logger, LogLevel level, const TimeSpan& time, 
		const StringView& message) = 0;

	//! @brief Installs a new handler to handle messages.
	//! @param handler The handler to install to actively handle messages.
	static void Install(LogHandler& handler);

protected:
	LogHandler();

private:
	static void PostMessage(const Logger& logger, LogLevel level, const StringView& message);

private:
	bool installed_;

	VEGA_NO_COPY(LogHandler)
	VEGA_NO_MOVE(LogHandler)
}; // class LogHandler


//! @brief Manages a named source of logging messages.
class VEGA_API Logger final
	: public std::enable_shared_from_this<Logger>
{
	friend class Log;

public:
	//! @brief The number of characters in a logger tag.
	inline static constexpr uint32 TAG_SIZE{ 8 };
	//! @brief Logger tag - 8 characters giving a short label for the logger.
	struct Tag final { char str[TAG_SIZE + 1]; };

	~Logger();

	//! @brief The full name of the logger.
	inline const String& name() const { return name_; }
	//! @brief The short tag label for the logger.
	inline const Tag& tag() const { return tag_; }

	//! @brief Writes a verbose message to the logger.
	//! @param message The message to write.
	inline void verbose(const StringView& message) const {
		LogHandler::PostMessage(*this, LogLevel::Verbose, message);
	}
	//! @brief Writes an info message to the logger.
	//! @param message The message to write.
	inline void info(const StringView& message) const {
		LogHandler::PostMessage(*this, LogLevel::Info, message);
	}
	//! @brief Writes a warning message to the logger.
	//! @param message The message to write.
	inline void warn(const StringView& message) const {
		LogHandler::PostMessage(*this, LogLevel::Warning, message);
	}
	//! @brief Writes an error message to the logger.
	//! @param message The message to write.
	inline void error(const StringView& message) const {
		LogHandler::PostMessage(*this, LogLevel::Error, message);
	}
	//! @brief Writes a fatal message to the logger.
	//! @param message The message to write.
	inline void fatal(const StringView& message) const {
		LogHandler::PostMessage(*this, LogLevel::Fatal, message);
	}

	//! @brief Writes a verbose message to the logger.
	//! @tparam ...Args The format argument types.
	//! @param message The message to write.
	//! @param ...args The format arguments.
	template<typename... Args>
	inline void verbose(const StringView& message, Args&&... args) const {
		LogHandler::PostMessage(*this, LogLevel::Verbose, to_string(message, std::forward<Args>(args)...));
	}
	//! @brief Writes an info message to the logger.
	//! @tparam ...Args The format argument types.
	//! @param message The message to write.
	//! @param ...args The format arguments.
	template<typename... Args>
	inline void info(const StringView& message, Args&&... args) const {
		LogHandler::PostMessage(*this, LogLevel::Info, to_string(message, std::forward<Args>(args)...));
	}
	//! @brief Writes a warning message to the logger.
	//! @tparam ...Args The format argument types.
	//! @param message The message to write.
	//! @param ...args The format arguments.
	template<typename... Args>
	inline void warn(const StringView& message, Args&&... args) const {
		LogHandler::PostMessage(*this, LogLevel::Warning, to_string(message, std::forward<Args>(args)...));
	}
	//! @brief Writes an error message to the logger.
	//! @tparam ...Args The format argument types.
	//! @param message The message to write.
	//! @param ...args The format arguments.
	template<typename... Args>
	inline void error(const StringView& message, Args&&... args) const {
		LogHandler::PostMessage(*this, LogLevel::Error, to_string(message, std::forward<Args>(args)...));
	}
	//! @brief Writes a fatal message to the logger.
	//! @tparam ...Args The format argument types.
	//! @param message The message to write.
	//! @param ...args The format arguments.
	template<typename... Args>
	inline void fatal(const StringView& message, Args&&... args) const {
		LogHandler::PostMessage(*this, LogLevel::Fatal, to_string(message, std::forward<Args>(args)...));
	}

	//! @brief Gets (or creates, if not found) the logger with the given name.
	//! @param name The name of the logger to get or create.
	//! @param tag The optional logger tag - if not specified, it will default to the first 8 characters of @p name.
	//! @return The logger instance with the given name.
	static SPtr<Logger> Get(const StringView& name, const Opt<StringView>& tag = {});

	//! @brief Gets the logger with the given name, returning @c nullptr if no such logger exists.
	//! @param name The name of the logger to get.
	//! @return The logger instance with the given name, or @c nullptr.
	static SPtr<Logger> GetExisting(const StringView& name);

private:
	Logger(const StringView& name, const StringView& tag);

private:
	const String name_;
	Tag tag_;

	VEGA_NO_COPY(Logger)
	VEGA_NO_MOVE(Logger)
}; // class Logger


//! @brief Streaming type that can write formatted messages to a LogHandler. The message must be manually flushed.
class VEGA_API LogStream final
{
	friend class Log;

public:
	~LogStream();

	//! @brief The logging level that this stream writes messages at.
	inline LogLevel level() const { return level_; }
	//! @brief If there is a pending message that has not yet been written.
	inline bool dirty() const { return dirty_; }

	//! @brief Streams the value into the message buffer.
	//! @param value The value to add to the message buffer, or a special object to flush the message.
	template<typename T>
	inline LogStream& operator << (const T& value) {
		buffer_ << value;
		dirty_ = true;
		return *this;
	}
	//! @brief Allows using `std::endl` and `std::flush` to flush the stream object.
	inline LogStream& operator << (decltype(std::endl<char, std::char_traits<char>>) pfn) {
		flush();
		return *this;
	}

private:
	LogStream(const Logger& logger, LogLevel level);

	void flush();

private:
	const Logger* logger_;
	LogLevel level_;
	std::stringstream buffer_;
	bool dirty_;

	VEGA_NO_COPY(LogStream)
	VEGA_NO_MOVE(LogStream)
}; // class LogStream


//! @brief Provides access to the default logging functionality.
class VEGA_API Log final
{
public:
	//! @brief Returns a new verbose-level LogStream for the default logger.
	inline static LogStream Verbose() { return { *DefaultLogger_, LogLevel::Verbose }; }
	//! @brief Returns a new info-level LogStream for the default logger.
	inline static LogStream Info() { return { *DefaultLogger_, LogLevel::Info }; }
	//! @brief Returns a new warning-level LogStream for the default logger.
	inline static LogStream Warn() { return { *DefaultLogger_, LogLevel::Warning }; }
	//! @brief Returns a new error-level LogStream for the default logger.
	inline static LogStream Error() { return { *DefaultLogger_, LogLevel::Error }; }
	//! @brief Returns a new fatal-level LogStream for the default logger.
	inline static LogStream Fatal() { return { *DefaultLogger_, LogLevel::Fatal }; }

	//! @brief Returns a new verbose-level LogStream for the given logger.
	inline static LogStream Verbose(const Logger& logger) { return { logger, LogLevel::Verbose }; }
	//! @brief Returns a new info-level LogStream for the given logger.
	inline static LogStream Info(const Logger& logger) { return { logger, LogLevel::Info }; }
	//! @brief Returns a new warning-level LogStream for the given logger.
	inline static LogStream Warn(const Logger& logger) { return { logger, LogLevel::Warning }; }
	//! @brief Returns a new error-level LogStream for the given logger.
	inline static LogStream Error(const Logger& logger) { return { logger, LogLevel::Error }; }
	//! @brief Returns a new fatal-level LogStream for the given logger.
	inline static LogStream Fatal(const Logger& logger) { return { logger, LogLevel::Fatal }; }

	//! @brief Logs a LogLevel::Verbose level message.
	inline static void Verbose(const StringView& message) { DefaultLogger_->verbose(message); }
	//! @brief Logs a LogLevel::Info level message.
	inline static void Info(const StringView& message) { DefaultLogger_->info(message); }
	//! @brief Logs a LogLevel::Warning level message.
	inline static void Warn(const StringView& message) { DefaultLogger_->warn(message); }
	//! @brief Logs a LogLevel::Error level message.
	inline static void Error(const StringView& message) { DefaultLogger_->error(message); }
	//! @brief Logs a LogLevel::Fatal level message.
	inline static void Fatal(const StringView& message) { DefaultLogger_->fatal(message); }

	//! @brief Logs a LogLevel::Verbose level formatted message.
	template<class... Args>
	inline static void Verbose(const StringView& message, Args&&... args) {
		DefaultLogger_->verbose(message, std::forward<Args>(args)...);
	}
	//! @brief Logs a LogLevel::Info level formatted message.
	template<class... Args>
	inline static void Info(const StringView& message, Args&&... args) {
		DefaultLogger_->info(message, std::forward<Args>(args)...);
	}
	//! @brief Logs a LogLevel::Warning level formatted message.
	template<class... Args>
	inline static void Warn(const StringView& message, Args&&... args) {
		DefaultLogger_->warn(message, std::forward<Args>(args)...);
	}
	//! @brief Logs a LogLevel::Error level formatted message.
	template<class... Args>
	inline static void Error(const StringView& message, Args&&... args) {
		DefaultLogger_->error(message, std::forward<Args>(args)...);
	}
	//! @brief Logs a LogLevel::Fatal level formatted message.
	template<class... Args>
	inline static void Fatal(const StringView& message, Args&&... args) {
		DefaultLogger_->fatal(message, std::forward<Args>(args)...);
	}

	//! @brief Enable the default console log handler.
	//! @param level The initial level mask for the console logger.
	//! @return The new default logger instance.
	static void EnableDefaultConsoleHandler(LogLevel level = LogLevel::Standard);
	//! @brief Enable the default file log handler.
	//! @param history Creates a history of log files, otherwise always overwrite the log file.
	//! @param level The initial levels mask for the console logger.
	//! @return The new default logger instance.
	static void EnableDefaultFileHandler(bool history, LogLevel level = LogLevel::Standard);

private:
	static UPtr<Logger> DefaultLogger_;

	VEGA_NO_COPY(Log)
	VEGA_NO_MOVE(Log)
	VEGA_NO_INIT(Log)
}; // class Log

} // namespace vega
