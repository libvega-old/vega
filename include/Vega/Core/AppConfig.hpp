/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AppConfig.hpp Application configuration.

#include "../Common.hpp"
#include "./Version.hpp"
#include "../Math/Vec2.hpp"
#include "../Audio/AudioDevice.hpp"
#include "../Graphics/GraphicsDevice.hpp"


namespace vega
{

//! @brief Provides launch-time application information.
class VEGA_API ApplicationInfo final
{
public:
	//! @brief Statically configure the application name and version.
	//! @param appName The application name.
	//! @param appVersion The application version.
	ApplicationInfo(const String& appName, Version appVersion);
	~ApplicationInfo() { }

	//! @brief The configured application name.
	inline static const String& AppName() { return AppName_; }
	//! @brief The configured application version.
	inline static Version AppVersion() { return AppVersion_; }

private:
	static String AppName_;
	static Version AppVersion_;

	VEGA_NO_COPY(ApplicationInfo)
	VEGA_NO_MOVE(ApplicationInfo)
}; // class ApplicationInfo

//! @brief Used to initialize an ApplicationInfo instance at init-time to configure the application information.
#define VEGA_APP_INFO(name,version) static const vega::ApplicationInfo VEGA_APP_INFO_{ (name), (version) };


//! @brief Allows configuration of an AppBase instance.
class VEGA_API AppConfig final
{
public:
	//! @brief Initialize a new configuration with default values.
	AppConfig();
	~AppConfig();

	//! @brief The currently configured window size. Default: { 1280, 720 }.
	inline const Vec2ui& windowSize() const { return windowSize_; }
	//! @brief Update the window size configuration.
	inline AppConfig& windowSize(const Vec2ui& size) { windowSize_ = size; return *this; }

	//! @brief The currently selected GraphicsDevice, or \c nullptr to use the system default. Default: \c nullptr.
	inline const GraphicsDevice* graphicsDevice() const { return gDevice_; } // TODO
	//! @brief Sets the new GraphicsDevice to use, or \c nullptr to use the system default.
	//! Use GraphicsDevice::AllDevices() to get the list of allowable devices to give to this function.
	inline AppConfig& graphicsDevice(GraphicsDevice* device) { gDevice_ = device; return *this; } // TODO

	//! @brief The currently selected AudioDevice, or \c nullptr to use the sustem default. Default: \c nullptr.
	inline const AudioDevice* audioDevice() const { return aDevice_; }
	//! @brief Sets the new AudioDevice to use, or \c nullptr to use the system default.
	//! Use AudioDevice::AllDevices() to get the list of allowable devices to give to this function.
	inline AppConfig& audioDevice(AudioDevice* device) { aDevice_ = device; return *this; }

	//! @brief The folder to use as the root for loading global assets. Default: \c "./Data".
	inline const String& assetsPath() const { return assetsPath_; }
	//! @brief Sets the root folder for loading global assets.
	inline AppConfig& assetsPath(const String& path) { assetsPath_ = path; return *this; }

private:
	Vec2ui windowSize_;
	const GraphicsDevice* gDevice_;
	const AudioDevice* aDevice_;
	String assetsPath_;
}; // class AppConfig

} // namespace vega
