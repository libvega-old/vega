/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Version.hpp Semantic versioning.

#include "../Common.hpp"


namespace vega
{

//! @brief Represents a `major.minor.patch` semantic version as a packed integer.
struct VEGA_API Version final
{
public:
	//! @brief Create a version representing 0.0.0.
	constexpr Version() noexcept : value_{ 0 } { }
	//! @brief Create a version from a packed value.
	//! @param value The version value.
	constexpr explicit Version(uint32 value) noexcept : value_{ value } { }
	//! @brief Create a version from separate major, minor, and patch values.
	//! @param major The major version.
	//! @param minor The minor version.
	//! @param patch The patch version.
	constexpr Version(uint32 major, uint32 minor, uint32 patch = 0) noexcept
		: value_{ VEGA_MAKE_VERSION(major, minor, patch) }
	{ }

	//! @brief The major version.
	inline constexpr uint32 major() const { return value_ >> 22; }
	//! @brief The minor version.
	inline constexpr uint32 minor() const { return (value_ >> 12) & 0x3FF; }
	//! @brief The patch version.
	inline constexpr uint32 patch() const { return (value_ & 0xFFF); }
	//! @brief The full 32-bit integer representing the version.
	inline constexpr uint32 value() const { return value_; }

	inline constexpr bool operator <  (Version r) const { return value_ < r.value_; }
	inline constexpr bool operator <= (Version r) const { return value_ <= r.value_; }
	inline constexpr bool operator >  (Version r) const { return value_ > r.value_; }
	inline constexpr bool operator >= (Version r) const { return value_ >= r.value_; }
	inline constexpr bool operator == (Version r) const { return value_ == r.value_; }
	inline constexpr bool operator != (Version r) const { return value_ != r.value_; }

private:
	uint32 value_;
}; // struct Version

} // namespace vega
