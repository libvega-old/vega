/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Flags.hpp Flag-like typesafe enum masks.

#include "../Common.hpp"


namespace vega
{

//! @brief Compile-time checked type for implementing enum flags as bitmasks.
//! @tparam T The enum type to use for flags, must have an unsigned underlying type.
template<typename T>
struct Flags final
{
	static_assert(std::is_enum_v<T>, "Flags<T> type parameter must be an enum");
	static_assert(std::is_unsigned_v<std::underlying_type_t<T>> && std::is_integral_v<std::underlying_type_t<T>>,
		"Flags<T> type parameter must have an unsigned underlying type");

public:
	//! @brief The enum type containing the flag values.
	using EnumType = T;
	//! @brief The unsigned integral type underlying the enum type.
	using BaseType = std::underlying_type_t<T>;

	//! @brief Create blank flags mask.
	constexpr Flags() noexcept : value_{ } { }
	//! @brief Create flags mask underlying enum value.
	constexpr Flags(T value) noexcept : value_{ value } { }

	//! @brief The underlying enum value for the current flag mask.
	inline constexpr T value() const { return value_; }
	//! @brief The raw unsigned integral value for the current flag mask.
	inline constexpr BaseType rawValue() const { return BaseType(value_); }

	//! @brief Sets the bits associcated with the given mask.
	inline void set(T mask) { value_ = T(BaseType(value_) | BaseType(mask)); }
	//! @brief Clears the bits associated with the given mask.
	inline void clear(T mask) { value_ = T(BaseType(value_) & ~BaseType(mask)); }

	//! @brief Gets if the given flag or flags is entirely contained within the flag mask.
	inline constexpr bool has(T value) const { return (BaseType(value_) & BaseType(value)) == BaseType(value); }
	//! @brief Gets if any bit within the given mask is present in the flag mask.
	inline constexpr bool hasAny(Flags<T> mask) const { return (BaseType(value_) & mask.RawValue()) != 0; }
	//! @brief Gets if any flags are set in the flag mask.
	inline constexpr bool any() const { return BaseType(value_) != 0; }

	inline constexpr Flags<T> operator | (T r) const { return { T(BaseType(value_) | BaseType(r)) }; }
	inline constexpr Flags<T> operator & (T r) const { return { T(BaseType(value_) & BaseType(r)) }; }
	inline Flags<T>& operator |= (T r) { value_ = T(BaseType(value_) | BaseType(r)); return *this; }
	inline Flags<T>& operator &= (T r) { value_ = T(BaseType(value_) & BaseType(r)); return *this; }

	inline constexpr bool operator == (Flags<T> r) const { return value_ == r.value_; }
	inline constexpr bool operator != (Flags<T> r) const { return value_ != r.value_; }
	inline constexpr bool operator <  (Flags<T> r) const { return value_ <  r.value_; }

	//! @brief Cast to bool, same as any().
	inline constexpr explicit operator bool() const { return any(); }

private:
	T value_;
}; // struct Flags

//! @brief Declares the standard | and & operators for enum flag types used with vega::Flags<T>.
#define VEGA_DEFINE_FLAGS_OPERATORS(enumType) \
	inline constexpr ::vega::Flags<enumType> operator | (enumType l, enumType r)\
		{ return ::vega::Flags<enumType>{ l } | r; } \
	inline constexpr ::vega::Flags<enumType> operator & (enumType l, enumType r)\
		{ return ::vega::Flags<enumType>{ l } & r; }

} // namespace vega
