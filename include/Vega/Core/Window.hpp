/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Window.hpp Application window types and operations.

#include "../Common.hpp"
#include "../Math/Vec2.hpp"
#include "../Math/Rect2.hpp"
#include "./Monitor.hpp"


namespace vega
{

class Keyboard; class Mouse;
class Renderer;

//! @brief Supported modes for Window instances.
enum class WindowMode : uint32
{
	//! @brief Windowed mode.
	Window,
	//! @brief Windowed fullscreen mode (also called borderless fullscreen).
	FullscreenWindow,
	//! @brief Exclusive fullscreen mode.
	FullscreenExclusive
}; // enum class WindowMode

//! @brief Gets the string representation of the given WindowMode.
VEGA_API const String& to_string(WindowMode mode);


//! @brief Manages a graphical window that can receive input events and output rendering results.
class VEGA_API Window
{
	friend class WindowImpl;

public:
	virtual ~Window();

	//! @brief The index of the window (the order in the window creation).
	uint32 index() const;
	//! @brief If the window is the main application window.
	bool isMain() const;

	//! @brief The current title of the window, visible in the window bar if decorated.
	const String& title() const;
	//! @brief Sets the window title.
	//! @param title The new window title.
	void title(const String& title);
	//! @brief Gets if the window is resizable (using the keyboard/mouse) when in windowed mode.
	bool resizeable() const;
	//! @brief Sets if the window is resizable (using the keyboard/mouse) when in windowed mode.
	//! @param flag If the window is resizable.
	void resizeable(bool flag);
	//! @brief Gets if the window is decorated when in windowed mode.
	bool decorated() const;
	//! @brief Sets if the window is decorated when in windowed mode.
	//! @param flag If the window is decorated.
	void decorated(bool flag);
	//! @brief Gets if the window is floating (always on top) when in windowed mode.
	bool floating() const;
	//! @brief Sets if the window is floating (always on top) when in windowed mode.
	//! @param flag If the window is floating.
	void floating(bool flag);
	//! @brief Gets the current mode for the window.
	WindowMode windowMode() const;
	//! @brief Sets the window mode. Providing a monitor will set fullscreen, and providing a video mode will use
	//!			exclusive fullscreen.
	//! @param monitor The monitor to use for fullscreen, \c std::nullopt will set to windowed mode.
	//! @param vidMode The video mode to use for exclusive fullscreen, \c std::nullopt will use windowed fullscreen.
	void windowMode(Opt<Monitor> monitor, Opt<VideoMode> vidMode);

	//! @brief The position of the top-left corner of the window in logical pixels.
	Vec2i position() const;
	//! @brief The size of the window in logical pixels.
	Vec2ui size() const;
	//! @brief The size of the window renderer in physical pixels.
	Vec2ui rendererSize() const;
	//! @brief Returns the rectangular area the contains the content area of the window.
	inline Rect2i contentArea() const { return { position(), size() }; }

	//! @brief Updates the size of the window, and the window Renderer if not locked.
	//! @param size The new window size - if either component is zero, then the window is minimized.
	void size(const Vec2ui& size);
	//! @brief Locks the Renderer to the given size, ignoring window resizes.
	void lockRendererSize(const Vec2ui& size);
	//! @brief Unlocks the Renderer, allowing it to automatically match the window size.
	//! @param immediate If the Renderer should be immediately resized. \c false will wait until the next window resize.
	void unlockRendererSize(bool immediate = true);
	//! @brief Gets if the window Renderer has been locked to a specific size.
	bool isRendererLocked() const;

	//! @brief Returns the current window monitor (based on maximum overlap), or Monitor::Invalid if not visible.
	const Monitor& monitor() const;

	//! @brief Gets if the window has been requested to close either programatically, or from a system event.
	bool isCloseRequested() const;
	//! @brief Marks that the window should be closed.
	void close();
	//! @brief Hides the window, removing the icon from the task bar and minimizing the window without closing it.
	void hide();
	//! @brief Shows a window that has been previously hidden.
	void show();
	//! @brief Gets if the window is currently shown (not hidden).
	bool isVisible() const;
	//! @brief Causes the window to become the top-level window, and gives the window input focus.
	void focus();
	//! @brief Gets if the window currently has input focus.
	bool isFocused() const;
	//! @brief Invokes platform-specific functionality for bringing user attention to the window.
	void requestAttention();
	//! @brief Minimizes the window to the task bar.
	void minimize();
	//! @brief Restores a previously minimized window from the task bar.
	void restore();
	//! @brief Sets the window to the largest possible size for the current monitor workarea.
	void maximize();
	//! @brief Gets if the window is currently minimized.
	bool isMinimized() const;
	//! @brief Gets if the window is currently maximized.
	bool isMaximized() const;

	//! @brief If the window render refresh is synchronized to vertical retrace.
	bool vsync() const;
	//! @brief Sets if the window render refresh is synchronized to vertical retrace.
	void vsync(bool vsync);

	//! @brief Returns the keyboard input manager for this window.
	Keyboard* keyboard() const;
	//! @brief Returns the mouse input manager for this window.
	Mouse* mouse() const;

	//! @brief The Renderer instance used by the window.
	Renderer* renderer() const;

private:
	Window();

	VEGA_NO_COPY(Window)
	VEGA_NO_MOVE(Window)
}; // class Window

} // namespace vega
