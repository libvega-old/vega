/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AppBase.hpp Core application runtime logic and resource management.

#include "../Common.hpp"
#include "./AppConfig.hpp"
#include "./AppThread.hpp"
#include "./Window.hpp"
#include "../Audio/AudioEngine.hpp"
#include "../Asset/AssetManager.hpp"
#include "../Graphics/GraphicsEngine.hpp"
#include "../Render/RenderLayout.hpp"

#include <mutex>


namespace vega
{

//! @brief Lifetime stages for AppBase instances.
enum class AppStage : uint32
{
	//! @brief The application has been created, but AppBase::run() has not yet been called.
	Created = 0,
	//! @brief AppBase::run() has been called and the application is initializing.
	Initialize = 1,
	//! @brief The application is performing initial asset loading.
	AssetLoad = 2,
	//! @brief The application is handling AppBase::beginFrame().
	BeginFrame = 3,
	//! @brief The application is handling AppBase::update().
	Update = 4,
	//! @brief The application is handling AppBase::midFrame().
	MidFrame = 5,
	//! @brief The application is handling AppBase::render().
	Render = 6,
	//! @brief The application is handling AppBase::endFrame().
	EndFrame = 7,
	//! @brief The application is inbetween the end of a frame, and the start of the next.
	InterFrame = 8,
	//! @brief The application is terminating.
	Terminate = 9
}; // enum class AppStage


//! @brief Base class for implementing the core application logic and resource management, derived in client code.
class VEGA_API AppBase
{
	struct _EXIT_HANDLER;

public:
	virtual ~AppBase();

	//! @brief The name of the application, as configured by ApplicationInfo.
	inline const String& name() const { return ApplicationInfo::AppName(); }
	//! @brief The version of the application, as configured by ApplicationInfo.
	inline vega::Version version() const { return ApplicationInfo::AppVersion(); }

	//! @brief The audio engine instance used by the application.
	inline AudioEngine* audio() const { return aEngine_.get(); }
	//! @brief The graphics engine instance used by the application.
	inline GraphicsEngine* graphics() const { return gEngine_.get(); }
	//! @brief The default manager for global assets.
	inline AssetManager* assets() const { return assets_.get(); }
	//! @brief The main application window, which wont be created until Run() is called.
	inline Window* mainWindow() const { return mainWindow_.get(); }
	//! @brief The Renderer instance for the main application window.
	//inline Renderer* MainRenderer() const { return mainWindow_->Renderer(); } // TODO

	//! @brief Launches the main application loop. This function does not return until the application loop ends.
	void run();
	//! @brief Notifies the application that it should exit the main loop at the end of the current frame.
	void exit();
	//! @brief Returns if \c Exit() has been called and the application will exit at the end of the current frame.
	inline bool willExit() const { return state_.shouldExit; }
	//! @brief The current stage of the application lifetime.
	inline AppStage stage() const { return state_.stage; }
	//! @brief Returns if the application is currently executing within the main loop.
	inline bool isLooping() const { 
		const auto stage = uint32(state_.stage);
		return (stage > uint32(AppStage::AssetLoad)) && (stage < uint32(AppStage::Terminate));
	}
	//! @brief Returns if the main application loop has been existed and cleanup code is currently running.
	inline bool isTerminating() const { return state_.stage == AppStage::Terminate; }

	//! @brief Creates a new application thread that will be started in the next frame (unless called in BeginFrame()).
	//! @tparam T The derived thread type to instantiate and add.
	//! @tparam ...Args The argument types for the thread type constructor.
	//! @param ...args The arguments to be forwarded to the type constructor.
	//! @return The newly created (but not started) application thread instance.
	template<class T, typename... Args>
	inline T* launchThread(Args&&... args) {
		static_assert(std::is_base_of_v<AppThread, T>,
			"CreateThread<T>() must be called with a type derived from AppThread");
		if (isTerminating()) {
			throw std::runtime_error("Cannot launch a new application thread while terminating");
		}
		auto threadPtr = std::make_unique<T>(std::forward<Args>(args)...);
		return dynamic_cast<T*>(addApplicationThread(std::move(threadPtr)));
	}

	//! @brief Gets the current active AppBase instance, or \c nullptr if no instance is active.
	inline static AppBase* Get() { return Instance_; }
	//! @brief Gets the current active AppBase instance as the derived type, or \c nullptr if inactive or not the type.
	//! @tparam T The derived AppBase type, which must be the correct type for the active instance.
	template<typename T>
	inline static T* Get() {
		static_assert(std::is_base_of_v<AppBase, T>, "AppBase::Get<T>() must have AppBase type as type argument");
		return dynamic_cast<AppBase*>(Instance_);
	}

protected:
	//! @brief Perform base app initialization. Note that many application components are not initialized here.
	//! @param config The application configuration.
	AppBase(const AppConfig& config);

	//! @brief Produces the RenderLayout that will be used for the main window.
	//! @remarks The default layout is a depth-enabled single-pass forward renderer.
	//! @return The RenderLayout to use for the main window. Cannot be null.
	virtual SPtr<RenderLayout> createDefaultRenderLayout();

	//! @brief Configures the RenderLayout for the main window. Default is forward renderer with depth layout.
	//! @param layoutBuilder The default builder to configure.
	//virtual void ConfigureDefaultRenderer(RenderLayoutBuilder& layoutBuilder); // TODO

	//! @brief Called after the application finishes construction to perform global application initialization.
	virtual void initialize() { }
	//! @brief Called after initialization is complete to perform global asset loading.
	virtual void loadAssets() { }
	//! @brief Called immediately before the main application loop starts.
	virtual void beforeStart() { }
	//! @brief Called after the main application loop exits and before Run() returns.
	virtual void terminate() { }

	//! @brief Called at the beginning of an application frame to allow for frame setup and preparation.
	virtual void beginFrame() { }
	//! @brief Called to perform update logic for components of the application.
	virtual void update() = 0;
	//! @brief Called between Update() and Render().
	virtual void midFrame() { }
	//! @brief Called to perform rendering operations for the application.
	virtual void render() = 0;
	//! @brief Called after rendering operations are complete to finish up the application frame.
	virtual void endFrame() { }

	//! @brief Called when an unhandled exception propogates to the top-level main loop. Release builds only.
	//! @param ex The unhandled exception.
	virtual void onRuntimeException(const std::exception& ex) { }

private:
	// Frame functions
	void doBeginFrame();
	void doUpdate();
	void doMidFrame();
	void doRender();
	void doEndFrame();
	void doTerminate();

	// Installs a newly created application thread
	AppThread* addApplicationThread(UPtr<AppThread>&& thread); // TODO

private:
	AppConfig config_;
	UPtr<AudioEngine> aEngine_;
	UPtr<GraphicsEngine> gEngine_;
	UPtr<AssetManager> assets_;
	UPtr<Window> mainWindow_;
	struct
	{
		AppStage stage{ AppStage::Created };
		bool shouldExit{ false };
	} state_;
	struct
	{
		std::vector<UPtr<AppThread>> newThreads{ };
		std::mutex threadMutex{ };
		std::vector<UPtr<AppThread>> currentThreads{ };
	} thread_;

	static AppBase* Instance_;

	VEGA_NO_COPY(AppBase)
	VEGA_NO_MOVE(AppBase)
}; // class AppBase

} // namespace vega


//! @brief Macro for quickly getting the active application instance.
#define VEGA_APP_PTR (vega::AppBase::Get())
