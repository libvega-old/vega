/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Time.hpp Time-related types and application timing queries and control.

#include "../Common.hpp"

#include <array>
#include <chrono>


namespace vega
{

//! @brief Represents a finite interval of time, either positive or negative.
//! @tparam TickNum The Duration tick ratio numerator.
//! @tparam TickDenom The Duration tick ratio denominator.
template<uint64 TickNum, uint64 TickDenom>
struct Duration final
{
public:
	//! @brief The size of a Duration tick, giving the resolution.
	using TickSize = std::ratio<TickNum, TickDenom>;
	//! @brief std::chrono representation of a Duration value.
	using Chrono = std::chrono::duration<int64, TickSize>;

	//! @brief Initialize a zero-length span of time.
	constexpr Duration() noexcept : duration_{ 0 } { }
	//! @brief Initialize a span of time equal to the given number of raw ticks.
	explicit constexpr Duration(int64 ticks) noexcept : duration_{ ticks } { }
	//! @brief Initialize a span of time equal to the given std::chrono::duration.
	template<typename Rep, typename Period>
	constexpr Duration(const std::chrono::duration<Rep, Period>& dur) noexcept
		: duration_{ std::chrono::duration_cast<Chrono>(dur) } 
	{ }
	//! @brief Initialize a span of time equal to a Duration with a different precision.
	template<uint64 OtherNum, uint64 OtherDenom>
	constexpr Duration(const Duration<OtherNum, OtherDenom>& other) noexcept
		: duration_{ std::chrono::duration_cast<Chrono>(other.duration_) }
	{ }

	//! @brief std::chrono::duration representation of the time interval.
	inline constexpr Chrono chrono() const { return duration_; }

	//! @brief Total duration as raw ticks.
	inline constexpr int64 ticks() const { return duration_.count(); }
	//! @brief Total duration as nanoseconds. 
	inline constexpr double nanoseconds() const {
		return std::chrono::duration_cast<std::chrono::duration<double, std::nano>>(duration_).count();
	}
	//! @brief Total duration as microseconds. 
	inline constexpr double microseconds() const {
		return std::chrono::duration_cast<std::chrono::duration<double, std::micro>>(duration_).count();
	}
	//! @brief Total duration as milliseconds.
	inline constexpr double milliseconds() const { 
		return std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(duration_).count();
	}
	//! @brief Total duration as seconds.
	inline constexpr double seconds() const {
		return std::chrono::duration_cast<std::chrono::duration<double>>(duration_).count();
	}
	//! @brief Total duration as minutes.
	inline constexpr double minutes() const {
		return std::chrono::duration_cast<std::chrono::duration<double, std::ratio<60, 1>>>(duration_).count();
	}
	//! @brief Total duration as hours.
	inline constexpr double hours() const {
		return std::chrono::duration_cast<std::chrono::duration<double, std::ratio<3600, 1>>>(duration_).count();
	}
	//! @brief Total duration as 24-hour days.
	inline constexpr double days() const {
		return std::chrono::duration_cast<std::chrono::duration<double, std::ratio<86400, 1>>>(duration_).count();
	}

	//! @brief Returns if the time interval has a length of zero.
	inline constexpr bool isZero() const { return duration_.count() == 0; }
	//! @brief Returns if the time interval has a non-zero negative length.
	inline constexpr bool isNegative() const { return duration_.count() < 0; }
	//! @brief Returns if the time interval has a non-zero positive length.
	inline constexpr bool isPositive() const { return duration_.count() > 0; }

	//! @brief Returns if the time interval is a non-zero value.
	inline constexpr explicit operator bool() const { return duration_.count() != 0; }

	inline constexpr bool operator == (const Duration& r) const { return duration_ == r.duration_; }
	inline constexpr bool operator != (const Duration& r) const { return duration_ != r.duration_; }
	inline constexpr bool operator <  (const Duration& r) const { return duration_ <  r.duration_; }
	inline constexpr bool operator <= (const Duration& r) const { return duration_ <= r.duration_; }
	inline constexpr bool operator >  (const Duration& r) const { return duration_ >  r.duration_; }
	inline constexpr bool operator >= (const Duration& r) const { return duration_ >= r.duration_; }

	inline constexpr Duration operator - () const { return Duration{ -duration_.count() }; }

	inline constexpr Duration operator * (double r)          const { return { duration_ * r }; }
	inline constexpr Duration operator / (double r)          const { return { duration_ / r }; }
	inline constexpr Duration operator + (const Duration& r) const { return { duration_ + r.duration_ }; }
	inline constexpr Duration operator - (const Duration& r) const { return { duration_ - r.duration_ }; }
	inline constexpr Duration operator % (const Duration& r) const { return { duration_ % r.duration_ }; }
	inline Duration& operator *= (double r)          { duration_ = Chrono(int64((duration_ * r).count())); return *this; }
	inline Duration& operator /= (double r)          { duration_ = Chrono(int64((duration_ / r).count())); return *this; }
	inline Duration& operator += (const Duration& r) { duration_ += r.duration_; return *this; }
	inline Duration& operator -= (const Duration& r) { duration_ -= r.duration_; return *this; }
	inline Duration& operator %= (const Duration& r) { duration_ %= r.duration_; return *this; }

	//! @brief Create a Duration from a number of nanoseconds.
	template<typename Rep>
	inline static constexpr Duration Nanoseconds(Rep nsec) {
		return { std::chrono::duration<Rep, std::nano>(nsec) };
	}
	//! @brief Create a Duration from a number of microseconds.
	template<typename Rep>
	inline static constexpr Duration Microseconds(Rep usec) {
		return { std::chrono::duration<Rep, std::micro>(usec) };
	}
	//! @brief Create a Duration from a number of milliseconds.
	template<typename Rep>
	inline static constexpr Duration Milliseconds(Rep msec) {
		return { std::chrono::duration<Rep, std::milli>(msec) };
	}
	//! @brief Create a Duration from a number of seconds.
	template<typename Rep>
	inline static constexpr Duration Seconds(Rep sec) {
		return { std::chrono::duration<Rep>(sec) };
	}
	//! @brief Create a Duration from a number of minutes.
	template<typename Rep>
	inline static constexpr Duration Minutes(Rep mins) {
		return { std::chrono::duration<Rep, std::ratio<60, 1>>(mins) };
	}
	//! @brief Create a Duration from a number of hours.
	template<typename Rep>
	inline static constexpr Duration Hours(Rep hrs) {
		return { std::chrono::duration<Rep, std::ratio<3600, 1>>(hrs) };
	}
	//! @brief Create a Duration from a number of 24-hour days.
	template<typename Rep>
	inline static constexpr Duration Days(Rep hrs) {
		return { std::chrono::duration<Rep, std::ratio<86400, 1>>(hrs) };
	}

private:
	Chrono duration_;
}; // struct Duration

//! @brief A high precision Duration, precise to 1 nanosecond, with a range of approx. +/- 292 years.
using PreciseTimeSpan = Duration<1, 1'000'000'000>;
//! @brief A standard precision Duration, precise to 0.1 microseconds, with a range of approx. +/- 29,228 years.
using TimeSpan = Duration<1, 10'000'000>;
//! @brief A low precision Duration, precise to 1 millisecond, with a range of approx. +/- 292 million years.
using LongTimeSpan = Duration<1, 1'000>;


//! @brief Microsecond literal for TimeSpan.
inline constexpr TimeSpan operator "" _usec(unsigned long long usec) { return TimeSpan::Microseconds(usec); }
//! @brief Microsecond literal for TimeSpan.
inline constexpr TimeSpan operator "" _usec(long double usec) { return TimeSpan::Microseconds(usec); }
//! @brief Millisecond literal for TimeSpan.
inline constexpr TimeSpan operator "" _msec(unsigned long long msec) { return TimeSpan::Milliseconds(msec); }
//! @brief Millisecond literal for TimeSpan.
inline constexpr TimeSpan operator "" _msec(long double msec) { return TimeSpan::Milliseconds(msec); }
//! @brief Second literal for TimeSpan.
inline constexpr TimeSpan operator "" _sec(unsigned long long sec) { return TimeSpan::Seconds(sec); }
//! @brief Second literal for TimeSpan.
inline constexpr TimeSpan operator "" _sec(long double sec) { return TimeSpan::Seconds(sec); }
//! @brief Minute literal for TimeSpan.
inline constexpr TimeSpan operator "" _min(unsigned long long min) { return TimeSpan::Minutes(min); }
//! @brief Minute literal for TimeSpan.
inline constexpr TimeSpan operator "" _min(long double min) { return TimeSpan::Minutes(min); }
//! @brief Hour literal for TimeSpan.
inline constexpr TimeSpan operator "" _hr(unsigned long long hrs) { return TimeSpan::Hours(hrs); }
//! @brief Hour literal for TimeSpan.
inline constexpr TimeSpan operator "" _hr(long double hrs) { return TimeSpan::Hours(hrs); }
//! @brief 24-hour day literal for TimeSpan.
inline constexpr TimeSpan operator "" _days(unsigned long long days) { return TimeSpan::Days(days); }
//! @brief 24-hour day literal for TimeSpan.
inline constexpr TimeSpan operator "" _days(long double days) { return TimeSpan::Days(days); }


//! @brief Provides timing information related to the application.
class VEGA_API Time final
{
	friend class AppBase;

public:
	//! @brief The time between the current application frame and the last, affected by the time scaling value.
	inline static const TimeSpan& Delta() { return Delta_; }
	//! @brief The raw frame delta time, unaffected by the time scaling value.
	inline static const TimeSpan& RealDelta() { return RealDelta_; }
	//! @brief The total elapsed time since the current application instance was created.
	inline static const TimeSpan& Elapsed() { return Elapsed_; }
	//! @brief The total elapsed application time in the last application frame.
	inline static const TimeSpan& LastElapsed() { return LastElapsed_; }
	//! @brief The number of frames executed by the current application.
	inline static uint64 FrameCount() { return FrameCount_; }
	//! @brief The scaling factor for the delta time returned by Delta().
	inline static float Scale() { return Scale_; }
	//! @brief The application FPS averaged over the last few frames.
	inline static float Fps() { return Fps_; }
	//! @brief The raw FPS of the last application frame.
	inline static float RawFps() { return FpsHistory_[FpsIndex_]; }

	//! @brief Gets the raw elapsed runtime of the application, independent of application frames.
	static TimeSpan ElapsedRaw();

	//! @brief Set the delta time scale - the new scale is not applied until the next application frame.
	//! @param newScale The new time scale, cannot be negative.
	//! @return The current time scale value.
	static float Scale(float newScale);

	//! @brief Returns if the application has reached the given elapsed time in the current frame.
	//! @param span The total elapsed time to check for.
	inline static bool IsTime(const TimeSpan& span) {
		return (LastElapsed_ < span) && (Elapsed_ >= span);
	}
	//! @brief Returns if the application is current in a frame that is a multiple of the given elapsed time.
	//! @param span The total elapsed time to check for a multiple of.
	inline static bool IsTimeMultiple(const TimeSpan& span) {
		return (Elapsed_ % span) < (LastElapsed_ % span);
	}
	//! @brief Returns if the application is current in a frame that is a multiple of the given elapsed time and offset.
	//! @param span The total elapsed time to check for a multiple of.
	//! @param offset The additive offset of the time multiple to check for.
	inline static bool IsTimeMultiple(const TimeSpan& span, const TimeSpan& offset) {
		return ((Elapsed_ + offset) % span) < ((LastElapsed_ + offset) % span);
	}

private:
	static void Initialize();
	static void NextFrame();

private:
	static TimeSpan Delta_;
	static TimeSpan RealDelta_;
	static TimeSpan Elapsed_;
	static TimeSpan LastElapsed_;
	static uint64 FrameCount_;
	static float Scale_;
	static Opt<float> NewScale_;
	static std::array<float, 10> FpsHistory_;
	static uint32 FpsIndex_;
	static float Fps_;
	static TimeSpan LastTime_;

	VEGA_NO_COPY(Time)
	VEGA_NO_MOVE(Time)
	VEGA_NO_INIT(Time)
}; // class Time

} // namespace vega
