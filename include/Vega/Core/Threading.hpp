/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Threading.hpp Multi-threading types and utilities.

#include "../Common.hpp"

#include <thread>


namespace vega
{

//! @brief Provides a variety of utility functionality related to multi-threading.
class VEGA_API Threading final
{
	friend class AppThread;

public:
	//! @brief The unique (within this process) id of the main thread.
	inline static std::thread::id MainThreadId() { return MainThreadId_; }
	//! @brief Gets if the calling code is currently executing within the main application thread.
	inline static bool IsMainThread() { return (MainThreadId_ == std::this_thread::get_id()); }
	//! @brief Gets if the calling code is executing within the main thread or an active AppThread.
	static bool IsAppThread();

	//! @brief Throws an exception if called from any thread other than the main thread.
	//! @param message The optional exception message.
	inline static void AssertMainThread(const StringView& message = {}) {
		if (!IsMainThread()) {
			if (!message.empty()) {
				throw std::runtime_error(to_string("Main thread assertion failed - %s", message.data()));
			}
			else {
				throw std::runtime_error(
					"Main thread assertion failed - attempted to execute main thread exclusive code"
				);
			}
		}
	}

private:
	// Called internally to mark the calling thread as an app thread
	static void MarkAsAppThread();

private:
	static const std::thread::id MainThreadId_;

	VEGA_NO_COPY(Threading)
	VEGA_NO_MOVE(Threading)
	VEGA_NO_INIT(Threading)
}; // class Threading

} // namespace vega
