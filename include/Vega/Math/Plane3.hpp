/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Plane3.hpp Description of a 3D plane.

#include "../Common.hpp"
#include "./Math.hpp"
#include "./Vec3.hpp"


namespace vega
{

//! @brief Describes an infinite plane oriented and positioned within 3D space.
struct VEGA_API Plane3 final
{
public:
	//! @brief Initializes a plane oriented to Vec3f::Up at the center.
	constexpr Plane3() noexcept : normal{ Vec3f::Up }, d{ } { }
	//! @brief Initializes a plane from a normal and a distance.
	//! @param normal The plane normal. Will be normalized for the plane.
	//! @param d The plane distance.
	constexpr Plane3(const Vec3f& normal, float d) noexcept : normal{ normal.normalized() }, d{ d } { }
	//! @brief Initializes a plane from normal components and a distance.
	//! @param x The normal x component.
	//! @param y The normal y component.
	//! @param z The normal z component.
	//! @param d The plane distance.
	constexpr Plane3(float x, float y, float z, float d) noexcept : normal{ Vec3f{ x, y, z }.normalized() }, d{ d } { }

	//! @brief Flips the plane orientation without moving it.
	inline constexpr Plane3 flipped() const { return { -normal, -d }; }
	//! @brief Orients the plane to have a normal away from the origin.
	inline constexpr Plane3 positive() const { return (d < 0) ? flipped() : *this; }

	//! @brief Calculates the distance of the point to the plane, giving the side of the plane the point is on.
	inline constexpr float dot(const Vec3f& point) const { return normal.dot(point) - d; }
	//! @brief Calculates the closest point on the plane to the given point.
	//! @param point The point to get the closest plane point to.
	inline constexpr Vec3f closestPoint(const Vec3f& point) const {
		const float d{ dot(point) };
		return (d < 0) ? (point + (d * normal)) : (point - (d * normal));
	}

	inline constexpr bool operator == (const Plane3& r) const { return normal == r.normal && d == r.d; }
	inline constexpr bool operator != (const Plane3& r) const { return normal != r.normal || d != r.d; }

	//! @brief Calculates a plane containing the point with the given normal.
	//! @param point The point to contain in the plane.
	//! @param normal The plane normal, expected to be normalized.
	inline static Plane3 FromPoint(const Vec3f& point, const Vec3f& normal) {
		return { normal, normal.dot(point) };
	}
	//! @brief Calculates a plane containing the points, oriented by right-hand winding of @p p1 -> @p p2 -> @p p3.
	inline static Plane3 FromPoints(const Vec3f& p1, const Vec3f& p2, const Vec3f& p3) {
		const Vec3f p12{ p2 - p1 }, p13{ p3 - p1 };
		const Vec3f normal{ p12.cross(p13).normalized() };
		return { normal, normal.dot(p1) };
	}

public:
	//! @brief The normal of the plane. Expected to be normalized in all calculations.
	Vec3f normal;
	//! @brief The scalar distance from the spatial origin to the nearest point on the plane.
	float d;
}; // struct Plane3

} // namespace vega
