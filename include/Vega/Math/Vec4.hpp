/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

 //! @file Vec4.hpp 4-component vector types.

#include "../Common.hpp"
#include "./Math.hpp"
#include "./Vec3.hpp"


namespace vega
{

//! @brief 4-component vector of boolean values.
struct VEGA_API BVec4 final
{
public:
	//! @brief Initialize all components to false.
	constexpr BVec4() noexcept : data{ false,false,false,false } { }
	//! @brief Initialize all components to the given value.
	explicit constexpr BVec4(bool b) noexcept : data{ b,b,b,b } { }
	//! @brief Initialize components with the given values.
	constexpr BVec4(bool x, bool y, bool z, bool w) noexcept : x{ x }, y{ y }, z{ z }, w{ w } { }

	//! @brief Returns if any component is \c true.
	inline constexpr bool any() const { return x || y || z || w; }
	//! @brief Returns if all components are \c true.
	inline constexpr bool all() const { return x && y && z && w; }
	//! @brief Returns if no components are \c true.
	inline constexpr bool none() const { return !x && !y && !z && !w; }
	//! @brief Returns the number of components that are \c true.
	inline constexpr uint32 count() const { return uint32(x + y + z + w); }

	inline constexpr bool operator == (BVec4 r) const { return x == r.x && y == r.y && z == r.z && w == r.w; }
	inline constexpr bool operator != (BVec4 r) const { return x != r.x || y != r.y || z != r.z || w != r.w; }
	inline bool operator < (BVec4 r) const {
		return *reinterpret_cast<const uint32*>(data) < *reinterpret_cast<const uint32*>(r.data);
	}
	inline constexpr BVec4 operator & (BVec4 r) const { return { x && r.x, y && r.y, z && r.z, w && r.w }; }
	inline constexpr BVec4 operator | (BVec4 r) const { return { x || r.x, y || r.y, z || r.z, w || r.w }; }
	inline constexpr BVec4 operator ^ (BVec4 r) const { return { x != r.x, y != r.y, z != r.z, w != r.w }; }

public:
	union {
		struct { bool x; bool y; bool z; bool w; };
		bool data[4];
	};
}; // struct BVec4


//! @brief 4-component vector of numeric values.
//! @tparam T The numeric type of the vector components.
template<typename T>
struct Vec4 final
{
public:
	constexpr Vec4() noexcept : x{ }, y{ }, z{ }, w{ } { }
	template<typename U>
	explicit constexpr Vec4(const U& v) noexcept
		: x{ static_cast<T>(v) }, y{ static_cast<T>(v) }, z{ static_cast<T>(v) }, w{ static_cast<T>(v) }
	{ }
	template<typename U, typename V, typename A, typename B>
	constexpr Vec4(const U& x, const V& y, const A& z, const B& w) noexcept
		: x{ static_cast<T>(x) }, y{ static_cast<T>(y) }, z{ static_cast<T>(z) }, w{ static_cast<T>(w) }
	{ }
	template<typename U>
	explicit constexpr Vec4(const Vec4<U>& o) noexcept
		: x{ static_cast<T>(o.x) }, y{ static_cast<T>(o.y) }, z{ static_cast<T>(o.z) }, w{ static_cast<T>(o.w) }
	{ }
	template<typename U, typename V, typename A>
	constexpr Vec4(const Vec2<U>& v, const V& z, const A& w) noexcept
		: x{ static_cast<T>(v.x) }, y{ static_cast<T>(v.y) }, z{ static_cast<T>(z) }, w{ static_cast<T>(w) }
	{ }
	template<typename U, typename V>
	constexpr Vec4(const Vec2<U>& xy, const Vec2<V>& zw) noexcept
		: x{ static_cast<T>(xy.x) }, y{ static_cast<T>(xy.y) }, z{ static_cast<T>(zw.x) }, w{ static_cast<T>(zw.y) }
	{ }
	template<typename U, typename V>
	constexpr Vec4(const Vec3<U>& v, const V& w) noexcept
		: x{ static_cast<T>(v.x) }, y{ static_cast<T>(v.y) }, z{ static_cast<T>(v.z) }, w{ static_cast<T>(w) }
	{ }

	constexpr const T& operator [] (size_t index) const { return data[index]; }
	constexpr T& operator [] (size_t index) { return data[index]; }

	//! @brief Converts the components of the vector into a different type.
	//! @tparam U The type to convert the components into.
	template<typename U>
	inline constexpr Vec4<U> as() const { return Vec4<U>(x, y, z, w); }

	template<typename U>
	inline constexpr explicit operator Vec2<U>() const { return { x, y }; }
	template<typename U>
	inline constexpr explicit operator Vec3<U>() const { return { x, y, z }; }

	//! @brief Gets the cartesian length of the vector.
	inline constexpr auto length() const { return std::sqrt((x * x) + (y * y) + (z * z) + (w * w)); }
	//! @brief Gets the square of the cartesian length of the vector.
	inline constexpr auto lengthSq() const { return (x * x) + (y * y) + (z * z) + (w * w); }
	//! @brief Gets the normalized (length 1) vector in the same direction as the vector.
	inline constexpr auto normalized() const {
		auto ilen = 1 / length();
		return Vec4<decltype(ilen)>(x * ilen, y * ilen, z * ilen, w * ilen);
	}

	//! @brief Gets the cartesian distance to the end of the other vector.
	template<typename U>
	constexpr auto distance(const Vec4<U>& o) const {
		auto dx = o.x - x, dy = o.y - y, dz = o.z - z, dw = o.w - w;
		return std::sqrt((dx * dx) + (dy * dy) + (dz * dz) + (dw * dw));
	}
	//! @brief Gets the square of the cartesian distance to the end of the other vector.
	template<typename U>
	constexpr auto distanceSq(const Vec4<U>& o) const {
		auto dx = o.x - x, dy = o.y - y, dz = o.z - z, dw = o.w - w;
		return (dx * dx) + (dy * dy) + (dz * dz) + (dw * dw);
	}

	//! @brief Gets the dot product with the other vector.
	template<typename U>
	inline constexpr auto dot(const Vec4<U>& o) const {
		return (o.x * x) + (o.y * y) + (o.z * z) + (o.w * w);
	}

	//! @brief Gets the component-wise minimum of the two vectors.
	template<typename U>
	inline constexpr auto min(const Vec4<U>& o) const {
		auto mx = math::min(x, o.x), my = math::min(y, o.y), mz = math::min(z, o.z), mw = math::min(w, o.w);
		return Vec4<decltype(mx)>(mx, my, mz, mw);
	}
	//! @brief Gets the component-wise maximum of the two vectors.
	template<typename U>
	inline constexpr auto max(const Vec4<U>& o) const {
		auto mx = math::max(x, o.x), my = math::max(y, o.y), mz = math::max(z, o.z), mw = math::max(w, o.w);
		return Vec4<decltype(mx)>(mx, my, mz, mw);
	}
	//! @brief Gets the component-wise clamp of this vector and the min and max vectors.
	template<typename U, typename V>
	inline constexpr auto clamp(const Vec4<U>& min, const Vec4<V>& max) const {
		auto cx = math::clamp(x, min.x, max.x), cy = math::clamp(y, min.y, max.y), cz = math::clamp(z, min.z, max.z),
			cw = math::clamp(w, min.w, max.w);
		return Vec4<decltype(cx)>(cx, cy, cz, cw);
	}

	//! @brief Performs a component-wise equality check between the two vectors.
	template<typename U>
	inline constexpr BVec4 compEq(const Vec4<U>& o) const {
		return { x == o.x, y == o.y, z == o.z, w == o.w };
	}
	//! @brief Performs a component-wise inequality check between the two vectors.
	template<typename U>
	inline constexpr BVec4 compNeq(const Vec4<U>& o) const {
		return { x != o.x, y != o.y, z != o.z, w != o.w };
	}

	template<typename U>
	Vec4& operator += (const Vec4<U>& v) {
		x = static_cast<T>(x + v.x); y = static_cast<T>(y + v.y); z = static_cast<T>(z + v.z);
		w = static_cast<T>(w + v.w);
		return *this;
	}
	template<typename U>
	Vec4& operator -= (const Vec4<U>& v) {
		x = static_cast<T>(x - v.x); y = static_cast<T>(y - v.y); z = static_cast<T>(z - v.z);
		w = static_cast<T>(w - v.w);
		return *this;
	}
	template<typename U>
	Vec4& operator *= (const Vec4<U>& v) {
		x = static_cast<T>(x * v.x); y = static_cast<T>(y * v.y); z = static_cast<T>(z * v.z);
		w = static_cast<T>(w * v.w);
		return *this;
	}
	template<typename U>
	Vec4& operator /= (const Vec4<U>& v) {
		x = static_cast<T>(x / v.x); y = static_cast<T>(y / v.y); z = static_cast<T>(z / v.z);
		w = static_cast<T>(w / v.w);
		return *this;
	}
	template<typename U>
	Vec4& operator += (const U& v) {
		x = static_cast<T>(x + v); y = static_cast<T>(y + v); z = static_cast<T>(z + v); w = static_cast<T>(w + v);
		return *this;
	}
	template<typename U>
	Vec4& operator -= (const U& v) {
		x = static_cast<T>(x - v); y = static_cast<T>(y - v); z = static_cast<T>(z - v); w = static_cast<T>(w - v);
		return *this;
	}
	template<typename U>
	Vec4& operator *= (const U& v) {
		x = static_cast<T>(x * v); y = static_cast<T>(y * v); z = static_cast<T>(z * v); w = static_cast<T>(w * v);
		return *this;
	}
	template<typename U>
	Vec4& operator /= (const U& v) {
		x = static_cast<T>(x / v); y = static_cast<T>(y / v); z = static_cast<T>(z / v); w = static_cast<T>(w / v);
		return *this;
	}

public:
	union {
		struct { T x; T y; T z; T w; };
		T data[4];
	};

	//! @brief Constant vector with components (0, 0, 0, 0).
	static const Vec4 Zero;
	//! @brief Constant vector with components (1, 1, 1, 1).
	static const Vec4 One;
	//! @brief Constant vector with components (1, 0, 0, 0).
	static const Vec4 UnitX;
	//! @brief Constant vector with components (0, 1, 0, 0).
	static const Vec4 UnitY;
	//! @brief Constant vector with components (0, 0, 1, 0).
	static const Vec4 UnitZ;
	//! @brief Constant vector with components (0, 0, 0, 1).
	static const Vec4 UnitW;
}; // struct Vec4

template<typename T> constexpr Vec4<T> Vec4<T>::Zero { 0, 0, 0, 0 };
template<typename T> constexpr Vec4<T> Vec4<T>::One  { 1, 1, 1, 1 };
template<typename T> constexpr Vec4<T> Vec4<T>::UnitX{ 1, 0, 0, 0 };
template<typename T> constexpr Vec4<T> Vec4<T>::UnitY{ 0, 1, 0, 0 };
template<typename T> constexpr Vec4<T> Vec4<T>::UnitZ{ 0, 0, 1, 0 };
template<typename T> constexpr Vec4<T> Vec4<T>::UnitW{ 0, 0, 0, 1 };

//! @brief Vec4 specialization with `float`.
using Vec4f  = Vec4<float>;
//! @brief Vec4 specialization with `double`.
using Vec4d  = Vec4<double>;
//! @brief Vec4 specialization with `int8`.
using Vec4b  = Vec4<int8>;
//! @brief Vec4 specialization with `uint8`.
using Vec4ub = Vec4<uint8>;
//! @brief Vec4 specialization with `int16`.
using Vec4s  = Vec4<int16>;
//! @brief Vec4 specialization with `uint16`.
using Vec4us = Vec4<uint16>;
//! @brief Vec4 specialization with `int32`.
using Vec4i  = Vec4<int32>;
//! @brief Vec4 specialization with `uint32`.
using Vec4ui = Vec4<uint32>;
//! @brief Vec4 specialization with `int64`.
using Vec4l  = Vec4<int64>;
//! @brief Vec4 specialization with `uint64`.
using Vec4ul = Vec4<uint64>;


template<typename T>
inline constexpr std::enable_if_t<std::is_signed_v<T>, Vec4<T>> operator - (const Vec4<T>& v) {
	return { -v.x, -v.y, -v.z, -v.w };
}
template<typename T, typename U>
inline constexpr bool operator == (const Vec4<T>& l, const Vec4<U>& r) {
	return (l.x == r.x) && (l.y == r.y) && (l.z == r.z) && (l.w == r.w);
}
template<typename T, typename U>
inline constexpr bool operator != (const Vec4<T>& l, const Vec4<U>& r) {
	return (l.x != r.x) || (l.y != r.y) || (l.z != r.z) || (l.w != r.w);
}
template<typename T, typename U>
inline constexpr BVec4 operator <= (const Vec4<T>& l, const Vec4<U>& r) {
	return { l.x <= r.x, l.y <= r.y, l.z <= r.z, l.w <= r.w };
}
template<typename T, typename U>
inline constexpr BVec4 operator < (const Vec4<T>& l, const Vec4<U>& r) {
	return { l.x < r.x, l.y < r.y, l.z < r.z, l.w < r.w };
}
template<typename T, typename U>
inline constexpr BVec4 operator >= (const Vec4<T>& l, const Vec4<U>& r) {
	return { l.x >= r.x, l.y >= r.y, l.z >= r.z, l.w >= r.w };
}
template<typename T, typename U>
inline constexpr BVec4 operator > (const Vec4<T>& l, const Vec4<U>& r) {
	return { l.x > r.x, l.y > r.y, l.z > r.z, l.w > r.w };
}
template<typename T, typename U>
inline constexpr auto operator + (const Vec4<T>& l, const Vec4<U>& r) {
	return Vec4<decltype(l.x + r.x)>(l.x + r.x, l.y + r.y, l.z + r.z, l.w + r.w);
}
template<typename T, typename U>
inline constexpr auto operator - (const Vec4<T>& l, const Vec4<U>& r) {
	return Vec4<decltype(l.x - r.x)>(l.x - r.x, l.y - r.y, l.z - r.z, l.w - r.w);
}
template<typename T, typename U>
inline constexpr auto operator * (const Vec4<T>& l, const Vec4<U>& r) {
	return Vec4<decltype(l.x* r.x)>(l.x * r.x, l.y * r.y, l.z * r.z, l.w * r.w);
}
template<typename T, typename U>
inline constexpr auto operator / (const Vec4<T>& l, const Vec4<U>& r) {
	return Vec4<decltype(l.x / r.x)>(l.x / r.x, l.y / r.y, l.z / r.z, l.w / r.w);
}
template<typename T, typename U>
inline constexpr auto operator + (const Vec4<T>& l, const U& r) {
	return Vec4<decltype(l.x + r)>(l.x + r, l.y + r, l.z + r, l.w + r);
}
template<typename T, typename U>
inline constexpr auto operator - (const Vec4<T>& l, const U& r) {
	return Vec4<decltype(l.x - r)>(l.x - r, l.y - r, l.z - r, l.w - r);
}
template<typename T, typename U>
inline constexpr auto operator * (const Vec4<T>& l, const U& r) {
	return Vec4<decltype(l.x* r)>(l.x * r, l.y * r, l.z * r, l.w * r);
}
template<typename T, typename U>
inline constexpr auto operator * (const T& l, const Vec4<U>& r) {
	return Vec4<decltype(l* r.x)>(l * r.x, l * r.y, l * r.z, l * r.w);
}
template<typename T, typename U>
inline constexpr auto operator / (const Vec4<T>& l, const U& r) {
	return Vec4<decltype(l.x / r)>(l.x / r, l.y / r, l.z / r, l.w / r);
}
template<typename T, typename U>
inline constexpr auto operator / (const T& l, const Vec4<U>& r) {
	return Vec4<decltype(l / r.x)>(l / r.x, l / r.y, l / r.z, l / r.w);
}

} // namespace vega
