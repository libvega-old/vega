/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Quat.hpp Compact representation of 3D rotations.

#include "../Common.hpp"
#include "./Math.hpp"
#include "./Mat4.hpp"


namespace vega
{

//! @brief Efficient representation of rotations in 3D space, free from gimbal lock.
struct VEGA_API Quat final
{
public:
	//! @brief Construct a default identity quaternion.
	constexpr Quat() noexcept : x{ 0 }, y{ 0 }, z{ 0 }, w{ 1 } { }
	//! @brief Construct a quaternion directly from raw components, which are not checked or normalized.
	constexpr Quat(float x, float y, float z, float w) noexcept : x{ x }, y{ y }, z{ z }, w{ w } { }

	//! @brief The magnitude of the quaternion components (1 implies normalized).
	inline float length() const { return std::sqrt(x * x + y * y + z * z + w * w); }
	//! @brief The square magnitude of the quaternion components (1 implies normalized).
	inline constexpr float lengthSq() const { return x * x + y * y + z * z + w * w; }
	//! @brief Gets the normalized version of the quaternion.
	inline Quat normalized() const {
		const float ilen{ 1 / length() };
		return { x * ilen, y * ilen, z * ilen, w * ilen };
	}
	//! @brief Gets the conjugate of the quaternion (identical to inversion for normalized quaternions).
	inline constexpr Quat conjugated() const { return { -x, -y, -z, w }; }

	//! @brief Computes the dot product with the other quaternion.
	inline constexpr float dot(const Quat& r) const { return x * r.x + y * r.y + z * r.z + w * r.w; }

	//! @brief Converts the quaternion into a rotation matrix describing the same rotation.
	inline constexpr Mat4f asMatrix() const {
		const float xx{ x * x }, yy{ y * y }, zz{ z * z }, xy{ x * y }, xz{ x * z }, xw{ x * w }, yz{ y * z },
			yw{ y * w }, zw{ z * w };
		return {
			1 - (2 * (yy + zz)),        2 * (xy + zw),        2 * (xz - yw),  0,
			      2 * (xy - zw),  1 - (2 * (xx + zz)),        2 * (yz + xw),  0,
			      2 * (xz + yw),        2 * (yz - xw),  1 - (2 * (xx + yy)),  0,
			                  0,                    0,                    0,  1
		};
	}

	//! @brief Performs component-wise addition of the quaternions.
	inline constexpr Quat operator + (const Quat& r) const { return { x + r.x, y + r.y, z + r.z, w + r.w }; }
	inline Quat& operator += (const Quat& r) { x += r.x; y += r.y; z += r.z; w += r.w; return *this; }
	//! @brief Performs component-wise subtraction of the quaternions.
	inline constexpr Quat operator - (const Quat& r) const { return { x - r.x, y - r.y, z - r.z, w - r.w }; }
	inline Quat& operator -= (const Quat& r) { x -= r.x; y -= r.y; z -= r.z; w -= r.w; return *this; }
	//! @brief Performs standard quaternion multiplication with the effect of applying both rotations.
	inline constexpr Quat operator * (const Quat& r) const {
		const float n1{ y * r.z - z * r.y }, n2{ z * r.x - x * r.z }, n3{ x * r.y - y * r.x },
			n4{ x * r.x + y * r.y + z * r.z };
		return {
			x * r.w + w * r.x + n1, y * r.w + w * r.y + n2,
			z * r.w + w * r.z + n3, w * r.w - n4
		};
	}
	inline Quat& operator *= (const Quat& r) { return (*this = *this * r); }
	//! @brief Performs standard quaternion division.
	inline constexpr Quat operator / (const Quat& r) const {
		const float a{ 1 / r.lengthSq() };
		return {
			a * ((x * r.w) - (w * r.x) - (z * r.y) + (y * r.z)),
			a * ((y * r.w) + (z * r.x) - (w * r.y) - (x * r.z)),
			a * ((z * r.w) - (y * r.x) + (x * r.y) - (w * r.z)),
			a * ((x * r.x) + (y * r.y) + (z * r.z) + (w * r.w))
		};
	}
	inline Quat& operator /= (const Quat& r) { return (*this = *this / r); }

	//! @brief Creates a quaternion representing the given yaw/pitch/roll angles.
	inline static Quat YawPitchRoll(Rad<float> yaw, Rad<float> pitch, Rad<float> roll) {
		const float sy{ math::sin(yaw / 2) }, cy{ math::cos(yaw / 2) }, sp{ math::sin(pitch / 2) },
			cp{ math::cos(pitch / 2) }, sr{ math::sin(roll / 2) }, cr{ math::cos(roll / 2) };
		return {
			(cy * sp * cr) + (sy * cp * sr), (sy * cp * cr) - (cy * sp * sr),
			(cy * cp * sr) - (sy * sp * cr), (cy * cp * cr) + (sy * sp * sr)
		};
	}
	//! @brief Creates a quaternion that describes a rotation around the given normalized axis.
	inline static Quat RotationAxis(const Vec3f& axis, Rad<float> angle) {
		const float s{ math::sin(angle / 2) }, c{ math::cos(angle / 2) };
		return { axis.x * s, axis.y * s, axis.z * s, c };
	}
	//! @brief Creates a quaternion that describes the same rotation as the matrix rotation components.
	static Quat FromRotationMatrix(const Mat4f& m);

public:
	//! @brief Quaternion x-axis component.
	float x;
	//! @brief Quaternion y-axis component.
	float y;
	//! @brief Quaternion z-axis component.
	float z;
	//! @brief Quaternion scalar component.
	float w;

	//! @brief The identity quaternion
	static const Quat Identity;
}; // struct Quat

} // namespace vega
