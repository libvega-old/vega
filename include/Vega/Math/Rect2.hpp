/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Rect2.hpp Representation of 2D rectangular regions.

#include "../Common.hpp"
#include "./Math.hpp"
#include "./Vec2.hpp"


namespace vega
{

//! @brief Represents an axis-aligned 2-dimensional rectangular region in cartesian space.
//! @tparam PosType The numeric type used for the region position.
//! @tparam SizeType The numeric type used for the region size.
template<typename PosType, typename SizeType = PosType>
struct Rect2 final
{
public:
	//! @brief The vector type that gives the region position.
	using PosVec  = Vec2<PosType>;
	//! @brief The vector type that gives the region size.
	using SizeVec = Vec2<SizeType>;

	constexpr Rect2() noexcept : x{ 0 }, y{ 0 }, w{ 0 }, h{ 0 } { }
	template<typename T, typename U, typename V, typename A>
	constexpr Rect2(const T& x, const U& y, const V& w, const A& h) noexcept
		: x{ static_cast<PosType>(x) }, y{ static_cast<PosType>(y) }
		, w{ static_cast<SizeType>(w) }, h{ static_cast<SizeType>(h) }
	{ }
	template<typename T, typename U>
	constexpr Rect2(const Vec2<T>& pos, const Vec2<U>& sz) noexcept
		: x{ static_cast<PosType>(pos.x) }, y{ static_cast<PosType>(pos.y) }
		, w{ static_cast<SizeType>(sz.x) }, h{ static_cast<SizeType>(sz.y) }
	{ }
	template<typename T, typename U>
	constexpr Rect2(const Rect2<T, U>& o) noexcept
		: x{ static_cast<PosType>(o.x) }, y{ static_cast<PosType>(o.y) }
		, w{ static_cast<SizeType>(o.w) }, h{ static_cast<SizeType>(o.h) }
	{ }

	//! @brief Gets the position of the outermost (most positive) corner of the region.
	inline constexpr PosVec outer() const { return { x + w, y + h }; }
	//! @brief Gets the outermost (post positive) x-coordinate of the region.
	inline constexpr auto outerX() const { return x + w; }
	//! @brief Gets the outermost (post positive) y-coordinate of the region.
	inline constexpr auto outerY() const { return y + h; }
	//! @brief Gets the area contained within the region.
	inline constexpr auto area() const { return w * h; }
	//! @brief Gets if the region is empty (area == zero).
	inline constexpr bool isEmpty() const { return w == 0 || h == 0; }

	//! @brief Sets the position of the region (the innermost/least positive corner).
	template<typename T, typename U>
	inline void setPos(const T& x, const U& y) { 
		this->x = static_cast<PosType>(x); this->y = static_cast<PosType>(y);
	}
	//! @brief Sets the position of the region (the innermost/least positive corner).
	inline void setPos(const PosVec& pos) { this->x = pos.x; this->y = pos.y; }
	//! @brief Sets the size of the region (when possible, negative sizes will cause unexpected values).
	template<typename T, typename U>
	inline void setSize(const T& w, const U& h) { 
		this->w = static_cast<SizeType>(w); this->h = static_cast<SizeType>(h);
	}
	//! @brief Sets the size of the region (when possible, negative sizes will cause unexpected values).
	inline void setSize(const SizeVec& sz) { this->w = sz.x; this->h = sz.y; }

	//! @brief Gets the minimal region that contains both this region and the other region.
	template<typename T, typename U>
	inline constexpr auto combine(const Rect2<T, U>& o) const {
		auto nx = math::min(x, o.x), ny = math::min(y, o.y);
		auto nr = math::max(outerX(), o.outerX()), nt = math::max(outerY(), o.outerY());
		return Rect2<decltype(nx), decltype(nr - nx)> { nx, ny, nr - nx, nt - ny };
	}
	//! @brief Gets the region that covers the overlap between this region and the other region.
	template<typename T, typename U>
	inline constexpr auto overlap(const Rect2<T, U>& o) const {
		if ((pos >= o.outer()).any() || (o.pos >= outer()).any()) {
			return Rect2<PosType, SizeType>::Empty;
		}
		auto nx = math::max(x, o.x), ny = math::max(y, o.y);
		auto nr = math::min(outerX(), o.outerX()), nt = math::min(outerY(), o.outerY());
		return Rect2<decltype(nx), decltype(nr - nx)> { nx, ny, nr - nx, nt - ny };
	}

	//! @brief Creates a new rectangular region which uses the given points as opposite corners.
	template<typename T, typename U>
	inline static constexpr Rect2<PosType, SizeType> FromPoints(const Vec2<T>& p0, const Vec2<U>& p1) {
		auto nx = math::min(p0.x, p1.x), ny = math::min(p0.y, p1.y), nr = math::max(p0.x, p1.x),
			nt = math::max(p0.y, p1.y);
		return { nx, ny, nr - nx, nt - ny };
	}

public:
	union {
		struct { PosType x; PosType y; };
		PosVec pos;
	};
	union {
		struct { SizeType w; SizeType h; };
		SizeVec size;
	};

	//! @brief Provides a constant value for an empty rectangle.
	static const Rect2 Empty;
}; // struct Rect2

template<typename T, typename U> const Rect2<T, U> Rect2<T, U>::Empty{ 0, 0, 0, 0 };

//! @brief Specialization of Rect2 using `int32` as the position and `uint32` as the size.
using Rect2i = Rect2<int32, uint32>;
//! @brief Specialization of Rect2 using `float` as the position and size.
using Rect2f = Rect2<float>;
//! @brief Specialization of Rect2 using `int64` as the position and `uint64` as the size.
using Rect2l = Rect2<int64, uint64>;
//! @brief Specialization of Rect2 using `double` as the position and size.
using Rect2d = Rect2<double>;

} // namespace vega
