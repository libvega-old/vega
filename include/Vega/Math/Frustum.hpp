/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Frustum.hpp Definition and operations for viewing volumes.

#include "../Common.hpp"
#include "./Math.hpp"
#include "./Plane3.hpp"
#include "./Mat4.hpp"

#include <array>


namespace vega
{

//! @brief Describes a viewing volume from a view and projection matrix.
struct VEGA_API Frustum final
{
public:
	//! @brief The number of planes in a frustum.
	inline static constexpr uint32 PLANE_COUNT{ 6 };
	//! @brief The number of corners in a frustum.
	inline static constexpr uint32 CORNER_COUNT{ 8 };

	//! @brief Creates a frustum described by the view*projection matrix.
	explicit Frustum(const Mat4f& viewProj = Mat4f::Identity);

	//! @brief Sets the frustim view*projection matrix and updates the volume.
	void matrix(const Mat4f& viewProj);

	//! @brief The view*projection matrix that the frustum describes.
	inline const Mat4f& matrix() const { return matrix_; }
	//! @brief The frustum bounding planes in the order (near, far, top, bottom, left, right).
	inline const std::array<Plane3, PLANE_COUNT>& planes() const { return planes_; }
	//! @brief The frustum corners in the order (ntl, ntr, nbl, nbr, ftl, ftr, fbl, fbr).
	inline const std::array<Vec3f, CORNER_COUNT>& corners() const { return corners_; }

	//! @brief The frustum near plane.
	inline const Plane3& near() const { return planes_[0]; }
	//! @brief The frustum far plane.
	inline const Plane3& far() const { return planes_[1]; }
	//! @brief The frustum top plane.
	inline const Plane3& top() const { return planes_[2]; }
	//! @brief The frustum bottom plane.
	inline const Plane3& bottom() const { return planes_[3]; }
	//! @brief The frustum left plane.
	inline const Plane3& left() const { return planes_[4]; }
	//! @brief The frustum right plane.
	inline const Plane3& right() const { return planes_[5]; }

	//! @brief The frustum near-top-left corner.
	inline const Vec3f& cornerNTL() const { return corners_[0]; }
	//! @brief The frustum near-top-right corner.
	inline const Vec3f& cornerNTR() const { return corners_[1]; }
	//! @brief The frustum near-bottom-left corner.
	inline const Vec3f& cornerNBL() const { return corners_[2]; }
	//! @brief The frustum near-bottom-right corner.
	inline const Vec3f& cornerNBR() const { return corners_[3]; }
	//! @brief The frustum far-top-left corner.
	inline const Vec3f& cornerFTL() const { return corners_[4]; }
	//! @brief The frustum far-top-right corner.
	inline const Vec3f& cornerFTR() const { return corners_[5]; }
	//! @brief The frustum far-bottom-left corner.
	inline const Vec3f& cornerFBL() const { return corners_[6]; }
	//! @brief The frustum far-bottom-right corner.
	inline const Vec3f& cornerFBR() const { return corners_[7]; }

	inline bool operator == (const Frustum& r) const { return matrix_ == r.matrix_; }
	inline bool operator != (const Frustum& r) const { return matrix_ != r.matrix_; }

private:
	void rebuild();

private:
	Mat4f matrix_;
	std::array<Plane3, PLANE_COUNT> planes_;
	std::array<Vec3f, CORNER_COUNT> corners_;

	VEGA_DEFAULT_COPY(Frustum)
	VEGA_DEFAULT_MOVE(Frustum)
}; // struct Frustum

} // namespace vega
