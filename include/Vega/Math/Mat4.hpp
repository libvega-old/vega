/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Mat4.hpp 4x4 matrices and their operations.

#include "../Common.hpp"
#include "./Math.hpp"
#include "./Vec4.hpp"


namespace vega
{

//! @brief 4x4 matrix of floating point values, stored and accessed in row-major order.
//! @tparam T The floating point type to store in the matrix.
template<typename T>
struct Mat4x4 final
{
	static_assert(std::is_same_v<T, float> || std::is_same_v<T, double>,
		"Mat4x4<T> must have a floating point type as the type parameter");

public:
	//! @brief Typedef of row vector.
	using RowType = Vec4<T>;

public:
	//! @brief Default construct the identity matrix.
	constexpr Mat4x4() noexcept : data{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 } { }
	//! @brief Construct a matrix with the given value on the diagonal.
	explicit constexpr Mat4x4(T diag) noexcept : data{ diag, 0, 0, 0, 0, diag, 0, 0, 0, 0, diag, 0, 0, 0, 0, diag } { }
	//! @brief Construct a matrix from the given components in row-major order.
	constexpr Mat4x4(
		T m00, T m01, T m02, T m03,
		T m10, T m11, T m12, T m13,
		T m20, T m21, T m22, T m23,
		T m30, T m31, T m32, T m33) noexcept
		: data{ m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33 }
	{ }
	//! @brief Construct a matrix from the given array of row-major components.
	constexpr Mat4x4(const T(&comp)[16]) noexcept
		: data{ comp[0], comp[1],  comp[2],  comp[3],  comp[4],  comp[5],  comp[6],  comp[7], 
				comp[8], comp[9], comp[10], comp[11], comp[12], comp[13], comp[14], comp[15] }
	{ }
	//! @brief Construct a matrix from the given rows.
	constexpr Mat4x4(const RowType& r0, const RowType& r1, const RowType& r2, const RowType& r3) noexcept
		: rows{ r0, r1, r2, r3 }
	{ }
	//! @brief Construct a matrix from the given array of rows.
	constexpr Mat4x4(const RowType(&rows)[4]) noexcept : rows{ rows[0], rows[1], rows[2], rows[3] } { }
	//! @brief Construct a matrix from a matrix of a different type.
	//! @tparam U The type of the other matrix.
	template<typename U>
	constexpr explicit Mat4x4(const Mat4x4<U>& mat) noexcept 
		: rows{ RowType(mat.rows[0]), RowType(mat.rows[1]), RowType(mat.rows[2]), RowType(mat.rows[3]) }
	{ }

	//! @brief Gets a reference to the row vector at the given index.
	inline RowType& operator [] (size_t idx) { return rows[idx]; }
	//! @brief Gets a reference to the row vector at the given index.
	inline const RowType& operator [] (size_t idx) const { return rows[idx]; }

	//! @brief Gets the transposed matrix.
	inline Mat4x4 transpose() const {
		return { m00, m10, m20, m30, m01, m11, m21, m31, m02, m12, m22, m32, m03, m13, m23, m33 };
	}
	//! @brief Gets the invertex matrix if invertable, otherwise returns Identity.
	Mat4x4 invert() const;
	//! @brief Gets the matrix determinant.
	T determinant() const;
	//! @brief Gets the matrix trace.
	inline T trace() const { return m00 + m11 + m22 + m33; }

	//! @brief Right-ward direction vector for world and view matrices.
	inline Vec3f right() const { return { m00, m10, m20 }; }
	//! @brief Up-ward direction vector for world and view matrices.
	inline Vec3f up() const { return { m01, m11, m21 }; }
	//! @brief Forward direction vector for world and view matrices.
	inline Vec3f forward() const { return { -m02, -m12, -m22 }; }

	//! @brief Element-wise addition.
	Mat4x4& operator += (const Mat4x4& r);
	//! @brief Element-wise subtraction.
	Mat4x4& operator -= (const Mat4x4& r);
	//! @brief Standard matrix-matrix multiplication.
	Mat4x4& operator *= (const Mat4x4& r);
	//! @brief Multiplies all elements by a constant.
	Mat4x4& operator *= (T r);
	//! @brief Divides all elements by a constant.
	Mat4x4& operator /= (T r);
	//! @brief Element-wise multiplication.
	Mat4x4 matCompMul(const Mat4x4& r) const;
	//! @brief Element-wise division.
	Mat4x4 matCompDiv(const Mat4x4& r) const;
	//! @brief Transforms the normal by the matrix, ignoring the translation component.
	//! @tparam U The normal vector type, must be a floating point type.
	template<typename U>
	std::enable_if_t<std::is_floating_point_v<U>, Vec3<math::widest<T, U>>> transformNormal(const Vec3<U>& norm) const {
		return { m00 * norm.x + m10 * norm.y + m20 * norm.z, m01 * norm.x + m11 * norm.y + m21 * norm.z,
			m02 * norm.x + m12 * norm.y + m22 * norm.z };
	}

	inline bool operator == (const Mat4x4& r) const {
		return rows[0] == r.rows[0] && rows[1] == r.rows[1] && rows[2] == r.rows[2] && rows[3] == r.rows[3];
	}
	inline bool operator != (const Mat4x4& r) const {
		return rows[0] != r.rows[0] || rows[1] != r.rows[1] || rows[2] != r.rows[2] || rows[3] != r.rows[3];
	}

	/***** World Matrices *****/
	//! @brief Creates a matrix that describes a rotation by the given angle about the x-axis.
	inline static Mat4x4 RotationX(Rad<T> angle) {
		const T c{ math::cos(angle) }, s{ math::sin(angle) };
		return { 1, 0, 0, 0,   0, c, s, 0,   0, -s, c, 0,   0, 0, 0, 1 };
	}
	//! @brief Creates a matrix that describes a rotation by the given angle about the y-axis.
	inline static Mat4x4 RotationY(Rad<T> angle) {
		const T c{ math::cos(angle) }, s{ math::sin(angle) };
		return { c, 0, -s, 0,   0, 1, 0, 0,   s, 0, c, 0,   0, 0, 0, 1 };
	}
	//! @brief Creates a matrix that describes a rotation by the given angle about the z-axis.
	inline static Mat4x4 RotationZ(Rad<T> angle) {
		const T c{ math::cos(angle) }, s{ math::sin(angle) };
		return { c, s, 0, 0,   -s, c, 0, 0,   0, 0, 1, 0,   0, 0, 0, 1 };
	}
	//! @brief Creates a matrix that describes a rotation by the given angle about the given normalized axis.
	//! @param axis The axis of rotation, expected to be normalized.
	static Mat4x4 RotationAxis(const Vec3<T>& axis, Rad<T> angle);
	//! @brief Creates a matrix that describes a yaw/pitch/roll rotation.
	static Mat4x4 YawPitchRoll(Rad<T> yaw, Rad<T> pitch, Rad<T> roll);
	//! @brief Creates a matrix that describes a uniform scaling.
	inline static Mat4x4 Scale(T scale) {
		return { scale, 0, 0, 0,   0, scale, 0, 0,   0, 0, scale, 0,   0, 0, 0, 1 };
	}
	//! @brief Creates a matrix that describes scaling along independent axes.
	inline static Mat4x4 Scale(T xscale, T yscale, T zscale) {
		return { xscale, 0, 0, 0,   0, yscale, 0, 0,   0, 0, zscale, 0,   0, 0, 0, 1 };
	}
	//! @brief Creates a matrix that describes a translation.
	inline static Mat4x4 Translation(T x, T y, T z) {
		return { 1, 0, 0, 0,   0, 1, 0, 0,   0, 0, 1, 0,   x, y, z, 1 };
	}
	//! @brief Creates a matrix that describes a translation.
	inline static Mat4x4 Translation(const Vec3<T>& pos) {
		return { 1, 0, 0, 0,   0, 1, 0, 0,   0, 0, 1, 0,   pos.x, pos.y, pos.z, 1 };
	}
	//! @brief Creates a matrix that describes a translation and rotation from orientation vectors.
	//! @param pos The translation to describe.
	//! @param forward The normalized forward vector for facing calculations.
	//! @param up The normalized up vector for facing calculations.
	static Mat4x4 World(const Vec3<T>& pos, const Vec3<T>& forward, const Vec3<T>& up);
	//! @brief Creates a matrix that describes an object transform to align it with a view position.
	//! @param objPos The position of the object to billboard.
	//! @param viewPos The position that the object is viewed from.
	//! @param viewUp The normalized up vector for the view.
	static Mat4x4 Billboard(const Vec3<T>& objPos, const Vec3<T>& viewPos, const Vec3<T>& viewUp);

	//! @brief Creates a look-at view matrix.
	//! @param viewPos The position of the view (camera).
	//! @param targetPos The target position to look at.
	//! @param viewUp The normalized view up vector, controls the "roll" of the view.
	static Mat4x4 LookAt(const Vec3<T>& viewPos, const Vec3<T>& targetPos, const Vec3<T>& viewUp);
	//! @brief Creates a perspective projection matrix.
	//! @param fov The horizonal field of view.
	//! @param aspect The projection aspect ratio.
	//! @param near The near clipping plane distance.
	//! @param far The far clipping plane distance.
	static Mat4x4 Perspective(Rad<T> fov, T aspect, T near, T far);
	//! @brief Creates an orthographic projection matrix.
	//! @param width The width of the orthographic view.
	//! @param height The height of the orthographic view.
	//! @param near The near clipping plane distance.
	//! @param far The far clipping plane distance.
	static Mat4x4 Orthographic(T width, T height, T near, T far);
	//! @brief Creates an off-center orthographics projection matrix.
	//! @param left The coordinate of the left view plane.
	//! @param right The coordinate of the right view plane.
	//! @param bottom The coordinate of the bottom view plane.
	//! @param top The coordinate of the top view plane.
	//! @param near The near clipping plane distance.
	//! @param far The far clipping plane distance.
	static Mat4x4 OrthographicOffCenter(T left, T right, T bottom, T top, T near, T far);

public:
	union
	{
		struct {
			//! @brief Row 0, Column 0
			T m00;
			//! @brief Row 0, Column 1
			T m01;
			//! @brief Row 0, Column 2
			T m02;
			//! @brief Row 0, Column 3
			T m03;
			//! @brief Row 1, Column 0
			T m10;
			//! @brief Row 1, Column 1
			T m11;
			//! @brief Row 1, Column 2
			T m12;
			//! @brief Row 1, Column 3
			T m13;
			//! @brief Row 2, Column 0
			T m20;
			//! @brief Row 2, Column 1
			T m21;
			//! @brief Row 2, Column 2
			T m22;
			//! @brief Row 2, Column 3
			T m23;
			//! @brief Row 3, Column 0
			T m30;
			//! @brief Row 3, Column 1
			T m31;
			//! @brief Row 3, Column 2
			T m32;
			//! @brief Row 3, Column 3
			T m33;
		};
		//! @brief Matrix data as rows.
		RowType rows[4];
		//! @brief Raw array of matrix data, stored in row-major order (first entries are m00, m01, ect...).
		T data[16];
	};

	//! @brief The identity matrix.
	static const Mat4x4 Identity;

	VEGA_DEFAULT_COPY(Mat4x4)
	VEGA_DEFAULT_MOVE(Mat4x4)
}; // struct Mat4x4

//! @brief Typedef of Mat4x4<float> to Mat4f
using Mat4f = Mat4x4<float>;
//! @brief Typedef of Mat4x4<double> to Mat4d
using Mat4d = Mat4x4<double>;


template<typename T> constexpr Mat4x4<T> Mat4x4<T>::Identity{ T(1) };

template<typename T> inline Mat4x4<T> Mat4x4<T>::invert() const {
	// Solution based on Laplace Expansion, found at https://stackoverflow.com/a/9614511
	const double
		s0{ (double)m00 * m11 - (double)m10 * m01 }, s1{ (double)m00 * m12 - (double)m10 * m02 },
		s2{ (double)m00 * m13 - (double)m10 * m03 }, s3{ (double)m01 * m12 - (double)m11 * m02 },
		s4{ (double)m01 * m13 - (double)m11 * m03 }, s5{ (double)m02 * m13 - (double)m12 * m03 },
		c5{ (double)m22 * m33 - (double)m32 * m23 }, c4{ (double)m21 * m33 - (double)m31 * m23 },
		c3{ (double)m21 * m32 - (double)m31 * m22 }, c2{ (double)m20 * m33 - (double)m30 * m23 },
		c1{ (double)m20 * m32 - (double)m30 * m22 }, c0{ (double)m20 * m31 - (double)m30 * m21 };

	const double det{ s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0 };
	if (math::close(det, 0.0)) return Identity;
	const double invdet = 1.0 / det;

	return {
		T(( m11 * c5 - m12 * c4 + m13 * c3) * invdet),
		T((-m01 * c5 + m02 * c4 - m03 * c3) * invdet),
		T(( m31 * s5 - m32 * s4 + m33 * s3) * invdet),
		T((-m21 * s5 + m22 * s4 - m23 * s3) * invdet),
		T((-m10 * c5 + m12 * c2 - m13 * c1) * invdet),
		T(( m00 * c5 - m02 * c2 + m03 * c1) * invdet),
		T((-m30 * s5 + m32 * s2 - m33 * s1) * invdet),
		T(( m20 * s5 - m22 * s2 + m23 * s1) * invdet),
		T(( m10 * c4 - m11 * c2 + m13 * c0) * invdet),
		T((-m00 * c4 + m01 * c2 - m03 * c0) * invdet),
		T(( m30 * s4 - m31 * s2 + m33 * s0) * invdet),
		T((-m20 * s4 + m21 * s2 - m23 * s0) * invdet),
		T((-m10 * c3 + m11 * c1 - m12 * c0) * invdet),
		T(( m00 * c3 - m01 * c1 + m02 * c0) * invdet),
		T((-m30 * s3 + m31 * s1 - m32 * s0) * invdet),
		T(( m20 * s3 - m21 * s1 + m22 * s0) * invdet)
	};
}

template<typename T> T inline Mat4x4<T>::determinant() const {
	const T
		s0{ m00 * m11 - m10 * m01 }, s1{ m00 * m12 - m10 * m02 },
		s2{ m00 * m13 - m10 * m03 }, s3{ m01 * m12 - m11 * m02 },
		s4{ m01 * m13 - m11 * m03 }, s5{ m02 * m13 - m12 * m03 },
		c5{ m22 * m33 - m32 * m23 }, c4{ m21 * m33 - m31 * m23 },
		c3{ m21 * m32 - m31 * m22 }, c2{ m20 * m33 - m30 * m23 },
		c1{ m20 * m32 - m30 * m22 }, c0{ m20 * m31 - m30 * m21 };
	return s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;
}

//! @brief Component-wise addition of the matrices.
template<typename T, typename U> 
inline constexpr Mat4x4<math::widest<T, U>> operator + (const Mat4x4<T>& l, const Mat4x4<U>& r) {
	return {
		l.m00 + r.m00, l.m01 + r.m01, l.m02 + r.m02, l.m03 + r.m03,
		l.m10 + r.m10, l.m11 + r.m11, l.m12 + r.m12, l.m13 + r.m13,
		l.m20 + r.m20, l.m21 + r.m21, l.m22 + r.m22, l.m23 + r.m23,
		l.m30 + r.m30, l.m31 + r.m31, l.m32 + r.m32, l.m33 + r.m33
	};
}
template<typename T> inline Mat4x4<T>& Mat4x4<T>::operator += (const Mat4x4<T>& r) {
	return *this = *this + r;
}

//! @brief Component-wise subtraction of the matrices.
template<typename T, typename U>
inline constexpr Mat4x4<math::widest<T, U>> operator - (const Mat4x4<T>& l, const Mat4x4<U>& r) {
	return {
		l.m00 - r.m00, l.m01 - r.m01, l.m02 - r.m02, l.m03 - r.m03,
		l.m10 - r.m10, l.m11 - r.m11, l.m12 - r.m12, l.m13 - r.m13,
		l.m20 - r.m20, l.m21 - r.m21, l.m22 - r.m22, l.m23 - r.m23,
		l.m30 - r.m30, l.m31 - r.m31, l.m32 - r.m32, l.m33 - r.m33
	};
}
template<typename T> inline Mat4x4<T>& Mat4x4<T>::operator -= (const Mat4x4<T>& r) {
	return *this = *this - r;
}

//! @brief Standard matrix/matrix multiplication.
template<typename T, typename U>
inline constexpr Mat4x4<math::widest<T, U>> operator * (const Mat4x4<T>& l, const Mat4x4<U>& r) {
	return {
		(l.m00 * r.m00 + l.m01 * r.m10 + l.m02 * r.m20 + l.m03 * r.m30),
		(l.m00 * r.m01 + l.m01 * r.m11 + l.m02 * r.m21 + l.m03 * r.m31),
		(l.m00 * r.m02 + l.m01 * r.m12 + l.m02 * r.m22 + l.m03 * r.m32),
		(l.m00 * r.m03 + l.m01 * r.m13 + l.m02 * r.m23 + l.m03 * r.m33),
		(l.m10 * r.m00 + l.m11 * r.m10 + l.m12 * r.m20 + l.m13 * r.m30),
		(l.m10 * r.m01 + l.m11 * r.m11 + l.m12 * r.m21 + l.m13 * r.m31),
		(l.m10 * r.m02 + l.m11 * r.m12 + l.m12 * r.m22 + l.m13 * r.m32),
		(l.m10 * r.m03 + l.m11 * r.m13 + l.m12 * r.m23 + l.m13 * r.m33),
		(l.m20 * r.m00 + l.m21 * r.m10 + l.m22 * r.m20 + l.m23 * r.m30),
		(l.m20 * r.m01 + l.m21 * r.m11 + l.m22 * r.m21 + l.m23 * r.m31),
		(l.m20 * r.m02 + l.m21 * r.m12 + l.m22 * r.m22 + l.m23 * r.m32),
		(l.m20 * r.m03 + l.m21 * r.m13 + l.m22 * r.m23 + l.m23 * r.m33),
		(l.m30 * r.m00 + l.m31 * r.m10 + l.m32 * r.m20 + l.m33 * r.m30),
		(l.m30 * r.m01 + l.m31 * r.m11 + l.m32 * r.m21 + l.m33 * r.m31),
		(l.m30 * r.m02 + l.m31 * r.m12 + l.m32 * r.m22 + l.m33 * r.m32),
		(l.m30 * r.m03 + l.m31 * r.m13 + l.m32 * r.m23 + l.m33 * r.m33)
	};
}
template<typename T> inline Mat4x4<T>& Mat4x4<T>::operator *= (const Mat4x4<T>& r) {
	return *this = *this * r;
}

//! @brief Standard matrix/vector multiplication to modify the vector by the matrix transform.
template<typename T, typename U>
inline constexpr auto operator * (const Mat4x4<T>& l, const Vec4<U>& r) {
	return Vec4<decltype(l.m00 * r.x)>{
		l.m00 * r.x + l.m10 * r.y + l.m20 * r.z + l.m30 * r.w,
		l.m01 * r.x + l.m11 * r.y + l.m21 * r.z + l.m31 * r.w,
		l.m02 * r.x + l.m12 * r.y + l.m22 * r.z + l.m32 * r.w,
		l.m03 * r.x + l.m13 * r.y + l.m23 * r.z + l.m33 * r.w,
	};
}

//! @brief Standard matrix/vector multiplication to modify the vector by the matrix transform.
template<typename T, typename U>
inline constexpr auto operator * (const Mat4x4<T>& l, const Vec3<U>& r) {
	return Vec3<decltype(l.m00 * r.x)>{
		l.m00 * r.x + l.m10 * r.y + l.m20 * r.z + l.m30,
		l.m01 * r.x + l.m11 * r.y + l.m21 * r.z + l.m31,
		l.m02 * r.x + l.m12 * r.y + l.m22 * r.z + l.m32
	};
}

//! @brief Component-wise multiplication by scalar.
template<typename T>
inline constexpr Mat4x4<T> operator * (const Mat4x4<T>& l, T r) {
	return {
		l.m00 * r, l.m01 * r, l.m02 * r, l.m03 * r,
		l.m10 * r, l.m11 * r, l.m12 * r, l.m13 * r,
		l.m20 * r, l.m21 * r, l.m22 * r, l.m23 * r,
		l.m30 * r, l.m31 * r, l.m32 * r, l.m33 * r
	};
}
template<typename T> inline Mat4x4<T>& Mat4x4<T>::operator *= (T r) {
	return *this = *this * r;
}

//! @brief Component-wise division by scalar.
template<typename T>
inline constexpr Mat4x4<T> operator / (const Mat4x4<T>& l, T r) {
	return {
		l.m00 / r, l.m01 / r, l.m02 / r, l.m03 / r,
		l.m10 / r, l.m11 / r, l.m12 / r, l.m13 / r,
		l.m20 / r, l.m21 / r, l.m22 / r, l.m23 / r,
		l.m30 / r, l.m31 / r, l.m32 / r, l.m33 / r
	};
}
template<typename T> inline Mat4x4<T>& Mat4x4<T>::operator /= (T r) {
	return *this = *this / r;
}

template<typename T> inline Mat4x4<T> Mat4x4<T>::matCompMul(const Mat4x4<T>& r) const {
	return {
		m00 * r.m00, m01 * r.m01, m02 * r.m02, m03 * r.m03,
		m10 * r.m10, m11 * r.m11, m12 * r.m12, m13 * r.m13,
		m20 * r.m20, m21 * r.m21, m22 * r.m22, m23 * r.m23,
		m30 * r.m30, m31 * r.m31, m32 * r.m32, m33 * r.m33
	};
}

template<typename T> inline Mat4x4<T> Mat4x4<T>::matCompDiv(const Mat4x4<T>& r) const {
	return {
		m00 / r.m00, m01 / r.m01, m02 / r.m02, m03 / r.m03,
		m10 / r.m10, m11 / r.m11, m12 / r.m12, m13 / r.m13,
		m20 / r.m20, m21 / r.m21, m22 / r.m22, m23 / r.m23,
		m30 / r.m30, m31 / r.m31, m32 / r.m32, m33 / r.m33
	};
}

template<typename T> inline Mat4x4<T> Mat4x4<T>::RotationAxis(const Vec3<T>& axis, Rad<T> angle) {
	const T c{ math::cos(angle) }, s{ math::sin(angle) },
		xx{ axis.x * axis.x }, yy{ axis.y * axis.y }, zz{ axis.z * axis.z },
		xy{ axis.x * axis.y }, xz{ axis.x * axis.z }, yz{ axis.y * axis.z };
	return {
				 xx + (c * (1.f - xx)),  (xy - (c * xy)) + (s * axis.z),  (xz - (c * xz)) - (s * axis.y),  0,
		(xy - (c * xy)) - (s * axis.z),           yy + (c * (1.f - yy)),  (yz - (c * yz)) + (s * axis.z),  0,
		(xz - (c * xz)) + (s * axis.y),  (yz - (c * yz)) - (s * axis.x),           zz + (c * (1.f - zz)),  0,
									 0,                               0,                               0,  1
	};
}

template<typename T> inline Mat4x4<T> Mat4x4<T>::YawPitchRoll(Rad<T> yaw, Rad<T> pitch, Rad<T> roll) {
	const T
		sy{ math::sin(yaw / 2) }, cy{ math::cos(yaw / 2) }, sp{ math::sin(pitch / 2) },
		cp{ math::cos(pitch / 2) }, sr{ math::sin(roll / 2) }, cr{ math::cos(roll / 2) },
		x{ (cy * sp * cr) + (sy * cp * sr) }, y{ (sy * cp * cr) - (cy * sp * sr) },
		z{ (cy * cp * sr) - (sy * sp * cr) }, w{ (cy * cp * cr) + (sy * sp * sr) },
		xx{ x * x }, yy{ y * y }, zz{ z * z }, xy{ x * y }, xz{ x * z }, xw{ x * w }, yz{ y * z },
		yw{ y * w }, zw{ z * w };
	return {
		1 - (2 * (yy + zz)),        2 * (xy + zw),        2 * (xz - yw),  0,
			  2 * (xy - zw),  1 - (2 * (xx + zz)),        2 * (yz + xw),  0,
			  2 * (xz + yw),        2 * (yz - xw),  1 - (2 * (xx + yy)),  0,
						  0,                    0,                    0,  1
	};
}

template<typename T> inline Mat4x4<T> Mat4x4<T>::World(const Vec3<T>& pos, const Vec3<T>& forward, const Vec3<T>& up) {
	const Vec3<T> right{ forward.cross(up) }, trueup{ right.cross(forward) };
	return {
		right.x, trueup.x, forward.x, 0,
		right.y, trueup.y, forward.y, 0,
		right.z, trueup.z, forward.z, 0,
		  pos.x,    pos.y,     pos.z, 1
	};
}

template<typename T> 
inline Mat4x4<T> Mat4x4<T>::Billboard(const Vec3<T>& objPos, const Vec3<T>& viewPos, const Vec3<T>& viewUp) {
	const Vec3<T> dir{ (objPos - viewPos).normalized() }, right{ viewUp.cross(dir) }, trueup{ dir.cross(right) };
	return {
		 right.x, trueup.x,    dir.x, 0,
		 right.y, trueup.y,    dir.y, 0,
		 right.z, trueup.z,    dir.z, 0,
		objPos.x, objPos.y, objPos.z, 1
	};
}

template<typename T> 
inline Mat4x4<T> Mat4x4<T>::LookAt(const Vec3<T>& viewPos, const Vec3<T>& targetPos, const Vec3<T>& viewUp) {
	const Vec3<T> forward{ (targetPos - viewPos).normalized() }, right{ forward.cross(viewUp).normalized() },
		trueup{ right.cross(forward) };
	const float d1{ right.dot(viewPos) }, d2{ trueup.dot(viewPos) }, d3{ forward.dot(viewPos) };
	return {
		right.x, trueup.x, -forward.x, 0,  
		right.y, trueup.y, -forward.y, 0,
		right.z, trueup.z, -forward.z, 0,      
		    -d1,      -d2,         d3, 1
	};
}

template<typename T> inline Mat4x4<T> Mat4x4<T>::Perspective(Rad<T> fov, T aspect, T near, T far) {
	const T f{ 1 / math::tan(fov / 2) }, depth{ near - far };
	return {
		f / aspect, 0, 0, 0,   0, -f, 0, 0,   0, 0, far / depth, -1,   0, 0, (near * far) / depth, 0
	};
}

template<typename T> inline Mat4x4<T> Mat4x4<T>::Orthographic(T width, T height, T near, T far) {
	const T depth{ near - far };
	return {
		2 / width, 0, 0, 0,   0, -2 / height, 0, 0,   0, 0, 1 / depth, 0,   0, 0, near / depth, 1
	};
}

template<typename T> 
inline Mat4x4<T> Mat4x4<T>::OrthographicOffCenter(T left, T right, T bottom, T top, T near, T far) {
	const T width{ right - left }, height{ bottom - top }, depth{ near - far };
	return {
		2 / width, 0,         0, 0,                         0,              -2 / height,            0, 0,
		0,         0, 1 / depth, 0,   -(right + left) / width, -(bottom + top) / height, near / depth, 1
	};
}

} // namespace vega
