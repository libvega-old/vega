/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Hash.hpp Data hashing functions.

#include "../Common.hpp"
#include "./Math.hpp"


namespace vega
{

//! @brief 32-bit hash digest result.
struct VEGA_API Hash32 final
{
public:
	//! @brief Initialize a zero-valued hash digest.
	constexpr Hash32() noexcept : value_{ 0 } { }
	//! @brief Initialize a hash from a raw hash digest.
	constexpr Hash32(uint32 hash) noexcept : value_{ hash } { }

	//! @brief The hash digest value.
	inline constexpr uint32 value() const { return value_; }

	inline constexpr bool operator == (Hash32 r) const { return value_ == r.value_; }
	inline constexpr bool operator != (Hash32 r) const { return value_ != r.value_; }
	inline constexpr bool operator <  (Hash32 r) const { return value_ <  r.value_; }

	//! @brief Construct a new 32-bit hash digest using MurmurHash3.
	//! @param data The data to hash.
	//! @param length The length of the data to hash.
	//! @param seed The initial hash seed.
	static Hash32 MurmurHash3(const void* data, uint32 length, uint32 seed = 0);
	//! @brief Construct a new 32-bit hash digest of the string data using MurmurHash3.
	template<size_t N>
	inline static Hash32 MurmurHash3(const char (&str)[N], uint32 seed = 0) {
		return MurmurHash3((const void*)str, N, seed);
	}
	//! @brief Construct a new 32-bit hash digest of the string data using MurmurHash3.
	inline static Hash32 MurmurHash3(const StringView& str, uint32 seed = 0) {
		return MurmurHash3(str.data(), uint32(str.length()), seed);
	}
	//! @brief Construct a new 32-bit hash digest of the array using MurmurHash3.
	template<typename T, size_t N>
	inline static Hash32 MurmurHash3(const std::array<T, N>& data) {
		static_assert(std::is_pod_v<T>, "std::array must contain POD types for hashing");
		return MurmurHash3((const void*)data.data(), uint32(data.size() * sizeof(T)));
	}
	//! @brief Construct a new 32-bit hash digest of the initializer_list using MurmurHash3.
	template<typename T>
	inline static Hash32 MurmurHash3(const std::initializer_list<T>& data) {
		static_assert(std::is_pod_v<T>, "std::initializer_list must contain POD types for hashing");
		return MurmurHash3((const void*)data.begin(), uint32(data.size() * sizeof(T)));
	}

private:
	uint32 value_;
}; // struct Hash32


//! @brief 64-bit hash digest result.
struct VEGA_API Hash64 final
{
public:
	//! @brief Initialize a zero-valued hash digest.
	constexpr Hash64() noexcept : value_{ 0 } { }
	//! @brief Initialize a hash from a raw hash digest.
	constexpr Hash64(uint64 hash) noexcept : value_{ hash } { }

	//! @brief The hash digest value.
	inline constexpr uint64 value() const { return value_; }

	inline constexpr bool operator == (Hash64 r) const { return value_ == r.value_; }
	inline constexpr bool operator != (Hash64 r) const { return value_ != r.value_; }
	inline constexpr bool operator <  (Hash64 r) const { return value_ <  r.value_; }

	//! @brief Construct a new 64-bit hash digest using xxHash64.
	//! @param data The data to hash.
	//! @param length The length of the data to hash.
	//! @param seed The initial hash seed.
	static Hash64 XXHash64(const void* data, uint32 length, uint32 seed = 0);
	//! @brief Construct a new 64-bit hash digest of the string data using xxHash64.
	template<size_t N>
	inline static Hash64 XXHash64(const char(&str)[N], uint32 seed = 0) {
		return XXHash64((const void*)str, N, seed);
	}
	//! @brief Construct a new 64-bit hash digest of the string data using xxHash64.
	inline static Hash64 XXHash64(const StringView& str, uint32 seed = 0) {
		return XXHash64(str.data(), uint32(str.length()), seed);
	}
	//! @brief Construct a new 64-bit hash digest of the array using xxHash64.
	template<typename T, size_t N>
	inline static Hash64 XXHash64(const std::array<T, N>& data) {
		static_assert(std::is_pod_v<T>, "std::array must contain POD types for hashing");
		return XXHash64((const void*)data.data(), uint32(data.size() * sizeof(T)));
	}
	//! @brief Construct a new 64-bit hash digest of the initializer_list using xxHash64.
	template<typename T>
	inline static Hash64 XXHash64(const std::initializer_list<T>& data) {
		static_assert(std::is_pod_v<T>, "std::initializer_list must contain POD types for hashing");
		return XXHash64((const void*)data.begin(), uint32(data.size() * sizeof(T)));
	}

private:
	uint64 value_;
}; // struct Hash64

} // namespace vega


template<> struct std::hash<vega::Hash32> final
{
	constexpr size_t operator () (vega::Hash32 hash) const {
		return vega::math::avalanche64(hash.value());
	}
}; // struct std::hash<vega::Hash32>

template<> struct std::hash<vega::Hash64> final
{
	constexpr size_t operator () (vega::Hash64 hash) const {
		return hash.value();
	}
}; // struct std::hash<vega::Hash64>
