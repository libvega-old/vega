/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Bitset.hpp Memory-efficient bit flags.

#include "../Common.hpp"
#include "./Math.hpp"

#include <vector>


namespace vega
{

//! @brief Memory-efficient dynamic-sized contiguous collection of 1-bit flags.
class VEGA_API Bitset final
{
	using Word = uint64;
	inline static constexpr size_t WORD_SIZE{ sizeof(Word) * 8 };
	inline static constexpr Word WORD_NONE{ Word(0) };
	inline static constexpr Word WORD_ALL{ ~Word(0) };

public:
	//! @brief Create a new Bitset of the given bit count with all bits cleared.
	//! @param count The maximum number of flags to support - most efficient to keep it a multiple of 64.
	explicit Bitset(size_t count = WORD_SIZE)
		: size_{ count }, data_{ }
	{
		data_.resize(CountToWords(size_), WORD_NONE);
	}

	//! @brief Set the new number of bits in the set without modifying existing data.
	inline void resize(size_t count) {
		size_ = count;
		data_.resize(CountToWords(count), WORD_NONE);
	}

	//! @brief Clears all bits in the set.
	inline void clear() {
		std::memset(data_.data(), WORD_NONE, data_.size() * WORD_SIZE);
	}

	//! @brief Gets if no bits are set.
	inline bool none() const {
		const auto end = data_.data() + data_.size();
		for (auto it = data_.data(); it != end; ++it) {
			if (*it != WORD_NONE) return false;
		}
		return true;
	}
	//! @brief Gets if any bits are set.
	inline bool any() const {
		const auto end = data_.data() + data_.size();
		for (auto it = data_.data(); it != end; ++it) {
			if (*it != WORD_NONE) return true;
		}
		return false;
	}
	//! @brief Gets if all bits are set.
	inline bool all() const {
		const bool padding{ (size_ % WORD_SIZE) != 0 };
		const auto end = data_.data() + (data_.size() - (padding ? 1 : 0));
		for (auto it = data_.data(); it != end; ++it) {
			if (*it != WORD_ALL) return false;
		}
		return !padding || (*data_.rbegin() == ((1ull << (size_ % WORD_SIZE)) - 1));
	}

	//! @brief Gets the value of the bit at the given index.
	inline bool get(size_t index) const {
		return (data_[index / WORD_SIZE] >> (index % WORD_SIZE)) & 0x1;
	}
	//! @brief Sets the bit at the given index to one.
	inline void set(size_t index) {
		const Word mask{ 1ull << (index % WORD_SIZE) };
		data_[index / WORD_SIZE] |= mask;
	}
	//! @brief Sets the bit at the given index to zero.
	inline void clear(size_t index) {
		const Word mask{ 1ull << (index % WORD_SIZE) };
		data_[index / WORD_SIZE] &= ~mask;
	}

	//! @brief Gets the index of the first set (== one) bit, or \c std::nullopt if none are set.
	Opt<size_t> firstSet() const;
	//! @brief Gets the index of the first clear (== zero) bit, or \c std::nullopt if all are set.
	Opt<size_t> firstClear() const;

private:
	inline static size_t CountToWords(size_t count) { return (count == 0) ? 0 : ((count - 1) / 64ull) + 1; }

private:
	size_t size_;
	std::vector<Word> data_;

	VEGA_DEFAULT_COPY(Bitset)
	VEGA_DEFAULT_MOVE(Bitset)
}; // class Bitset

} // namespace vega
