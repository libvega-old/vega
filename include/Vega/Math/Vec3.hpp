/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

 //! @file Vec3.hpp 3-component vector types.

#include "../Common.hpp"
#include "./Math.hpp"
#include "./Vec2.hpp"


namespace vega
{

//! @brief 3-component vector of boolean values.
struct VEGA_API BVec3 final
{
public:
	//! @brief Initialize all components to false.
	constexpr BVec3() noexcept : data{ false,false,false } { }
	//! @brief Initialize all components to the given value.
	explicit constexpr BVec3(bool b) noexcept : data{ b,b,b } { }
	//! @brief Initialize components with the given values.
	constexpr BVec3(bool x, bool y, bool z) noexcept : x{ x }, y{ y }, z{ z } { }

	//! @brief Returns if any component is \c true.
	inline constexpr bool any() const { return x || y || z; }
	//! @brief Returns if all components are \c true.
	inline constexpr bool all() const { return x && y && z; }
	//! @brief Returns if no components are \c true.
	inline constexpr bool none() const { return !x && !y && !z; }
	//! @brief Returns the number of components that are \c true.
	inline constexpr uint32 count() const { return uint32(x + y + z); }

	inline constexpr bool operator == (BVec3 r) const { return x == r.x && y == r.y && z == r.z; }
	inline constexpr bool operator != (BVec3 r) const { return x != r.x || y != r.y || z != r.z; }
	inline bool operator < (BVec3 r) const { 
		return (x + (y << 1) + (z << 2)) < (r.x + (r.y << 1) + (r.z << 2));
	}
	inline constexpr BVec3 operator & (BVec3 r) const { return { x && r.x, y && r.y, z && r.z }; }
	inline constexpr BVec3 operator | (BVec3 r) const { return { x || r.x, y || r.y, z || r.z }; }
	inline constexpr BVec3 operator ^ (BVec3 r) const { return { x != r.x, y != r.y, z != r.z }; }

public:
	union {
		struct { bool x; bool y; bool z; };
		bool data[3];
	};
}; // struct BVec3


//! @brief 3-component vector of numeric values.
//! @tparam T The numeric type of the vector components.
template<typename T>
struct Vec3 final
{
public:
	constexpr Vec3() noexcept : x{ }, y{ }, z{ } { }
	template<typename U>
	explicit constexpr Vec3(const U& v) noexcept
		: x{ static_cast<T>(v) }, y{ static_cast<T>(v) }, z{ static_cast<T>(v) }
	{ }
	template<typename U, typename V, typename A>
	constexpr Vec3(const U& x, const V& y, const A& z) noexcept
		: x{ static_cast<T>(x) }, y{ static_cast<T>(y) }, z{ static_cast<T>(z) }
	{ }
	template<typename U>
	explicit constexpr Vec3(const Vec3<U>& o) noexcept
		: x{ static_cast<T>(o.x) }, y{ static_cast<T>(o.y) }, z{ static_cast<T>(o.z) }
	{ }
	template<typename U, typename V>
	constexpr Vec3(const Vec2<U>& v, const V& z) noexcept
		: x{ static_cast<T>(v.x) }, y{ static_cast<T>(v.y) }, z{ static_cast<T>(z) }
	{ }

	constexpr const T& operator [] (size_t index) const { return data[index]; }
	constexpr T& operator [] (size_t index) { return data[index]; }

	//! @brief Converts the components of the vector into a different type.
	//! @tparam U The type to convert the components into.
	template<typename U>
	inline constexpr Vec3<U> as() const { return Vec3<U>(x, y, z); }

	template<typename U>
	inline constexpr explicit operator Vec2<U>() const { return { x, y }; }

	//! @brief Gets the cartesian length of the vector.
	inline constexpr auto length() const { return std::sqrt((x * x) + (y * y) + (z * z)); }
	//! @brief Gets the square of the cartesian length of the vector.
	inline constexpr auto lengthSq() const { return (x * x) + (y * y) + (z * z); }
	//! @brief Gets the normalized (length 1) vector in the same direction as the vector.
	inline constexpr auto normalized() const {
		auto ilen = 1 / length();
		return Vec3<decltype(ilen)>(x * ilen, y * ilen, z * ilen);
	}

	//! @brief Gets the cartesian distance to the end of the other vector.
	template<typename U>
	constexpr auto distance(const Vec3<U>& o) const {
		auto dx = o.x - x, dy = o.y - y, dz = o.z - z;
		return std::sqrt((dx * dx) + (dy * dy) + (dz * dz));
	}
	//! @brief Gets the square of the cartesian distance to the end of the other vector.
	template<typename U>
	constexpr auto distanceSq(const Vec3<U>& o) const {
		auto dx = o.x - x, dy = o.y - y, dz = o.z - z;
		return (dx * dx) + (dy * dy) + (dz * dz);
	}

	//! @brief Gets the dot product with the other vector.
	template<typename U>
	inline constexpr auto dot(const Vec3<U>& o) const {
		return (o.x * x) + (o.y * y) + (o.z * z);
	}
	//! @brief Gets this vector projected onto the other vector.
	template<typename U>
	inline constexpr auto project(const Vec3<U>& o) const {
		auto a = dot(o);
		return Vec3<decltype(a)>(o.x * a, o.y * a, o.z * a);
	}
	//! @brief Gets this vector around the other vector.
	template<typename U>
	inline constexpr auto reflect(const Vec3<U>& n) const {
		auto f = 2 * dot(n);
		return Vec3<decltype(f)>(x - (n.x * f), y - (n.y * f), z - (n.z * f));
	}
	//! @brief Refracts this vector past the given normal vector and index of refraction.
	//! @param eta The index of reflection.
	template<typename U>
	inline constexpr auto refract(const Vec3<U>& n, float eta) const {
		auto d = dot(n);
		auto k = 1 - (eta * eta * (1 - (d * d)));
		return (k < 0) ? Zero : ((*this * eta) - (n * ((eta * d) + std::sqrt(k))));
	}
	//! @brief Gets the angle (in radians) between this vector and the other.
	template<typename U>
	inline constexpr auto angleWith(const Vec3<U>& o) const {
		auto d = dot(o) / (length() * o.length());
		return Rad<decltype(d)>(std::acos(d));
	}
	//! @brief Gets the cross product of this vector and the other vector.
	template<typename U>
	inline constexpr auto cross(const Vec3<U>& o) const {
		auto nx = (y * o.z) - (z * o.y);
		auto ny = (z * o.x) - (x * o.z);
		auto nz = (x * o.y) - (y * o.x);
		return Vec3<decltype(nx)>(nx, ny, nz);
	}

	//! @brief Gets the component-wise minimum of the two vectors.
	template<typename U>
	inline constexpr auto min(const Vec3<U>& o) const {
		auto mx = math::min(x, o.x), my = math::min(y, o.y), mz = math::min(z, o.z);
		return Vec3<decltype(mx)>(mx, my, mz);
	}
	//! @brief Gets the component-wise maximum of the two vectors.
	template<typename U>
	inline constexpr auto max(const Vec3<U>& o) const {
		auto mx = math::max(x, o.x), my = math::max(y, o.y), mz = math::max(z, o.z);
		return Vec3<decltype(mx)>(mx, my, mz);
	}
	//! @brief Gets the component-wise clamp of this vector and the min and max vectors.
	template<typename U, typename V>
	inline constexpr auto clamp(const Vec3<U>& min, const Vec3<V>& max) const {
		auto cx = math::clamp(x, min.x, max.x), cy = math::clamp(y, min.y, max.y), cz = math::clamp(z, min.z, max.z);
		return Vec3<decltype(cx)>(cx, cy, cz);
	}

	//! @brief Performs a component-wise equality check between the two vectors.
	template<typename U>
	inline constexpr BVec3 compEq(const Vec3<U>& o) const {
		return { x == o.x, y == o.y, z == o.z };
	}
	//! @brief Performs a component-wise inequality check between the two vectors.
	template<typename U>
	inline constexpr BVec3 compNeq(const Vec3<U>& o) const {
		return { x != o.x, y != o.y, z != o.z };
	}

	template<typename U>
	Vec3& operator += (const Vec3<U>& v) {
		x = static_cast<T>(x + v.x); y = static_cast<T>(y + v.y); z = static_cast<T>(z + v.z);
		return *this;
	}
	template<typename U>
	Vec3& operator -= (const Vec3<U>& v) {
		x = static_cast<T>(x - v.x); y = static_cast<T>(y - v.y); z = static_cast<T>(z - v.z);
		return *this;
	}
	template<typename U>
	Vec3& operator *= (const Vec3<U>& v) {
		x = static_cast<T>(x * v.x); y = static_cast<T>(y * v.y); z = static_cast<T>(z * v.z);
		return *this;
	}
	template<typename U>
	Vec3& operator /= (const Vec3<U>& v) {
		x = static_cast<T>(x / v.x); y = static_cast<T>(y / v.y); z = static_cast<T>(z / v.z);
		return *this;
	}
	template<typename U>
	Vec3& operator += (const U& v) {
		x = static_cast<T>(x + v); y = static_cast<T>(y + v); z = static_cast<T>(z + v);
		return *this;
	}
	template<typename U>
	Vec3& operator -= (const U& v) {
		x = static_cast<T>(x - v); y = static_cast<T>(y - v); z = static_cast<T>(z - v);
		return *this;
	}
	template<typename U>
	Vec3& operator *= (const U& v) {
		x = static_cast<T>(x * v); y = static_cast<T>(y * v); z = static_cast<T>(z * v);
		return *this;
	}
	template<typename U>
	Vec3& operator /= (const U& v) {
		x = static_cast<T>(x / v); y = static_cast<T>(y / v); z = static_cast<T>(z / v);
		return *this;
	}

public:
	union {
		struct { T x; T y; T z; };
		T data[3];
	};

	//! @brief Constant vector with components (0, 0, 0).
	static const Vec3 Zero;
	//! @brief Constant vector with components (1, 1, 1).
	static const Vec3 One;
	//! @brief Constant vector with components (1, 0, 0).
	static const Vec3 UnitX;
	//! @brief Constant vector with components (0, 1, 0).
	static const Vec3 UnitY;
	//! @brief Constant vector with components (0, 0, 1).
	static const Vec3 UnitZ;
	//! @brief Constant vector with components (1, 0, 0) - the rightward vector in right-handed space.
	static const Vec3 Right;
	//! @brief Constant vector with components (-1, 0, 0) - the leftward vector in right-handed space.
	static const Vec3 Left;
	//! @brief Constant vector with components (0, 1, 0) - the upward vector in right-handed space.
	static const Vec3 Up;
	//! @brief Constant vector with components (0, -1, 0) - the downward vector in right-handed space.
	static const Vec3 Down;
	//! @brief Constant vector with components (0, 0, 1) - the backward vector in right-handed space.
	static const Vec3 Backward;
	//! @brief Constant vector with components (0, 0, -1) - the forward vector in right-handed space.
	static const Vec3 Forward;
}; // struct Vec3

template<typename T> constexpr Vec3<T> Vec3<T>::Zero    {  0,  0,  0 };
template<typename T> constexpr Vec3<T> Vec3<T>::One     {  1,  1,  1 };
template<typename T> constexpr Vec3<T> Vec3<T>::UnitX   {  1,  0,  0 };
template<typename T> constexpr Vec3<T> Vec3<T>::UnitY   {  0,  1,  0 };
template<typename T> constexpr Vec3<T> Vec3<T>::UnitZ   {  0,  0,  1 };
template<typename T> constexpr Vec3<T> Vec3<T>::Right   {  1,  0,  0 };
template<typename T> constexpr Vec3<T> Vec3<T>::Left    { -1,  0,  0 };
template<typename T> constexpr Vec3<T> Vec3<T>::Up      {  0,  1,  0 };
template<typename T> constexpr Vec3<T> Vec3<T>::Down    {  0, -1,  0 };
template<typename T> constexpr Vec3<T> Vec3<T>::Backward{  0,  0,  1 };
template<typename T> constexpr Vec3<T> Vec3<T>::Forward {  0,  0, -1 };

//! @brief Vec3 specialization with `float`.
using Vec3f  = Vec3<float>;
//! @brief Vec3 specialization with `double`.
using Vec3d  = Vec3<double>;
//! @brief Vec3 specialization with `int8`.
using Vec3b  = Vec3<int8>;
//! @brief Vec3 specialization with `uint8`.
using Vec3ub = Vec3<uint8>;
//! @brief Vec3 specialization with `int16`.
using Vec3s  = Vec3<int16>;
//! @brief Vec3 specialization with `uint16`.
using Vec3us = Vec3<uint16>;
//! @brief Vec3 specialization with `int32`.
using Vec3i  = Vec3<int32>;
//! @brief Vec3 specialization with `uint32`.
using Vec3ui = Vec3<uint32>;
//! @brief Vec3 specialization with `int64`.
using Vec3l  = Vec3<int64>;
//! @brief Vec3 specialization with `uint64`.
using Vec3ul = Vec3<uint64>;


template<typename T>
inline constexpr std::enable_if_t<std::is_signed_v<T>, Vec3<T>> operator - (const Vec3<T>& v) {
	return { -v.x, -v.y, -v.z };
}
template<typename T, typename U>
inline constexpr bool operator == (const Vec3<T>& l, const Vec3<U>& r) {
	return (l.x == r.x) && (l.y == r.y) && (l.z == r.z);
}
template<typename T, typename U>
inline constexpr bool operator != (const Vec3<T>& l, const Vec3<U>& r) {
	return (l.x != r.x) || (l.y != r.y) || (l.z != r.z);
}
template<typename T, typename U>
inline constexpr BVec3 operator <= (const Vec3<T>& l, const Vec3<U>& r) {
	return { l.x <= r.x, l.y <= r.y, l.z <= r.z };
}
template<typename T, typename U>
inline constexpr BVec3 operator < (const Vec3<T>& l, const Vec3<U>& r) {
	return { l.x < r.x, l.y < r.y, l.z < r.z };
}
template<typename T, typename U>
inline constexpr BVec3 operator >= (const Vec3<T>& l, const Vec3<U>& r) {
	return { l.x >= r.x, l.y >= r.y, l.z >= r.z };
}
template<typename T, typename U>
inline constexpr BVec3 operator > (const Vec3<T>& l, const Vec3<U>& r) {
	return { l.x > r.x, l.y > r.y, l.z > r.z };
}
template<typename T, typename U>
inline constexpr auto operator + (const Vec3<T>& l, const Vec3<U>& r) {
	return Vec3<decltype(l.x + r.x)>(l.x + r.x, l.y + r.y, l.z + r.z);
}
template<typename T, typename U>
inline constexpr auto operator - (const Vec3<T>& l, const Vec3<U>& r) {
	return Vec3<decltype(l.x - r.x)>(l.x - r.x, l.y - r.y, l.z - r.z);
}
template<typename T, typename U>
inline constexpr auto operator * (const Vec3<T>& l, const Vec3<U>& r) {
	return Vec3<decltype(l.x* r.x)>(l.x * r.x, l.y * r.y, l.z * r.z);
}
template<typename T, typename U>
inline constexpr auto operator / (const Vec3<T>& l, const Vec3<U>& r) {
	return Vec3<decltype(l.x / r.x)>(l.x / r.x, l.y / r.y, l.z / r.z);
}
template<typename T, typename U>
inline constexpr auto operator + (const Vec3<T>& l, const U& r) {
	return Vec3<decltype(l.x + r)>(l.x + r, l.y + r, l.z + r);
}
template<typename T, typename U>
inline constexpr auto operator - (const Vec3<T>& l, const U& r) {
	return Vec3<decltype(l.x - r)>(l.x - r, l.y - r, l.z - r);
}
template<typename T, typename U>
inline constexpr auto operator * (const Vec3<T>& l, const U& r) {
	return Vec3<decltype(l.x* r)>(l.x * r, l.y * r, l.z * r);
}
template<typename T, typename U>
inline constexpr auto operator * (const T& l, const Vec3<U>& r) {
	return Vec3<decltype(l* r.x)>(l * r.x, l * r.y, l * r.z);
}
template<typename T, typename U>
inline constexpr auto operator / (const Vec3<T>& l, const U& r) {
	return Vec3<decltype(l.x / r)>(l.x / r, l.y / r, l.z / r);
}
template<typename T, typename U>
inline constexpr auto operator / (const T& l, const Vec3<U>& r) {
	return Vec3<decltype(l / r.x)>(l / r.x, l / r.y, l / r.z);
}

} // namespace vega
