/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Math.hpp Types and functions for common mathematics.

#include "../Common.hpp"

#include <cmath>
#include <limits>


namespace vega
{

template<typename> struct Rad; template<typename> struct Deg;

//! @brief Represents an angle measurement in radians.
template<typename T = float>
struct Rad final
{
	static_assert(std::is_floating_point_v<T>, "Rad<T> must have a floating point type argument");

public:
	//! @brief Initialize a value of zero radians.
	constexpr Rad() noexcept : value_{ T(0) } { }
	//! @brief Initialize a value with the given number of radians.
	constexpr Rad(T value) noexcept : value_{ value } { }
	//! @brief Initialize a value with a conversion from the given degrees value.
	constexpr Rad(const Deg<T>& deg) noexcept;

	//! @brief The raw number of radians.
	inline constexpr T value() const { return value_; }
	//! @brief The raw number of radians.
	inline explicit constexpr operator T () const { return value_; }

	inline constexpr Rad<T> operator + (Rad<T> r) const { return { value_ + r.value_ }; }
	inline constexpr Rad<T> operator - (Rad<T> r) const { return { value_ - r.value_ }; }
	inline Rad<T>& operator += (Rad<T> r) { value_ += r.value_; return *this; }
	inline Rad<T>& operator -= (Rad<T> r) { value_ -= r.value_; return *this; }
	inline constexpr Rad<T> operator * (T r) const { return { value_ * r }; }
	inline constexpr Rad<T> operator / (T r) const { return { value_ / r }; }
	inline Rad<T>& operator *= (T r) { value_ *= r; return *this; }
	inline Rad<T>& operator /= (T r) { value_ /= r; return *this; }
	inline constexpr Rad<T> operator - () const { return { -value_ }; }

	inline constexpr bool operator == (Rad<T> r) const { return value_ == r.value_; }
	inline constexpr bool operator != (Rad<T> r) const { return value_ != r.value_; }
	inline constexpr bool operator <= (Rad<T> r) const { return value_ <= r.value_; }
	inline constexpr bool operator <  (Rad<T> r) const { return value_ < r.value_; }
	inline constexpr bool operator >= (Rad<T> r) const { return value_ >= r.value_; }
	inline constexpr bool operator >  (Rad<T> r) const { return value_ > r.value_; }

private:
	T value_;
}; // class Rad<T>


//! @brief Represents an angle measurement in degrees.
template<typename T = float>
struct Deg final
{
	static_assert(std::is_floating_point_v<T>, "Deg<T> must have a floating point type argument");

public:
	//! @brief Initialize a value of zero degrees.
	constexpr Deg() noexcept : value_{ T(0) } { }
	//! @brief Initialize a value with the given number of degrees.
	constexpr Deg(T value) noexcept : value_{ value } { }
	//! @brief Initialize a value with a conversion from the given radians value.
	constexpr Deg(const Rad<T>& rad) noexcept;

	//! @brief The raw number of degrees.
	inline constexpr T value() const { return value_; }
	//! @brief The raw number of degrees.
	inline explicit constexpr operator T () const { return value_; }

	inline constexpr Deg<T> operator + (Deg<T> r) const { return { value_ + r.value_ }; }
	inline constexpr Deg<T> operator - (Deg<T> r) const { return { value_ - r.value_ }; }
	inline Deg<T>& operator += (Deg<T> r) { value_ += r.value_; return *this; }
	inline Deg<T>& operator -= (Deg<T> r) { value_ -= r.value_; return *this; }
	inline constexpr Deg<T> operator * (T r) const { return { value_ * r }; }
	inline constexpr Deg<T> operator / (T r) const { return { value_ / r }; }
	inline Deg<T>& operator *= (T r) { value_ *= r; return *this; }
	inline Deg<T>& operator /= (T r) { value_ /= r; return *this; }
	inline constexpr Deg<T> operator - () const { return { -value_ }; }

	inline constexpr bool operator == (Deg<T> r) const { return value_ == r.value_; }
	inline constexpr bool operator != (Deg<T> r) const { return value_ != r.value_; }
	inline constexpr bool operator <= (Deg<T> r) const { return value_ <= r.value_; }
	inline constexpr bool operator <  (Deg<T> r) const { return value_ < r.value_; }
	inline constexpr bool operator >= (Deg<T> r) const { return value_ >= r.value_; }
	inline constexpr bool operator >  (Deg<T> r) const { return value_ > r.value_; }

private:
	T value_;
}; // class Deg<T>


//! @brief Common mathematical operations and constants.
namespace math
{

//! @brief Multiplicitive constant for converting a degree angle to a radian angle.
template<typename T = float>
constexpr T deg_2_rad{ T(0.0174532925199432957l) };

//! @brief Multiplicitive constant for converting a radian angle to a degree angle.
template<typename T = float>
constexpr T rad_2_deg{ T(57.295779513082320876l) };

//! @brief The absolute value limit for degree measurements before entering degenerate angles.
template<typename T = float>
constexpr T deg_lim{ T(180.0l) };

//! @brief The absolute value limit for radian measurements before entering degenerate angles.
template<typename T = float>
constexpr T rad_lim{ T(3.1415926535897932384l) };

//! @brief Mathematical constant pi.
template<typename T = float>
constexpr T pi{ T(3.1415926535897932384l) };

//! @brief Mathematical constant pi divided by two.
template<typename T = float>
constexpr T pi_o2{ T(1.5707963267948966192l) };

//! @brief Mathematical constant pi divided by three.
template<typename T = float>
constexpr T pi_o3{ T(1.047197551196597746l) };

//! @brief Mathematical constant pi divided by four.
template<typename T = float>
constexpr T pi_o4{ T(0.7853981633974483096l) };

//! @brief Mathematical constant pi times two.
template<typename T = float>
constexpr T two_pi{ T(6.2831853071795864769l) };

//! @brief Mathematical constant pi inverted.
template<typename T = float>
constexpr T inv_pi{ T(0.3183098861837906715l) };

//! @brief Mathematical constant e - Euler's Number.
template<typename T = float>
constexpr T e{ T(2.7182818284590452353l) };

//! @brief Base-2 logarithm of Euler's Number e.
template<typename T = float>
constexpr T log2_e{ T(1.4426950408889634073) };

//! @brief Base-10 logarithm of Euler's Number e.
template<typename T = float>
constexpr T log10_e{ T(0.4342944819032518276l) };

//! @brief Natural logarithm of 2. 
template<typename T = float>
constexpr T ln2{ T(0.6931471805599453094l) };

//! @brief Natural logarithm of 10.
template<typename T = float>
constexpr T ln10{ T(2.3025850929940456840l) };

//! @brief Square root of 2. 
template<typename T = float>
constexpr T sqrt2{ T(1.4142135623730950488l) };

//! @brief Square root of 3. 
template<typename T = float>
constexpr T sqrt3{ T(1.7320508075688772935l) };

//! @brief Mathematical constant phi - Golden Ratio.
template<typename T = float>
constexpr T phi{ T(1.6180339887498948482l) };

//! @brief Representation of positive infinity.
template<typename T = float>
constexpr T inf{ T(std::numeric_limits<T>::infinity()) };

//! @brief Representation of a quiet (non-signalling) NaN.
template<typename T = float>
constexpr T nan{ T(std::numeric_limits<T>::quiet_NaN()) };

//! @brief Value of the floating point epsilon.
template<typename T = float>
constexpr T eps{ T(std::numeric_limits<T>::epsilon()) };


//! @brief Calculates the sine of the given angle.
template<typename T>
inline T sin(Rad<T> rad) { return T(std::sin(rad.value())); }

//! @brief Calculates the cosine of the given angle.
template<typename T>
inline T cos(Rad<T> rad) { return T(std::cos(rad.value())); }

//! @brief Calculates the tangent of the given angle.
template<typename T>
inline T tan(Rad<T> rad) { return T(std::tan(rad.value())); }

//! @brief Calculates the arcsine of the given value.
template<typename T>
inline Rad<T> asin(T value) { return { T(std::asin(value)) }; }

//! @brief Calculates the arccosine of the given value.
template<typename T>
inline Rad<T> acos(T value) { return { T(std::acos(value)) }; }

//! @brief Calculates the arctangent of the given value.
template<typename T>
inline Rad<T> atan(T value) { return { T(std::atan(value)) }; }

//! @brief Calculates the arctangent of the given y and x values.
template<typename T>
inline Rad<T> atan2(T y, T x) { return { T(std::atan2(y, x)) }; }

//! @brief Calculates the hyperbolic sine of the given angle.
template<typename T>
inline T sinh(Rad<T> rad) { return T(std::sinh(rad.value())); }

//! @brief Calculates the hyperbolic cosine of the given angle.
template<typename T>
inline T cosh(Rad<T> rad) { return T(std::cosh(rad.value())); }

//! @brief Calculates the hyperbolic tangent of the given angle.
template<typename T>
inline T tanh(Rad<T> rad) { return T(std::tanh(rad.value())); }

//! @brief Calculates the inverse hyperbolic sine of the given value.
template<typename T>
inline Rad<T> asinh(T value) { return { T(std::asinh(value)) }; }

//! @brief Calculates the inverse hyperbolic cosine of the given value.
template<typename T>
inline Rad<T> acosh(T value) { return { T(std::acosh(value)) }; }

//! @brief Calculates the inverse hyperbolic tangent of the given value.
template<typename T>
inline Rad<T> atanh(T value) { return { T(std::atanh(value)) }; }

//! @brief Calculates the angular modulo to wrap the angle within (-@ref rad_lim<T>, @ref rad_lim<T>).
template<typename T>
inline Rad<T> angle_wrap(Rad<T> rad) {
	return
		(rad.value() < -rad_lim<T>) ? Rad<T>{ T(std::fmod(rad.value(), rad_lim<T>)) + rad_lim<T> } :
		(rad.value() > rad_lim<T>) ? Rad<T>{ T(std::fmod(rad.value(), rad_lim<T>)) - rad_lim<T> } :
		rad;
}

//! @brief Calculates the angular modulo to wrap the angle within (-@ref deg_lim<T>, @ref deg_lim<T>).
template<typename T>
inline Deg<T> angle_wrap(Deg<T> deg) {
	return
		(deg.value() < -deg_lim<T>) ? Deg<T>{ T(std::fmod(deg.value(), deg_lim<T>)) + deg_lim<T> } :
		(deg.value() > deg_lim<T>) ? Deg<T>{ T(std::fmod(deg.value(), deg_lim<T>)) - deg_lim<T> } :
		deg;
}


//! @brief Calculates the absolute (non-negative) value.
template<typename T>
inline constexpr T abs(const T& v) { return (v < T(0)) ? -v : v; }

//! @brief Calculates the minimum of the values.
template<typename T, typename U>
inline constexpr auto min(const T& v1, const U& v2) { return (v1 <= v2) ? v1 : v2; }

//! @brief Calculates the minimum of the values.
template<typename T, typename U, typename V>
inline constexpr auto min(const T& v1, const U& v2, const V& v3) { 
	return (v1 <= v2) ? ((v1 <= v3) ? v1 : v3) : ((v2 <= v3) ? v2 : v3);
}

//! @brief Calculates the maximum of the values.
template<typename T, typename U>
inline constexpr auto max(const T& v1, const U& v2) { return (v1 >= v2) ? v1 : v2; }

//! @brief Calculates the maximum of the values.
template<typename T, typename U, typename V>
inline constexpr auto max(const T& v1, const U& v2, const V& v3) {
	return (v1 >= v2) ? ((v1 >= v3) ? v1 : v3) : ((v2 >= v3) ? v2 : v3);
}

//! @brief Clamps the value into the range provided by @p min and @p max.
template<typename T>
inline constexpr T clamp(const T& val, const T& min, const T& max) {
	return (max < val) ? max : (min > val) ? min : val;
}


//! @brief Calculates the absolute difference of the two values without type conversions or underflows.
template<typename T>
inline constexpr T abs_diff(const T& l, const T& r) {
	return (l < r) ? (r - l) : (r < l) ? (l - r) : T(0);
}

//! @brief Calculates if two values are nearly equal within a relative error term.
template<typename T>
inline constexpr bool close(const T& l, const T& r, const T& epsilon = eps<T>) {
	const T diff{ abs_diff(l, r) };
	return diff <= (l * eps<T>);
}


//! @brief Performs a bitwise rotation (pivot).
//! @param value The value to rotate.
//! @param pivot The bit index for the pivot, results undefined if not in [0, 31].
inline constexpr uint32 rot32(uint32 value, uint32 pivot) {
	return ((value << pivot) | (value >> (32 - pivot)));
}

//! @brief Performs a bitwise rotation (pivot).
//! @param value The value to rotate.
//! @param pivot The bit index for the pivot, results undefined if not in [0, 63].
inline constexpr uint64 rot64(uint64 value, uint64 pivot) {
	return ((value << pivot) | (value >> (64 - pivot)));
}

//! @brief General-use bit avalanche function (uses MurmurHash3 constants).
inline constexpr uint32 avalanche32(uint32 value) {
	value ^= (value >> 16); value *= 0xCC9E2D51;
	value ^= (value >> 13); value *= 0x1B873593;
	return value ^ (value >> 16);
}

//! @brief General-use bit avalanche function (uses xxHash64 constants).
inline constexpr uint64 avalanche64(uint64 value) {
	value ^= (value >> 33); value *= 0xC2B2AE3D27D4EB4Full;
	value ^= (value >> 29); value *= 0x165667B19E3779F9ull;
	return value ^ (value >> 32);
}


//! @brief Checks if the two types are the same arithmetic class (both floating point, or both integral).
template<typename T, typename U>
constexpr bool is_same_arithmetic_class =
	std::is_arithmetic_v<T> && std::is_arithmetic_v<U> && (std::is_floating_point_v<T> == std::is_floating_point_v<U>);

//! @brief Selects the wider of the two numeric types, which must both belong to the same arithmetic class.
template<typename T, typename U>
using widest = std::enable_if_t<
	is_same_arithmetic_class<T, U>,
	std::conditional_t<std::numeric_limits<U>::digits >= std::numeric_limits<T>::digits, U, T>
>;

} // namespace math


template<typename T>
constexpr Rad<T>::Rad(const Deg<T>& deg) noexcept : value_{ deg.value() * math::deg_2_rad<T> } { }

template<typename T>
constexpr Deg<T>::Deg(const Rad<T>& rad) noexcept : value_{ rad.value() * math::rad_2_deg<T> } { }

} // namespace vega


//! @brief Creates a radian value from a numeric literal.
inline constexpr vega::Rad<float> operator  "" _radf(long double d) { return { float(d) }; }
//! @brief Creates a radian value from a numeric literal.
inline constexpr vega::Rad<float> operator  "" _radf(unsigned long long d) { return { float(d) }; }
//! @brief Creates a radian value from a numeric literal.
inline constexpr vega::Rad<double> operator "" _radd(long double d) { return { double(d) }; }
//! @brief Creates a radian value from a numeric literal.
inline constexpr vega::Rad<double> operator "" _radd(unsigned long long d) { return { double(d) }; }
//! @brief Creates a degree value from a numeric literal.
inline constexpr vega::Deg<float> operator  "" _degf(long double d) { return { float(d) }; }
//! @brief Creates a degree value from a numeric literal.
inline constexpr vega::Deg<float> operator  "" _degf(unsigned long long d) { return { float(d) }; }
//! @brief Creates a degree value from a numeric literal.
inline constexpr vega::Deg<double> operator "" _degd(long double d) { return { double(d) }; }
//! @brief Creates a degree value from a numeric literal.
inline constexpr vega::Deg<double> operator "" _degd(unsigned long long d) { return { double(d) }; }
