/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Vec2.hpp 2-component vector types.

#include "../Common.hpp"
#include "./Math.hpp"


namespace vega
{

//! @brief 2-component vector of boolean values.
struct VEGA_API BVec2 final
{
public:
	//! @brief Initialize all components to false.
	constexpr BVec2() noexcept : data{ false,false } { }
	//! @brief Initialize all components to the given value.
	explicit constexpr BVec2(bool b) noexcept : data{ b,b } { }
	//! @brief Initialize components with the given values.
	constexpr BVec2(bool x, bool y) noexcept : x{ x }, y{ y } { }

	//! @brief Returns if any component is \c true.
	inline constexpr bool any() const { return x || y; }
	//! @brief Returns if all components are \c true.
	inline constexpr bool all() const { return x && y; }
	//! @brief Returns if no components are \c true.
	inline constexpr bool none() const { return !x && !y; }
	//! @brief Returns the number of components that are \c true.
	inline constexpr uint32 count() const { return uint32(x + y); }

	inline constexpr bool operator == (BVec2 r) const { return x == r.x && y == r.y; }
	inline constexpr bool operator != (BVec2 r) const { return x != r.x || y != r.y; }
	inline bool operator < (BVec2 r) const { return (x + (y << 1)) < (r.x + (r.y << 1)); }
	inline constexpr BVec2 operator & (BVec2 r) const { return { x && r.x, y && r.y }; }
	inline constexpr BVec2 operator | (BVec2 r) const { return { x || r.x, y || r.y }; }
	inline constexpr BVec2 operator ^ (BVec2 r) const { return { x != r.x, y != r.y }; }

public:
	union {
		struct { bool x; bool y; };
		bool data[2];
	};
}; // struct BVec2


//! @brief 2-component vector of numeric values.
//! @tparam T The numeric type of the vector components.
template<typename T>
struct Vec2 final
{
public:
	constexpr Vec2() : x{ }, y{ } { }
	explicit constexpr Vec2(const T& v) noexcept
		: x{ v }, y{ v }
	{ }
	template<typename U, typename V>
	constexpr Vec2(const U& x, const V& y) noexcept
		: x{ static_cast<T>(x) }, y{ static_cast<T>(y) }
	{ }
	template<typename U>
	explicit constexpr Vec2(const Vec2<U>& o) noexcept
		: x{ static_cast<T>(o.x) }, y{ static_cast<T>(o.y) }
	{ }

	constexpr const T& operator [] (size_t index) const { return data[index]; }
	constexpr T& operator [] (size_t index) { return data[index]; }

	//! @brief Converts the components of the vector into a different type.
	//! @tparam U The type to convert the components into.
	template<typename U>
	inline constexpr Vec2<U> as() const { return Vec2<U>(x, y); }

	//! @brief Gets the cartesian length of the vector.
	inline constexpr auto length() const { return std::sqrt((x * x) + (y * y)); }
	//! @brief Gets the square of the cartesian length of the vector.
	inline constexpr auto lengthSq() const { return (x * x) + (y * y); }
	//! @brief Gets the normalized (length 1) vector in the same direction as the vector.
	inline constexpr auto normalized() const {
		auto ilen = 1 / length();
		return Vec2<decltype(ilen)>(x * ilen, y * ilen);
	}

	//! @brief Gets the cartesian distance to the end of the other vector.
	template<typename U>
	constexpr auto distance(const Vec2<U>& o) const {
		auto dx = o.x - x, dy = o.y - y;
		return std::sqrt((dx * dx) + (dy * dy));
	}
	//! @brief Gets the square of the cartesian distance to the end of the other vector.
	template<typename U>
	constexpr auto distanceSq(const Vec2<U>& o) const {
		auto dx = o.x - x, dy = o.y - y;
		return (dx * dx) + (dy * dy);
	}

	//! @brief Gets the dot product with the other vector.
	template<typename U>
	inline constexpr auto dot(const Vec2<U>& o) const {
		return (o.x * x) + (o.y * y);
	}
	//! @brief Gets this vector projected onto the other vector.
	template<typename U>
	inline constexpr auto project(const Vec2<U>& o) const {
		auto a = dot(o);
		return Vec2<decltype(a)>(o.x * a, o.y * a);
	}
	//! @brief Gets this vector around the other vector.
	template<typename U>
	inline constexpr auto reflect(const Vec2<U>& n) const {
		auto f = 2 * dot(n);
		return Vec2<decltype(f)>(x - (n.x * f), y - (n.y * f));
	}
	//! @brief Refracts this vector past the given normal vector and index of refraction.
	//! @param eta The index of reflection.
	template<typename U>
	inline constexpr auto refract(const Vec2<U>& n, float eta) const {
		auto d = dot(n);
		auto k = 1 - (eta * eta * (1 - (d * d)));
		return (k < 0) ? Zero : ((*this * eta) - (n * ((eta * d) + std::sqrt(k))));
	}
	//! @brief Gets the angle between this vector and the other.
	template<typename U>
	inline constexpr auto angleWith(const Vec2<U>& o) const {
		auto d = dot(o) / (length() * o.length());
		return Rad<decltype(d)>(std::acos(d));
	}

	//! @brief Gets the component-wise minimum of the two vectors.
	template<typename U>
	inline constexpr auto min(const Vec2<U>& o) const {
		auto mx = math::min(x, o.x), my = math::min(y, o.y);
		return Vec2<decltype(mx)>(mx, my);
	}
	//! @brief Gets the component-wise maximum of the two vectors.
	template<typename U>
	inline constexpr auto max(const Vec2<U>& o) const {
		auto mx = math::max(x, o.x), my = math::max(y, o.y);
		return Vec2<decltype(mx)>(mx, my);
	}
	//! @brief Gets the component-wise clamp of this vector and the min and max vectors.
	template<typename U, typename V>
	inline constexpr auto clamp(const Vec2<U>& min, const Vec2<V>& max) const {
		auto cx = math::clamp(x, min.x, max.x), cy = math::clamp(y, min.y, max.y);
		return Vec2<decltype(cx)>(cx, cy);
	}

	//! @brief Performs a component-wise equality check between the two vectors.
	template<typename U>
	inline constexpr BVec2 compEq(const Vec2<U>& o) const {
		return { x == o.x, y == o.y };
	}
	//! @brief Performs a component-wise inequality check between the two vectors.
	template<typename U>
	inline constexpr BVec2 compNeq(const Vec2<U>& o) const {
		return { x != o.x, y != o.y };
	}

	template<typename U>
	Vec2& operator += (const Vec2<U>& v) {
		x = static_cast<T>(x + v.x); y = static_cast<T>(y + v.y);
		return *this;
	}
	template<typename U>
	Vec2& operator -= (const Vec2<U>& v) {
		x = static_cast<T>(x - v.x); y = static_cast<T>(y - v.y);
		return *this;
	}
	template<typename U>
	Vec2& operator *= (const Vec2<U>& v) {
		x = static_cast<T>(x * v.x); y = static_cast<T>(y * v.y);
		return *this;
	}
	template<typename U>
	Vec2& operator /= (const Vec2<U>& v) {
		x = static_cast<T>(x / v.x); y = static_cast<T>(y / v.y);
		return *this;
	}
	template<typename U>
	Vec2& operator += (const U& v) {
		x = static_cast<T>(x + v); y = static_cast<T>(y + v);
		return *this;
	}
	template<typename U>
	Vec2& operator -= (const U& v) {
		x = static_cast<T>(x - v); y = static_cast<T>(y - v);
		return *this;
	}
	template<typename U>
	Vec2& operator *= (const U& v) {
		x = static_cast<T>(x * v); y = static_cast<T>(y * v);
		return *this;
	}
	template<typename U>
	Vec2& operator /= (const U& v) {
		x = static_cast<T>(x / v); y = static_cast<T>(y / v);
		return *this;
	}

public:
	union {
		struct { T x; T y; };
		T data[2];
	};

	//! @brief Constant vector with components (0, 0).
	static const Vec2 Zero;
	//! @brief Constant vector with components (1, 1).
	static const Vec2 One;
	//! @brief Constant vector with components (1, 0).
	static const Vec2 UnitX;
	//! @brief Constant vector with components (0, 1).
	static const Vec2 UnitY;
}; // struct Vec2

template<typename T> constexpr Vec2<T> Vec2<T>::Zero { 0, 0 };
template<typename T> constexpr Vec2<T> Vec2<T>::One  { 1, 1 };
template<typename T> constexpr Vec2<T> Vec2<T>::UnitX{ 1, 0 };
template<typename T> constexpr Vec2<T> Vec2<T>::UnitY{ 0, 1 };

//! @brief Vec2 specialization with `float`.
using Vec2f = Vec2<float>;
//! @brief Vec2 specialization with `double`.
using Vec2d = Vec2<double>;
//! @brief Vec2 specialization with `int8`.
using Vec2b = Vec2<int8>;
//! @brief Vec2 specialization with `uint8`.
using Vec2ub = Vec2<uint8>;
//! @brief Vec2 specialization with `int16`.
using Vec2s = Vec2<int16>;
//! @brief Vec2 specialization with `uint16`.
using Vec2us = Vec2<uint16>;
//! @brief Vec2 specialization with `int32`.
using Vec2i = Vec2<int32>;
//! @brief Vec2 specialization with `uint32`.
using Vec2ui = Vec2<uint32>;
//! @brief Vec2 specialization with `int64`.
using Vec2l = Vec2<int64>;
//! @brief Vec2 specialization with `uint64`.
using Vec2ul = Vec2<uint64>;


template<typename T>
inline constexpr std::enable_if_t<std::is_signed_v<T>, Vec2<T>> operator - (const Vec2<T>& v) {
	return { -v.x, -v.y };
}
template<typename T, typename U>
inline constexpr bool operator == (const Vec2<T>& l, const Vec2<U>& r) {
	return (l.x == r.x) && (l.y == r.y);
}
template<typename T, typename U>
inline constexpr bool operator != (const Vec2<T>& l, const Vec2<U>& r) {
	return (l.x != r.x) || (l.y != r.y);
}
template<typename T, typename U>
inline constexpr BVec2 operator <= (const Vec2<T>& l, const Vec2<U>& r) {
	return { l.x <= r.x, l.y <= r.y };
}
template<typename T, typename U>
inline constexpr BVec2 operator < (const Vec2<T>& l, const Vec2<U>& r) {
	return { l.x < r.x, l.y < r.y };
}
template<typename T, typename U>
inline constexpr BVec2 operator >= (const Vec2<T>& l, const Vec2<U>& r) {
	return { l.x >= r.x, l.y >= r.y };
}
template<typename T, typename U>
inline constexpr BVec2 operator > (const Vec2<T>& l, const Vec2<U>& r) {
	return { l.x > r.x, l.y > r.y };
}
template<typename T, typename U>
inline constexpr auto operator + (const Vec2<T>& l, const Vec2<U>& r) {
	return Vec2<decltype(l.x + r.x)>(l.x + r.x, l.y + r.y);
}
template<typename T, typename U>
inline constexpr auto operator - (const Vec2<T>& l, const Vec2<U>& r) {
	return Vec2<decltype(l.x - r.x)>(l.x - r.x, l.y - r.y);
}
template<typename T, typename U>
inline constexpr auto operator * (const Vec2<T>& l, const Vec2<U>& r) {
	return Vec2<decltype(l.x* r.x)>(l.x * r.x, l.y * r.y);
}
template<typename T, typename U>
inline constexpr auto operator / (const Vec2<T>& l, const Vec2<U>& r) {
	return Vec2<decltype(l.x / r.x)>(l.x / r.x, l.y / r.y);
}
template<typename T, typename U>
inline constexpr auto operator + (const Vec2<T>& l, const U& r) {
	return Vec2<decltype(l.x + r)>(l.x + r, l.y + r);
}
template<typename T, typename U>
inline constexpr auto operator - (const Vec2<T>& l, const U& r) {
	return Vec2<decltype(l.x - r)>(l.x - r, l.y - r);
}
template<typename T, typename U>
inline constexpr auto operator * (const Vec2<T>& l, const U& r) {
	return Vec2<decltype(l.x* r)>(l.x * r, l.y * r);
}
template<typename T, typename U>
inline constexpr auto operator * (const T& l, const Vec2<U>& r) {
	return Vec2<decltype(l* r.x)>(l * r.x, l * r.y);
}
template<typename T, typename U>
inline constexpr auto operator / (const Vec2<T>& l, const U& r) {
	return Vec2<decltype(l.x / r)>(l.x / r, l.y / r);
}
template<typename T, typename U>
inline constexpr auto operator / (const T& l, const Vec2<U>& r) {
	return Vec2<decltype(l / r.x)>(l / r.x, l / r.y);
}

} // namespace vega
