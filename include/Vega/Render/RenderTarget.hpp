/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file RenderTarget.hpp Results from Renderer operations.

#include "../Common.hpp"


namespace vega
{

class Renderer;

//! @brief Contains a reference to the render results for a specific target in a Renderer instance.
//! @remarks A RenderTarget will use the render results that are ACTIVE at the time of use. The ACTIVE result is the
//! result that is currently being recorded for, or the most recent result to start processing if the source Renderer
//! is not actively recording. This means that, in order to use the most up-to-date RenderTarget data, the RenderTarget
//! must be used after the source Renderer has started recording.
class VEGA_API RenderTarget
{
	friend class RenderTargetImpl;

public:
	virtual ~RenderTarget();

	//! @brief The Renderer instance that the target belongs to.
	const Renderer* renderer() const;
	//! @brief The target index within the parent Renderer.
	uint32 target() const;

private:
	RenderTarget();

	VEGA_NO_COPY(RenderTarget)
	VEGA_NO_MOVE(RenderTarget)
}; // class RenderTarget

} // namespace vega
