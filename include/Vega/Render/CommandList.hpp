/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file CommandList.hpp Recording of commands to be submitted to Renderer instances.

#include "../Common.hpp"
#include "./Pipeline.hpp"
#include "./Viewport.hpp"
#include "./Sampler.hpp"
#include "./Camera3D.hpp"
#include "./RenderTarget.hpp"
#include "../Graphics/Texture.hpp"
#include "../Graphics/TexelBuffer.hpp"
#include "../Graphics/StorageBuffer.hpp"
#include "../Graphics/VertexBuffer.hpp"
#include "../Graphics/IndexBuffer.hpp"
#include "../Math/Rect2.hpp"


namespace vega
{

class Renderer;

//! @brief Redefinition of a Rect2 to act as a render scissor region.
using Scissor = Rect2<int32, uint32>;


//! @brief Manages rendering state and command recording for Renderer instances.
class VEGA_API CommandList
{
	friend class CommandListImpl;

public:
	//! @brief The maximum size of uniform data, in bytes.
	inline static constexpr uint32 MAX_UNIFORM_SIZE{ 256 };

	virtual ~CommandList();

	//! @brief The Renderer instance that owns this CommandList.
	const Renderer* renderer() const;
	//! @brief The Renderer subpass index that this list is recording commands for.
	uint32 subpass() const;
	//! @brief The application frame index that this list is recording commands for.
	uint64 frame() const;
	//! @brief If the CommandList is actively recording and has not been submitted yet.
	bool isRecording() const;

	//! @brief Binds a new pipeline to be used for future draw commands.
	//! @remarks Many of the other functions in CommandList require that a Pipeline be bound before they are called.
	//! @param pipeline The pipeline to bind and use for draw commands.
	void usePipeline(const Pipeline& pipeline);
	//! @brief Returns if a pipeline has been bound to the CommandList.
	bool hasPipeline() const;

	//! @brief Sets the render viewport.
	void setViewport(const Viewport& viewport);
	//! @brief Sets the render scissor.
	void setScissor(const Scissor& scissor);
	//! @brief Sets the reference constant value for stencil operations.
	//! @param ref The reference value to set.
	//! @param front If the value should be updated for the front-facing fragments.
	//! @param back If the value should be updated for the back-facing fragments.
	void setStencilReference(uint8 ref, bool front = true, bool back = true);

	//! @brief Bind a texture to the given shader resource slot.
	//! @param slot The slot to bind the texture to.
	//! @param texture The texture to bind - must match the type expected by the bound pipeline.
	//! @param sampler The sampler to use for texture reads.
	void bind(uint32 slot, const Texture& texture, Sampler sampler);
	//! @brief Bind a render target to the given shader Sampler2D slot.
	//! @param slot The slot to bind the render target to.
	//! @param target The render target to bind.
	//! @param sampler The sample to use for target reads.
	void bind(uint32 slot, const RenderTarget& target, Sampler sampler);
	//! @brief Bind a texel buffer to the given shader resource slot.
	//! @param slot The slot to bind the buffer to.
	//! @param buffer The buffer to bind - must match the read/write state expected in the shader.
	void bind(uint32 slot, const TexelBuffer& buffer);
	//! @brief Bind a storage buffer to the given shader resource slot.
	//! @param slot The slot to bind the buffer to.
	//! @param buffer The buffer to bind - must match the read/write state expected in the shader.
	void bind(uint32 slot, const StorageBuffer& buffer);

	//! @brief Allocates a new chunk of memory to write uniform data to for draw commands.
	//! @param size The size, in bytes, of the area of memory to allocate. Must be in [1, MAX_UNIFORM_SIZE].
	//! @param zero If true, the allocated space is zeroed out before being returned.
	//! @return A pointer to the uniform memory space.
	void* allocateUniformData(uint32 size, bool zero = false);
	//! @brief Allocates a new area of memory to write uniform data of the given type to for draw commands.
	//! @tparam T The type to write into the uniform memory.
	//! @param zero If true, the allocated space is zeroed out before being returned.
	//! @return A pointer to the uniform type memory.
	template<typename T>
	inline T* allocateUniformData(bool zero = false) {
		static_assert(sizeof(T) != 0, "Uniform data type cannot be an incomplete type");
		static_assert(sizeof(T) <= MAX_UNIFORM_SIZE, "Uniform data size must be between 1 and MAX_UNIFORM_SIZE");
		return reinterpret_cast<T*>(allocateUniformData(uint32(sizeof(T)), zero));
	}
	//! @brief Set the currently bound uniform data to the passed argument.
	//! @tparam T The type of uniform data to set.
	//! @param data The data to set as the active uniform data.
	template<typename T>
	inline void setUniformData(const T& data) {
		static_assert(sizeof(T) != 0, "Uniform data type cannot be an incomplete type");
		static_assert(sizeof(T) <= MAX_UNIFORM_SIZE, "Uniform data size must be between 1 and MAX_UNIFORM_SIZE");
		T* ptr = allocateUniformData<T>();
		*ptr = data;
	}

	//! @brief Updates the camera parameters bound to the shader global data.
	//! @param camera The camera to bind the data from.
	void setCameraParams(const Camera3D& camera);

	//! @brief Update the vertex buffers that are read from for draw commands.
	//! @param buffers The buffers to read vertex data from. Pass in no buffers to reset all bindings.
	//! @param unbindAll If all existing vertex bindings should be cleared before binding the new buffers.
	void bindVertexBuffers(const std::initializer_list<const VertexBuffer*> buffers, bool unbindAll = true);
	//! @brief Resets the given vertex use to unbind and always load zeros.
	//! @param use The vertex use to unbind.
	void unbindVertexUse(VertexUse use);

	//! @brief Submit a draw command using the bound pipeline and resources.
	//! @param vertCount The number of vertices to draw.
	//! @param instCount The number of instances to draw.
	//! @param firstVert The index of the first vertex to draw.
	//! @param firstInst The index of the first instance to draw.
	void draw(uint32 vertCount, uint32 instCount = 1, uint32 firstVert = 0, uint32 firstInst = 0);
	//! @brief Submit an indexed draw command using the given index buffer.
	//! @param buffer The index buffer to draw with.
	//! @param indexCount The number of indexed vertices to draw.
	//! @param instCount The number of instances to draw.
	//! @param firstIndex The index of the first index to draw.
	//! @param vertOffset The offset to apply to the indices loaded from the buffer.
	//! @param firstInst The index of the first instance to draw.
	void drawIndexed(const IndexBuffer& buffer, uint32 indexCount, uint32 instCount = 1, 
		uint32 firstIndex = 0, int32 vertOffset = 0, uint32 firstInst = 0);

private:
	CommandList();

	VEGA_NO_COPY(CommandList)
	VEGA_NO_MOVE(CommandList)
}; // class CommandList

} // namespace vega
