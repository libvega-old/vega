/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Viewport.hpp Description of view-space volumes.

#include "../Common.hpp"
#include <Vega/Math/Math.hpp>


namespace vega
{

//! @brief Describes a region of view-space.
struct VEGA_API Viewport final
{
public:
	constexpr Viewport() : l_{ 0 }, t_{ 0 }, w_{ 0 }, h_{ 0 }, mind_{ 0 }, maxd_{ 0 } { }
	//! @brief Construct a viewport.
	//! @param left The left coordinate of the viewport, in pixels.
	//! @param top The top coordinate of the viewport, in pixels.
	//! @param width The width of the viewport, in pixels.
	//! @param height The height of the viewport, in pixels.
	//! @param minDepth The min depth value, in clip space. Must be in [0, 1].
	//! @param maxDepth The max depth value, in clip space. Must be in [0, 1].
	constexpr Viewport(float left, float top, float width, float height, float minDepth = 0, float maxDepth = 1)
		: l_{ left }, t_{ top }, w_{ width }, h_{ height }, mind_{ math::clamp(minDepth, 0.f, 1.f) }
		, maxd_{ math::clamp(maxDepth, 0.f, 1.f) }
	{ }

	//! @brief The left coordinate of the viewport, in pixels.
	inline constexpr float left() const { return l_; }
	//! @brief The top coordinate of the viewport, in pixels.
	inline constexpr float top() const { return t_; }
	//! @brief The width of the viewport, in pixels.
	inline constexpr float width() const { return w_; }
	//! @brief The height of the viewport, in pixels.
	inline constexpr float height() const { return h_; }
	//! @brief The min depth value in clip space.
	inline constexpr float minDepth() const { return mind_; }
	//! @brief The max depth value in clip space.
	inline constexpr float maxDepth() const { return maxd_; }

	//! @brief The left coordinate of the viewport, in pixels.
	inline void left(float left) { l_ = left; }
	//! @brief The top coordinate of the viewport, in pixels.
	inline void top(float top) { t_ = top; }
	//! @brief The width of the viewport, in pixels.
	inline void width(float width) { w_ = width; }
	//! @brief The height of the viewport, in pixels.
	inline void height(float height) { h_ = height; }
	//! @brief The min depth value in clip space.
	inline void minDepth(float minDepth) { mind_ = minDepth; }
	//! @brief The max depth value in clip space.
	inline void maxDepth(float maxDepth) { maxd_ = maxDepth; }

	inline constexpr bool operator == (const Viewport& r) const {
		return (l_ == r.l_) && (t_ == r.t_) && (w_ == r.w_) && (h_ == r.h_) && (mind_ == r.mind_) && (maxd_ == r.maxd_);
	}
	inline constexpr bool operator != (const Viewport& r) const {
		return (l_ != r.l_) || (t_ != r.t_) || (w_ != r.w_) || (h_ != r.h_) || (mind_ != r.mind_) || (maxd_ != r.maxd_);
	}

private:
	float l_;
	float t_;
	float w_;
	float h_;
	float mind_;
	float maxd_;
}; // class Viewport

} // namespace vega
