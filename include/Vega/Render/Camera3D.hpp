/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Camera3D.hpp Defining and manipulating 3D camera views.

#include "../Common.hpp"
#include "../Math/Frustum.hpp"
#include "../Math/Mat4.hpp"
#include "../Math/Math.hpp"


namespace vega
{

//! @brief Parameters for perspective-style camera projection.
struct PerspectiveParams final
{
public:
	PerspectiveParams() = default;
	PerspectiveParams(Rad<float> fov, float aspect, float near, float far)
		: fov{ fov }, aspect{ aspect }, near{ near }, far{ far }
	{ }

	//! @brief Horizontal field-of-view angle.
	Rad<float> fov{ };
	//! @brief Projection aspect ratio (width / height).
	float aspect{ };
	//! @brief Near view plane distance.
	float near{ };
	//! @brief Far view plane distance.
	float far{ };
}; // struct PerspectiveParams


//! @brief Parameters for centered orthographic projection.
struct OrthographicParams final
{
public:
	OrthographicParams() = default;
	OrthographicParams(float width, float height, float near, float far)
		: width{ width }, height{ height }, near{ near }, far{ far }
	{ }

	//! @brief Width of the projection.
	float width{ };
	//! @brief Height of the projection.
	float height{ };
	//! @brief Near view plane distance.
	float near{ };
	//! @brief Far view plane distance.
	float far{ };
}; // struct OrthographicParams


//! @brief Describes an orientable camera in 3D world space with a configurable projection.
class VEGA_API Camera3D final
{
public:
	//! @brief The set of parameter types for supported projections.
	using ProjectionParams = Var<PerspectiveParams, OrthographicParams>;

	//! @brief Initializes a default camera, looking from (0, 0, 1) to (0, 0, 0) with a 1x1 orthographic projection.
	Camera3D();

	//! @brief The camera view matrix.
	const Mat4f& view() const;
	//! @brief The camera projection matrix.
	const Mat4f& projection() const;
	//! @brief The combined view*projection matrix.
	const Mat4f& viewProjection() const;
	//! @brief The camera view bounding volume.
	const Frustum& frustum() const;

	//! @brief Get the camera view position.
	inline const Vec3f& position() const { return view_.position; }
	//! @brief Set the camera view position.
	inline void position(const Vec3f& pos) { view_.position = pos; view_.dirty = true; viewProj_.dirty = true; }
	//! @brief Get the camera target position.
	inline const Vec3f& target() const { return view_.target; }
	//! @brief Set the camera target position.
	inline void target(const Vec3f& pos) { view_.target = pos; view_.dirty = true; viewProj_.dirty = true; }
	//! @brief Get the camera roll, in radians. Positive roll corresponds to a counter-clockwise rotation in view space.
	inline Rad<float> roll() const { return view_.roll; }
	//! @brief Set the camera roll, in radians. Positive roll corresponds to a counter-clockwise rotation in view space.
	inline void roll(Rad<float> roll) { view_.roll = roll; view_.dirty = true; viewProj_.dirty = true; }

	//! @brief Gets if the camera is currently using a perspecive projection.
	inline bool isPerspective() const { return std::holds_alternative<PerspectiveParams>(proj_.params); }
	//! @brief Gets if the camera is currently using an orthographic projection.
	inline bool isOrthographic() const { return std::holds_alternative<OrthographicParams>(proj_.params); }
	//! @brief Get the current projection parameters.
	inline const ProjectionParams& params() const { return proj_.params; }
	//! @brief Set the current projection parameters.
	inline void params(const ProjectionParams& params) { 
		proj_.params = params; proj_.dirty = true; viewProj_.dirty = true;
	}
	//! @brief Returns the [near, far] depth values
	std::tuple<float, float> depth() const;

	//! @brief Unit vector pointing camera right.
	inline Vec3f right() const { return view().right(); }
	//! @brief Unit vector pointing camera up.
	inline Vec3f up() const { return view().up(); }
	//! @brief Unit vector pointing camera forward.
	inline Vec3f forward() const { return view().forward(); }

private:
	struct
	{
		Vec3f position{ };
		Vec3f target{ };
		Rad<float> roll{ };
		mutable Mat4f matrix{ };
		mutable bool dirty{ };
	} view_;
	struct
	{
		ProjectionParams params{ OrthographicParams{ } };
		mutable Mat4f matrix{ };
		mutable bool dirty{ };
	} proj_;
	mutable struct
	{
		vega::Frustum frustum{ };
		Mat4f matrix{ };
		bool dirty{ };
	} viewProj_;

	VEGA_DEFAULT_MOVE(Camera3D)
	VEGA_DEFAULT_COPY(Camera3D)
}; // class Camera3D


//! @brief Base type for controllers that implement camera functionality over a Camera3D instance.
class VEGA_API Camera3DController
{
public:
	virtual ~Camera3DController() { }

	//! @brief The camera instance managed by the controller.
	inline const Camera3D& camera() const { return camera_; }

	//! @brief Set the projection parameters of the underlying camera.
	inline void params(const Camera3D::ProjectionParams& params) {
		mutCamera().params(params);
	}

protected:
	//! @brief Initialize a controller with a default camera setup.
	Camera3DController() : camera_{ } { }

	//! @brief Mutable reference to the managed camera for child classes.
	inline Camera3D& mutCamera() { return camera_; }

private:
	Camera3D camera_;
}; // class CameraController


//! @brief Camera controller implementing an arc-ball camera type.
class VEGA_API ArcBallController final
	: public Camera3DController
{
	struct Params { Rad<float> altitude; Rad<float> azimuth; Rad<float> roll; float dist; };

public:
	//! @brief Initializes a new controller with the given target.
	//! @param target The initial target position for the camera.
	explicit ArcBallController(const Vec3f& target = Vec3f::Zero);

	//! @brief Get the camera look-at target.
	inline const Vec3f& target() const { return camera().target(); }
	//! @brief Set the camera look-at target.
	inline void target(const Vec3f& target) { mutCamera().target(target); }
	//! @brief Get the camera altitude angle in radians (above/below XZ-plane).
	inline Rad<float> altitude() const { return params_.altitude; }
	//! @brief Set the camera altitude angle in radians (above/below XZ-plane).
	inline void altitude(Rad<float> alt) {
		params_.altitude = math::clamp(alt, min_.altitude, max_.altitude); updateCamera();
	}
	//! @brief Get the camera azimuth angle in radians (around the Y-axis).
	inline Rad<float> azimuth() const { return params_.azimuth; }
	//! @brief Set the camera azimuth angle in radians (around the Y-axis).
	inline void azimuth(Rad<float> az) {
		params_.azimuth = math::clamp(az, min_.azimuth, max_.azimuth); updateCamera();
	}
	//! @brief Get the camera distance to target.
	inline float distance() const { return params_.dist; }
	//! @brief Set the camera distance to target.
	inline void distance(float dist) {
		params_.dist = math::clamp(dist, min_.dist, max_.dist); updateCamera();
	}
	//! @brief Get the camera roll.
	inline Rad<float> roll() const { return camera().roll(); }
	//! @brief Set the camera roll.
	inline void roll(Rad<float> roll) { mutCamera().roll(roll); }

	//! @brief Gets the [min, max] range of allowable altitude angles.
	inline std::tuple<Rad<float>, Rad<float>> altitudeLimits() const { return { min_.altitude, max_.altitude }; }
	//! @brief Sets the range of allowable altitude angles, \c std::nullopt will keep the value the same.
	inline void altitudeLimits(Opt<Rad<float>> min, Opt<Rad<float>> max) {
		min_.altitude = min.value_or(min_.altitude);
		max_.altitude = max.value_or(max_.altitude);
		altitude(params_.altitude);
	}
	//! @brief Gets the [min, max] range of allowable azimuth angles.
	inline std::tuple<Rad<float>, Rad<float>> azimuthLimits() const { return { min_.azimuth, max_.azimuth }; }
	//! @brief Sets the range of allowable azimuth angles, \c std::nullopt will keep the value the same.
	inline void azimuthLimits(Opt<Rad<float>> min, Opt<Rad<float>> max) {
		min_.azimuth = min.value_or(min_.azimuth);
		max_.azimuth = max.value_or(max_.azimuth);
		azimuth(params_.azimuth);
	}
	//! @brief Gets the [min, max] range of allowable distances.
	inline std::tuple<float, float> distanceLimits() const { return { min_.dist, max_.dist }; }
	//! @brief Sets the range of allowable distances, \c std::nullopt will keep the value the same.
	inline void distanceLimits(Opt<float> min, Opt<float> max) {
		min_.dist = min.value_or(min_.dist);
		max_.dist = max.value_or(max_.dist);
		distance(params_.dist);
	}

private:
	void updateCamera();

private:
	Params params_;
	Params min_;
	Params max_;
}; // class ArcBallController


//! @brief Camera controller implementing an FPS camera type.
class VEGA_API FPSController final
	: public Camera3DController
{
public:
	//! @brief The absolute value default pitch limit (89.5 degrees).
	inline static constexpr Rad<float> PITCH_LIMIT{ 89.5f * math::deg_2_rad<float> };

	//! @brief Initialize a new FPS controller with the given yaw and pitch.
	explicit FPSController(Rad<float> yaw = 0, Rad<float> pitch = 0);

	//! @brief Get the camera position.
	inline const Vec3f& position() const { return camera().position(); }
	//! @brief Set the camera position.
	inline void position(const Vec3f& pos) { mutCamera().position(pos); }
	//! @brief Get the camera roll.
	inline Rad<float> roll() const { return camera().roll(); }
	//! @brief Set the camera roll.
	inline void roll(Rad<float> roll) { mutCamera().roll(roll); }
	//! @brief Get the camera pitch in radians, with positive pitch up.
	inline Rad<float> pitch() const { return pitch_.value; }
	//! @brief Set the camera pitch in radians, with positive pitch up.
	inline void pitch(Rad<float> pitch) { pitch_.value = math::clamp(pitch, pitch_.min, pitch_.max); updateCamera(); }
	//! @brief Get the camera yaw in radians.
	inline Rad<float> yaw() const { return yaw_; }
	//! @brief Set the camera yaw in radians.
	inline void yaw(Rad<float> yaw) { yaw_ = math::angle_wrap(yaw); updateCamera(); }

	//! @brief Gets the [min, max] range of allowable pitch angles.
	inline std::tuple<Rad<float>, Rad<float>> pitchLimits() const { return { pitch_.min, pitch_.max }; }
	//! @brief Sets the range of allowable distances, \c std::nullopt will keep the value the same.
	inline void pitchLimits(Opt<Rad<float>> min, Opt<Rad<float>> max) {
		pitch_.min = std::max(min.value_or(pitch_.min), -PITCH_LIMIT);
		pitch_.max = std::min(max.value_or(pitch_.max), PITCH_LIMIT);
		pitch(pitch_.value);
	}

	//! @brief Right direction vector for camera.
	inline Vec3f right() const { return camera().right(); }
	//! @brief Up direction vector for camera.
	inline Vec3f up() const { return camera().up(); }
	//! @brief Forward direction vector for camera.
	inline Vec3f forward() const { return camera().forward(); }

private:
	void updateCamera();

private:
	struct
	{
		Rad<float> value{ };
		Rad<float> min{ };
		Rad<float> max{ };
	} pitch_;
	Rad<float> yaw_;
}; // class FPSController

} // namespace vega
