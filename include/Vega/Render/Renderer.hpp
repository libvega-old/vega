/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Renderer.hpp Core functionality for recording and submitting rendering tasks and using the results.

#include "../Common.hpp"
#include "./RenderLayout.hpp"
#include "./Viewport.hpp"
#include "../Graphics/Multisampling.hpp"
#include "../Graphics/Color.hpp"
#include "../Math/Vec2.hpp"
#include "../Math/Rect2.hpp"


namespace vega
{

class RenderTarget;
class CommandList;

//! @brief Manages a specific set of rendering tasks to create an output image, either offscreen or to a window.
class VEGA_API Renderer
{
	friend class RendererImpl;

public:
	virtual ~Renderer();

	//! @brief The RenderLayout that describes this Renderer.
	const SPtr<RenderLayout>& layout() const;
	//! @brief Gets if the Renderer is associated with a window.
	bool isWindow() const;

	//! @brief The current MSAA level of the Renderer.
	MSAA msaa() const;
	//! @brief The current extent of the Renderer, in pixels.
	const Vec2ui& extent() const;
	//! @brief Updates the extent and msaa of the Renderer, causing a rebuild.
	//! @remarks This is an expensive call, causing a full GPU stall. It is best to update extent and msaa at the
	//! same time, if possible.
	//! @param extent The new surface extent, or \c std::nullopt to keep the same extent.
	//! @param msaa The new surface MSAA, or \c std::nullopt to keep the same MSAA.
	void setSurfaceParams(Opt<Vec2ui> extent, Opt<MSAA> msaa = std::nullopt);

	//! @brief Gets the default Viewport for the current Renderer parameters.
	inline Viewport defaultViewport() const {
		return { 0, 0, float(extent().x), float(extent().y), 0, 1 };
	}
	//! @brief Gets the default scissor for the current Renderer parameters.
	inline Rect2i defaultScissor() const {
		return { Vec2f{ }, extent() };
	}

	//! @brief Begins recording a new set of render commands to the Renderer.
	//! @remarks This function cannot be called on Renderers attached to windows.
	void begin();
	//! @brief Ends the current set of render commands, submitting them for execution.
	//! @remarks This function must not be called if the Renderer is not actively recording the final subpass. This
	//! function cannot be called on Renderers attached to windows.
	void end();
	//! @brief For multi-subpass RenderLayouts, this moves the command recording into the next subpass.
	void nextSubpass();
	//! @brief Gets if the Renderer is currently recording commands (after begin(), and before end()).
	bool isRecording() const;
	//! @brief Gets the current subpass, or zero if the Renderer is not recording.
	uint32 currentSubpass() const;

	//! @brief Begins a new command list to record commands for the given subpass.
	//! @param subpass The Renderer subpass index that the given commands will be recorded for.
	//! @return A newly started CommandList, which must be submitted with commit(CommandList&).
	UPtr<CommandList> startCommandList(uint32 subpass);
	//! @brief Submit a valid command list to be executed in the Renderer.
	//! @param list A valid command list - must be started for this Renderer and the active subpass.
	void commit(CommandList& list);

	//! @brief Gets a pointer to the RenderTarget results for the given target.
	//! @param targetIndex The index of the target, which must be a valid index to a preserved target.
	//! @return A reference to the target results. The returned pointer will live as long as the parent Renderer.
	const RenderTarget* getRenderTarget(uint32 targetIndex) const;

	//! @brief Set the clear color for the color target at the given index.
	void setClearColor(uint32 targetIndex, Color color);
	//! @brief Set the depth and stencil clear values for the depth/stencil target at the given index.
	void setClearDepthStencilValue(uint32 targetIndex, float depth, uint32 stencil = 0);

private:
	Renderer();

	VEGA_NO_COPY(Renderer)
	VEGA_NO_MOVE(Renderer)
}; // class VEGA_API Renderer

} // namespace vega
