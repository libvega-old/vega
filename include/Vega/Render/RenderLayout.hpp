/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file RenderLayout.hpp Describing the layout of Renderer instances.

#include "../Common.hpp"
#include "../Core/Flags.hpp"
#include "../Graphics/TexelFormat.hpp"


namespace vega
{

//! @brief Supported uses for a target within a Renderer subpass.
enum class RenderUse : uint16
{
	//! @brief The attachment is unused within the subpass.
	None         = 0x0000,
	//! @brief The attachment is used as the index 0 color output in the subpass.
	Color0       = 0x0001,
	//! @brief The attachment is used as the index 1 color output in the subpass.
	Color1       = 0x0002,
	//! @brief The attachment is used as the index 2 color output in the subpass.
	Color2       = 0x0004,
	//! @brief The attachment is used as the index 3 color output in the subpass.
	Color3       = 0x0008,
	//! @brief The attachment is used as the index 0 color input in the subpass.
	Input0       = 0x0010,
	//! @brief The attachment is used as the index 1 color input in the subpass.
	Input1       = 0x0020,
	//! @brief The attachment is used as the index 2 color input in the subpass.
	Input2       = 0x0040,
	//! @brief The attachment is used as the index 3 color input in the subpass.
	Input3       = 0x0080,
	//! @brief The attachmend is used as the depth/stencil output in the subpass.
	Depth        = 0x0100
}; // enum class RenderUse

//! @brief Mask of RenderUse values.
using RenderUseMask = Flags<RenderUse>;
VEGA_DEFINE_FLAGS_OPERATORS(RenderUse)

//! @brief Get the string representation of the mask of render uses.
VEGA_API String to_string(RenderUseMask mask);


//! @brief RenderLayout subpass identifiers.
enum class Subpass : uint16
{
	//! @brief Pass index 0 (first pass).
	Pass0 = 0x0001,
	//! @brief Pass index 1.
	Pass1 = 0x0002,
	//! @brief Pass index 2.
	Pass2 = 0x0004,
	//! @brief Pass index 3.
	Pass3 = 0x0008,
	//! @brief Pass index 4.
	Pass4 = 0x0010,
	//! @brief Pass index 5.
	Pass5 = 0x0020,
	//! @brief Pass index 6.
	Pass6 = 0x0040,
	//! @brief Pass index 7 (maximum pass).
	Pass7 = 0x0080,
};

//! @brief Mask of Subpass values.
using SubpassMask = Flags<Subpass>;
VEGA_DEFINE_FLAGS_OPERATORS(Subpass)

//! @brief Get the string representation of the mask of subpasses.
VEGA_API String to_string(SubpassMask mask);


//! @brief Describes the layout of a Renderer.
class VEGA_API RenderLayout
{
	friend class RenderLayoutImpl;

public:
	//! @brief Maximum number of subpasses in a single RenderLayout.
	inline static constexpr uint32 MAX_SUBPASSES{ 8u };
	//! @brief Maximum number of targets in a single RenderLayout.
	inline static constexpr uint32 MAX_TARGETS{ 8u };

	virtual ~RenderLayout();

	//! @brief Gets if the layout has support for MSAA operations.
	bool supportsMSAA() const;

	//! @brief The number of subpasses in the layout.
	uint32 subpassCount() const;

private:
	RenderLayout();

	VEGA_NO_COPY(RenderLayout)
	VEGA_NO_MOVE(RenderLayout)
}; // class RenderLayout


//! @brief Exception type that is thrown when an attempting to build an invalid RenderLayout.
class VEGA_API BadLayoutError final
	: public std::runtime_error
{
public:
	BadLayoutError() : std::runtime_error("Attempted to build an invalid RenderLayout") { }
	BadLayoutError(const String& msg) : std::runtime_error(msg) { }
}; // class BadLayoutError


//! @brief Supports progressive descriptions of Renderer subpass and render target layouts.
class VEGA_API RenderLayoutBuilder
{
	friend class RenderLayoutBuilderImpl;

public:
	virtual ~RenderLayoutBuilder();

	//! @brief The number of subpasses in the description.
	uint32 subpassCount() const;
	//! @brief The number of targets currently defined in the description.
	uint32 targetCount() const;

	//! @brief Adds a target to the render layout. Will throw a BadLayoutError if the target is invalid or any reason.
	//! @remarks If an error occurs, the builder remains unchanged.
	//! @param format The format of the target.
	//! @param preserve If the target is preserved after rendering - allows it to be used in other renderers.
	//! @param uses The timeline of uses for the target, which must match the number of subpasses in the layout.
	//! @return The same layout builder to facilitate chaining.
	RenderLayoutBuilder& addTarget(TexelFormat format, bool preserve, const std::initializer_list<RenderUse>& uses);

	//! @brief Builds the current description into a RenderLayout object.
	//! @return The new RenderLayout object.
	SPtr<RenderLayout> build() const;

private:
	RenderLayoutBuilder();

	VEGA_NO_COPY(RenderLayoutBuilder)
	VEGA_NO_MOVE(RenderLayoutBuilder)
}; // class RenderLayoutBuilder

} // namespace vega
