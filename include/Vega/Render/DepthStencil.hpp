/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file DepthState.hpp Depth buffer operations

#include "../Common.hpp"


namespace vega
{

//! @brief Comparison operations for stencil and depth values.
enum class CompareOp : uint32
{
	//! @brief Evaluates to false always.
	Never = 0,
	//! @brief Evaluates `A < B`.
	Less = 1,
	//! @brief Evaluates `A == B`.
	Equal = 2,
	//! @brief Evaluates `A <= B`.
	LessEqual = 3,
	//! @brief Evaluates `A > B`.
	Greater = 4,
	//! @brief Evaluates `A != B`.
	NotEqual = 5,
	//! @brief Evaluates `A >= B`.
	GreaterEqual = 6,
	//! @brief Evaluates to true always.
	Always = 7
}; // enum class CompareOp

//! @brief Gets the string representation of the CompareOp.
VEGA_API const String& to_string(CompareOp op);


//! @brief Read/Write modes for depth buffers.
enum class DepthMode : uint32
{
	//! @brief Both depth testing and depth writes are disabled.
	Disabled = 0,
	//! @brief Depth testing is enabled but depth values are not written to the depth buffer.
	ReadOnly = 1,
	//! @brief Depth testing and depth write-backs are both enabled.
	ReadWrite = 2
};

//! @brief Gets the string representation of the DepthMode.
VEGA_API const String& to_string(DepthMode mode);


//! @brief Stencil buffer writeback operations.
enum class StencilOp
{
	//! @brief Value is unchanged by the operation.
	Keep = 0,
	//! @brief Value is set to zero.
	Zero = 1,
	//! @brief Value is replaced with the reference value.
	Replace = 2,
	//! @brief Value is incremented with max value clamp.
	IncClamp = 3,
	//! @brief Value is decremented with min value clamp.
	DecClamp = 4,
	//! @brief Value is bit-wise inverted.
	Invert = 5,
	//! @brief Value is incremented with wrap-around at max value.
	IncWrap = 6,
	//! @brief Value is decremented with wrap-around at min value.
	DecWrap = 7
}; // enum class StencilOp

//! @brief Gets the string representation of the StencilOp.
VEGA_API const String& to_string(StencilOp op);


//! @brief Stencil buffer operations.
struct VEGA_API StencilMode final
{
	friend struct std::hash<StencilMode>;

public:
	//! @brief Create the default disabled stencil mode.
	constexpr StencilMode()
		: pass_{ uint16(StencilOp::Keep) }
		, fail_{ uint16(StencilOp::Keep) }
		, depthFail_{ uint16(StencilOp::Keep) }
		, compare_{ uint16(CompareOp::Never) }
	{ }
	//! @brief Define a new stencil mode.
	//! @param compare The stencil value comparison op.
	//! @param passOp The stencil op to perform on comparison pass.
	//! @param failOp The stencil op to perform on comparison fail and depth fail.
	//! @param depthFailOp The stencil op to perform on comparison pass, but depth fail. Defaults to @p failOp.
	constexpr StencilMode(CompareOp compare, StencilOp passOp, StencilOp failOp, Opt<StencilOp> depthFailOp = {})
		: pass_{ uint16(passOp) }
		, fail_{ uint16(failOp) }
		, depthFail_{ uint16(depthFailOp.value_or(failOp)) }
		, compare_{ uint16(compare) }
	{ }

	//! @brief Gets if the stencil mode actually performs operations on the stencil buffer.
	inline constexpr bool isEnabled() const {
		return (pass() == StencilOp::Keep) && (fail() == StencilOp::Keep) && (depthFail() == StencilOp::Keep);
	}

	//! @brief The stencil op for passing fragments.
	inline constexpr StencilOp pass() const { return StencilOp(pass_); }
	//! @brief The stencil op for failing fragments that also fail the depth test.
	inline constexpr StencilOp fail() const { return StencilOp(fail_); }
	//! @brief The stencil op for passing fragments that fail the depth test.
	inline constexpr StencilOp depthFail() const { return StencilOp(depthFail_); }
	//! @brief The compare op for stencil fragments.
	inline constexpr CompareOp compare() const { return CompareOp(compare_); }

	//! @brief The stencil op for passing fragments.
	inline void pass(StencilOp pass) { pass_ = uint16(pass); }
	//! @brief The stencil op for failing fragments that also fail the depth test.
	inline void fail(StencilOp fail) { fail_ = uint16(fail); }
	//! @brief The stencil op for passing fragments that fail the depth test.
	inline void depthFail(StencilOp depthFail) { depthFail_ = uint16(depthFail); }
	//! @brief The compare op for stencil fragments.
	inline void compare(CompareOp compare) { compare_ = uint16(compare); }

	inline constexpr bool operator == (StencilMode r) const { return raw_ == r.raw_; }
	inline constexpr bool operator != (StencilMode r) const { return raw_ != r.raw_; }
	inline constexpr bool operator <  (StencilMode r) const { return raw_ < r.raw_; }

private:
	union
	{
		struct
		{
			uint16 pass_      : 3;
			uint16 fail_      : 3;
			uint16 depthFail_ : 3;
			uint16 compare_   : 3;
			uint16 _pad_      : 4;
		};
		uint16 raw_;
	};
}; // struct StencilMode


//! @brief Predefined StencilMode values.
class VEGA_API StencilModes final
{
public:
	//! @brief Performs no stencil testing or replacement.
	inline static constexpr StencilMode None{ CompareOp::Never, StencilOp::Keep, StencilOp::Keep };
	//! @brief Unconditionally writes a value to the stencil buffer.
	inline static constexpr StencilMode WriteMask{ CompareOp::Always, StencilOp::Replace, StencilOp::Keep };
	//! @brief Tests against a specific value in the stencil buffer.
	inline static constexpr StencilMode TestMask{ CompareOp::Equal, StencilOp::Keep, StencilOp::Keep };
	//! @brief Accumulates a counter into the stencil buffer, with a clamp at the max value.
	inline static constexpr StencilMode Accumulate{ CompareOp::Always, StencilOp::IncClamp, StencilOp::Keep };

	VEGA_NO_COPY(StencilModes)
	VEGA_NO_MOVE(StencilModes)
	VEGA_NO_INIT(StencilModes)
}; // class StencilModes

} // namespace vega


template <> struct std::hash<vega::StencilMode>
{
	inline size_t operator () (vega::StencilMode mode) const {
		static const std::hash<vega::uint16> HASH{ }; return HASH(mode.raw_);
	}
}; // struct std::hash<vega::StencilMode>
