/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Sampler.hpp Texture sampling.

#include "../Common.hpp"


namespace vega
{

//! @brief Sampling access configurations for shader texture reads.
enum class Sampler : uint32
{
	//! @brief Min/mag = nearest, mip = nearest, border = repeat
	NearNearRepeat          = 0,
	//! @brief Min/mag = nearest, mip = nearest, border = mirror repeat
	NearNearMirror          = 1,
	//! @brief Min/mag = nearest, mip = nearest, border = edge clamp
	NearNearEdge            = 2,
	//! @brief Min/mag = nearest, mip = nearest, border = black clamp
	NearNearBlack           = 3,
	//! @brief Min/mag = nearest, mip = nearest, border = transparent clamp
	NearNearTransparent     = 4,
	//! @brief Min/mag = nearest, mip = linear, border = repeat
	NearLinearRepeat        = 5,
	//! @brief Min/mag = nearest, mip = linear, border = mirror repeat
	NearLinearMirror        = 6,
	//! @brief Min/mag = nearest, mip = linear, border = edge clamp
	NearLinearEdge          = 7,
	//! @brief Min/mag = nearest, mip = linear, border = black clamp
	NearLinearBlack         = 8,
	//! @brief Min/mag = nearest, mip = linear, border = transparent clamp
	NearLinearTransparent   = 9,
	//! @brief Min/mag = linear, mip = nearest, border = repeat
	LinearNearRepeat        = 10,
	//! @brief Min/mag = linear, mip = nearest, border = mirror repeat
	LinearNearMirror        = 11,
	//! @brief Min/mag = linear, mip = nearest, border = edge clamp
	LinearNearEdge          = 12,
	//! @brief Min/mag = linear, mip = nearest, border = black clamp
	LinearNearBlack         = 13,
	//! @brief Min/mag = linear, mip = nearest, border = transparent clamp
	LinearNearTransparent   = 14,
	//! @brief Min/mag = linear, mip = linear, border = repeat
	LinearLinearRepeat      = 15,
	//! @brief Min/mag = linear, mip = linear, border = mirror repeat
	LinearLinearMirror      = 16,
	//! @brief Min/mag = linear, mip = linear, border = edge clamp
	LinearLinearEdge        = 17,
	//! @brief Min/mag = linear, mip = linear, border = black clamp
	LinearLinearBlack       = 18,
	//! @brief Min/mag = linear, mip = linear, border = transparent clamp
	LinearLinearTransparent = 19
}; // enum class Sampler

//! @brief Gets the string representation of the sampler type.
VEGA_API const String& to_string(Sampler sampler);

} // namespace vega
