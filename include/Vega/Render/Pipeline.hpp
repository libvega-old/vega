/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Pipeline.hpp Describing and supporting bulk render states.

#include "../Common.hpp"
#include "./RenderState.hpp"
#include "../Graphics/Shader.hpp"


namespace vega
{

//! @brief Describes a (Shader, RenderState) pair for fast bulk state configuration in CommandList instances.
class VEGA_API Pipeline
{
	friend class PipelineImpl;

public:
	virtual ~Pipeline();

	//! @brief The shader that is used by the pipeline.
	AssetHandle<Shader> shader() const;
	//! @brief The RenderState values used by the pipeline.
	const RenderState& renderState() const;

private:
	Pipeline();

	VEGA_NO_COPY(Pipeline)
	VEGA_NO_MOVE(Pipeline)
}; // class Pipeline

} // namespace vega
