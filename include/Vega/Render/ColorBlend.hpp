/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file ColorBlend.hpp Shader color fragment blending operations.

#include "../Common.hpp"
#include "../Core/Flags.hpp"


namespace vega
{

//! @brief Color fragment blending operations.
enum class BlendOp : uint32
{
	//! @brief Adds the src and dst values.
	Add = 0,
	//! @brief Subtracts the dst value from the src value.
	Subtract = 1,
	//! @brief Subtracts the src value from the dst value.
	InvSubtract = 2,
	//! @brief Takes the minimum of the src and dst values.
	Min = 3,
	//! @brief Takes the maximum of the src and dst values.
	Max = 4
}; // enum class BlendOp

//! @brief Gets a string representation of the BlendOp
VEGA_API const String& to_string(BlendOp value);


//! @brief Color attachment value blending factors.
enum class BlendFactor : uint32
{
	//! @brief Value becomes zero.
	Zero = 0,
	//! @brief Value is unchanged.
	One = 1,
	//! @brief Value is multiplied by source color.
	SrcColor = 2,
	//! @brief Value is multiplied by inverse source color.
	InvSrcColor = 3,
	//! @brief Value is multiplied by destination color.
	DstColor = 4,
	//! @brief Value is multiplied by inverse destination color.
	InvDstColor = 5,
	//! @brief Value is multiplied by source alpha.
	SrcAlpha = 6,
	//! @brief Value is multiplied by inverse source alpha.
	InvSrcAlpha = 7,
	//! @brief Value is multiplied by destination alpha.
	DstAlpha = 8,
	//! @brief Value is multiplied by inverse destination alpha.
	InvDstAlpha = 9,
	//! @brief Value is multiplied by the reference color.
	ConstColor = 10,
	//! @brief Value is multiplied by the inverse reference color.
	InvConstColor = 11,
	//! @brief Value is multiplied by the reference alpha.
	ConstAlpha = 12,
	//! @brief Value is multiplied by the inverse reference alpha.
	InvConstAlpha = 13,
	//! @brief Value is multiplied by the minimum of the source alpha and inverse destination alpha.
	AlphaSaturate = 14
}; // enum class BlendFactor

//! @brief Gets a string representation of the blend factor.
VEGA_API const String& to_string(BlendFactor value);


//! @brief Set of color channels.
enum class ColorChannel : uint32
{
	//! @brief The red channel.
	R = 1,
	//! @brief The green channel.
	G = 2,
	//! @brief The blue channel.
	B = 4,
	//! @brief The alpha channel.
	A = 8,
	//! @brief Mask of all channels.
	All = (R | G | B | A)
}; // enum class ColorChannel

//! @brief Mask of color channels.
using ColorChannelMask = Flags<ColorChannel>;
VEGA_DEFINE_FLAGS_OPERATORS(ColorChannel)

//! @brief Gets a string representation of the mask of color channels.
VEGA_API String to_string(ColorChannelMask value);


//! @brief Describes the blending operations to perform on a color fragment with a color attachment.
struct VEGA_API ColorBlend final
{
	friend class ColorBlends;

public:
	//! @brief Create the default blend state with no blending and all channels being written to.
	constexpr ColorBlend() : packed_{ 0 } {
		writeMask_ = uint32(ColorChannel::All);
	}
	//! @brief Create a blend state with blending disabled, and a custom channel write mask.
	constexpr explicit ColorBlend(ColorChannelMask writeMask) : packed_{ 0 } {
		writeMask_ = uint32(writeMask.rawValue());
	}
	//! @brief Create a fully described blend state.
	constexpr ColorBlend(
		BlendFactor srcColor, BlendFactor dstColor, BlendOp colorOp,
		BlendFactor srcAlpha, BlendFactor dstAlpha, BlendOp alphaOp,
		ColorChannelMask writeMask = ColorChannel::All)
		: packed_{ 0 }
	{
		colorOp_ = uint32(colorOp);
		srcColor_ = uint32(srcColor);
		dstColor_ = uint32(dstColor);
		alphaOp_ = uint32(alphaOp);
		srcAlpha_ = uint32(srcAlpha);
		dstAlpha_ = uint32(dstAlpha);
		writeMask_ = uint32(writeMask.rawValue());
	}

	//! @brief Gets a packed version of the state values.
	inline constexpr uint32 packed() const { return packed_; }

	//! @brief If blending operations are enabled.
	inline constexpr bool enabled() const {
		const bool disabled =
			(srcColor() == BlendFactor::One) && (dstColor() == BlendFactor::Zero) && (colorOp() == BlendOp::Add) &&
			(srcAlpha() == BlendFactor::One) && (dstAlpha() == BlendFactor::Zero) && (alphaOp() == BlendOp::Add);
		return !disabled;
	}
	//! @brief The color blending operation.
	inline constexpr BlendOp colorOp() const { return BlendOp(colorOp_); }
	//! @brief The color blending operation.
	inline void colorOp(BlendOp value) { colorOp_ = uint32(value); }
	//! @brief The source color factor.
	inline constexpr BlendFactor srcColor() const { return BlendFactor(srcColor_); }
	//! @brief The source color factor.
	inline void srcColor(BlendFactor value) { srcColor_ = uint32(value); }
	//! @brief The destination color factor.
	inline constexpr BlendFactor dstColor() const { return BlendFactor(dstColor_); }
	//! @brief The destination color factor.
	inline void dstColor(BlendFactor value) { dstColor_ = uint32(value); }
	//! @brief The alpha blending operation.
	inline constexpr BlendOp alphaOp() const { return BlendOp(alphaOp_); }
	//! @brief The alpha blending operation.
	inline void alphaOp(BlendOp value) { alphaOp_ = uint32(value); }
	//! @brief The source alpha factor.
	inline constexpr BlendFactor srcAlpha() const { return BlendFactor(srcAlpha_); }
	//! @brief The source alpha factor.
	inline void srcAlpha(BlendFactor value) { srcAlpha_ = uint32(value); }
	//! @brief The destination alpha factor.
	inline constexpr BlendFactor dstAlpha() const { return BlendFactor(dstAlpha_); }
	//! @brief The destination alpha factor.
	inline void dstAlpha(BlendFactor value) { dstAlpha_ = uint32(value); }
	//! @brief The mask of color channels to write to.
	inline constexpr ColorChannelMask writeMask() const { return ColorChannel(writeMask_); }
	//! @brief The mask of color channels to write to.
	inline void writeMask(ColorChannelMask value) { writeMask_ = uint32(value.rawValue()); }

	inline constexpr bool operator == (ColorBlend r) const { return packed_ == r.packed_; }
	inline constexpr bool operator != (ColorBlend r) const { return packed_ != r.packed_; }
	inline constexpr bool operator <  (ColorBlend r) const { return packed_ < r.packed_; }

private:
	union {
		struct {
			uint32 colorOp_   : 3;
			uint32 srcColor_  : 4;
			uint32 dstColor_  : 4;
			uint32 alphaOp_   : 3;
			uint32 srcAlpha_  : 4;
			uint32 dstAlpha_  : 4;
			uint32 writeMask_ : 4;
			uint32 _pad_      : 6;
		};
		uint32 packed_;
	};
}; // struct ColorBlend


//! @brief Standard set of supported ColorBlend values.
class VEGA_API ColorBlends final
{
public:
	//! @brief Disabled blending - One Add Zero, One Add Zero
	inline static constexpr ColorBlend None{
		BlendFactor::One, BlendFactor::Zero, BlendOp::Add,
		BlendFactor::One, BlendFactor::Zero, BlendOp::Add
	};
	//! @brief Traditional alpha blending - SrcA Add ISrcA, One Add ISrcA
	inline static constexpr ColorBlend Alpha{
		BlendFactor::SrcAlpha, BlendFactor::InvSrcAlpha, BlendOp::Add,
		BlendFactor::One, BlendFactor::InvSrcAlpha, BlendOp::Add
	};
	//! @brief Alpha-aware additive blending - SrcA Add One, One Add One
	inline static constexpr ColorBlend Add{
		BlendFactor::SrcAlpha, BlendFactor::One, BlendOp::Add,
		BlendFactor::One, BlendFactor::One, BlendOp::Add
	};
	//! @brief Direct multiplicitive blending - DstC Add Zero, DstA Add Zero
	inline static constexpr ColorBlend Mul{
		BlendFactor::DstColor, BlendFactor::Zero, BlendOp::Add,
		BlendFactor::DstAlpha, BlendFactor::Zero, BlendOp::Add
	};
	//! @brief Maximum value replacement blending - One Max One, One Max One
	inline static constexpr ColorBlend Max{
		BlendFactor::One, BlendFactor::One, BlendOp::Max,
		BlendFactor::One, BlendFactor::One, BlendOp::Max
	};
	//! @brief Minimum value replacement blending - One Min One, One Min One
	inline static constexpr ColorBlend Min{
		BlendFactor::One, BlendFactor::One, BlendOp::Min,
		BlendFactor::One, BlendFactor::One, BlendOp::Min
	};

	VEGA_NO_COPY(ColorBlends)
	VEGA_NO_MOVE(ColorBlends)
	VEGA_NO_INIT(ColorBlends)
}; // struct ColorBlends

} // namespace vega


template<> struct std::hash<vega::ColorBlend>
{
	size_t operator () (vega::ColorBlend blend) const {
		static const std::hash<vega::uint32> HASH{}; return HASH(blend.packed());
	}
}; // struct std::hash<vega::ColorBlend>
