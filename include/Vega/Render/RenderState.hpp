/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file RenderState.hpp Describing the core set of rendering operation states.

#include "../Common.hpp"
#include "./ColorBlend.hpp"
#include "./DepthStencil.hpp"
#include "../Graphics/Shader.hpp"


namespace vega
{

//! @brief Primtive input assembly topologies.
enum class Topology : uint32
{
	//! @brief Input primitive is disjoint points.
	PointList     = 0,
	//! @brief Input primitive is pairs of points that create disjoint line segments.
	LineList      = 1,
	//! @brief Input primitive is a series of points that create a continuous set of line segments.
	LineStrip     = 2,
	//! @brief Input primitive is triples of points that create disjoint triangular planes.
	TriangleList  = 3,
	//! @brief Input primitive is a series of points, where every two new points create a triangle with the previous point.
	TriangleStrip = 4,
	//! @brief Input primitive is a series of points, where every new point creates a triangle with the previous two.
	TriangleFan   = 5
}; // enum class Topology

//! @brief Gets the string representation of the topology.
VEGA_API const String& to_string(Topology topo);


//! @brief Primitive rasterizer fill mode.
enum class FillMode : uint32
{
	//! @brief Primitives are filled solid.
	Solid = 0,
	//! @brief Primitives are outlines on their edges (commonly known as wireframe mode).
	Lines = 1,
	//! @brief Primitives are point clouds of their vertices.
	Points = 2
}; // enum class FillMode

//! @brief Gets the string representation of the fill mode.
VEGA_API const String& to_string(FillMode mode);


//! @brief Primitive culling modes (front face is always clockwise winding).
enum class CullMode : uint32
{
	//! @brief No polygons are discarded.
	None         = 0,
	//! @brief Front-facing polygons (clockwise winding) are discarded.
	Front        = 1,
	//! @brief Back-facing polygons (counter-clockwise winding) are discarded.
	Back         = 2,
	//! @brief All polygons are discarded.
	FrontAndBack = 3
}; // enum class CullMode

//! @brief Gets the string representation of the cull mode.
VEGA_API const String& to_string(CullMode mode);


//! @brief Represents a compiled set of core rendering states used to setup rendering in a CommandList.
class VEGA_API RenderState final
{
	friend class PipelineImpl;

	using StateStorage = std::aligned_storage_t<sizeof(uint64) * 3, alignof(uint64)>;

public:
	//! @brief Create a default set of rendering states.
	RenderState();
	~RenderState();

	//! @brief Color blending for attachment 0, if not using the active Shader defaults.
	Opt<ColorBlend> color0() const;
	//! @brief Color blending for attachment 1, if not using the active Shader defaults.
	Opt<ColorBlend> color1() const;
	//! @brief Color blending for attachment 2, if not using the active Shader defaults.
	Opt<ColorBlend> color2() const;
	//! @brief Color blending for attachment 3, if not using the active Shader defaults.
	Opt<ColorBlend> color3() const;
	//! @brief Stencil operations for front-facing fragments - defaults to no stencil operations.
	StencilMode stencilFront() const;
	//! @brief Stencil operations for back-facing fragments - defaults to no stencil operations.
	StencilMode stencilBack() const;
	//! @brief Depth test/write mode, if the active Renderer supports depth operations.
	DepthMode depthMode() const;
	//! @brief Depth comparison operatior, if the active Renderer supports depth operations.
	CompareOp depthOp() const;
	//! @brief The input data primitive topology - defaults to Topology::TriangleList.
	Topology topology() const;
	//! @brief If the primitive topology can be restarted with special index values.
	bool primitiveRestart() const;
	//! @brief The primitive fill mode - defaults to FillMode::Solid.
	FillMode fillMode() const;
	//! @brief The primitive cull mode - defaults to CullMode::None.
	CullMode cullMode() const;

	//! @brief Set the color blending for attachment 0, or \c std::nullopt to use the Shader defaults.
	RenderState& color0(Opt<ColorBlend> blend);
	//! @brief Set the color blending for attachment 1, or \c std::nullopt to use the Shader defaults.
	RenderState& color1(Opt<ColorBlend> blend);
	//! @brief Set the color blending for attachment 2, or \c std::nullopt to use the Shader defaults.
	RenderState& color2(Opt<ColorBlend> blend);
	//! @brief Set the color blending for attachment 3, or \c std::nullopt to use the Shader defaults.
	RenderState& color3(Opt<ColorBlend> blend);
	//! @brief Set the stencil operations for front-facing fragments.
	RenderState& stencilFront(StencilMode mode);
	//! @brief Set the stencil operations for back-facing fragments.
	RenderState& stencilBack(StencilMode mode);
	//! @brief Set the depth test/write mode.
	RenderState& depthMode(DepthMode mode);
	//! @brief Set the depth fragment comparison operator.
	RenderState& depthOp(CompareOp compare);
	//! @brief Set the input data primitive topology.
	RenderState& topology(Topology topology);
	//! @brief Set if primitive restarting is enabled.
	RenderState& primitiveRestart(bool restart);
	//! @brief Set the primitive shading fill mode.
	RenderState& fillMode(FillMode mode);
	//! @brief Set the primitive culling mode.
	RenderState& cullMode(CullMode mode);

private:
	uint64 raw0() const;
	uint64 raw1() const;
	uint64 raw2() const;

private:
	StateStorage state_;

	VEGA_DEFAULT_COPY(RenderState)
	VEGA_DEFAULT_MOVE(RenderState)
}; // class RenderState

} // namespace vega
