/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AudioDevice.hpp Physical audio playback device reflection and info.

#include "../Common.hpp"

#include <vector>


namespace vega
{

//! @brief Reflects a physical or virtual audio playback device.
class VEGA_API AudioDevice final
{
public:
	//! @brief The name of the audio device.
	inline const String& name() const { return name_; }
	//! @brief Gets if the device is the default system playback device.
	bool isDefault() const;

	//! @brief A list of all valid audio playback devices on the current system. Must be called from main thread only.
	static const std::vector<UPtr<const AudioDevice>>& AllDevices();
	//! @brief The default audio playback device on the system. Must be called from the main thread only.
	static const AudioDevice* DefaultDevice();

private:
	AudioDevice(const String& name);

private:
	const String name_;

	static String DefaultDeviceName_;

	VEGA_NO_COPY(AudioDevice)
	VEGA_NO_MOVE(AudioDevice)
}; // class AudioDevice

} // namespace vega
