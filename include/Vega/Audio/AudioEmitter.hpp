/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AudioEmitter.hpp Globally positioned audio emitters.

#include "../Common.hpp"
#include "./Sound.hpp"
#include "../Math/Vec3.hpp"


namespace vega
{

//! @brief Represents an audio emitting object that is positioned in 3D space.
class VEGA_API AudioEmitter
{
	friend class AudioEmitterImpl;

public:
	virtual ~AudioEmitter();

	//! @brief Gets the Sound asset that is played by the emitter.
	const Sound* sound() const;

	//! @brief Sets the volume of the emitter (clamped to [0, 1]).
	void volume(float volume);
	//! @brief The volume of the emitter.
	float volume() const;
	//! @brief Sets the playback speed of the emitter (clamped to >= 0).
	void speed(float speed);
	//! @brief The playback speed of the emitter.
	float speed() const;
	//! @brief Sets if the emitter will loop when finished playing.
	void looping(bool looping);
	//! @brief If the emitter loops when finished playing.
	bool looping() const;

	//! @brief Sets the emitter position.
	void position(const Vec3f& pos);
	//! @brief Gets the emitter position.
	Vec3f position() const;
	//! @brief Sets the emitter velocity.
	void velocity(const Vec3f& vel);
	//! @brief Gets the emitter velocity.
	Vec3f velocity() const;

private:
	AudioEmitter();

	VEGA_NO_COPY(AudioEmitter)
	VEGA_NO_MOVE(AudioEmitter)
}; // class AudioEmitter

} // namespace vega
