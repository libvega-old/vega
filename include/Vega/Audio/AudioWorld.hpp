/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AudioWorld.hpp Manages 3D spatial sound rendering.

#include "../Common.hpp"
#include "../Math/Vec3.hpp"


namespace vega
{

class AudioEmitter;
class Sound;

//! @brief Manages spatial audio emitters and global audio listener, and audio model settings.
class VEGA_API AudioWorld final
{
	friend class AudioEngineImpl;

public:
	~AudioWorld();

	//! @brief Get the global position of the listener in 3D space.
	Vec3f listenerPosition() const;
	//! @brief Set the global position of the listener in 3D space.
	void listenerPosition(const Vec3f& pos);
	//! @brief Get the velocity of the listener in 3D space.
	Vec3f listenerVelocity() const;
	//! @brief Set the velocity of the listener in 3D space.
	void listenerVelocity(const Vec3f& vel);
	//! @brief Get the orientation of the listener in 3D space.
	//! @return A tuple of the `{ forward, up }` vectors describing the listener orientation.
	std::tuple<Vec3f, Vec3f> listenerOrientation() const;
	//! @brief Set the orientation of the listener in 3D space.
	//! @param forward The forward vector.
	//! @param up The up vector, which will be orthogonalized with the forward vector.
	void listenerOrientation(const Vec3f& forward, const Vec3f& up = Vec3f::Up);
	//! @brief Set the orientation of the listener in 3D space.
	//! @param forward The forward vector.
	//! @param roll The roll around the forward vector.
	void listenerOrientation(const Vec3f& forward, Rad<float> roll);

	//! @brief Creates a new position emitter using the given sound.
	//! @param sound The sound to playback from the emitter.
	UPtr<AudioEmitter> createEmitter(const Sound& sound);

private:
	AudioWorld();

	VEGA_NO_COPY(AudioWorld)
	VEGA_NO_MOVE(AudioWorld)
}; // class AudioWorld

} // namespace vega
