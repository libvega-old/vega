/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AudioEngine.hpp Core logic and management for audio operations.

#include "../Common.hpp"
#include "./AudioDevice.hpp"
#include "./AudioFormat.hpp"
#include "../Math/Vec3.hpp"
#include "../Math/Math.hpp"


namespace vega
{

class Sound;
class AudioWorld;

//! @brief Core management of audio playback and recording.
class VEGA_API AudioEngine
{
	friend class AudioEngineImpl;

public:
	//! @brief The maximum number of concurrently playing audio sources.
	inline static constexpr uint32 MAX_SOURCES{ 32u };

	virtual ~AudioEngine();

	//! @brief The active engine instance, if any. Same as AppBase::Get()->audio().
	inline static AudioEngine* Get() { return Instance_; }
	
	//! @brief The playback device in use by the program.
	const AudioDevice* playbackDevice() const;

	//! @brief Immediately stops all playing audio sources.
	void stopAll();

	//! @brief Gets the AudioWorld instance that is controlling spatial audio rendering.
	AudioWorld* world();

	//! @brief Creates a new Sound instance with the given parameters and data.
	//! @param format The format of the audio data.
	//! @param frequency The playback frequency of the audio data.
	//! @param frameCount The number of audio frames in the data.
	//! @param data The audio data.
	UPtr<Sound> createSound(AudioFormat format, uint32 frequency, uint32 frameCount, const void* data);

private:
	AudioEngine();

private:
	static AudioEngine* Instance_;

	VEGA_NO_COPY(AudioEngine)
	VEGA_NO_MOVE(AudioEngine)
}; // class AudioEngine

} // namespace vega
