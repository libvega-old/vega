/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Sound.hpp In-memory storage of audio playback data.

#include "../Common.hpp"
#include "../Asset/AssetBase.hpp"
#include "./AudioState.hpp"


namespace vega
{

class SoundInstance;

//! @brief Allows playback of in-memory audio data.
class VEGA_API Sound
	: public AssetBase
{
	friend class SoundImpl;

public:
	virtual ~Sound();

	//! @brief Creates a playable instance of the sound.
	UPtr<SoundInstance> createInstance() const;
	//! @brief Starts a new fire-and-forget instance of this Sound.
	//! @param volume The volume of the sound instance.
	//! @param speed The playback speed of the sound instance.
	//! @return If the sound instance could be started (\c false implies the playback limit was reached).
	bool play(float volume = 1, float speed = 1) const;

private:
	Sound();

	VEGA_NO_COPY(Sound)
	VEGA_NO_MOVE(Sound)
}; // class Sound


//! @brief An active and playable instance of a Sound.
class VEGA_API SoundInstance
{
	friend class SoundInstanceImpl;

public:
	virtual ~SoundInstance();

	//! @brief The current playback state of the instance.
	AudioState state() const;
	//! @brief If the instance is stopped (no longer playing).
	inline bool isStopped() const { return state() == AudioState::Stopped; }
	//! @brief If the instance is paused.
	inline bool isPaused() const { return state() == AudioState::Paused; }
	//! @brief If the instance is currently playing.
	inline bool isPlaying() const { return state() == AudioState::Playing; }

	//! @brief Sets the volume of the instance.
	//! @param volume The new instance volume, will be clamped to [0, 1].
	void volume(float volume);
	//! @brief The volume of the instance.
	float volume() const;
	//! @brief Sets the playback speed of the instance.
	//! @param speed The new instance playback speed, will be clamped to >= 0.
	void speed(float speed);
	//! @brief The playback speed of the instance.
	float speed() const;
	//! @brief Sets if the instance will loop when finished playing.
	void looping(bool looping);
	//! @brief If the instance loops when finished playing.
	bool looping() const;
	//! @brief Sets the panning direction for the sound.
	//! @param pan The pan direction in [-1, 1], with -1 = left, 0 = center, and 1 = right.
	void pan(float pan);
	//! @brief The panning direction.
	float pan() const;

	//! @brief If the sound is not playing, this will start the instance playback.
	//! @return If the sound could be played, \c false implies the source playback limit has been reached.
	bool play();
	//! @brief If the sound is currently playing, this pauses the instance playback.
	void pause();
	//! @brief If the sound is currently playing or paused, this fully stops the instance playback.
	void stop();

private:
	SoundInstance();

	VEGA_NO_COPY(SoundInstance)
	VEGA_NO_MOVE(SoundInstance)
}; // class SoundInstance

} // namespace vega
