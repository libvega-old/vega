/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AudioFormat.hpp Audio data formats.

#include "../Common.hpp"


namespace vega
{

//! @brief Describes a specific data format for audio samples.
struct AudioFormat final
{
	struct FormatInfo
	{
		uint32 format;
		uint8 ccount;
		uint8 ssize;
	}; // struct FormatInfo

public:
	constexpr AudioFormat() : value_{ 0 } { }

	//! @brief Unique integer value specifying the format.
	VEGA_API inline constexpr uint32 value() const { return value_; }
	//! @brief Gets the string representation of the format.
	VEGA_API const String& str() const;

	//! @brief The number of audio channels in the format.
	VEGA_API inline uint32 channelCount() const { return info().ccount; }
	//! @brief The size of a single audio sample in the format, in bytes.
	VEGA_API inline uint32 sampleSize() const { return info().ssize; }
	//! @brief The size of a single audio frame (all samples at a single time point) in the format, in bytes.
	VEGA_API inline uint32 frameSize() const { 
		const auto& finfo = info();
		return uint32(finfo.ccount) * finfo.ssize;
	}

	//! @brief A different format identifier used internally.
	VEGA_API inline uint32 formatId() const { return info().format; }

	inline constexpr bool operator == (AudioFormat r) const { return value_ == r.value_; }
	inline constexpr bool operator != (AudioFormat r) const { return value_ != r.value_; }
	inline constexpr bool operator <  (AudioFormat r) const { return value_ < r.value_; }
	
private:
	constexpr AudioFormat(uint32 value) : value_{ value } { }

	const FormatInfo& info() const;

private:
	uint32 value_;

public:
	static const AudioFormat Mono8, Stereo8, Mono16, Stereo16, MonoFloat, StereoFloat;
}; // struct AudioFormat

//! @brief Single-channel signed 8-bit normalized integer data.
inline constexpr AudioFormat AudioFormat::Mono8{ 0 };
//! @brief Dual-channel signed 8-bit normalized integer data.
inline constexpr AudioFormat AudioFormat::Stereo8{ 1 };
//! @brief Single-channel signed 16-bit normalized integer data.
inline constexpr AudioFormat AudioFormat::Mono16{ 2 };
//! @brief Dual-channel signed 16-bit normalized integer data.
inline constexpr AudioFormat AudioFormat::Stereo16{ 3 };
//! @brief Single-channel signed 32-bit float data.
inline constexpr AudioFormat AudioFormat::MonoFloat{ 4 };
//! @brief Dual-channel signed 32-bit float data.
inline constexpr AudioFormat AudioFormat::StereoFloat{ 5 };

} // namespace vega
