/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file AudioState.hpp Audio playback state.

#include "../Common.hpp"


namespace vega
{

//! @brief Playback states that an audio source can be in.
enum class AudioState : uint32
{
	//! @brief The source is stopped - not actively playing and will start from the beginning if played.
	Stopped = 0,
	//! @brief The source is paused - not actively playing but will resume at its previous position
	Paused  = 1,
	//! @brief The source is playing.
	Playing = 2
}; // enum class AudioState

//! @brief Gets the string representation of the audio state.
VEGA_API const String& to_string(AudioState state);

} // namespace vega
