/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Mouse.hpp Standard mouse polling-based input.

#include "../Common.hpp"
#include "../Core/Flags.hpp"
#include "../Core/Time.hpp"
#include "../Math/Vec2.hpp"

#include <array>


namespace vega
{

class Window;

//! @brief Supported display modes for cursors over windows.
enum class CursorMode : uint32
{
	//! @brief The cursor is visible and is free to move around and on/off the window.
	Normal,
	//! @brief The cursor is invisible, but is still free to move around and on/off the window.
	Hidden,
	//! @brief The cursor is invisible, and is locked to the center of the screen. This is an "FPS-style" mode.
	Locked
}; // enum class CursorMode

//! @brief Gets the string representation of the cursor mode.
VEGA_API const String& to_string(CursorMode mode);


//! @brief Standard and extended mouse buttons.
enum class MouseButton : uint32
{
	//! @brief Left primary mouse button.
	Left   = 0x01,
	//! @brief Middle mouse button (often wheel click).
	Middle = 0x02,
	//! @brief Right primary mouse button.
	Right  = 0x04,
	//! @brief Extra mouse button 1.
	X1     = 0x08,
	//! @brief Extra mouse button 2.
	X2     = 0x10,
	//! @brief Extra mouse button 3.
	X3     = 0x20,
	//! @brief Extra mouse button 4.
	X4     = 0x40,
	//! @brief Extra mouse button 5.
	X5     = 0x80
}; // enum class MouseButton

//! @brief Mask of MouseButton values.
using MouseButtonMask = Flags<MouseButton>;
VEGA_DEFINE_FLAGS_OPERATORS(MouseButton)

//! @brief Gets the string representation of the mask of mouse buttons.
VEGA_API String to_string(MouseButtonMask mask);


//! @brief Reports the state of a mouse peripheral for a specific Window.
class VEGA_API Mouse final
{
	friend class WindowImpl;

public:
	~Mouse();

	//! @brief The number of unique supported mouse buttons.
	inline static constexpr uint32 BUTTON_COUNT{ 8 };

	//! @brief The window that this object is managing input for.
	inline Window* window() const { return window_; }

	//! @brief The mode for the mouse cursor.
	inline vega::CursorMode cursorMode() const { return cursorMode_; }
	//! @brief The mode for the mouse cursor.
	void cursorMode(vega::CursorMode mode);

	//! @brief The current mask of mouse buttons.
	inline MouseButtonMask buttons() const { return buttons_.current; }
	//! @brief The mask of mouse buttons in the previous application frame.
	inline MouseButtonMask prevButtons() const { return buttons_.previous; }

	//! @brief If the given mouse button is currently down.
	inline bool buttonDown(MouseButton mb)     const { return bool(buttons_.current & mb); }
	//! @brief If the given mouse button was down in the previous frame.
	inline bool buttonPrevDown(MouseButton mb) const { return bool(buttons_.previous & mb); }
	//! @brief If the given mouse button was newly pressed in the current frame.
	inline bool buttonPressed(MouseButton mb)  const {
		return bool(buttons_.current & mb) && !bool(buttons_.previous & mb);
	}
	//! @brief If the given mouse button was newly released in the current frame.
	inline bool buttonReleased(MouseButton mb) const {
		return !bool(buttons_.current & mb) && bool(buttons_.previous & mb);
	}
	//! @brief If the given button was clicked since the last frame.
	inline bool buttonClicked(MouseButton mb) const {
		return click_.clickInLastFrame[ButtonIndex(mb)];
	}
	//! @brief If the given button was double clicked since the last frame.
	inline bool buttonDoubleClicked(MouseButton mb) const {
		const auto bindex = ButtonIndex(mb);
		return click_.clickInLastFrame[bindex] && !click_.nextClickIsDouble[bindex];
	}

	//! @brief The amount of time the given button has been held.
	inline TimeSpan holdTime(MouseButton mb) const {
		return TimeSpan::Seconds(buttons_.holdTimes[ButtonIndex(mb)]);
	}
	//! @brief The elapsed application time when the given button was last pressed.
	inline TimeSpan lastPressTime(MouseButton mb) const {
		return TimeSpan::Seconds(click_.lastPressTime[ButtonIndex(mb)]);
	}
	//! @brief The elapsed application time when the given button was last released.
	inline TimeSpan lastReleaseTime(MouseButton mb) const {
		return TimeSpan::Seconds(click_.lastReleaseTime[ButtonIndex(mb)]);
	}
	//! @brief The elapsed application time when the given button was last clicked.
	inline TimeSpan lastClickTime(MouseButton mb) const {
		return TimeSpan::Seconds(click_.lastClickTime[ButtonIndex(mb)]);
	}

	//! @brief The position of the last press for the given button, in logical pixels.
	inline const Vec2i& lastPressPosition(MouseButton mb) const { return click_.lastPressPos[ButtonIndex(mb)]; }
	//! @brief The position of the last release for the given button, in logical pixels.
	inline const Vec2i& lastReleasePosition(MouseButton mb) const { return click_.lastReleasePos[ButtonIndex(mb)]; }
	//! @brief The position of the last click for the given button, in logical pixels.
	inline const Vec2i& lastClickPosition(MouseButton mb) const { return click_.lastClickPos[ButtonIndex(mb)]; }

	//! @brief The current position of the mouse in logical pixels from the top left of the window content area.
	inline const Vec2i& position() const { return position_.current; }
	//! @brief The previous position of the mouse in logical pixels from the top left of the window content area.
	inline const Vec2i& prevPosition() const { return position_.previous; }
	//! @brief The difference in the mouse position between this frame and last.
	inline Vec2i deltaPosition() const { return position_.current - position_.previous; }

	//! @brief The current position of the scroll wheel (\c Y() is the standard up/down direction).
	inline const Vec2i& wheel() const { return scroll_.current; }
	//! @brief The previous position of the scroll wheel (\c Y() is the standard up/down direction).
	inline const Vec2i& prevWheel() const { return scroll_.previous; }
	//! @brief The difference in the mouse wheel between this frame and last.
	inline Vec2i deltaWheel() const { return scroll_.current - scroll_.previous; }

	//! @brief If the mouse cursor is currently in the window content area.
	inline bool hover() const { return hover_.current; }
	//! @brief If the mouse cursor was previous in the window content area.
	inline bool prevHover() const { return hover_.previous; }
	//! @brief If the mouse cursor has entered the window content area in the current frame.
	inline bool windowEnter() const { return !hover_.previous && hover_.current; }
	//! @brief If the mouse cursor has left the window content area in the current frame.
	inline bool windowExit() const { return hover_.previous && !hover_.current; }

	//! @brief The time between a button press and release to count as a click event.
	inline TimeSpan clickTime() const { return TimeSpan::Seconds(click_.clickTime); }
	//! @brief Sets the click time limit.
	inline void clickTime(TimeSpan time) { click_.clickTime = float(time.seconds()); }
	//! @brief The time between successive click events to count as a double click event.
	inline TimeSpan doubleClickTime() const { return TimeSpan::Seconds(click_.doubleClickTime); }
	//! @brief Sets the double click time limit.
	inline void doubleClickTime(TimeSpan time) { click_.doubleClickTime = float(time.seconds()); }
	//! @brief The distance between a button press and release to count as a click event.
	inline uint32 clickDistance() const { return click_.clickDistance; }
	//! @brief Sets the click distance limit.
	inline void clickDistance(uint32 distance) { click_.clickDistance = distance; }

private:
	Mouse(Window* window);

	void handleButton(int button, int action, int mods);
	void handleCursorPos(double xpos, double ypos);
	void handleCursorEnter(int entered);
	void handleScroll(double xoffset, double yoffset);

	void nextFrame();

	static uint32 ButtonIndex(MouseButton mb);

private:
	Window* const window_;
	vega::CursorMode cursorMode_;
	struct
	{
		MouseButtonMask current{ };
		MouseButtonMask previous{ };
		std::array<float, BUTTON_COUNT> holdTimes{ };
	} buttons_;
	struct
	{
		std::array<float, BUTTON_COUNT> lastPressTime{ };
		std::array<float, BUTTON_COUNT> lastReleaseTime{ };
		std::array<float, BUTTON_COUNT> lastClickTime{ };
		std::array<Vec2i, BUTTON_COUNT> lastPressPos{ };
		std::array<Vec2i, BUTTON_COUNT> lastReleasePos{ };
		std::array<Vec2i, BUTTON_COUNT> lastClickPos{ };
		std::array<bool, BUTTON_COUNT> clickInLastFrame{ };
		std::array<bool, BUTTON_COUNT> nextClickIsDouble{ };
		float clickTime{ };
		float doubleClickTime{ };
		uint32 clickDistance{ };
	} click_;
	struct
	{
		Vec2i current{ };
		Vec2i previous{ };
	} position_;
	struct
	{
		Vec2i current{ };
		Vec2i previous{ };
	} scroll_;
	struct
	{
		bool current{ };
		bool previous{ };
	} hover_;

	VEGA_NO_COPY(Mouse)
	VEGA_NO_MOVE(Mouse)
}; // class Mouse

} // namespace vega
