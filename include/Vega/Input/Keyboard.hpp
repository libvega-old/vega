/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Keyboard.hpp Standard keyboard polling-based input.

#include "../Common.hpp"
#include "../Core/Flags.hpp"
#include "../Core/Time.hpp"

#include <array>
#include <set>


namespace vega
{

class Window;

//! @brief Keyboard modifier keys.
enum class ModKey : uint32
{
	//! @brief Left shift key.
	LShift = 0x01,
	//! @brief Left control key.
	LCtrl  = 0x02,
	//! @brief Left alt key.
	LAlt   = 0x04,
	//! @brief Right shift key.
	RShift = 0x10,
	//! @brief Right control key.
	RCtrl  = 0x20,
	//! @brief Right alt key.
	RAlt   = 0x40,
	//! @brief Mask of both shift keys.
	Shift = LShift | RShift,
	//! @brief Mask of both control keys.
	Ctrl = LCtrl | RCtrl,
	//! @brief Mask of both alt keys.
	Alt = LAlt | RAlt
}; // enum class ModKey

//! @brief Mask of keyboard modifier keys.
using ModKeyMask = Flags<ModKey>;
VEGA_DEFINE_FLAGS_OPERATORS(ModKey)

//! @brief Gets the string representation of the mask of modifier keys.
VEGA_API String to_string(ModKeyMask mask);


//! @brief Represents a unique key on a keyboard.
struct VEGA_API Key final
{
	friend struct Keys;

public:
	constexpr Key() : index_{ 0 } { }

	//! @brief The unique numeric index for the key.
	inline constexpr uint8 index() const { return index_; }
	//! @brief The human-readable name for the key assuming .
	const String& name() const;

	inline constexpr bool operator == (Key r) const { return index_ == r.index_; }
	inline constexpr bool operator != (Key r) const { return index_ != r.index_; }
	inline constexpr bool operator <  (Key r) const { return index_ <  r.index_; }

private:
	constexpr Key(uint8 index) : index_{ index } { }

private:
	uint8 index_;
}; // struct Key


//! @brief List of avaiable Key values.
struct VEGA_API Keys final
{

static constexpr Key Unknown{ 0 }; //!< Represents any unknown, unsupported, or error key.
static constexpr Key A  { 1 };  //!< Alpha key 'A'.
static constexpr Key B  { 2 };  //!< Alpha key 'B'.
static constexpr Key C  { 3 };  //!< Alpha key 'C'.
static constexpr Key D  { 4 };  //!< Alpha key 'D'.
static constexpr Key E  { 5 };  //!< Alpha key 'E'.
static constexpr Key F  { 6 };  //!< Alpha key 'F'.
static constexpr Key G  { 7 };  //!< Alpha key 'G'.
static constexpr Key H  { 8 };  //!< Alpha key 'H'.
static constexpr Key I  { 9 };  //!< Alpha key 'I'.
static constexpr Key J  { 10 }; //!< Alpha key 'J'.
static constexpr Key K  { 11 }; //!< Alpha key 'K'.
static constexpr Key L  { 12 }; //!< Alpha key 'L'.
static constexpr Key M  { 13 }; //!< Alpha key 'M'.
static constexpr Key N  { 14 }; //!< Alpha key 'N'.
static constexpr Key O  { 15 }; //!< Alpha key 'O'.
static constexpr Key P  { 16 }; //!< Alpha key 'P'.
static constexpr Key Q  { 17 }; //!< Alpha key 'Q'.
static constexpr Key R  { 18 }; //!< Alpha key 'R'.
static constexpr Key S  { 19 }; //!< Alpha key 'S'.
static constexpr Key T  { 20 }; //!< Alpha key 'T'.
static constexpr Key U  { 21 }; //!< Alpha key 'U'.
static constexpr Key V  { 22 }; //!< Alpha key 'V'.
static constexpr Key W  { 23 }; //!< Alpha key 'W'.
static constexpr Key X  { 24 }; //!< Alpha key 'X'.
static constexpr Key Y  { 25 }; //!< Alpha key 'Y'.
static constexpr Key Z  { 26 }; //!< Alpha key 'Z'.
static constexpr Key D1 { 27 }; //!< Main keyboard number 1.
static constexpr Key D2 { 28 }; //!< Main keyboard number 2.
static constexpr Key D3 { 29 }; //!< Main keyboard number 3.
static constexpr Key D4 { 30 }; //!< Main keyboard number 4.
static constexpr Key D5 { 31 }; //!< Main keyboard number 5.
static constexpr Key D6 { 32 }; //!< Main keyboard number 6.
static constexpr Key D7 { 33 }; //!< Main keyboard number 7.
static constexpr Key D8 { 34 }; //!< Main keyboard number 8.
static constexpr Key D9 { 35 }; //!< Main keyboard number 9.
static constexpr Key D0 { 36 }; //!< Main keyboard number 0.
static constexpr Key KP1{ 37 }; //!< Keypad number 1.
static constexpr Key KP2{ 38 }; //!< Keypad number 2.
static constexpr Key KP3{ 39 }; //!< Keypad number 3.
static constexpr Key KP4{ 40 }; //!< Keypad number 4.
static constexpr Key KP5{ 41 }; //!< Keypad number 5.
static constexpr Key KP6{ 42 }; //!< Keypad number 6.
static constexpr Key KP7{ 43 }; //!< Keypad number 7.
static constexpr Key KP8{ 44 }; //!< Keypad number 8.
static constexpr Key KP9{ 45 }; //!< Keypad number 9.
static constexpr Key KP0{ 46 }; //!< Keypad number 0.
static constexpr Key KPDivide  { 47 }; //!< Keypad divide ('/').
static constexpr Key KPMultiply{ 48 }; //!< Keypad multiply ('*').
static constexpr Key KPSubtract{ 49 }; //!< Keypad subtract ('-').
static constexpr Key KPAdd     { 50 }; //!< Keypad add ('+').
static constexpr Key KPDecimal { 51 }; //!< Keypad decimal ('.').
static constexpr Key Grave     { 52 }; //!< Grave/Tilde.
static constexpr Key Minus     { 53 }; //!< Main keyboard minus/underscore.
static constexpr Key Equals    { 54 }; //!< Main keyboard equals/plus.
static constexpr Key LeftBracket { 55 }; //!< Main keyboard left bracket/left brace.
static constexpr Key RightBracket{ 56 }; //!< Main keyboard right bracket/right brace.
static constexpr Key Backslash   { 57 }; //!< Main keyboard backslash/pipe.
static constexpr Key Semicolon   { 58 }; //!< Main keyboard semicolon/colon.
static constexpr Key Apostrophe  { 59 }; //!< Main keyboard apostrophe/quote.
static constexpr Key Comma       { 60 }; //!< Main keyboard comma/less-than.
static constexpr Key Period      { 61 }; //!< Main keyboard period/greater-than.
static constexpr Key Slash       { 62 }; //!< Main keyboard slash/question mark.
static constexpr Key F1 { 63 }; //!< Function key 1 (F1).
static constexpr Key F2 { 64 }; //!< Function key 2 (F1).
static constexpr Key F3 { 65 }; //!< Function key 3 (F1).
static constexpr Key F4 { 66 }; //!< Function key 4 (F1).
static constexpr Key F5 { 67 }; //!< Function key 5 (F1).
static constexpr Key F6 { 68 }; //!< Function key 6(F1).
static constexpr Key F7 { 69 }; //!< Function key 7 (F1).
static constexpr Key F8 { 70 }; //!< Function key 8 (F1).
static constexpr Key F9 { 71 }; //!< Function key 9 (F1).
static constexpr Key F10{ 72 }; //!< Function key 10 (F1).
static constexpr Key F11{ 73 }; //!< Function key 11 (F1).
static constexpr Key F12{ 74 }; //!< Function key 12 (F1).
static constexpr Key Up   { 75 }; //!< Up arrow.
static constexpr Key Down { 76 }; //!< Down arrow.
static constexpr Key Left { 77 }; //!< Left arrow.
static constexpr Key Right{ 78 }; //!< Right arrow.
static constexpr Key Tab       { 79 };
static constexpr Key CapsLock  { 80 };
static constexpr Key Backspace { 81 };
static constexpr Key Enter     { 82 };
static constexpr Key Insert    { 83 };
static constexpr Key Home      { 84 };
static constexpr Key PageUp    { 85 };
static constexpr Key Delete    { 86 };
static constexpr Key End       { 87 };
static constexpr Key PageDown  { 88 };
static constexpr Key KPEnter   { 89 }; //!< Keypad enter key (return).
static constexpr Key Space     { 90 };
static constexpr Key Escape    { 91 };
static constexpr Key LeftSuper { 92 }; //!< Left system key (Windows key).
static constexpr Key RightSuper{ 93 }; //!< Right system key (Windows key).
static constexpr Key Menu       { 94 };
static constexpr Key PrintScreen{ 95 };
static constexpr Key ScrollLock { 96 };
static constexpr Key Pause      { 97 };
static constexpr Key NumLock    { 98 };
static constexpr Key LeftShift   { 99 }; //!< Left shift key.
static constexpr Key LeftControl { 100 }; //!< Left control key.
static constexpr Key LeftAlt     { 101 }; //!< Left alt key.
static constexpr Key RightShift  { 102 }; //!< Right shift key.
static constexpr Key RightControl{ 103 }; //!< Right control key.
static constexpr Key RightAlt    { 104 }; //!< Right alt key.

//! @brief The total number of unique supported keyboard keys.
static constexpr uint32 KEY_COUNT{ uint32(RightAlt.index()) + 1 };

}; // struct Keys


//! @brief Reports the state of a keyboard peripheral for a specific Window.
class VEGA_API Keyboard final
{
	friend class WindowImpl;

public:
	~Keyboard();

	//! @brief The window that this keyboard instance is managing input for.
	inline Window* window() const { return window_; }

	//! @brief The set of all keys currently down.
	inline const std::set<Key>& currentKeys() const { return keys_.pressed; }

	//! @brief If the given key is currently down.
	inline bool keyDown(Key key) const { return keys_.current[key.index()]; }
	//! @brief If the given key was down in the last frame.
	inline bool keyPrevDown(Key key) const { return keys_.previous[key.index()]; }
	//! @brief If the given key is newly pressed in the current frame.
	inline bool keyPressed(Key key) const { return keys_.current[key.index()] && !keys_.previous[key.index()]; }
	//! @brief If the given key is newly released in the current frame.
	inline bool keyReleased(Key key) const { return !keys_.current[key.index()] && keys_.previous[key.index()]; }
	//! @brief If the given key is tapped in the current frame.
	inline bool keyTapped(Key key) const {
		return keyReleased(key) && (time_.release[key.index()] - time_.press[key.index()]) <= time_.tap;
	}
	//! @brief If the given key is currently held down for at least the configured amount of time.
	inline bool keyHeld(Key key) const {
		return keys_.current[key.index()] && (time_.last - time_.press[key.index()]) >= time_.hold;
	}

	//! @brief The current mask of modifier keys.
	inline ModKeyMask modifiers() const { return mods_.current; }
	//! @brief The mask of modifier keys in the last frame.
	inline ModKeyMask prevModifiers() const { return mods_.previous; }

	//! @brief The time a key will be pressed down for to be considered "held".
	inline TimeSpan holdTime() const { return TimeSpan::Seconds(time_.hold); }
	//! @brief The time between a key press and release to be considered a tap.
	inline TimeSpan tapTime() const { return TimeSpan::Seconds(time_.tap); }
	//! @brief Sets the time for hold events.
	inline void holdTime(TimeSpan time) { time_.hold = float(time.seconds()); }
	//! @brief Sets the time for tap events.
	inline void tapTime(TimeSpan time) { time_.tap = float(time.seconds()); }

private:
	Keyboard(Window* window);

	void nextFrame();
	void handleKey(int keycode, int scancode, int action, int mods);

	static Key KeyFromSystemValue(int value);

private:
	Window* const window_;
	struct
	{
		std::array<bool, Keys::KEY_COUNT> current{ };
		std::array<bool, Keys::KEY_COUNT> previous{ };
		std::set<Key> pressed{ };
	} keys_;
	struct
	{
		ModKeyMask current{ };
		ModKeyMask previous{ };
	} mods_;
	struct
	{
		float last{ };
		std::array<float, Keys::KEY_COUNT> press{ };
		std::array<float, Keys::KEY_COUNT> release{ };
		float hold{ };
		float tap{ };
	} time_;

	VEGA_NO_COPY(Keyboard)
	VEGA_NO_MOVE(Keyboard)
}; // class Keyboard

} // namespace vega
