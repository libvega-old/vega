/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

 //! @file TexelBuffer.hpp Random access texel data for shaders.

#include "../Common.hpp"
#include "./TexelFormat.hpp"


namespace vega
{

class StagingBuffer;

//! @brief A buffer of read-only texel data accessible in shaders - similar to a 1D texture, but can be much larger.
class VEGA_API TexelBuffer
{
	friend class TexelBufferImpl;

public:
	virtual ~TexelBuffer();

	//! @brief The number of texels in the buffer.
	uint32 texelCount() const;
	//! @brief The format of the texels in the buffer.
	TexelFormat format() const;
	//! @brief The size of the buffer data, in bytes.
	uint64 size() const;

	//! @brief Updates the texel data in the buffer.
	//! @param data The data to upload to the buffer.
	//! @param texelCount The number of texels to update with the data.
	//! @param texelOffset The offset into the buffer data to start updating data at, in texels.
	void setData(const void* data, uint32 texelCount, uint32 texelOffset = 0);
	//! @brief Updates the texel data in the buffer.
	//! @param data The buffer of host data to upload to the texel buffer.
	//! @param texelCount The number of texels to update with the data.
	//! @param dataOffset The offset in @p data to start updating data from, in bytes.
	//! @param texelOffset The offset into the buffer data to start updating data at, in texels.
	void setData(const StagingBuffer& data, uint32 texelCount, uint64 dataOffset = 0, uint32 texelOffset = 0);

private:
	TexelBuffer();

	VEGA_NO_COPY(TexelBuffer)
	VEGA_NO_MOVE(TexelBuffer)
}; // class TexelBuffer

} // namespace vega
