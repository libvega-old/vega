/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Texture.hpp Texture types for sampling texel data in shaders.

#include "../Common.hpp"
#include "../Math/Vec3.hpp"
#include "./TexelFormat.hpp"
#include "../Asset/AssetBase.hpp"


namespace vega
{

class StagingBuffer;

//! @brief Collection of read-only 1D, 2D, or 3D texel data that can be sampled in shaders.
class VEGA_API Texture
	: public AssetBase
{
	friend class TextureImpl;

public:
	//! @brief Texture data dimensionalities.
	enum class Dims : uint32
	{
		//! @brief Texture data is 1-dimensional.
		D1 = 0,
		//! @brief Texture data is 2-dimensional.
		D2,
		//! @brief Texture data is 3-dimensional.
		D3
	}; // enum class Dims

	virtual ~Texture();

	//! @brief The dimensionality (also known as rank) of the texture data.
	Dims dimensions() const;
	//! @brief Gets if the texture data is 1D.
	inline bool is1D() const { return dimensions() == Dims::D1; }
	//! @brief Gets if the texture data is 2D.
	inline bool is2D() const { return dimensions() == Dims::D2; }
	//! @brief Gets if the texture data is 3D.
	inline bool is3D() const { return dimensions() == Dims::D3; }

	//! @brief The size of the texture, in texels.
	const Vec3ui& size() const;
	//! @brief The width (x-size) of the data, in texels.
	inline uint32 width() const { return size().x; }
	//! @brief The height (y-size) of the data, in texels.
	inline uint32 height() const { return size().y; }
	//! @brief The depth (z-size) of the data, in texels.
	inline uint32 depth() const { return size().z; }

	//! @brief The format of the texel data.
	TexelFormat format() const;
	
	//! @brief Gets if the texture has been given data. It is an error to use textures without data.
	bool hasData() const;
	//! @brief Updates the texel data in the given texture region.
	//! @param data The data to upload to the texture.
	//! @param x The starting x-coordinate of the update region, in texels.
	//! @param width The width of the update region, in texels.
	void setData(const void* data, uint32 x, uint32 width);
	//! @brief Updates the texel data in the given texture region.
	//! @param data The host data buffer to upload to the texture.
	//! @param x The starting x-coordinate of the update region, in texels.
	//! @param width The width of the update region, in texels.
	//! @param dataOffset The offset into @p data to start uploading data from, in bytes.
	void setData(const StagingBuffer& data, uint32 x, uint32 width, uint64 dataOffset = 0);
	//! @brief Updates the texel data in the given texture region.
	//! @param data The data to upload to the texture.
	//! @param x The starting x-coordinate of the update region, in texels.
	//! @param y The starting y-coordinate of the update region, in texels.
	//! @param width The width of the update region, in texels.
	//! @param height The height of the update region, in texels.
	void setData(const void* data, uint32 x, uint32 y, uint32 width, uint32 height);
	//! @brief Updates the texel data in the given texture region.
	//! @param data The host data buffer to upload to the texture.
	//! @param x The starting x-coordinate of the update region, in texels.
	//! @param y The starting y-coordinate of the update region, in texels.
	//! @param width The width of the update region, in texels.
	//! @param height The height of the update region, in texels.
	//! @param dataOffset The offset into @p data to start uploading data from, in bytes.
	void setData(const StagingBuffer& data, uint32 x, uint32 y, uint32 width, uint32 height, uint64 dataOffset = 0);
	//! @brief Updates the texel data in the given texture region.
	//! @param data The data to upload to the texture.
	//! @param x The starting x-coordinate of the update region, in texels.
	//! @param y The starting y-coordinate of the update region, in texels.
	//! @param z The starting z-coordinate of the update region, in texels.
	//! @param width The width of the update region, in texels.
	//! @param height The height of the update region, in texels.
	//! @param depth The depth of the update region, in texels.
	void setData(const void* data, uint32 x, uint32 y, uint32 z, uint32 width, uint32 height, uint32 depth);
	//! @brief Updates the texel data in the given texture region.
	//! @param data The host data buffer to upload to the texture.
	//! @param x The starting x-coordinate of the update region, in texels.
	//! @param y The starting y-coordinate of the update region, in texels.
	//! @param z The starting z-coordinate of the update region, in texels.
	//! @param width The width of the update region, in texels.
	//! @param height The height of the update region, in texels.
	//! @param depth The depth of the update region, in texels.
	//! @param dataOffset The offset into @p data to start uploading data from, in bytes.
	void setData(const StagingBuffer& data, uint32 x, uint32 y, uint32 z, uint32 width, uint32 height, uint32 depth, 
		uint64 dataOffset = 0);

private:
	Texture();

	VEGA_NO_COPY(Texture)
	VEGA_NO_MOVE(Texture)
}; // class Texture

//! @brief Gets the string representation of the texture dims.
VEGA_API const String& to_string(Texture::Dims dims);

} // namespace vega
