/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file IndexBuffer.hpp Buffer for indexed rendering

#include "../Common.hpp"


namespace vega
{

class StagingBuffer;

//! @brief Data types for index values.
struct IndexType final
{
public:
	constexpr IndexType() : value_{ 0 } { }

	//! @brief The unique value specifying the index type.
	VEGA_API inline constexpr uint32 value() const { return value_; }
	//! @brief Gets a string representation of the index type.
	VEGA_API const String& str() const;
	//! @brief The size of a single element of the index type, in bytes.
	VEGA_API uint32 size() const;

	VEGA_API inline constexpr bool operator == (IndexType r) const { return value_ == r.value_; }
	VEGA_API inline constexpr bool operator != (IndexType r) const { return value_ != r.value_; }
	VEGA_API inline constexpr bool operator <  (IndexType r) const { return value_ < r.value_; }

private:
	constexpr IndexType(uint32 value) : value_{ value } { }

private:
	uint32 value_;

public:
	static const IndexType U16, U32;
}; // struct IndexType

//! @brief Index data is unsigned 16-bit integers.
inline constexpr IndexType IndexType::U16{ 0 };
//! @brief Index data is unsigned 32-bit integers.
inline constexpr IndexType IndexType::U32{ 1 };


//! @brief Buffer of indices to feed indexed draw commands.
class VEGA_API IndexBuffer
{
	friend class IndexBufferImpl;

public:
	virtual ~IndexBuffer();

	//! @brief Type of index data in the buffer.
	IndexType indexType() const;
	//! @brief Number of indices in the buffer.
	uint32 indexCount() const;

	//! @brief The size of the buffer, in bytes.
	uint64 size() const;

	//! @brief Updates the index data in the buffer.
	//! @param data The data to upload to the buffer.
	//! @param indexCount The number of indices to update with the data.
	//! @param indexOffset The offset into the buffer data to start updating data at, in indices.
	void setData(const void* data, uint32 indexCount, uint32 indexOffset = 0);
	//! @brief Updates the index data in the buffer.
	//! @param data The buffer of host data to upload to the index buffer.
	//! @param indexCount The number of indices to update with the data.
	//! @param dataOffset The offset in @p data to start updating data from, in bytes.
	//! @param indexOffset The offset into the buffer data to start updating data at, in indices.
	void setData(const StagingBuffer& data, uint32 indexCount, uint64 dataOffset = 0, uint32 indexOffset = 0);

private:
	IndexBuffer();

	VEGA_NO_COPY(IndexBuffer)
	VEGA_NO_MOVE(IndexBuffer)
}; // class IndexBuffer

}
