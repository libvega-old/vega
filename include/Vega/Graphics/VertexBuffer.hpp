/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file VertexBuffer.hpp Vertex data storage for rendering.

#include "../Common.hpp"
#include "./VertexLayout.hpp"


namespace vega
{

class StagingBuffer;

//! @brief Buffer of vertex data for feeding draw commands.
class VEGA_API VertexBuffer
{
	friend class VertexBufferImpl;

public:
	virtual ~VertexBuffer();

	//! @brief Layout of the vertex data in the buffer.
	const VertexLayout& layout() const;
	//! @brief Number of vertices in the buffer.
	uint32 vertexCount() const;

	//! @brief The size of the buffer, in bytes.
	uint64 size() const;

	//! @brief Updates the vertex data in the buffer.
	//! @param data The data to upload to the buffer.
	//! @param vertexCount The number of vertices to update with the data.
	//! @param vertexOffset The offset into the buffer data to start updating data at, in vertices.
	void setData(const void* data, uint32 vertexCount, uint32 vertexOffset = 0);
	//! @brief Updates the vertex data in the buffer.
	//! @param data The buffer of host data to upload to the vertex buffer.
	//! @param vertexCount The number of vertices to update with the data.
	//! @param dataOffset The offset in @p data to start updating data from, in bytes.
	//! @param vertexOffset The offset into the buffer data to start updating data at, in vertices.
	void setData(const StagingBuffer& data, uint32 vertexCount, uint64 dataOffset = 0, uint32 vertexOffset = 0);

private:
	VertexBuffer();

	VEGA_NO_COPY(VertexBuffer)
	VEGA_NO_MOVE(VertexBuffer)
}; // class VertexBuffer

} // namespace vega
