/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file GraphicsDevice.hpp Physical graphics device reflection and info.

#include "../Common.hpp"
#include "./Multisampling.hpp"


namespace vega
{

//! @brief The set of properties and limits for a GraphicsDevice.
struct VEGA_API DeviceProperties final
{
	//! @brief A mask of the supported MSAA levels for Renderer targets.
	MSAAMask supportedMSAA{ MSAA::X1 };
	//! @brief Maximum size of a 1D Texture.
	uint32 maxImageSize1D{ 0 };
	//! @brief Maximum size of a 2D Texture.
	uint32 maxImageSize2D{ 0 };
	//! @brief Maximum size of a 3D Texture.
	uint32 maxImageSize3D{ 0 };
	//! @brief Maximum size of a StorageBuffer, in bytes.
	uint32 maxStorageBufferSize{ 0 };
	//! @brief Maximum number of texels for a TexelBuffer.
	uint32 maxTexelBufferCount{ 0 };
	//! @brief Maximum width of a Renderer instance.
	uint32 maxFramebufferWidth{ 0 };
	//! @brief Maximum height of the Renderer instance.
	uint32 maxFramebufferHeight{ 0 };
	//! @brief The minimum supported uniform data alignment.
	uint64 uniformAlignment{ 0 };
}; // struct GraphicsProperties


//! @brief Graphics device processor types.
enum class DeviceType : uint32
{
	//! @brief The device is a physical processor that is distinct and separate from the CPU.
	Discrete = 0,
	//! @brief The device is a physical processor that is tightly coupled with, or is the same as, the CPU.
	Integrated,
	//! @brief The device is emulated in software, or is otherwise virtualized.
	Virtual
}; // enum class DeviceType

//! @brief Gets the string representation of the device type
VEGA_API String to_string(DeviceType type);


//! @brief Contains information about a specific graphics device vendor.
struct VEGA_API DeviceVendor final
{
	friend class GraphicsDevice;

public:
	DeviceVendor() : value_{ Unknown.value_ } { }

	//! @brief The name of the vendor.
	const String& name() const;
	//! @brief The unique integer ID assigned to the vendor.
	inline uint32 id() const { return value_; }
	//! @brief If the vendor is a known and valid vendor.
	inline bool isValid() const { return name() != "Unknown"; }

	inline bool operator == (DeviceVendor r) const { return value_ == r.value_; }
	inline bool operator != (DeviceVendor r) const { return value_ != r.value_; }
	inline bool operator <  (DeviceVendor r) const { return value_ < r.value_; }

public:
	//! @brief Unknown or invalid graphics vendor.
	static const DeviceVendor Unknown;
	//! @brief AMD - Advanced Micro Devices
	static const DeviceVendor AMD;
	//! @brief ARM
	static const DeviceVendor ARM;
	//! @brief ImgTec - Imagination Technologies
	static const DeviceVendor ImgTec;
	//! @brief Intel
	static const DeviceVendor Intel;
	//! @brief Nvidia
	static const DeviceVendor Nvidia;
	//! @brief Qualcomm
	static const DeviceVendor Qualcomm;

private:
	DeviceVendor(uint32 value) : value_{ value } { }

private:
	uint32 value_;
}; // struct DeviceVendor


//! @brief Reflects a physical or virtual graphics processing device.
class VEGA_API GraphicsDevice final
{
	friend class GraphicsEngineImpl;

public:
	~GraphicsDevice() { }

	//! @brief The type of the device.
	inline DeviceType type() const { return type_; }
	//! @brief The vendor of the device.
	inline DeviceVendor vendor() const { return vendor_; }
	//! @brief The manufacturer provided name of the device.
	inline const String& name() const { return name_; }
	//! @brief The set of properties and limits for the device.
	inline const DeviceProperties* properties() const { return &properties_; }

	//! @brief A list of all valid graphics devices on the current system. Must be called from main thread only.
	static const std::vector<UPtr<const GraphicsDevice>>& AllDevices();
	//! @brief The system default device, if any are supported. Must be called from main thread only. 
	static const GraphicsDevice* DefaultDevice();

private:
	GraphicsDevice(uint64 handle);

private:
	const uint64 handle_;
	DeviceType type_;
	DeviceVendor vendor_;
	String name_;
	DeviceProperties properties_;

	VEGA_NO_COPY(GraphicsDevice)
	VEGA_NO_MOVE(GraphicsDevice)
}; // class GraphicsDevice

} // namespace vega
