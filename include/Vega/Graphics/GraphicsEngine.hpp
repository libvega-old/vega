/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file GraphicsEngine.hpp Core logic and management for graphics operations.

#include "../Common.hpp"
#include "./GraphicsDevice.hpp"
#include "./StagingBuffer.hpp"
#include "./Multisampling.hpp"
#include "../Core/Flags.hpp"
#include "../Math/Vec2.hpp"


namespace vega
{

class IndexBuffer; struct IndexType;
class VertexBuffer; class VertexLayout;
class TexelBuffer; struct TexelFormat;
class StorageBuffer;
class Texture;
class RenderLayoutBuilder; enum class Subpass : uint16; class RenderLayout;
class Renderer;
class Pipeline; class RenderState;
class Shader;

//! @brief Core management of an active connection to a specific GraphicsDevice and the associated functionality.
class VEGA_API GraphicsEngine
{
	friend class GraphicsEngineImpl;

public:
	//! @brief While virtual, this type is not indended to be derived from.
	virtual ~GraphicsEngine();

	//! @brief The GraphicsDevice instance used by the engine.
	const GraphicsDevice* device() const;
	//! @brief The properties of the GraphicsDevice used by the engine.
	inline const DeviceProperties* properties() const { return device()->properties(); }

	//! @brief Gets if the calling thread can perform graphics operations (caller is main thread or AppThread).
	bool isThreadRegistered() const;

	//! @brief Create a new host-visible staging buffer for efficient transfers.
	//! @param size The size of the buffer, in bytes.
	UPtr<StagingBuffer> createStagingBuffer(uint64 size);
	//! @brief Create a new IndexBuffer with uninitialized memory.
	//! @param indexType The index type.
	//! @param indexCount The number of indices.
	UPtr<IndexBuffer> createIndexBuffer(const IndexType& indexType, uint32 indexCount);
	//! @brief Create a new IndexBuffer with initialized memory.
	//! @param indexType The index type.
	//! @param indexCount The number of indices.
	//! @param buffer The initial data to upload to the buffer.
	//! @param dataOffset The offset, in bytes, into @p buffer to start copying data from.
	UPtr<IndexBuffer> createIndexBuffer(const IndexType& indexType, uint32 indexCount,
		const StagingBuffer& buffer, uint64 dataOffset = 0);
	//! @brief Create a new VertexBuffer with uninitialized memory.
	//! @param layout The vertex layout.
	//! @param vertexCount The number of vertices.
	UPtr<VertexBuffer> createVertexBuffer(const VertexLayout& layout, uint32 vertexCount);
	//! @brief Create a new VertexBuffer with initialized memory.
	//! @param layout The vertex layout.
	//! @param vertexCount The number of vertices.
	//! @param buffer The initial data to upload to the buffer.
	//! @param dataOffset The offset, in bytes, into @p buffer to start copying data from.
	UPtr<VertexBuffer> createVertexBuffer(const VertexLayout& layout, uint32 vertexCount,
		const StagingBuffer& buffer, uint64 dataOffset = 0);
	//! @brief Create a new read-only TexelBuffer with uninitialized memory.
	//! @param format The format of the buffer texels.
	//! @param texelCount The number of texels.
	UPtr<TexelBuffer> createROTexelBuffer(const TexelFormat& format, uint32 texelCount);
	//! @brief Create a new read-only TexelBuffer with initialized memory.
	//! @param format The format of the buffer texels.
	//! @param texelCount The number of texels.
	//! @param buffer The initial data to upload to the buffer.
	//! @param dataOffset The offset, in bytes, into @p buffer to start copying data from.
	UPtr<TexelBuffer> createROTexelBuffer(const TexelFormat& format, uint32 texelCount,
		const StagingBuffer& buffer, uint64 dataOffset = 0);
	//! @brief Create a new read-only StorageBuffer with uninitialized memory.
	//! @param elementSize The size of each buffer element, in bytes.
	//! @param elementCount The number of buffer elements.
	UPtr<StorageBuffer> createROStorageBuffer(uint32 elementSize, uint32 elementCount);
	//! @brief Create a new read-only StorageBuffer with initialized memory.
	//! @param elementSize The size of each buffer element, in bytes.
	//! @param elementCount The number of buffer elements.
	//! @param buffer The initial data to upload to the buffer.
	//! @param dataOffset The offset, in bytes, into @p buffer to start copying data from.
	UPtr<StorageBuffer> createROStorageBuffer(uint32 elementSize, uint32 elementCount,
		const StagingBuffer& buffer, uint64 dataOffset = 0);

	//! @brief Creates a new 1D texture with uninitialized data.
	//! @param format The format of the texels.
	//! @param width The number of texels.
	UPtr<Texture> create1DTexture(const TexelFormat& format, uint32 width);
	//! @brief Creates a new 1D texture with uninitialized data.
	//! @param format The format of the texels.
	//! @param width The number of texels.
	//! @param buffer The initial data to upload to the texture.
	//! @param dataOffset The offset, in bytes, into @p buffer to stary copying data from.
	UPtr<Texture> create1DTexture(const TexelFormat& format, uint32 width,
		const StagingBuffer& buffer, uint64 dataOffset = 0);
	//! @brief Creates a new 2D texture with uninitialized data.
	//! @param format The format of the texels.
	//! @param width The width of the 2D texture.
	//! @param height The height of the 2D texture.
	UPtr<Texture> create2DTexture(const TexelFormat& format, uint32 width, uint32 height);
	//! @brief Creates a new 2D texture with uninitialized data.
	//! @param format The format of the texels.
	//! @param width The width of the 2D texture.
	//! @param height The height of the 2D texture.
	//! @param buffer The initial data to upload to the texture.
	//! @param dataOffset The offset, in bytes, into @p buffer to stary copying data from.
	UPtr<Texture> create2DTexture(const TexelFormat& format, uint32 width, uint32 height,
		const StagingBuffer& buffer, uint64 dataOffset = 0);
	//! @brief Creates a new 3D texture with uninitialized data.
	//! @param format The format of the texels.
	//! @param width The width of the 3D texture.
	//! @param height The height of the 3D texture.
	//! @param depth The depth of the 3D texture.
	UPtr<Texture> create3DTexture(const TexelFormat& format, uint32 width, uint32 height, uint32 depth);
	//! @brief Creates a new 3D texture with uninitialized data.
	//! @param format The format of the texels.
	//! @param width The width of the 3D texture.
	//! @param height The height of the 3D texture.
	//! @param depth The depth of the 3D texture.
	//! @param buffer The initial data to upload to the texture.
	//! @param dataOffset The offset, in bytes, into @p buffer to stary copying data from.
	UPtr<Texture> create3DTexture(const TexelFormat& format, uint32 width, uint32 height, uint32 depth,
		const StagingBuffer& buffer, uint64 dataOffset = 0);

	//! @brief Creates a blank RenderLayoutBuilder for describing a new RenderLayout.
	//! @param subpassCount The number of subpasses in the layout.
	//! @param msaaMask The mask of subpasses that support MSAA operations.
	UPtr<RenderLayoutBuilder> startNewRenderLayout(uint32 subpassCount, Flags<Subpass> msaaMask);
	//! @brief Create a new offscreen Renderer with the given layout and paramters.
	//! @param layout The layout to use for the Renderer.
	//! @param extent The initial size of the Renderer surface (in pixels).
	//! @param msaa The initial Renderer msaa. The layout must support MSAA operations.
	UPtr<Renderer> createRenderer(const SPtr<RenderLayout>& layout, const Vec2ui& extent, MSAA msaa = MSAA::X1);

	//! @brief Create a new Pipeline from the given Shader and RenderState.
	//! @param shader The shader to use when the Pipeline is bound.
	//! @param state The set of rendering states to enable when the Pipeline is bound.
	UPtr<Pipeline> createPipeline(const Shader& shader, const RenderState& state);

	//! @brief The active engine instance, if any. Same as AppBase::Get()->Graphics().
	inline static GraphicsEngine* Get() { return Instance_; }

	//! @brief Throws an exception if the calling thread is not registered for graphics operations.
	inline static void AssertThreadRegistered(const String& message = {}) {
		if (Instance_ && !Instance_->isThreadRegistered()) {
			if (!message.empty()) {
				throw std::runtime_error(to_string("Graphics thread assert failed - %s", message.c_str()));
			}
			else {
				throw std::runtime_error(
					"Graphics thread assert failed - cannot perform graphics operations on unregistered thread"
				);
			}
		}
	}

private:
	GraphicsEngine();

private:
	static GraphicsEngine* Instance_;

	VEGA_NO_COPY(GraphicsEngine)
	VEGA_NO_MOVE(GraphicsEngine)
}; // class GraphicsEngine

} // namespace vega
