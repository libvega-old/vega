/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file StorageBuffer.hpp Random access structured data for shaders.

#include "../Common.hpp"


namespace vega
{

class StagingBuffer;

//! @brief A buffer of read-only, random-access structured data available in shaders.
class VEGA_API StorageBuffer
{
	friend class StorageBufferImpl;

public:
	virtual ~StorageBuffer();

	//! @brief The size of a single element of structured data, in bytes.
	uint32 elementSize() const;
	//! @brief The number of elements in the buffer.
	uint32 elementCount() const;
	//! @brief The size of the buffer data, in bytes.
	uint64 size();

	//! @brief Updates the structured data in the buffer.
	//! @param data The data to upload to the buffer.
	//! @param elementCount The number of elements to update with the data.
	//! @param elementOffset The offset into the buffer data to start updating data at, in elements.
	void setData(const void* data, uint32 elementCount, uint32 elementOffset = 0);
	//! @brief Updates the structured data in the buffer.
	//! @param data The buffer of host data to upload to the storage buffer.
	//! @param elementCount The number of elements to update with the data.
	//! @param dataOffset The offset in @p data to start updating data from, in bytes.
	//! @param elementOffset The offset into the buffer data to start updating data at, in elements.
	void setData(const StagingBuffer& data, uint32 elementCount, uint64 dataOffset = 0, uint32 elementOffset = 0);

private:
	StorageBuffer();

	VEGA_NO_COPY(StorageBuffer)
	VEGA_NO_MOVE(StorageBuffer)
}; // class StorageBuffer

} // namespace vega
