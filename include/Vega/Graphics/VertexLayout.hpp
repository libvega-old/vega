/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file VertexLayout.hpp Vertex data usage and layouts.

#include "../Common.hpp"
#include "./VertexFormat.hpp"
#include "../Math/Hash.hpp"
#include "../Core/InlineVector.hpp"

#include <array>


namespace vega
{

//! @brief Usage semantics for shader vertex inputs.
struct VertexUse final
{
	friend struct VertexElement;

public:
	//! @brief The total number of usage semantics.
	inline static constexpr uint32 COUNT{ 32u };

	//! @brief Construct default vertex use (POS0).
	constexpr VertexUse() noexcept : value_{ 0 } { }

	//! @brief The underlying unique numeric value for the use.
	VEGA_API inline constexpr uint32 value() const { return value_; }
	//! @brief Gets a string representation of the use.
	VEGA_API const String& str() const;
	//! @brief Gets if the usage represents instance-rate data for use in instanced drawing.
	VEGA_API inline bool isInstanceUse() const {
		return (value_ >= INST0.value()) && (value_ <= INST3.value());
	}

	inline constexpr bool operator == (VertexUse r) const { return value_ == r.value_; }
	inline constexpr bool operator != (VertexUse r) const { return value_ == r.value_; }
	inline constexpr bool operator <  (VertexUse r) const { return value_ < r.value_; }

private:
	constexpr VertexUse(uint32 value) noexcept : value_{ value } { }

private:
	uint32 value_;

public:
	static const VertexUse
		POS0, POS1, COLOR0, COLOR1, COLOR2, COLOR3, NORM0, NORM1, TAN0, TAN1, BINORM0, BINORM1, UV0, UV1, UV2, UV3,
		BWEIGHT0, BWEIGHT1, BWEIGHT2, BWEIGHT3, BINDEX0, BINDEX1, BINDEX2, BINDEX3, USER0, USER1, USER2, USER3, INST0,
		INST1, INST2, INST3;
}; // struct VertexUse

//! @brief Vertex position, index 0.
inline constexpr VertexUse VertexUse::POS0    { 0 };
//! @brief Vertex position, index 1.
inline constexpr VertexUse VertexUse::POS1    { 1 };
//! @brief Vertex color, index 0.
inline constexpr VertexUse VertexUse::COLOR0  { 2 };
//! @brief Vertex color, index 1.
inline constexpr VertexUse VertexUse::COLOR1  { 3 };
//! @brief Vertex color, index 2.
inline constexpr VertexUse VertexUse::COLOR2  { 4 };
//! @brief Vertex color, index 3.
inline constexpr VertexUse VertexUse::COLOR3  { 5 };
//! @brief Vertex normal, index 0.
inline constexpr VertexUse VertexUse::NORM0   { 6 };
//! @brief Vertex normal, index 1.
inline constexpr VertexUse VertexUse::NORM1   { 7 };
//! @brief Vertex tangent, index 0.
inline constexpr VertexUse VertexUse::TAN0    { 8 };
//! @brief Vertex tangent, index 1.
inline constexpr VertexUse VertexUse::TAN1    { 9 };
//! @brief Vertex bi-normal, index 0.
inline constexpr VertexUse VertexUse::BINORM0 { 10 };
//! @brief Vertex bi-normal, index 1.
inline constexpr VertexUse VertexUse::BINORM1 { 11 };
//! @brief Vertex UV texture coordinate, index 0.
inline constexpr VertexUse VertexUse::UV0     { 12 };
//! @brief Vertex UV texture coordinate, index 1.
inline constexpr VertexUse VertexUse::UV1     { 13 };
//! @brief Vertex UV texture coordinate, index 2.
inline constexpr VertexUse VertexUse::UV2     { 14 };
//! @brief Vertex UV texture coordinate, index 3.
inline constexpr VertexUse VertexUse::UV3     { 15 };
//! @brief Vertex blend weight, index 0.
inline constexpr VertexUse VertexUse::BWEIGHT0{ 16 };
//! @brief Vertex blend weight, index 1.
inline constexpr VertexUse VertexUse::BWEIGHT1{ 17 };
//! @brief Vertex blend weight, index 2.
inline constexpr VertexUse VertexUse::BWEIGHT2{ 18 };
//! @brief Vertex blend weight, index 3.
inline constexpr VertexUse VertexUse::BWEIGHT3{ 19 };
//! @brief Vertex blend index, index 0.
inline constexpr VertexUse VertexUse::BINDEX0 { 20 };
//! @brief Vertex blend index, index 1.
inline constexpr VertexUse VertexUse::BINDEX1 { 21 };
//! @brief Vertex blend index, index 2.
inline constexpr VertexUse VertexUse::BINDEX2 { 22 };
//! @brief Vertex blend index, index 3.
inline constexpr VertexUse VertexUse::BINDEX3 { 23 };
//! @brief Vertex user-specified data, index 0.
inline constexpr VertexUse VertexUse::USER0   { 24 };
//! @brief Vertex user-specified data, index 1.
inline constexpr VertexUse VertexUse::USER1   { 25 };
//! @brief Vertex user-specified data, index 2.
inline constexpr VertexUse VertexUse::USER2   { 26 };
//! @brief Vertex user-specified data, index 3.
inline constexpr VertexUse VertexUse::USER3   { 27 };
//! @brief Instance-rate data, index 0.
inline constexpr VertexUse VertexUse::INST0   { 28 };
//! @brief Instance-rate data, index 1.
inline constexpr VertexUse VertexUse::INST1   { 29 };
//! @brief Instance-rate data, index 2.
inline constexpr VertexUse VertexUse::INST2   { 30 };
//! @brief Instance-rate data, index 3.
inline constexpr VertexUse VertexUse::INST3   { 31 };


//! @brief Represents a mask of VertexUse values.
struct VEGA_API VertexUseMask final
{
public:
	constexpr VertexUseMask() noexcept : mask_{ 0 } { }
	constexpr VertexUseMask(VertexUse use) noexcept : mask_{ 1u << use.value() } { }

	//! @brief The underlying mask value.
	inline constexpr uint32 mask() const { return mask_; }

	//! @brief Sets the use flag in the mask.
	inline void set(VertexUse use) { mask_ |= (1u << use.value()); }
	//! @brief Clears the use flag in the mask.
	inline void clear(VertexUse use) { mask_ &= ~(1u << use.value()); }

	//! @brief Checks if any uses are in the mask.
	inline constexpr explicit operator bool () const { return !!mask_; }
	inline constexpr VertexUseMask operator | (VertexUseMask r) const { return { mask_ | r.mask_ }; }
	inline constexpr VertexUseMask operator & (VertexUseMask r) const { return { mask_ & r.mask_ }; }
	inline VertexUseMask operator |= (VertexUseMask r) { mask_ |= r.mask_; return *this; }
	inline VertexUseMask operator &= (VertexUseMask r) { mask_ &= r.mask_; return *this; }

private:
	constexpr VertexUseMask(uint32 mask) noexcept : mask_{ mask } { }

private:
	uint32 mask_;
}; // struct VertexUseMask

inline constexpr VertexUseMask operator | (VertexUse l, VertexUse r) { return VertexUseMask{ l } | r; }
inline constexpr VertexUseMask operator & (VertexUse l, VertexUse r) { return VertexUseMask{ l } & r; }


//! @brief Describes a single vertex element within a buffer data layout.
struct VEGA_API VertexElement final
{
public:
	constexpr VertexElement() noexcept : packed_{ 0 } { }
	//! @brief Describe a new element.
	//! @param use The usage semantic for the element.
	//! @param format The format for the element.
	//! @param offset The offset of the element into the vertex data.
	//! @param arraySize The size of the element array, or 1 for elements that aren't arrays.
	constexpr VertexElement(VertexUse use, VertexFormat format, uint16 offset, uint32 arraySize = 1) noexcept
		: packed_{ 0 }
	{
		use_ = uint32(use.value());
		format_ = uint32(format.value());
		arraySize_ = math::max(arraySize, 1u);
		offset_ = offset;
	}

	//! @brief The usage semantic for the element.
	inline constexpr VertexUse use() const { return VertexUse(use_); }
	//! @brief The usage semantic for the element.
	inline void use(VertexUse use) { use_ = uint32(use.value()); }
	//! @brief The format of the element data.
	inline constexpr VertexFormat format() const { return VertexFormat(format_); }
	//! @brief The format of the element data.
	inline void format(VertexFormat format) { format_ = uint32(format.value()); }
	//! @brief The number of items in the element array, or 1 if not an array.
	inline constexpr uint32 arraySize() const { return arraySize_; }
	//! @brief The number of items in the element array, or 1 if not an array.
	inline void arraySize(uint32 arraySize) { arraySize_ = math::max(arraySize, 1u); }
	//! @brief The offset, in bytes, from the start of the vertex data to the element.
	inline constexpr uint16 offset() const { return offset_; }
	//! @brief The offset, in bytes, from the start of the vertex data to the element.
	inline void offset(uint16 offset) { offset_ = offset; }

private:
	union
	{
		struct {
			uint32 use_ : 5;
			uint32 format_ : 6;
			uint32 arraySize_ : 5;
			uint32 offset_ : 16;
		};
		uint32 packed_;
	};
}; // struct VertexElement


//! @brief Describes the layout of a collection of vertex elements within a vertex buffer.
class VEGA_API VertexLayout final
{
public:
	//! @brief Maximum number of elements supported in a single layout.
	inline static constexpr uint32 MAX_ELEMENTS{ 16u };
	//! @brief List of vertex elements.
	using ElementVector = InlineVector<VertexElement, MAX_ELEMENTS>;

	//! @brief Construct a new vertex layout. Invalid layouts will throw \c std::invalid_argument.
	//! 
	//! Layouts will be invalid if:
	//!   - More than MAX_ELEMENTS elements are given, or zero elements are given.
	//!   - Two or more elements have the same VertexUse.
	//!   - There is a mix of vertex-rate and instance-rate VertexUse elements.
	//! @param elems The elements in the layout.
	//! @param stride The explicit layout stride, in bytes. Stride is calculated automatically if not specified.
	VertexLayout(const std::initializer_list<VertexElement>& elems, Opt<uint32> stride = std::nullopt);

	//! @brief The layout hash.
	inline Hash64 hash() const { return hash_; }
	//! @brief The collection of elements for the layout.
	inline const ElementVector& elements() const { return elements_; }
	//! @brief The stride of the vertex data, in bytes.
	inline uint32 stride() const { return stride_; }
	//! @brief The mask of element usage semantics provided by the layout.
	inline VertexUseMask useMask() const { return useMask_; }

private:
	Hash64 hash_;
	ElementVector elements_;
	uint32 stride_;
	VertexUseMask useMask_;
}; // class VertexLayout

} // namespace vega


template <> struct std::hash<vega::VertexUse> final
{
	size_t operator () (vega::VertexUse format) const {
		static const std::hash<vega::uint32> HASH{}; return HASH(format.value());
	}
}; // struct std::hash<vega::VertexUse>
