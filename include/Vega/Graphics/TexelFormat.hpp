/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

 //! @file TexelFormat.hpp Formats for image texel data.

#include "../Common.hpp"


namespace vega
{

//! @brief Describes a specific data format for objects that contain texel data.
struct VEGA_API TexelFormat final
{
	friend class TexelFormats;

private:
	struct FormatInfo final
	{
		uint32 format{ 0 };
		uint16 size{ 0 };
		uint8 dims{ 0 };
		struct
		{
			uint8 c : 1;
			uint8 d : 1;
			uint8 s : 1;
			uint8 p : 1;
		} flags{ 0 };
	}; // struct FormatInfo

public:
	//! @brief Constructs the default undefined format.
	constexpr TexelFormat() : value_{ 0 } { }

	//! @brief The unique integer identifier for the format.
	inline constexpr uint32 value() const { return value_; }
	//! @brief Gets a string representation of the format.
	const String& str() const;
	//! @brief The number of channels in the format.
	inline uint32 componentCount() const { return info().dims; }
	//! @brief The size of a single texel of the format.
	inline uint32 size() const { return info().size; }
	//! @brief Gets if the format is a color format.
	inline bool isColor() const { return info().flags.c; }
	//! @brief Gets if the format is a depth or depth/stencil format.
	inline bool isDepth() const { return info().flags.d; }
	//! @brief Gets if the format has a stencil component.
	inline bool hasStencil() const { return info().flags.s; }
	//! @brief Gets if the format has bit-packed data.
	inline bool isPacked() const { return info().flags.p; }

	//! @brief A different format identifier used internally. Not unique.
	inline uint32 formatId() const { return info().format; }

	inline constexpr bool operator == (TexelFormat r) const { return value_ == r.value_; }
	inline constexpr bool operator != (TexelFormat r) const { return value_ != r.value_; }
	inline constexpr bool operator <  (TexelFormat r) const { return value_ < r.value_; }

	//! @brief Gets the TexelFormat object for the id, or Undefined if not a valid id.
	static TexelFormat FromId(uint32 id);

private:
	constexpr TexelFormat(uint32 value) noexcept : value_{ value } { }

	const FormatInfo& info() const;

private:
	uint32 value_;
}; // struct TexelFormat


//! @brief Contains the set of supported TexelFormat values.
class VEGA_API TexelFormats final
{
public:
	//! @brief Special value representing an uninitialized, invalid, or unknown format. Channels: N/A.
	static constexpr TexelFormat Undefined{ 0 };
	//! @brief An unsigned 8-bit integer representing a normalized value. Channels: R.
	static constexpr TexelFormat UNorm{ 1 };
	//! @brief Two unsigned 8-bit integers representing normalized values. Channels: RG.
	static constexpr TexelFormat UNorm2{ 2 };
	//! @brief Four unsigned 8-bit integers representing normalized values. Channels: RGBA.
	static constexpr TexelFormat UNorm4{ 3 };
	//! @brief Identical to UNorm4, compatible with the Color struct.
	static constexpr TexelFormat Color{ UNorm4 };
	//! @brief Four unsigned 8-bit integers representing normalized values, in reversed color order. Channels: BGRA.
	static constexpr TexelFormat Unorm4Bgra{ 4 };
	//! @brief A signed 8-bit integer representing a normalized value. Channels: R.
	static constexpr TexelFormat SNorm{ 5 };
	//! @brief Two signed 8-bit integers representing normalized values. Channels: RG.
	static constexpr TexelFormat SNorm2{ 6 };
	//! @brief Four signed 8-bit integers representing normalized values. Channels: RGBA.
	static constexpr TexelFormat SNorm4{ 7 };
	//! @brief An unsigned 16-bit integer representing a normalized value. Channels: R.
	static constexpr TexelFormat U16Norm{ 8 };
	//! @brief Two unsigned 16-bit integers representing normalized values. Channels: RG.
	static constexpr TexelFormat U16Norm2{ 9 };
	//! @brief Four unsigned 16-bit integers representing normalized values. Channels: RGBA.
	static constexpr TexelFormat U16Norm4{ 10 };
	//! @brief A signed 16-bit integer representing a normalized value. Channels: R.
	static constexpr TexelFormat S16Norm{ 11 };
	//! @brief Two signed 16-bit integers representing normalized values. Channels: RG.
	static constexpr TexelFormat S16Norm2{ 12 };
	//! @brief Four signed 16-bit integers representing normalized values. Channels: RGBA.
	static constexpr TexelFormat S16Norm4{ 13 };
	//! @brief An unsigned 8-bit integer. Channels: R.
	static constexpr TexelFormat UByte{ 14 };
	//! @brief Two unsigned 8-bit integers. Channels: RG.
	static constexpr TexelFormat UByte2{ 15 };
	//! @brief Four unsigned 8-bit integers. Channels: RGBA.
	static constexpr TexelFormat UByte4{ 16 };
	//! @brief A signed 8-bit integer. Channels: R.
	static constexpr TexelFormat SByte{ 17 };
	//! @brief Two signed 8-bit integers. Channels: RG.
	static constexpr TexelFormat SByte2{ 18 };
	//! @brief Four signed 8-bit integers. Channels: RGBA.
	static constexpr TexelFormat SByte4{ 19 };
	//! @brief An unsigned 16-bit integer. Channels: R.
	static constexpr TexelFormat UShort{ 20 };
	//! @brief Two unsigned 16-bit integers. Channels: RG.
	static constexpr TexelFormat UShort2{ 21 };
	//! @brief Four unsigned 16-bit integers. Channels: RGBA.
	static constexpr TexelFormat UShort4{ 22 };
	//! @brief A signed 16-bit integer. Channels: R.
	static constexpr TexelFormat SShort{ 23 };
	//! @brief Two signed 16-bit integers. Channels: RG.
	static constexpr TexelFormat SShort2{ 24 };
	//! @brief Four signed 16-bit integers. Channels: RGBA.
	static constexpr TexelFormat SShort4{ 25 };
	//! @brief An unsigned 32-bit integer. Channels: R.
	static constexpr TexelFormat UInt{ 26 };
	//! @brief Two unsigned 32-bit integers. Channels: RG.
	static constexpr TexelFormat UInt2{ 27 };
	//! @brief Four unsigned 32-bit integers. Channels: RGBA.
	static constexpr TexelFormat UInt4{ 28 };
	//! @brief A signed 32-bit integer. Channels: R.
	static constexpr TexelFormat SInt{ 29 };
	//! @brief Two signed 32-bit integers. Channels: RG.
	static constexpr TexelFormat SInt2{ 30 };
	//! @brief Four signed 32-bit integers. Channels: RGBA.
	static constexpr TexelFormat SInt4{ 31 };
	//! @brief A 32-bit float. Channels: R.
	static constexpr TexelFormat Float{ 32 };
	//! @brief Two 32-bit floats. Channels: RG.
	static constexpr TexelFormat Float2{ 33 };
	//! @brief Four 32-bit floats. Channels: RGBA.
	static constexpr TexelFormat Float4{ 34 };
	//! @brief 16-bit unsigned normalized depth value.
	static constexpr TexelFormat Depth16{ 35 };
	//! @brief 32-bit float depth value.
	static constexpr TexelFormat Depth32{ 36 };
	//! @brief 32-bit float depth value with unsigned 8-bit integer stencil value.
	static constexpr TexelFormat Depth32Stencil8{ 37 };
	//! @brief Unsigned normalized values, 2 bits for alpha, 10 bits for color channels. Channels: ABGR.
	static constexpr TexelFormat A2BGR10{ 38 };
}; // class TexelFormats

} // namespace vega


template <> struct std::hash<vega::TexelFormat> final
{
	size_t operator () (vega::TexelFormat format) const {
		static const std::hash<vega::uint32> HASH{}; return HASH(format.value());
	}
}; // struct std::hash<vega::TexelFormat>
