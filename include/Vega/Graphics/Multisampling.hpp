/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Multisampling.hpp Multisampling operations.

#include "../Common.hpp"
#include "../Core/Flags.hpp"


namespace vega
{

//! @brief Levels of multisample anti-aliasing.
enum class MSAA : uint32
{
	//! @brief One sample per fragment. Equivalent to disabling MSAA.
	X1  = 1,
	//! @brief Two samples per fragment.
	X2  = 2,
	//! @brief Four samples per fragment.
	X4  = 4,
	//! @brief Eight samples per fragment.
	X8  = 8,
	//! @brief Sixteen samples per fragment.
	X16 = 16
}; // enum class MSAA

//! @brief Mask of MSAA values.
using MSAAMask = Flags<MSAA>;
VEGA_DEFINE_FLAGS_OPERATORS(MSAA)

//! @brief Gets the string representation of the MSAA value or mask.
VEGA_API String to_string(MSAAMask msaa);

} // namespace vega
