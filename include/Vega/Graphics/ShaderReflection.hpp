/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file ShaderReflection.hpp Shader program reflection.

#include "../Common.hpp"
#include "../Core/Flags.hpp"
#include "./VertexFormat.hpp"
#include "./VertexLayout.hpp"
#include "./TexelFormat.hpp"
#include "../Render/ColorBlend.hpp"


namespace vega
{

//! @brief Supported graphics shader stages.
enum class ShaderStage : uint32
{
	//! @brief Mask representing no shader stages.
	None = 0,
	//! @brief Vertex shader stage.
	Vertex = 1 << 0,
	//! @brief Fragment shader stage.
	Fragment = 1 << 4,
	//! @brief Mask of all graphics stages.
	AllGraphics = Vertex | Fragment
}; // enum class ShaderStage

//! @brief Mask of ShaderStage values.
using ShaderStageMask = Flags<ShaderStage>;
VEGA_DEFINE_FLAGS_OPERATORS(ShaderStage)

//! @brief Gets a string representation of the shader stage mask.
VEGA_API String to_string(ShaderStageMask mask);


//! @brief Supported shader binding variable types.
enum class BindingType : uint32
{
	//! @brief Sampled texture of 1-dimensional data.
	Sampler1D,
	//! @brief Sampled texture of 2-dimensional data.
	Sampler2D,
	//! @brief Sampled texture of 3-dimensional data.
	Sampler3D,
	//! @brief Sampled texture array of 1-dimensional data.
	//Sampler1DArray,
	//! @brief Sampled texture array of 2-dimensional data.
	//Sampler2DArray,
	//! @brief Sampled cubemap texture.
	//SamplerCube,
	//! @brief Read/write image of 1-dimensional data.
	//Image1D,
	//! @brief Read/write image of 2-dimensional data.
	//Image2D,
	//! @brief Read/write image of 3-dimensional data.
	//Image3D,
	//! @brief Read/write image array of 1-dimensional data.
	//Image1DArray,
	//! @brief Read/write image array of 2-dimensional data.
	//Image2DArray,
	//! @brief Read/write cubemap image.
	//ImageCube,
	//! @brief Buffer of read-only structured data.
	ROBuffer,
	//! @brief Buffer of read-write structured data.
	//RWBuffer,
	//! @brief Buffer of read-only texel data.
	ROTexels,
	//! @brief Buffer of read-write texel data.
	//RWTexels
}; // enum class BindingType

//! @brief Gets a string representation of the binding type.
VEGA_API String to_string(BindingType type);


//! @brief Describes a vertex shader input attribute for a shader program.
struct VEGA_API ShaderInput final
{
	ShaderInput() = default;
	ShaderInput(VertexUse use, uint32 location, VertexFormat format, uint32 arraySize)
		: use_{ use }, location_{ location }, format_{ format }, arraySize_{ arraySize }
	{ }

	//! @brief The usage semantic for the input.
	inline VertexUse use() const { return use_; }
	//! @brief The base attribute binding index for the input.
	inline uint32 location() const { return location_; }
	//! @brief The format of the input attribute.
	inline VertexFormat format() const { return format_; }
	//! @brief The array size of the attribute, or 1 if not an array.
	inline uint32 arraySize() const { return arraySize_; }

private:
	VertexUse use_{ };
	uint32 location_{ };
	VertexFormat format_{ };
	uint32 arraySize_{ };
}; // struct ShaderInput


//! @brief Describes a fragment shader output for a shader program.
struct VEGA_API ShaderOutput final
{
	ShaderOutput() = default;
	ShaderOutput(TexelFormat format, ColorBlend blend)
		: format_{ format }, blend_{ blend }
	{ }

	//! @brief The format of the output data.
	inline TexelFormat format() const { return format_; }
	//! @brief The default blending mode for the output.
	inline ColorBlend blend() const { return blend_; }

private:
	TexelFormat format_{ };
	ColorBlend blend_{ };
}; // struct ShaderOutput


//! @brief Describes a binding variable within a shader program.
struct VEGA_API ShaderBinding final
{
	ShaderBinding() = default;
	ShaderBinding(uint32 slot, BindingType type, ShaderStageMask stages)
		: slot_{ slot }, type_{ type }, stages_{ stages }
	{ }

	//! @brief The slot index for the binding.
	inline uint32 slot() const { return slot_; }
	//! @brief The variable type for the binding.
	inline BindingType type() const { return type_; }
	//! @brief The mask of stages that access the binding variable.
	inline ShaderStageMask stages() const { return stages_; }

private:
	uint32 slot_{ };
	BindingType type_{ };
	ShaderStageMask stages_{ };
}; // struct ShaderBinding


//! @brief Describes a member of the uniform struct for a shader program.
struct VEGA_API ShaderUniformMember final
{
	ShaderUniformMember() = default;
	ShaderUniformMember(const String& name, uint32 offset, VertexFormat format, uint32 arraySize)
		: name_{ name }, offset_{ offset }, format_{ format }, arraySize_{ arraySize }
	{ }

	//! @brief The name of the uniform member.
	inline const String& name() const { return name_; }
	//! @brief The offset, in bytes, of the member into the uniform data.
	inline uint32 offset() const { return offset_; }
	//! @brief The data type of the member.
	inline VertexFormat format() const { return format_; }
	//! @brief The size of the member array, or 1 if the member is not an array.
	inline uint32 arraySize() const { return arraySize_; }

private:
	String name_{ };
	uint32 offset_{ };
	VertexFormat format_{ };
	uint32 arraySize_{ };
}; // struct ShaderUniformMember


//! @brief Describes a subpass input variable in a shader program.
struct VEGA_API ShaderSubpassInput final
{
	ShaderSubpassInput() = default;
	ShaderSubpassInput(uint32 index, TexelFormat format) :
		index_{ index }, format_{ format }
	{ }

	//! @brief The subpass input index.
	inline uint32 index() const { return index_; }
	//! @brief The format of the input, will always be a numeric 4-component vector type.
	inline TexelFormat format() const { return format_; }

private:
	uint32 index_{ };
	TexelFormat format_{ };
}; // struct ShaderSubpassInput

} // namespace vega
