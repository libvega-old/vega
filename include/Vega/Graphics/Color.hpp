/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Color.hpp Color descriptions and pre-defined colors.

#include "../Common.hpp"
#include "../Math/Math.hpp"


namespace vega
{

//! @brief Represents a 4-component (RGBA) color, compatible with TexelFormats::Color.
//! @remarks Stored as a 4-byte packed integer with the red channel in the LSB (making the layout \c 0xAABBGGRR).
struct VEGA_API Color final
{
private:
	inline static constexpr uint32 A_SHIFT{ 24 };
	inline static constexpr uint32 B_SHIFT{ 16 };
	inline static constexpr uint32 G_SHIFT{ 8 };
	inline static constexpr uint32 A_MASK{ 0xFF000000 };
	inline static constexpr uint32 B_MASK{ 0x00FF0000 };
	inline static constexpr uint32 G_MASK{ 0x0000FF00 };
	inline static constexpr uint32 R_MASK{ 0x000000FF };
	inline static constexpr uint32 C_MASK{ 0x00FFFFFF };

	inline static constexpr uint8 F2U(float f) { return uint8(math::clamp(f, 0.0f, 1.0f) * 255); }
	inline static constexpr float U2F(uint8 u) { return u / 255.0f; }

public:
	//! @brief Default opaque black color.
	constexpr Color() : packed_{ 0xFF000000 } { }
	//! @brief Create a color from a packed integer value in the form \c 0xAABBGGRR.
	constexpr explicit Color(uint32 packed) : packed_{ packed } { }
	//! @brief Create a color from RGBA components in the range [0, 255].
	constexpr Color(uint8 r, uint8 g, uint8 b, uint8 a = 255)
		: packed_{ uint32((a << A_SHIFT) | (b << B_SHIFT) | (g << G_SHIFT) | r) }
	{ }
	//! @brief Create a color from RGBA components clamped in the range [0, 1].
	constexpr Color(float r, float g, float b, float a = 1.0f)
		: packed_{ uint32((F2U(a) << A_SHIFT) | (F2U(b) << B_SHIFT) | (F2U(g) << G_SHIFT) | F2U(r)) }
	{ }
	//! @brief Create a color from an existing color with a new alpha value.
	constexpr Color(Color c, uint8 a) : packed_{ (a << A_SHIFT) | (c.packed_ & C_MASK) } { }
	//! @brief Create a color from an existing color with a new alpha value.
	constexpr Color(Color c, float a) : packed_{ (F2U(a) << A_SHIFT) | (c.packed_ & C_MASK) } { }

	//! @brief Gets a string representation of the color.
	String str() const;

	//! @brief The value of the color as a packed integer of the form \c 0xAABBGGRR (RR at LSB).
	inline constexpr uint32 packed() const { return packed_; }

	//! @brief The value of the R channel in [0, 255].
	inline constexpr uint8 r() const { return uint8(packed_ & R_MASK); }
	//! @brief The value of the G channel in [0, 255].
	inline constexpr uint8 g() const { return uint8((packed_ & G_MASK) >> G_SHIFT); }
	//! @brief The value of the B channel in [0, 255].
	inline constexpr uint8 b() const { return uint8((packed_ & B_MASK) >> B_SHIFT); }
	//! @brief The value of the A channel in [0, 255].
	inline constexpr uint8 a() const { return uint8((packed_ & A_MASK) >> A_SHIFT); }
	//! @brief The value of the R channel in [0, 1].
	inline constexpr float rf() const { return U2F((uint8(packed_ & R_MASK))); }
	//! @brief The value of the G channel in [0, 1].
	inline constexpr float gf() const { return U2F(uint8((packed_ & G_MASK) >> G_SHIFT)); }
	//! @brief The value of the B channel in [0, 1].
	inline constexpr float bf() const { return U2F(uint8((packed_ & B_MASK) >> B_SHIFT)); }
	//! @brief The value of the A channel in [0, 1].
	inline constexpr float af() const { return U2F(uint8((packed_ & A_MASK) >> A_SHIFT)); }

	//! @brief Set the value of the R channel.
	inline void r(uint8 r) { packed_ = (packed_ & ~R_MASK) | r; }
	//! @brief Set the value of the G channel.
	inline void g(uint8 g) { packed_ = (packed_ & ~G_MASK) | (g << G_SHIFT); }
	//! @brief Set the value of the B channel.
	inline void b(uint8 b) { packed_ = (packed_ & ~B_MASK) | (b << B_SHIFT); }
	//! @brief Set the value of the A channel.
	inline void a(uint8 a) { packed_ = (packed_ & ~A_MASK) | (a << A_SHIFT); }
	//! @brief Set the value of the R channel.
	inline void rf(float r) { packed_ = (packed_ & ~R_MASK) | F2U(r); }
	//! @brief Set the value of the G channel.
	inline void gf(float g) { packed_ = (packed_ & ~G_MASK) | (F2U(g) << G_SHIFT); }
	//! @brief Set the value of the B channel.
	inline void bf(float b) { packed_ = (packed_ & ~B_MASK) | (F2U(b) << B_SHIFT); }
	//! @brief Set the value of the A channel.
	inline void af(float a) { packed_ = (packed_ & ~A_MASK) | (F2U(a) << A_SHIFT); }

	inline constexpr bool operator == (Color r) const { return packed_ == r.packed_; }
	inline constexpr bool operator != (Color r) const { return packed_ != r.packed_; }
	inline constexpr bool operator <  (Color r) const { return packed_ < r.packed_; }

	//! @brief Returns a random color.
	//! @param opaque If the random color should be opaque (alpha == 255).
	static Color Random(bool opaque = true);

private:
	uint32 packed_;
}; // struct Color


//! @brief A collection of pre-defined Color values.
class VEGA_API Colors final
{
public:
	//! @brief Opaque black.
	inline static constexpr Color Black{ 0xFF000000 };
	//! @brief Transparent black.
	inline static constexpr Color TransparentBlack{ 0x00000000 };
	//! @brief Opaque white.
	inline static constexpr Color White{ 0xFFFFFFFF };
	//! @brief Transparent white.
	inline static constexpr Color TransparentWhite{ 0x00FFFFFF };

	//! @brief Dark gray.
	inline static constexpr Color DarkGray{ 0xFF222222 };
	//! @brief Standard middle-gray.
	inline static constexpr Color Gray{ 0xFF555555 };
	//! @brief Ligher gray.
	inline static constexpr Color LightGray{ 0xFF999999 };

	//! @brief Pure red.
	inline static constexpr Color Red{ 0xFF0000FF };
	//! @brief Pure green.
	inline static constexpr Color Green{ 0xFF00FF00 };
	//! @brief Pure blue.
	inline static constexpr Color Blue{ 0xFFFF0000 };
	//! @brief Pure yellow.
	inline static constexpr Color Yellow{ 0xFF00FFFF };
	//! @brief Pure magenta.
	inline static constexpr Color Magenta{ 0xFFFF00FF };
	//! @brief Pure cyan.
	inline static constexpr Color Cyan{ 0xFFFFFF00 };

	//! @brief Official MonoGame orange.
	inline static constexpr Color MonogameOrange{ 0xFF003CE7 };
	//! @brief Everyone's favorite default clear color.
	inline static constexpr Color CornflowerBlue{ 0xFFED9564 };

	VEGA_NO_COPY(Colors)
	VEGA_NO_MOVE(Colors)
	VEGA_NO_INIT(Colors)
}; // class Colors 

} // namespace vega


template <> struct std::hash<vega::Color>
{
	size_t operator () (vega::Color color) const {
		static const std::hash<vega::uint32> HASH{}; return HASH(color.packed());
	}
}; // struct std::hash<vega::Color>
