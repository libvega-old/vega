/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file VertexFormat.hpp Vertex data formats.

#include "../Common.hpp"


namespace vega
{

//! @brief Describes a specific data format for a vertex component.
struct VEGA_API VertexFormat final
{
private:
	friend struct VertexElement;
	friend class VertexFormats;

	struct FormatInfo final
	{
		uint32 format{ 0 };
		uint8 dim0{ 0 };
		uint8 dim1{ 0 };
		uint8 size{ 0 };
	}; // struct FormatInfo

public:
	//! @brief Constructs the default undefined format.
	constexpr VertexFormat() : value_{ 0 } { }

	//! @brief The unique integer identifier for the format.
	inline constexpr uint32 value() const { return value_; }
	//! @brief Gets a string representation of the format.
	const String& str() const;

	//! @brief The dimensions of the format as a [vertex size/matrix columns, matrix rows] pair.
	inline std::pair<uint32, uint32> dimensions() const {
		const auto& finfo = info();
		return { finfo.dim0, finfo.dim1 };
	}
	//! @brief The total number of scalar components in the format.
	inline uint32 componentCount() const {
		const auto& finfo = info();
		return uint32(finfo.dim0) * finfo.dim1;
	}
	//! @brief The size of the format, in bytes.
	inline uint32 formatSize() const { return info().size; }
	
	//! @brief Gets if the format is a scalar value.
	inline bool isScalar() const { return info().dim0 == 1; }
	//! @brief Gets if the format is a vector value.
	inline bool isVector() const {
		const auto& finfo = info();
		return (finfo.dim0 > 1) && (finfo.dim1 == 1);
	}
	//! @brief Gets if the format is a matrix value.
	inline bool isMatrix() const { return info().dim1 > 1; }

	//! @brief A different format identifier used internally. Not unique.
	inline uint32 formatId() const { return info().format; }

	inline constexpr bool operator == (VertexFormat r) const { return value_ == r.value_; }
	inline constexpr bool operator != (VertexFormat r) const { return value_ != r.value_; }
	inline constexpr bool operator <  (VertexFormat r) const { return value_ < r.value_; }

	//! @brief Gets the VertexFormat object for the id, or Undefined if not a valid id.
	static VertexFormat FromId(uint32 id);

private:
	constexpr VertexFormat(uint32 value) : value_{ value } { }

	const FormatInfo& info() const;

private:
	uint32 value_;
}; // struct VertexFormat


//! @brief Contains the set of supported VertexFormat values.
class VEGA_API VertexFormats final
{
public:
	//! @brief Special undefined or invalid format.
	static constexpr VertexFormat Undefined{ 0 };
	//! @brief Signed 1-byte integer scalar.
	static constexpr VertexFormat Byte{ 1 };
	//! @brief Signed 1-byte integer 2-component vector.
	static constexpr VertexFormat Byte2{ 2 };
	//! @brief Signed 1-byte integer 4-component vector.
	static constexpr VertexFormat Byte4{ 3 };
	//! @brief Unsigned 1-byte integer scalar.
	static constexpr VertexFormat UByte{ 4 };
	//! @brief Unsigned 1-byte integer 2-component vector.
	static constexpr VertexFormat UByte2{ 5 };
	//! @brief Unsigned 1-byte integer 4-component vector.
	static constexpr VertexFormat UByte4{ 6 };
	//! @brief Signed 2-byte integer scalar.
	static constexpr VertexFormat Short{ 7 };
	//! @brief Signed 2-byte integer 2-component vector.
	static constexpr VertexFormat Short2{ 8 };
	//! @brief Signed 2-byte integer 4-component vector.
	static constexpr VertexFormat Short4{ 9 };
	//! @brief Unsigned 2-byte integer scalar.
	static constexpr VertexFormat UShort{ 10 };
	//! @brief Unsigned 2-byte integer 2-component vector.
	static constexpr VertexFormat UShort2{ 11 };
	//! @brief Unsigned 2-byte integer 4-component vector.
	static constexpr VertexFormat UShort4{ 12 };
	//! @brief Signed 4-byte integer scalar.
	static constexpr VertexFormat Int{ 13 };
	//! @brief Signed 4-byte integer 2-component vector.
	static constexpr VertexFormat Int2{ 14 };
	//! @brief Signed 4-byte integer 3-component vector.
	static constexpr VertexFormat Int3{ 15 };
	//! @brief Signed 4-byte integer 4-component vector.
	static constexpr VertexFormat Int4{ 16 };
	//! @brief Unsigned 4-byte integer scalar.
	static constexpr VertexFormat UInt{ 17 };
	//! @brief Unsigned 4-byte integer 2-component vector.
	static constexpr VertexFormat UInt2{ 18 };
	//! @brief Unsigned 4-byte integer 3-component vector.
	static constexpr VertexFormat UInt3{ 19 };
	//! @brief Unsigned 4-byte integer 4-component vector.
	static constexpr VertexFormat UInt4{ 20 };
	//! @brief Signed 1-byte normalized scalar.
	static constexpr VertexFormat S8Norm{ 21 };
	//! @brief Signed 1-byte normalized 2-component vector.
	static constexpr VertexFormat S8Norm2{ 22 };
	//! @brief Signed 1-byte normalized 4-component vector.
	static constexpr VertexFormat S8Norm4{ 23 };
	//! @brief Unsigned 1-byte normalized scalar.
	static constexpr VertexFormat U8Norm{ 24 };
	//! @brief Unsigned 1-byte normalized 2-component vector.
	static constexpr VertexFormat U8Norm2{ 25 };
	//! @brief Unsigned 1-byte normalized 4-component vector.
	static constexpr VertexFormat U8Norm4{ 26 };
	//! @brief Signed 2-byte normalized scalar.
	static constexpr VertexFormat S16Norm{ 27 };
	//! @brief Signed 2-byte normalized 2-component vector.
	static constexpr VertexFormat S16Norm2{ 28 };
	//! @brief Signed 2-byte normalized 4-component vector.
	static constexpr VertexFormat S16Norm4{ 29 };
	//! @brief Unsigned 2-byte normalized scalar.
	static constexpr VertexFormat U16Norm{ 30 };
	//! @brief Unsigned 2-byte normalized 2-component vector.
	static constexpr VertexFormat U16Norm2{ 31 };
	//! @brief Unsigned 2-byte normalized 4-component vector.
	static constexpr VertexFormat U16Norm4{ 32 };
	//! @brief 2-byte floating point scalar.
	static constexpr VertexFormat Half{ 33 };
	//! @brief 2-byte floating point 2-component vector.
	static constexpr VertexFormat Half2{ 34 };
	//! @brief 2-byte floating point 4-component vector.
	static constexpr VertexFormat Half4{ 35 };
	//! @brief 4-byte floating point scalar.
	static constexpr VertexFormat Float{ 36 };
	//! @brief 4-byte floating point 2-component vector.
	static constexpr VertexFormat Float2{ 37 };
	//! @brief 4-byte floating point 3-component vector.
	static constexpr VertexFormat Float3{ 38 };
	//! @brief 4-byte floating point 4-component vector.
	static constexpr VertexFormat Float4{ 39 };
	//! @brief A 32-bit floating point matrix with 2 columns and 2 rows.
	static constexpr VertexFormat Float2x2{ 40 };
	//! @brief A 32-bit floating point matrix with 2 columns and 3 rows.
	static constexpr VertexFormat Float2x3{ 41 };
	//! @brief A 32-bit floating point matrix with 2 columns and 4 rows.
	static constexpr VertexFormat Float2x4{ 42 };
	//! @brief A 32-bit floating point matrix with 3 columns and 2 rows.
	static constexpr VertexFormat Float3x2{ 43 };
	//! @brief A 32-bit floating point matrix with 3 columns and 3 rows.
	static constexpr VertexFormat Float3x3{ 44 };
	//! @brief A 32-bit floating point matrix with 3 columns and 4 rows.
	static constexpr VertexFormat Float3x4{ 45 };
	//! @brief A 32-bit floating point matrix with 4 columns and 2 rows.
	static constexpr VertexFormat Float4x2{ 46 };
	//! @brief A 32-bit floating point matrix with 4 columns and 3 rows.
	static constexpr VertexFormat Float4x3{ 47 };
	//! @brief A 32-bit floating point matrix with 4 columns and 4 rows.
	static constexpr VertexFormat Float4x4{ 48 };

	VEGA_NO_COPY(VertexFormats)
	VEGA_NO_MOVE(VertexFormats)
	VEGA_NO_INIT(VertexFormats)
}; // class VertexFormats

} // namespace vega
