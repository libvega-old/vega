/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file StagingBuffer.hpp Data transfers to GPU resources.

#include "../Common.hpp"


namespace vega
{

//! @brief Manages a contiguous area of host-visible memory used to write GPU resource data.
class VEGA_API StagingBuffer
{
	friend class StagingBufferImpl;

public:
	virtual ~StagingBuffer();

	//! @brief The size of the buffer, in bytes.
	uint64 size() const;
	//! @brief Gets if the buffer is currently being used for an active transfer and thus modifying it is unsafe.
	bool isPending() const;

	//! @brief The raw unsafe pointer to the buffer memory. Do not modify this data if the buffer is pending.
	void* data();
	//! @brief Raw constant pointer to buffer memory.
	const void* data() const;

private:
	StagingBuffer();

	VEGA_NO_COPY(StagingBuffer)
	VEGA_NO_MOVE(StagingBuffer)
}; // class StagingBuffer

} // namespace vega
