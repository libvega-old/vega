/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

//! @file Shader.hpp Shader program assets and reflection.

#include "../Common.hpp"
#include "./ShaderReflection.hpp"
#include "../Asset/AssetBase.hpp"


namespace vega
{

//! @brief A compiled VSL graphics shader program and reflection info.
class VEGA_API Shader
	: public AssetBase
{
	friend class ShaderImpl;

public:
	//! @brief The maximum number of bindings supported in a single shader.
	inline static constexpr uint32 MAX_BINDING_COUNT{ 32u };
	//! @brief The maximum number of vertex attributes supported in a single shader.
	inline static constexpr uint32 MAX_VERTEX_ATTRIB_COUNT{ 16u };

	virtual ~Shader();

	//! @brief Mask of stages present in the shader.
	ShaderStageMask stageMask() const;
	//! @brief Mask of vertex input uses for the shader.
	VertexUseMask vertexMask() const;
	//! @brief List of shader inputs.
	const std::vector<ShaderInput>& inputs() const;
	//! @brief List of shader outputs.
	const std::vector<ShaderOutput>& outputs() const;
	//! @brief List of shader bindings.
	const std::vector<ShaderBinding>& bindings() const;
	//! @brief List of shader subpass inputs.
	const std::vector<ShaderSubpassInput>& subpassInputs() const;
	//! @brief Size of the shader uniform data, in bytes.
	uint32 uniformSize() const;
	//! @brief List of shader uniform data members.
	const std::vector<ShaderUniformMember>& uniformMembers() const;

	//! @brief Gets the input with the given usage, or \c nullptr if none exists.
	const ShaderInput* findInput(VertexUse use) const;
	//! @brief Gets the binding at the given slot, or \c nullptr if the slot is invalid or unused.
	const ShaderBinding* findBinding(uint32 slot) const;

private:
	Shader();

	VEGA_NO_COPY(Shader)
	VEGA_NO_MOVE(Shader)
}; // class Shader

} // namespace vega
