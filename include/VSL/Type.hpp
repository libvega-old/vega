/*
 * Microsoft Public License (Ms-PL) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

#include "./Common.hpp"

#include <unordered_map>


namespace vsl
{

class ShaderType;

// The base shader types.
enum class BaseType : uint32
{
	Void = 0,
	Boolean,
	Signed,
	Unsigned,
	Float,
	Sampler,
	Image,
	ROBuffer,
	RWBuffer,
	ROTexels,
	RWTexels,
	SubpassInput,
	Struct
}; // enum class BaseType

// BaseType ToString
const String& ToString(BaseType type);


// Describes the texel ranks (dimension counts/types) that texel-like objects can have.
struct TexelRank final
{
public:
	const String& ToString() const;
	inline constexpr uint32 Value() const { return value_; }

	// The number of dimensional coordinates of the rank.
	uint32 DimensionCount() const;
	// The type name suffix for texel types that use this rank.
	const String& TypeSuffix() const;

	inline constexpr bool operator == (TexelRank r) const { return value_ == r.value_; }
	inline constexpr bool operator != (TexelRank r) const { return value_ != r.value_; }
	inline constexpr bool operator <  (TexelRank r) const { return value_ <  r.value_; }

private:
	constexpr TexelRank(uint32 value) : value_{ value } { }

private:
	uint32 value_;

public:
	static const TexelRank E1D;
	static const TexelRank E2D;
	static const TexelRank E3D;
	static const TexelRank E1DArray;
	static const TexelRank E2DArray;
	static const TexelRank Cube;
	static const TexelRank Buffer;
}; // struct TexelRank

inline constexpr TexelRank TexelRank::E1D     { 0 };
inline constexpr TexelRank TexelRank::E2D     { 1 };
inline constexpr TexelRank TexelRank::E3D     { 2 };
inline constexpr TexelRank TexelRank::E1DArray{ 3 };
inline constexpr TexelRank TexelRank::E2DArray{ 4 };
inline constexpr TexelRank TexelRank::Cube    { 5 };
inline constexpr TexelRank TexelRank::Buffer  { 6 };


// Describes the base numeric types that texel-like objects can have.
enum class TexelType : uint32
{
	Signed = 0,
	Unsigned,
	Float,
	UNorm,
	SNorm
}; // enum class TexelType

// TexelType ToString
const String& ToString(TexelType type);


// Describes the supported texel formats for texel-like objects.
struct TexelFormat final
{
	struct TexelInfo
	{
		TexelType baseType;
		uint32 count;
		uint32 size;
	}; // struct TexelInfo

public:
	const String& ToString() const;
	inline constexpr uint32 Value() const { return value_; }

	// The base TexelType of the format.
	inline TexelType BaseType() const { return info().baseType; }
	// The number of components in the format.
	inline uint32 ComponentCount() const { return info().count; }
	// The size of a single component in the format, in bytes.
	inline uint32 ComponentSize() const { return info().size; }
	// The size of a single texel in the format, in bytes.
	inline uint32 TexelSize() const { return ComponentCount() * ComponentSize(); }

	inline bool IsSigned() const { return BaseType() == TexelType::Signed; }
	inline bool IsUnsigned() const { return BaseType() == TexelType::Unsigned; }
	inline bool IsFloat() const { return BaseType() == TexelType::Float; }
	inline bool IsUNorm() const { return BaseType() == TexelType::UNorm; }
	inline bool IsSNorm() const { return BaseType() == TexelType::SNorm; }
	inline bool IsIntegerType() const { return IsSigned() || IsUnsigned(); }
	inline bool IsFloatType() const { return IsFloat() || IsUNorm() || IsSNorm(); }
	inline bool IsNormalizedType() const { return IsUNorm() || IsSNorm(); }

	// Gets the ShaderType that the texels of this format are treated as when operated on.
	const ShaderType* AsShaderType() const;

	// Gets the format decorator for VSL.
	const String& GetVSLDecorator() const;

	// Gets the format associated with the VSL format string, or nullopt for unknown formats.
	static std::optional<TexelFormat> FromVSLFormat(const String& format);

	inline constexpr bool operator == (TexelFormat r) const { return value_ == r.value_; }
	inline constexpr bool operator != (TexelFormat r) const { return value_ != r.value_; }
	inline constexpr bool operator <  (TexelFormat r) const { return value_ <  r.value_; }

private:
	constexpr TexelFormat(uint32 value) : value_{ value } { }

	const TexelInfo& info() const;

private:
	uint32 value_;

public:
	static const TexelFormat U8Norm;
	static const TexelFormat U8Norm2;
	static const TexelFormat U8Norm4;
	static const TexelFormat S8Norm;
	static const TexelFormat S8Norm2;
	static const TexelFormat S8Norm4;
	static const TexelFormat U16Norm;
	static const TexelFormat U16Norm2;
	static const TexelFormat U16Norm4;
	static const TexelFormat S16Norm;
	static const TexelFormat S16Norm2;
	static const TexelFormat S16Norm4;
	static const TexelFormat Int;
	static const TexelFormat Int2;
	static const TexelFormat Int4;
	static const TexelFormat UInt;
	static const TexelFormat UInt2;
	static const TexelFormat UInt4;
	static const TexelFormat Float;
	static const TexelFormat Float2;
	static const TexelFormat Float4;
}; // struct TexelFormat

inline constexpr TexelFormat TexelFormat::U8Norm  { 0 };
inline constexpr TexelFormat TexelFormat::U8Norm2 { 1 };
inline constexpr TexelFormat TexelFormat::U8Norm4 { 2 };
inline constexpr TexelFormat TexelFormat::S8Norm  { 3 };
inline constexpr TexelFormat TexelFormat::S8Norm2 { 4 };
inline constexpr TexelFormat TexelFormat::S8Norm4 { 5 };
inline constexpr TexelFormat TexelFormat::U16Norm { 6 };
inline constexpr TexelFormat TexelFormat::U16Norm2{ 7 };
inline constexpr TexelFormat TexelFormat::U16Norm4{ 8 };
inline constexpr TexelFormat TexelFormat::S16Norm { 9 };
inline constexpr TexelFormat TexelFormat::S16Norm2{ 10 };
inline constexpr TexelFormat TexelFormat::S16Norm4{ 11 };
inline constexpr TexelFormat TexelFormat::Int     { 12 };
inline constexpr TexelFormat TexelFormat::Int2    { 13 };
inline constexpr TexelFormat TexelFormat::Int4    { 14 };
inline constexpr TexelFormat TexelFormat::UInt    { 15 };
inline constexpr TexelFormat TexelFormat::UInt2   { 16 };
inline constexpr TexelFormat TexelFormat::UInt4   { 17 };
inline constexpr TexelFormat TexelFormat::Float   { 18 };
inline constexpr TexelFormat TexelFormat::Float2  { 19 };
inline constexpr TexelFormat TexelFormat::Float4  { 20 };


// Describes a user-defined POD struct type.
struct StructType final
{
	friend class Parser;
	friend class ShaderType;

public:
	// Describes a member of the struct type.
	struct Member final
	{
		// The member name.
		String name;
		// The member type.
		const ShaderType* type;
		// The size of the member array, or 1 if the member is not an array.
		uint32 arraySize;
		// The offset of the member into the struct binary representation, in bytes.
		uint32 offset;
	}; // struct Member

	~StructType() { }

	// The user-provided name of the struct type.
	inline const String& Name() const { return name_; }
	// The collection of members in the struct type.
	inline const std::vector<Member>& Members() const { return members_; }
	// The size of the struct type, in bytes.
	inline uint32 Size() const { return size_; }
	// The alignment of the struct type, in bytes.
	inline uint32 Alignment() const { return alignment_; }

	// Gets the member with the given name, or \c nullptr if no member exists.
	//! @param name The name of the member to get.
	const Member* GetMember(const String& name) const;

private:
	StructType(const String& name, const std::vector<Member>& members);

private:
	String name_;
	std::vector<Member> members_;
	uint32 size_;
	uint32 alignment_;

	VSL_NO_MOVE(StructType)
	VSL_DEFAULT_COPY(StructType)
}; // struct StructType


// Fully describes a concrete object or value type within the VSL shader type system, minus array size.
class ShaderType final
{
	friend class Parser;

public:
	class Builtin;

	~ShaderType() { }

	inline bool IsVoid() const { return baseType == BaseType::Void; }
	inline bool IsBoolean() const { return baseType == BaseType::Boolean; }
	inline bool IsSigned() const { return baseType == BaseType::Signed; }
	inline bool IsUnsigned() const { return baseType == BaseType::Unsigned; }
	inline bool IsFloat() const { return baseType == BaseType::Float; }
	inline bool IsSampler() const { return baseType == BaseType::Sampler; }
	inline bool IsImage() const { return baseType == BaseType::Image; }
	inline bool IsROBuffer() const { return baseType == BaseType::ROBuffer; }
	inline bool IsRWBuffer() const { return baseType == BaseType::RWBuffer; }
	inline bool IsROTexels() const { return baseType == BaseType::ROTexels; }
	inline bool IsRWTexels() const { return baseType == BaseType::RWTexels; }
	inline bool IsSubpassInput() const { return baseType == BaseType::SubpassInput; }
	inline bool IsStruct() const { return baseType == BaseType::Struct; }

	// Gets if the base type is either a signed or unsigned integer type.
	inline bool IsInteger() const { return IsSigned() || IsUnsigned(); }
	// Gets if the base type is a numeric type (integer or float).
	inline bool IsNumericType() const { return IsInteger() || IsFloat(); }
	// Gets if the type is a numeric or boolean type that is a scalar.
	inline bool IsScalar() const {
		return (IsNumericType() || IsBoolean()) && (numeric.dims[0] == 1) && (numeric.dims[1] == 1);
	}
	// Gets if the type is a numeric or boolean type that is a vector.
	inline bool IsVector() const {
		return (IsNumericType() || IsBoolean()) && (numeric.dims[0] != 1) && (numeric.dims[1] == 1);
	}
	// Gets if the type is a numeric type that is a matrix.
	inline bool IsMatrix() const {
		return IsNumericType() && (numeric.dims[0] != 1) && (numeric.dims[1] != 1);
	}
	// Gets if the type is a texel-like object that operates on texel data.
	inline bool IsTexelType() const { return IsSampler() || IsImage() || IsROTexels() || IsRWTexels() || IsSubpassInput(); }
	// Gets if the type is a buffer object that operates on struct data.
	inline bool IsBufferType() const { return IsROBuffer() || IsRWBuffer(); }
	// Gets if the type is a struct type, or buffer or uniform type that operates on struct data.
	inline bool HasStructType() const { return IsBufferType() || IsStruct(); }

	// Gets if the two types represent the same type.
	bool IsSame(const ShaderType& otherType) const;
	// Gets if the current type has an implicit cast to the given type.
	bool HasImplicitCast(const ShaderType& targetType) const;

	// The number of binding slots (vertex, fragment, or local) taken by a single instance of the type.
	uint32 BindingCount() const;

	// Gets the VSL name for the type.
	String GetVSLName() const;

	inline bool operator == (const ShaderType& r) const { return (this == &r) || IsSame(r); }
	inline bool operator != (const ShaderType& r) const { return (this != &r) && !IsSame(r); }

public:
	ShaderType() : baseType{ BaseType::Void }, numeric{ } { }
	ShaderType(BaseType numericType, uint32 size, uint32 components, uint32 columns)
		: baseType{ numericType }, numeric{ size, { components, columns } }
	{ }
	ShaderType(BaseType texelObjectType, TexelRank rank, TexelFormat format)
		: baseType{ texelObjectType }, numeric{ }
	{
		texel = { rank, format };
	}
	ShaderType(const BaseType bufferType, const ShaderType* structType)
		: baseType{ bufferType }, numeric{ }
	{
		buffer.structType = structType;
	}
	ShaderType(const StructType* structType)
		: baseType{ BaseType::Struct }, numeric{ }
	{
		userStruct.type = structType;
	}

public:
	BaseType baseType; // The base type for the type.
	union
	{
		struct
		{
			uint32 size;    // The size, in bytes, of the numeric type.
			uint32 dims[2]; // The dimensions of the numeric type, in [components, columns] order.
		} numeric;
		struct
		{
			TexelRank rank;     // The rank of the texel-like object.
			TexelFormat format; // The texel format used by the texel-like object.
		} texel;
		struct
		{
			const ShaderType* structType; // The struct type stored in the buffer or uniform type.
		} buffer;
		struct
		{
			const StructType* type; // The type definition for the struct type.
		} userStruct;
	};

	VSL_NO_MOVE(ShaderType)
	VSL_DEFAULT_COPY(ShaderType)
}; // class ShaderType

} // namespace vsl


template <> struct std::hash<vsl::TexelRank>
{
	size_t operator () (vsl::TexelRank type) const {
		static const std::hash<vsl::uint32> HASH{}; return HASH(type.Value());
	}
}; // struct std::hash<vsl::TexelRank>

template <> struct std::hash<vsl::TexelFormat>
{
	size_t operator () (vsl::TexelFormat type) const {
		static const std::hash<vsl::uint32> HASH{}; return HASH(type.Value());
	}
}; // struct std::hash<vsl::TexelFormat>
